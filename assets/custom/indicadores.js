var validaInd = false;
var validaAnyo = false;
var validaTri = false;
var validaUbi = false;
var indicador = "";
var anyo = "";
var trimestre = "";
var ubicacion = "";
var ruta = window.location.protocol + "//" + window.location.host;

document.getElementById("ind1").addEventListener("click", function( event ) {
    reloadColorClick();
    document.getElementById('ind1').style.backgroundColor = '#9d262b';
    var text = "Dengue sin signos de alarma";
    document.getElementById("text_indicador").innerHTML = text;
    var btn = document.getElementById("ind1").addEventListener("click", cambiaIndicador('1'));
    activarUbicacion();
  }, false);

  document.getElementById("ind2").addEventListener("click", function( event ) {
    reloadColorClick();
    document.getElementById('ind2').style.backgroundColor = '#9d262b';
    var text = "Dengue con signos de alarma";
    document.getElementById("text_indicador").innerHTML = text;
    var btn = document.getElementById("ind1").addEventListener("click", cambiaIndicador('2'));
    activarUbicacion();
  }, false);

 /* document.getElementById("ind3").addEventListener("click", function( event ) {
    reloadColorClick();
    document.getElementById('ind3').style.backgroundColor = '#9d262b';
    var text = "Hepatitis A";
    document.getElementById("text_indicador").innerHTML = text;
    var btn = document.getElementById("ind1").addEventListener("click", cambiaIndicador('3'));
    activarUbicacion();
  }, false); */

  document.getElementById("ind4").addEventListener("click", function( event ) {
    reloadColorClick();
    document.getElementById('ind4').style.backgroundColor = '#9d262b';
    var text = "Hepatitis B Aguda";
    document.getElementById("text_indicador").innerHTML = text;
    var btn = document.getElementById("ind1").addEventListener("click", cambiaIndicador('4'));
    activarUbicacion();
  }, false);

  document.getElementById("ind5").addEventListener("click", function( event ) {
    reloadColorClick();
    document.getElementById('ind5').style.backgroundColor = '#9d262b';
    var text = "Hepatitis B Crónica";
    document.getElementById("text_indicador").innerHTML = text;
    var btn = document.getElementById("ind1").addEventListener("click", cambiaIndicador('5'));
    activarUbicacion();
  }, false);

 /* document.getElementById("ind6").addEventListener("click", function( event ) {
    reloadColorClick();
    document.getElementById('ind6').style.backgroundColor = '#5647f7';
    var text = "Hepatitis B por Transmisión Perinatal";
    document.getElementById("text_indicador").innerHTML = text;
    var btn = document.getElementById("ind1").addEventListener("click", cambiaIndicador('6'));
    activarUbicacion();
  }, false);

  document.getElementById("ind7").addEventListener("click", function( event ) {
    reloadColorClick();
    document.getElementById('ind7').style.backgroundColor = '#5647f7';
    var text = "Hepatitis Coinfección B-D";
    document.getElementById("text_indicador").innerHTML = text;
    var btn = document.getElementById("ind1").addEventListener("click", cambiaIndicador('7'));
    activarUbicacion();
  }, false);
*/
  document.getElementById("ind8").addEventListener("click", function( event ) {
    reloadColorClick();
    document.getElementById('ind8').style.backgroundColor = '#9d262b';
    var text = "Hepatitis C";
    document.getElementById("text_indicador").innerHTML = text;
    var btn = document.getElementById("ind1").addEventListener("click", cambiaIndicador('8'));
    activarUbicacion();
  }, false);

  document.getElementById("ind9").addEventListener("click", function( event ) {
    reloadColorClick();
    document.getElementById('ind9').style.backgroundColor = '#9d262b';
    var text = "TB Pulmonares";
    document.getElementById("text_indicador").innerHTML = text;
    var btn = document.getElementById("ind1").addEventListener("click", cambiaIndicador('9'));
    activarUbicacion();
  }, false);

  document.getElementById("ind10").addEventListener("click", function( event ) {
    reloadColorClick();
    document.getElementById('ind10').style.backgroundColor = '#9d262b';
    var text = "TB Extrapulmonares";
    document.getElementById("text_indicador").innerHTML = text;
    var btn = document.getElementById("ind1").addEventListener("click", cambiaIndicador('10'));
    activarUbicacion();
  }, false);

  document.getElementById("ind11").addEventListener("click", function( event ) {
    reloadColorClick();
    document.getElementById('ind11').style.backgroundColor = '#9d262b';
    var text = "VIH";
    document.getElementById("text_indicador").innerHTML = text;
    var btn = document.getElementById("ind1").addEventListener("click", cambiaIndicador('11'));
    activarUbicacion();
  }, false);

 /* document.getElementById("ind12").addEventListener("click", function( event ) {
    reloadColorClick();
    document.getElementById('ind12').style.backgroundColor = '#5647f7';
    var text = "VIH con coinfección TB";
    document.getElementById("text_indicador").innerHTML = text;
    var btn = document.getElementById("ind1").addEventListener("click", cambiaIndicador('12'));
    activarUbicacion();
  }, false); */

  document.getElementById("ind13").addEventListener("click", function( event ) {
    reloadColorClick();
    document.getElementById('ind13').style.backgroundColor = '#9d262b';
    var text = "COVID confirmados";
    document.getElementById("text_indicador").innerHTML = text;
    var btn = document.getElementById("ind1").addEventListener("click", cambiaIndicador('13'));
    activarUbicacion();
  }, false);

  document.getElementById("ind14").addEventListener("click", function( event ) {
    reloadColorClick();
    document.getElementById('ind14').style.backgroundColor = '#9d262b';
    var text = "COVID Recuperados";
    document.getElementById("text_indicador").innerHTML = text;
    var btn = document.getElementById("ind1").addEventListener("click", cambiaIndicador('14'));
    activarUbicacion();
  }, false);

  document.getElementById("ind15").addEventListener("click", function( event ) {
    reloadColorClick();
    document.getElementById('ind15').style.backgroundColor = '#9d262b';
    var text = "COVID Fallecidos";
    document.getElementById("text_indicador").innerHTML = text;
    var btn = document.getElementById("ind1").addEventListener("click", cambiaIndicador('15'));
    activarUbicacion();
  }, false);
 

  function reloadColorClick() {
    document.getElementById('ind1').style.backgroundColor = '#0d8aee';
    document.getElementById('ind2').style.backgroundColor = '#0d8aee';
 //   document.getElementById('ind3').style.backgroundColor = '#0d8aee';
    document.getElementById('ind4').style.backgroundColor = '#0d8aee';
    document.getElementById('ind5').style.backgroundColor = '#0d8aee';
  //  document.getElementById('ind6').style.backgroundColor = '#ffca5b';
  //  document.getElementById('ind7').style.backgroundColor = '#ffca5b';
    document.getElementById('ind8').style.backgroundColor = '#0d8aee';
    document.getElementById('ind9').style.backgroundColor = '#0d8aee';
    document.getElementById('ind10').style.backgroundColor = '#0d8aee';
    document.getElementById('ind11').style.backgroundColor = '#0d8aee';
 //   document.getElementById('ind12').style.backgroundColor = '#6ab8f7';
    document.getElementById('ind13').style.backgroundColor = '#0d8aee';
    document.getElementById('ind14').style.backgroundColor = '#0d8aee';
    document.getElementById('ind15').style.backgroundColor = '#0d8aee';
 

  }

  function activarUbicacion(){

    document.getElementById("ubi1").addEventListener("click", function( event ) {
      reloadColorUbicacion();
      document.getElementById('ubi1').style.backgroundColor = '#9d262b';
      var text = "Norte de Santander";
      document.getElementById("text_ubi").innerHTML = text;
      activarAnyo();
      cambiaUbicacion('Norte de Santander');
    }, false);
  
    document.getElementById("ubi2").addEventListener("click", function( event ) {
      reloadColorUbicacion();
      document.getElementById('ubi2').style.backgroundColor = '#9d262b';
      var text = "Cúcuta";
      document.getElementById("text_ubi").innerHTML = text;
      activarAnyo();
      cambiaUbicacion('Cúcuta');
    }, false);
  
    document.getElementById("ubi3").addEventListener("click", function( event ) {
      reloadColorUbicacion();
      document.getElementById('ubi3').style.backgroundColor = '#9d262b';
      var text = "Villa del Rosario";
      document.getElementById("text_ubi").innerHTML = text;
      activarAnyo();
      cambiaUbicacion('Villa del Rosario');
    }, false);
  
    document.getElementById("ubi4").addEventListener("click", function( event ) {
      reloadColorUbicacion();
      document.getElementById('ubi4').style.backgroundColor = '#9d262b';
      var text = "Los Patios";
      document.getElementById("text_ubi").innerHTML = text;
      activarAnyo();
      cambiaUbicacion('Los Patios');
    }, false);

    document.getElementById("ubi5").addEventListener("click", function( event ) {
      reloadColorUbicacion();
      document.getElementById('ubi5').style.backgroundColor = '#9d262b';
      var text = "El Zulia";
      document.getElementById("text_ubi").innerHTML = text;
      activarAnyo();
      cambiaUbicacion('El Zulia');
    }, false);
  
  }
  
  function reloadColorUbicacion() {
    document.getElementById('ubi1').style.backgroundColor = '#ffba28';
    document.getElementById('ubi2').style.backgroundColor = '#ffba28';
    document.getElementById('ubi3').style.backgroundColor = '#ffba28';
    document.getElementById('ubi4').style.backgroundColor = '#ffba28';
    document.getElementById('ubi5').style.backgroundColor = '#ffba28';
  }



function activarAnyo(){

  document.getElementById("any1").addEventListener("click", function( event ) {
    reloadColorAnyo();
    document.getElementById('any1').style.backgroundColor = '#9d262b';
    var text = "2019";
    document.getElementById("text_anyo").innerHTML = text;
    activarTrimestre();
    cambiaAnyo('2019');
  }, false);

  document.getElementById("any2").addEventListener("click", function( event ) {
    reloadColorAnyo();
    document.getElementById('any2').style.backgroundColor = '#9d262b';
    var text = "2020";
    document.getElementById("text_anyo").innerHTML = text;
    activarTrimestre();
    cambiaAnyo('2020');
  }, false);

  document.getElementById("any3").addEventListener("click", function( event ) {
    reloadColorAnyo();
    document.getElementById('any3').style.backgroundColor = '#9d262b';
    var text = "2021";
    document.getElementById("text_anyo").innerHTML = text;
    activarTrimestre();
    cambiaAnyo('2021');
  }, false);

  document.getElementById("any4").addEventListener("click", function( event ) {
    reloadColorAnyo();
    document.getElementById('any4').style.backgroundColor = '#9d262b';
    var text = "2022";
    document.getElementById("text_anyo").innerHTML = text;
    activarTrimestre();
    cambiaAnyo('2022');
  }, false);

}

function reloadColorAnyo() {
  document.getElementById('any1').style.backgroundColor = '#1ab3a3';
  document.getElementById('any2').style.backgroundColor = '#1ab3a3';
  document.getElementById('any3').style.backgroundColor = '#1ab3a3';
  document.getElementById('any4').style.backgroundColor = '#1ab3a3';
}



function activarTrimestre(){

  document.getElementById("tri1").addEventListener("click", function( event ) {
    reloadColorTri();
    document.getElementById('tri1').style.backgroundColor = '#9d262b';
    var text = "1 trimestre";
    document.getElementById("text_tri").innerHTML = text;
    cambiaTrimestre('1');
  }, false);

  document.getElementById("tri2").addEventListener("click", function( event ) {
    reloadColorTri();
    document.getElementById('tri2').style.backgroundColor = '#9d262b';
    var text = "2 trimestre";
    document.getElementById("text_tri").innerHTML = text;
    cambiaTrimestre('2');
  }, false);

  document.getElementById("tri3").addEventListener("click", function( event ) {
    reloadColorTri();
    document.getElementById('tri3').style.backgroundColor = '#9d262b';
    var text = "3 trimestre";
    document.getElementById("text_tri").innerHTML = text;
    cambiaTrimestre('3');
  }, false);

  document.getElementById("tri4").addEventListener("click", function( event ) {
    reloadColorTri();
    document.getElementById('tri4').style.backgroundColor = '#9d262b';
    var text = "4 trimestre";
    document.getElementById("text_tri").innerHTML = text;
    cambiaTrimestre('4');
  }, false);

  document.getElementById("tri5").addEventListener("click", function( event ) {
    reloadColorTri();
    document.getElementById('tri5').style.backgroundColor = '#9d262b';
    var text = "Todos";
    document.getElementById("text_tri").innerHTML = text;
    cambiaTrimestre('5');
  }, false);

}
 
function reloadColorTri() {
  document.getElementById('tri1').style.backgroundColor = '#7a59ad';
  document.getElementById('tri2').style.backgroundColor = '#7a59ad';
  document.getElementById('tri3').style.backgroundColor = '#7a59ad';
  document.getElementById('tri4').style.backgroundColor = '#7a59ad';
  document.getElementById('tri5').style.backgroundColor = '#7a59ad';
}




  function cambiaIndicador(ind) {
    validaInd = true;
    indicador = ind;
 //   var inputNombre = document.getElementById("arreglo");
 //   inputNombre.value = ind;
  } 

  function cambiaAnyo(any) {
    validaAnyo = true;
    anyo = any;
  } 

  function cambiaUbicacion(any) {
    validaUbi = true;
    ubicacion = any;
  } 

  function cambiaTrimestre(tri) {
    validaTri = true;
    trimestre = tri;
  } 

  function placeOrder(form){

    if(validaInd && validaUbi && validaAnyo && validaTri){

      var inputNombre = document.getElementById("arreglo");
      inputNombre.value = indicador + '_' + ubicacion + '_' + anyo + '_' + trimestre;

  //    console.log(inputNombre.value);
      var aux = inputNombre.value.split('_');
      if(aux.length > 0){
        localStorage.setItem('property', JSON.stringify(aux));
      }
    //  localStorage.setItem('dataSet1', JSON.stringify(dataSet1));
      switch (indicador) {
        case '1':
          dataDengue(indicador, anyo, trimestre, ubicacion);
          break;
        case '2':
          dataDengue(indicador, anyo, trimestre, ubicacion);
          break;
        case '3':
          dataHepatitis(indicador, anyo, trimestre, ubicacion);
          break;
        case '4':
          dataHepatitis(indicador, anyo, trimestre, ubicacion);
          break;
        case '5':
          dataHepatitis(indicador, anyo, trimestre, ubicacion);
          break;
        case '6':
          dataHepatitis(indicador, anyo, trimestre, ubicacion);
          break;
        case '7':
          dataHepatitis(indicador, anyo, trimestre, ubicacion);
          break;
        case '8':
          dataHepatitis(indicador, anyo, trimestre, ubicacion);
          break;
        case '9':
          dataTbc(indicador, anyo, trimestre, ubicacion);
          break;
        case '10':
          dataTbc(indicador, anyo, trimestre, ubicacion);
          break;
        case '11':
          dataVih(indicador, anyo, trimestre, ubicacion);
          break;
        case '13':
          dataCovid(indicador, anyo, trimestre, ubicacion, 1);
          break;
        case '14':
          dataCovid(indicador, anyo, trimestre, ubicacion, 2);
          break;
        case '15':
          dataCovid(indicador, anyo, trimestre, ubicacion, 3);
          break;
        default:
          break;
      }
 /*     if(indicador == 11 && sessionStorage.getItem('sessionInfIndSal') == 'IJ9*BXTwKKSf@gbDts'){
          dataVih(indicador, anyo, trimestre, ubicacion);
          console.log("aca");
          form.submit();
      }
      if(indicador == 11 && sessionStorage.getItem('sessionInfIndSal') != 'IJ9*BXTwKKSf@gbDts'){
        $('#loginIndicador').modal('show');
      } */
   
      $('#Bcargando').show();
      $('#Bconsultar').hide();
      setTimeout(function(){ 
        form.submit(); 
      }, 3000);
       
    }

  }

  function validarLogin(form){
    console.log("validar");
    var inputUsuario = document.getElementById("usuario").value;
    var inputPassword = document.getElementById("password").value;
    loginIndicador(inputUsuario, inputPassword, form);
  }

  function limpiar(){
    validaInd = false;
    validaAnyo = false;
    validaTri = false;
    validaUbi = false;
    indicador = "";
    anyo = "";
    trimestre = "";
    ubicacion = ""
    var inputNombre = document.getElementById("arreglo");
    inputNombre.value = "";
    reloadColorClick();
    reloadColorAnyo();
    reloadColorTri();
    reloadColorUbicacion();
    document.getElementById("text_indicador").innerHTML = 'Seleccione un indicador.';
    document.getElementById("text_ubi").innerHTML = 'Seleccione una ubicación.';
    document.getElementById("text_anyo").innerHTML = 'Seleccione un año.';
    document.getElementById("text_tri").innerHTML = 'Seleccione un trimestre.';
  }

function dataDengue(indicador, anyo, trimestre, ubicacion){
  totalDengue(indicador, anyo, trimestre, ubicacion);
  consultasDengue(indicador, anyo, trimestre, ubicacion);
  afiliacionDengue(indicador, anyo, trimestre, ubicacion);
  rangoEdadesDengue(indicador, anyo, trimestre, ubicacion);
  hospitalizadoDengue(indicador, anyo, trimestre, ubicacion);
  municipioOcuDengue(indicador, anyo, trimestre, ubicacion);
  barriosOcuDengue(indicador, anyo, trimestre, ubicacion);
  mapaTub(indicador, anyo, trimestre, ubicacion);
}

function dataHepatitis(indicador, anyo, trimestre, ubicacion){
  semanasHep(indicador, anyo, trimestre, ubicacion);
  municipioOcuHep(indicador, anyo, trimestre, ubicacion);
  gestantesHep(indicador, anyo, trimestre, ubicacion);
  coinfeccionHep(indicador, anyo, trimestre, ubicacion);
  rangoEdadesHep(indicador, anyo, trimestre, ubicacion);
  transmisionHep(indicador, anyo, trimestre, ubicacion);
  afiliacionHep(indicador, anyo, trimestre, ubicacion);
  barriosOcuHep(indicador, anyo, trimestre, ubicacion);
  totalHepatitis(indicador, anyo, trimestre, ubicacion);
  discapaHep(indicador, anyo, trimestre, ubicacion);
  desplaHep(indicador, anyo, trimestre, ubicacion);
  migraHep(indicador, anyo, trimestre, ubicacion);
  carceHep(indicador, anyo, trimestre, ubicacion);
  indiHep(indicador, anyo, trimestre, ubicacion);
  icbfHep(indicador, anyo, trimestre, ubicacion);
  madComHep(indicador, anyo, trimestre, ubicacion);
  desmoHep(indicador, anyo, trimestre, ubicacion);
  psiquiHep(indicador, anyo, trimestre, ubicacion);
  vicHep(indicador, anyo, trimestre, ubicacion);
  otrosHep(indicador, anyo, trimestre, ubicacion);
  mapaTub(indicador, anyo, trimestre, ubicacion);
}

function dataTbc(indicador, anyo, trimestre, ubicacion){
  tipoCondicionTub(indicador, anyo, trimestre, ubicacion);
  coinVihTub(indicador, anyo, trimestre, ubicacion);
  municipioOcuTub(indicador, anyo, trimestre, ubicacion);
  semanasTub(indicador, anyo, trimestre, ubicacion);
  rangoEdadesTub(indicador, anyo, trimestre, ubicacion);
  afiliacionTub(indicador, anyo, trimestre, ubicacion);
  barriosOcuTub(indicador, anyo, trimestre, ubicacion);
  nacionalidadTub(indicador, anyo, trimestre, ubicacion);
  dptoEntIndTub(indicador, anyo, trimestre, ubicacion);
  etniaTub(indicador, anyo, trimestre, ubicacion);
  totalTub(indicador, anyo, trimestre, ubicacion);
  etniaCucTub(indicador, anyo, trimestre, ubicacion);
  etniaTeoTub(indicador, anyo, trimestre, ubicacion);
  etniaConTub(indicador, anyo, trimestre, ubicacion);
  etniaCarTub(indicador, anyo, trimestre, ubicacion);
  etniaTibTub(indicador, anyo, trimestre, ubicacion);
  etniaTolTub(indicador, anyo, trimestre, ubicacion);
  diaTub(indicador, anyo, trimestre, ubicacion);
  renTub(indicador, anyo, trimestre, ubicacion);
  desTub(indicador, anyo, trimestre, ubicacion);
  trabTub(indicador, anyo, trimestre, ubicacion);
  mapaTub(indicador, anyo, trimestre, ubicacion);
}

function dataVih(indicador, anyo, trimestre, ubicacion){
  rangoEdadesVih(indicador, anyo, trimestre, ubicacion);
  nacionalidadVih(indicador, anyo, trimestre, ubicacion);
  estadoClinicoVih(indicador, anyo, trimestre, ubicacion);
  afiliacionVih(indicador, anyo, trimestre, ubicacion);
  municipioOcuVih(indicador, anyo, trimestre, ubicacion);
  barriosOcuVih(indicador, anyo, trimestre, ubicacion);
  totalVih(indicador, anyo, trimestre, ubicacion);
  mapaTub(indicador, anyo, trimestre, ubicacion);

}

function dataCovid(indicador, anyo, trimestre, ubicacion, tipo){
  rangoEdadesCov(indicador, anyo, trimestre, ubicacion, tipo);
  totalCov(indicador, anyo, trimestre, ubicacion, tipo);
  barriosOcuCov(indicador, anyo, trimestre, ubicacion, tipo);
  municipioOcuCov(indicador, anyo, trimestre, ubicacion, tipo);
  semanasCov(indicador, anyo, trimestre, ubicacion, tipo);
  mapaTub(indicador, anyo, trimestre, ubicacion);

}
  
function totalDengue(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonTotalDengue', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('total', "");
    if (datos.carga) {
        dataSet1 = datos.data;
        localStorage.setItem('total', JSON.stringify(dataSet1));
    } else {
        localStorage.setItem('total', "");
    }
  }, 'json');
}

function consultasDengue(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonSemanasDengue', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('dataSet1', "");
    if (datos.carga) {
        dataSet1 = datos.data;
        localStorage.setItem('dataSet1', JSON.stringify(dataSet1));
    } else {
        localStorage.setItem('dataSet1', "");
    }
  }, 'json');
}

function afiliacionDengue(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonAfiliacionDengue', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('dataFuente', "");
    if (datos.carga) {
        dataFuente = datos.data;
        localStorage.setItem('dataFuente', JSON.stringify(dataFuente));
    } else {
        localStorage.setItem('dataFuente', "");
    }
}, 'json');
}

function rangoEdadesDengue(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonEdadesDengue', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('dataEdades', "");
    if (datos.carga) {
        dataEdades = datos.data;
        localStorage.setItem('dataEdades', JSON.stringify(dataEdades));
    }else{
      localStorage.setItem('dataEdades', "");
    }
}, 'json');
}

function hospitalizadoDengue(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonHospitalizadoDengue', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('dataHosp', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('dataHosp', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('dataHosp', "");
    }
}, 'json');
}

function municipioOcuDengue(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonMunOcuDengue', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('dataIdMuniOcu', "");
    localStorage.setItem('dataMuniOcu', "");
    if (datos.carga) {
        dataIdMuniOcu = datos.data;
        dataMuniOcu = datos.data2;
        localStorage.setItem('dataIdMuniOcu', JSON.stringify(dataIdMuniOcu));
        localStorage.setItem('dataMuniOcu', JSON.stringify(dataMuniOcu));
    } else {
        localStorage.setItem('dataIdMuniOcu', "");
        localStorage.setItem('dataMuniOcu', "");
    }
}, 'json');
}

function barriosOcuDengue(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonBarOcuDengue', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('dataIdBarOcu', "");
    localStorage.setItem('dataBarOcu', "");
    if (datos) {
        dataIdBarOcu = datos.data;
        dataBarOcu = datos.data2;
        localStorage.setItem('dataIdBarOcu', JSON.stringify(dataIdBarOcu));
        localStorage.setItem('dataBarOcu', JSON.stringify(dataBarOcu));
    } 
}, 'json');
}

function semanasHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonSemanasHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('semHep', "");
    if (datos.carga) {
        dataSet1 = datos.data;
        localStorage.setItem('semHep', JSON.stringify(dataSet1));
    } else {
        localStorage.setItem('semHep', "");
    }
  }, 'json');
}

function discapaHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonDiscapaHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('disHep', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('disHep', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('disHep', "");
    }
}, 'json');
}

function desplaHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonDesplaHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('desplaHep', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('desplaHep', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('desplaHep', "");
    }
}, 'json');
}

function migraHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonMigraHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('migraHep', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('migraHep', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('migraHep', "");
    }
}, 'json');
}

function carceHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonCarceHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('carceHep', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('carceHep', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('carceHep', "");
    }
}, 'json');
}

function indiHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonIndiHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('indiHep', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('indiHep', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('indiHep', "");
    }
}, 'json');
}

function icbfHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonIcbfHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    dataHosp = datos.data;
    console.log(dataHosp);
    localStorage.setItem('icbfHep', JSON.stringify(dataHosp));
}, 'json');
}

function madComHep(indicador, anyo, trimestre, ubicacion){
    localStorage.setItem('madComHep', JSON.stringify([]));
  $.post(ruta+'/graficas/jsonMadComHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    
    dataHosp = datos.data;
    localStorage.setItem('madComHep', JSON.stringify(dataHosp));
}, 'json');
}

function desmoHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonDesmoHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('desmoHep', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('desmoHep', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('desmoHep', "");
    }
}, 'json');
}

function psiquiHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonPsiquiHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('psiHep', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('psiHep', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('psiHep', "");
    }
}, 'json');
}

function vicHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonVicHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('vicHep', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('vicHep', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('vicHep', "");
    }
}, 'json');
}

function otrosHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonOtrosHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('otrosHep', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('otrosHep', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('otrosHep', "");
    }
}, 'json');
}

function municipioOcuHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonMunOcuHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('dataIdMuniHep', "");
    localStorage.setItem('dataMuniHep', "");
    if (datos.carga) {
        dataIdMuniOcu = datos.data;
        dataMuniOcu = datos.data2;
        localStorage.setItem('dataIdMuniHep', JSON.stringify(dataIdMuniOcu));
        localStorage.setItem('dataMuniHep', JSON.stringify(dataMuniOcu));
    } else {
        localStorage.setItem('dataIdMuniHep', "");
        localStorage.setItem('dataMuniHep', "");
    }
}, 'json');
}

function gestantesHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonGestantesHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('gestanHep', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('gestanHep', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('gestanHep', "");
    }
}, 'json');
}

function coinfeccionHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonCoinfeccionHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('coinHep', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('coinHep', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('coinHep', "");
    }
}, 'json');
}

function rangoEdadesHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonEdadesHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('edadesHep', "");
    if (datos.carga) {
        dataEdades = datos.data;
        localStorage.setItem('edadesHep', JSON.stringify(dataEdades));
    }else{
      localStorage.setItem('edadesHep', "");
    }
}, 'json');
}

function transmisionHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonTransmisionHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('transmisionHep', "");
    if (datos.carga) {
      //  console.log(datos);
        dataFuente = datos.data;
        localStorage.setItem('transmisionHep', JSON.stringify(dataFuente));
    } else {
        localStorage.setItem('transmisionHep', "");
    }
}, 'json');
}

function afiliacionHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonAfiliacionHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('afiliacionHep', "");
    if (datos.carga) {
      //  console.log(datos);
        dataFuente = datos.data;
        localStorage.setItem('afiliacionHep', JSON.stringify(dataFuente));
    } else {
        localStorage.setItem('afiliacionHep', "");
    }
}, 'json');
}

function totalHepatitis(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonTotalHepatitis', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('total', "");
    if (datos.carga) {
        dataSet1 = datos.data;
        localStorage.setItem('total', JSON.stringify(dataSet1));
    } else {
        localStorage.setItem('total', "");
    }
  }, 'json');
}


function barriosOcuHep(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonBarOcuHep', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('dataIdBarOcuHep', "");
    localStorage.setItem('dataBarOcuHep', "");
    if (datos) {
        dataIdBarOcu = datos.data;
        dataBarOcu = datos.data2;
        localStorage.setItem('dataIdBarOcuHep', JSON.stringify(dataIdBarOcu));
        localStorage.setItem('dataBarOcuHep', JSON.stringify(dataBarOcu));
    } 
}, 'json');
}

function tipoCondicionTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonCondicionTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('condTub', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('condTub', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('condTub', "");
    }
}, 'json');
}

function coinVihTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonCoinfeccionTbc', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('coinTub', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('coinTub', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('coinTub', "");
    }
}, 'json');
}

function mapaTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonMapaTbc', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    console.log("estoy aqui post");
    localStorage.setItem('mapaTub', "");
    console.log(datos);
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('mapaTub', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('mapaTub', "");
    }
}, 'json');
}

function municipioOcuTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonMunOcuTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('dataIdMuniTub', "");
    localStorage.setItem('dataMuniTub', "");
    if (datos.carga) {
        dataIdMuniOcu = datos.data;
        dataMuniOcu = datos.data2;
        localStorage.setItem('dataIdMuniTub', JSON.stringify(dataIdMuniOcu));
        localStorage.setItem('dataMuniTub', JSON.stringify(dataMuniOcu));
    } else {
        localStorage.setItem('dataIdMuniTub', "");
        localStorage.setItem('dataMuniTub', "");
    }
}, 'json');
}

function semanasTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonSemanasTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('semTub', "");
    if (datos.carga) {
        dataSet1 = datos.data;
        localStorage.setItem('semTub', JSON.stringify(dataSet1));
    } else {
        localStorage.setItem('semTub', "");
    }
  }, 'json');
}

function rangoEdadesTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonEdadesTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('edadesTub', "");
    if (datos.carga) {
        dataEdades = datos.data;
        localStorage.setItem('edadesTub', JSON.stringify(dataEdades));
    }else{
      localStorage.setItem('edadesTub', "");
    }
}, 'json');
}

function afiliacionTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonAfiliacionTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('afiliacionTub', "");
    if (datos.carga) {
      //  console.log(datos);
        dataFuente = datos.data;
        localStorage.setItem('afiliacionTub', JSON.stringify(dataFuente));
    } else {
        localStorage.setItem('afiliacionTub', "");
    }
}, 'json');
}

function barriosOcuTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonBarOcuTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('dataIdBarOcuTub', "");
    localStorage.setItem('dataBarOcuTub', "");
    if (datos) {
        dataIdBarOcu = datos.data;
        dataBarOcu = datos.data2;
        localStorage.setItem('dataIdBarOcuTub', JSON.stringify(dataIdBarOcu));
        localStorage.setItem('dataBarOcuTub', JSON.stringify(dataBarOcu));
    } 
}, 'json');
}

function nacionalidadTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonNacOcuTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('dataIdNacTub', "");
    localStorage.setItem('dataNacTub', "");
    if (datos.carga) {
        dataIdMuniOcu = datos.data;
        dataMuniOcu = datos.data2;
        localStorage.setItem('dataIdNacTub', JSON.stringify(dataIdMuniOcu));
        localStorage.setItem('dataNacTub', JSON.stringify(dataMuniOcu));
    } else {
        localStorage.setItem('dataIdNacTub', "");
        localStorage.setItem('dataNacTub', "");
    }
}, 'json');
}

function totalTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonTotalTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('total', "");
    if (datos.carga) {
        dataSet1 = datos.data;
        localStorage.setItem('total', JSON.stringify(dataSet1));
    } else {
        localStorage.setItem('total', "");
    }
  }, 'json');
}

function etniaTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonEtniaTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('etnTub', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('etnTub', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('etnTub', "");
    }
}, 'json');
}

function dptoEntIndTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonDptoEntIndTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('etnIdDptoEntIndTub', "");
    localStorage.setItem('etnDptoEntIndTub', "");
    if (datos.carga) {
      console.log(datos);
      etnIdDptoEntIndTub = datos.data;
      etnDptoEntIndTub = datos.data2;
        localStorage.setItem('etnIdDptoEntIndTub', JSON.stringify(etnIdDptoEntIndTub));
        localStorage.setItem('etnDptoEntIndTub', JSON.stringify(etnDptoEntIndTub));
    } else {
        localStorage.setItem('etnIdDptoEntIndTub', "");
        localStorage.setItem('etnDptoEntIndTub', "");
    }
}, 'json');
}

function etniaCucTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonEtniaCucTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('etnCucTub', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('etnCucTub', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('etnCucTub', "");
    }
}, 'json');
}

function etniaTeoTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonEtniaTeoTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('etnTeoTub', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('etnTeoTub', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('etnTeoTub', "");
    }
}, 'json');
}

function etniaConTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonEtniaConTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('etnConTub', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('etnConTub', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('etnConTub', "");
    }
}, 'json');
}

function etniaCarTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonEtniaCarTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('etnCarTub', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('etnCarTub', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('etnCarTub', "");
    }
}, 'json');
}

function etniaTibTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonEtniaTibTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('etnTibTub', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('etnTibTub', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('etnTibTub', "");
    }
}, 'json');
}

function etniaTolTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonEtniaTolTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('etnTolTub', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('etnTolTub', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('etnTolTub', "");
    }
}, 'json');
}

function diaTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonDiaTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('diaTub', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('diaTub', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('diaTub', "");
    }
}, 'json');
}

function renTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonRenTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('renTub', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('renTub', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('renTub', "");
    }
}, 'json');
}

function desTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonDesTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('desTub', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('desTub', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('desTub', "");
    }
}, 'json');
}

function trabTub(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonTrabTub', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('trabTub', "");
    if (datos.carga) {
        dataHosp = datos.data;
        localStorage.setItem('trabTub', JSON.stringify(dataHosp));
    } else {
      localStorage.setItem('trabTub', "");
    }
}, 'json');
}

function rangoEdadesVih(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonEdadesVih', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('edadesVih', "");
    if (datos.carga) {
        dataEdades = datos.data;
        localStorage.setItem('edadesVih', JSON.stringify(dataEdades));
    }else{
      localStorage.setItem('edadesVih', "");
    }
}, 'json');
}

function nacionalidadVih(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonNacOcuVih', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('dataIdNacVih', "");
    localStorage.setItem('dataNacVih', "");
    if (datos.carga) {
        dataIdMuniOcu = datos.data;
        dataMuniOcu = datos.data2;
        localStorage.setItem('dataIdNacVih', JSON.stringify(dataIdMuniOcu));
        localStorage.setItem('dataNacVih', JSON.stringify(dataMuniOcu));
    } else {
      localStorage.setItem('dataIdNacVih', "");
      localStorage.setItem('dataNacVih', "");
    }
}, 'json');
}

function estadoClinicoVih(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonEstadoClinicoVih', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('estCliVih', "");
    if (datos.carga) {
      //  console.log(datos);
        dataFuente = datos.data;
        localStorage.setItem('estCliVih', JSON.stringify(dataFuente));
    } else {
      localStorage.setItem('estCliVih', "");
    }
}, 'json');
}

function afiliacionVih(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonAfiliacionVih', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('afiliacionVih', "");
    if (datos.carga) {
      //  console.log(datos);
        dataFuente = datos.data;
   //     localStorage.setItem('afiliacionVih', JSON.stringify(dataFuente));
      localStorage.setItem('afiliacionVih', JSON.stringify(dataFuente));

    } else {
      localStorage.setItem('afiliacionVih', "");
    }
}, 'json');
}

function municipioOcuVih(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonMunOcuVih', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('dataIdMuniVih', "");
    localStorage.setItem('dataMuniVih', "");
    if (datos.carga) {
        dataIdMuniOcu = datos.data;
        dataMuniOcu = datos.data2;
        localStorage.setItem('dataIdMuniVih', JSON.stringify(dataIdMuniOcu));
        localStorage.setItem('dataMuniVih', JSON.stringify(dataMuniOcu));
    } else {
      localStorage.setItem('dataIdMuniVih', "");
      localStorage.setItem('dataMuniVih', "");
    }
}, 'json');
}

function barriosOcuVih(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonBarOcuVih', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('dataIdBarOcuVih', "");
    localStorage.setItem('dataBarOcuVih', "");
    if (datos) {
        dataIdBarOcu = datos.data;
        dataBarOcu = datos.data2;
        localStorage.setItem('dataIdBarOcuVih', JSON.stringify(dataIdBarOcu));
        localStorage.setItem('dataBarOcuVih', JSON.stringify(dataBarOcu));
    } 
}, 'json');
}

function totalVih(indicador, anyo, trimestre, ubicacion){
  $.post(ruta+'/graficas/jsonTotalVih', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion}, function (datos) {
    localStorage.setItem('total', "");
    console.log(datos);
    if (datos.carga) {
        dataSet1 = datos.data;
        localStorage.setItem('total', JSON.stringify(dataSet1));
    } else {
      localStorage.setItem('total', "");
    }
  }, 'json');
}

function loginIndicador(usuario, pass, form){
  $.post(ruta+'/graficas/jsonLoginPass', {usuario: usuario, pass: pass}, function (datos) {
    if (datos.carga) {
        dataSet1 = datos.data;
        sessionStorage.setItem('sessionInfIndSal', dataSet1);
        if(dataSet1 == 'IJ9*BXTwKKSf@gbDts'){
          placeOrder(form);
        }
    } else {
        $('#loginError').modal('show');
        dataSet1 = datos.data;
        sessionStorage.setItem('sessionInfIndSal', dataSet1);
    }
  }, 'json');
}

function rangoEdadesCov(indicador, anyo, trimestre, ubicacion, tipo){
  $.post(ruta+'/graficas/jsonEdadesCov', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion, tipo: tipo}, function (datos) {
    localStorage.setItem('edadesCov', "");
    if (datos.carga) {
        dataEdades = datos.data;
        localStorage.setItem('edadesCov', JSON.stringify(dataEdades));
    }else{
      localStorage.setItem('edadesCov', "");
    }
}, 'json');
}

function totalCov(indicador, anyo, trimestre, ubicacion, tipo){
  $.post(ruta+'/graficas/jsonTotalCov', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion, tipo: tipo}, function (datos) {
    localStorage.setItem('total', "");
    if (datos.carga) {
        dataSet1 = datos.data;
        localStorage.setItem('total', JSON.stringify(dataSet1));
    } else {
      localStorage.setItem('total', "");
    }
  }, 'json');
}

function barriosOcuCov(indicador, anyo, trimestre, ubicacion, tipo){
  $.post(ruta+'/graficas/jsonBarOcuCov', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion, tipo: tipo}, function (datos) {
    localStorage.setItem('dataIdBarOcuCov', "");
    localStorage.setItem('dataBarOcuCov', "");
    if (datos) {
        dataIdBarOcu = datos.data;
        dataBarOcu = datos.data2;
        localStorage.setItem('dataIdBarOcuCov', JSON.stringify(dataIdBarOcu));
        localStorage.setItem('dataBarOcuCov', JSON.stringify(dataBarOcu));
    } 
}, 'json');
}

function municipioOcuCov(indicador, anyo, trimestre, ubicacion, tipo){
  $.post(ruta+'/graficas/jsonMunOcuCov', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion, tipo: tipo}, function (datos) {
    localStorage.setItem('dataIdMuniCov', "");
    localStorage.setItem('dataMuniCov', "");
    if (datos.carga) {
        dataIdMuniOcu = datos.data;
        dataMuniOcu = datos.data2;
        localStorage.setItem('dataIdMuniCov', JSON.stringify(dataIdMuniOcu));
        localStorage.setItem('dataMuniCov', JSON.stringify(dataMuniOcu));
    } else {
      localStorage.setItem('dataIdMuniCov', "");
      localStorage.setItem('dataMuniCov', "");
    }
}, 'json');
}

function semanasCov(indicador, anyo, trimestre, ubicacion, tipo){
  $.post(ruta+'/graficas/jsonSemanasCov', {indicador: indicador, anyo: anyo, trimestre: trimestre, dpto_muni: ubicacion, tipo: tipo}, function (datos) {
    localStorage.setItem('semCov', "");
    console.log(datos);
    if (datos.carga) {
        dataSet1 = datos.data;
        localStorage.setItem('semCov', JSON.stringify(dataSet1));
    } else {
        localStorage.setItem('semCov', "");
    }
  }, 'json');
}





