$(document).ready(function()
{

    var datos;
    var ruta = window.location.protocol + "//" + window.location.host;
    // initialize datatable
    $('#dt-basic-example').dataTable(
    {
        responsive:
        {
            details:
            {
                display: $.fn.dataTable.Responsive.display.modal(
                {
                    header: function(row)
                    {
                        var data = row.data();
                        datos = data[0];
                        return 'Detalles para el registro ' + data[0];
                    }
                }),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll(
                {
                    tableClass: 'table table-responsive'
                })
            }
        },
    });
});