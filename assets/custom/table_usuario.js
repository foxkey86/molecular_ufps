$(document).ready(function()
{
    var datos;
    var ruta = window.location.protocol + "//" + window.location.host;
    // initialize datatable
    $('#dt-basic-example').dataTable(
    {
        responsive:
        {
            details:
            {
                display: $.fn.dataTable.Responsive.display.modal(
                {
                    header: function(row)
                    {
                        var data = row.data();
                        datos = data[0];
                        return 'Detalles para el usuario ' + data[2];
                    }
                }),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll(
                {
                    tableClass: 'table table-responsive'
                })
            }
        },
        columnDefs: [
        {
            targets: -1,
            title: 'Admin Control',
            orderable: false,
            render: function(data, type, full, meta)
            {
                return "\n\t\t\t\t\t\t<div class='d-flex mt-2'>\n\t\t\t\t\t\t\t<a href='"+ruta+"/usuarios/editar_usuario/"+full[0]+"' class='btn btn-sm btn-outline-primary mr-2' title='Editar'><i class=\"fal fa-edit\"></i> Editar</a>\n\t\t\t\t\t\t\t\t<a class='btn btn-sm btn-outline-danger mr-2' href='"+ruta+"/usuarios/borrar_usuario/"+full[0]+"'><i class=\"fal fa-trash-alt\"></i> Eliminar</a>\n\t\t\t\t\t\t\t\t\t</div>";
            },
        },
        {
            targets: 8,
            /*	The `data` parameter refers to the data for the cell (defined by the
                `data` option, which defaults to the column being worked with, in this case `data: 16`.*/
            render: function(data, type, full, meta)
            {
                var badge = {
                    2:
                    {
                        'title': 'Pendiente',
                        'class': 'badge-warning'
                    },
                    1:
                    {
                        'title': 'Activo',
                        'class': 'badge-success'
                    },
                    0:
                    {
                        'title': 'Inactivo',
                        'class': 'badge-secondary'
                    },
                    4:
                    {
                        'title': 'Attempt #1',
                        'class': 'bg-danger-100 text-white'
                    },
                    5:
                    {
                        'title': 'Attempt #2',
                        'class': 'bg-danger-300 text-white'
                    },
                    6:
                    {
                        'title': 'Failed',
                        'class': 'badge-danger'
                    },
                    7:
                    {
                        'title': 'Attention!',
                        'class': 'badge-primary'
                    },
                    8:
                    {
                        'title': 'In Progress',
                        'class': 'badge-success'
                    },
                };
                if (typeof badge[data] === 'undefined')
                {
                    return data;
                }
                return '<span class="badge ' + badge[data].class + ' badge-pill">' + badge[data].title + '</span>';
            },
        },
        {
            targets: 11,
            /*	The `data` parameter refers to the data for the cell (defined by the
                `data` option, which defaults to the column being worked with, in this case `data: 16`.*/
            render: function(data, type, full, meta)
            {
                var badge = {
                    1:
                    {
                        'title': 'Administrador',
                        'class': 'bg-primary-600'
                    },
                    2:
                    {
                        'title': 'Coordinador Sistema',
                        'class': 'bg-danger-900'
                    },
                    3:
                    {
                        'title': 'Usuario Particular',
                        'class': 'bg-info-900'
                    },
                    4:
                    {
                        'title': 'Coordinador IDS',
                        'class': 'bg-info-500'
                    },
                    5:
                    {
                        'title': 'Personal IDS',
                        'class': 'bg-info-500'
                    },
                    6:
                    {
                        'title': 'Analista de Laboratorio',
                        'class': 'bg-info-500'
                    },
                    7:
                    {
                        'title': 'Jefe de Laboratorio',
                        'class': 'bg-info-500'
                    },
                    8:
                    {
                        'title': 'Coordinador de Laboratorio',
                        'class': 'bg-info-500'
                    },
                    9:
                    {
                        'title': 'Gestor de Calidad',
                        'class': 'bg-info-500'
                    },
                    10:
                    {
                        'title': 'Responsable de Dimensión',
                        'class': 'bg-fusion-800'
                    },
                };
                if (typeof badge[data] === 'undefined')
                {
                    return data;
                }
                return '<span class="badge ' + badge[data].class + ' badge-pill">' + badge[data].title + '</span>';
            },
        }],
    });
});