$("#profesion").hide();
$("#institucion").hide();
$("#oficio").hide();

if(document.getElementById("tipo_usuario").value == '1'){
    $("#profesion").show();
    $("#institucion").show();
}else{
    if(document.getElementById("tipo_usuario").value == '2'){
        $("#oficio").show();
    }
}

function cambia_tipo_usuario(opcion) {
    if (opcion == '1') {
        console.log(opcion);
        $("#profesion").show();
        $("#institucion").show();
        $("#oficio").hide();
        document.getElementById("profesion").required = true;
        document.getElementById("oficio").required = false;

    } else {
        $("#profesion").hide();
        $("#institucion").hide();
        $("#oficio").show();
        document.getElementById("profesion").required = false;
        document.getElementById("oficio").required = true;
        console.log(opcion);
    }
}

function validarRegistro(){

    var a = document.getElementById("password").value;
    var b = document.getElementById("confirm_password").value;

    if(a.length >= 8 && b.length >= 8){
        if (a!=b) {
            document.getElementById("confirm_password").className += " is-invalid";
        }else{
            form.submit();
        }
    }else{
        document.getElementById("password").className += " is-invalid";
        document.getElementById("confirm_password").className += " is-invalid";
    }


}