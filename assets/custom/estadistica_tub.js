$('#js-page-content').smartPanel({
    localStorage: true,
    onChange: function () {},
    onSave: function () {},
    opacity: 1,
    deleteSettingsKey: '#deletesettingskey-options',
    settingsKeyLabel: 'Reset settings?',
    deletePositionKey: '#deletepositionkey-options',
    positionKeyLabel: 'Reset position?',    
    sortable: true,
    buttonOrder: '%collapse% %fullscreen% %close%',
    buttonOrderDropdown: '%refresh% %locked% %color% %custom% %reset%',
    customButton: true,
    customButtonLabel: "Custom Button",
    onCustom: function () {},
    closeButton: true,
    onClosepanel: function() {
      if (myapp_config.debugState)
        console.log($(this).closest(".panel").attr('id') + " onClosepanel")
    },
    fullscreenButton: true,
    onFullscreen: function() {
      if (myapp_config.debugState)
        console.log($(this).closest(".panel").attr('id') + " onFullscreen")
    },
    collapseButton: true,
    onCollapse: function() {
      if (myapp_config.debugState)
        console.log($(this).closest(".panel").attr('id') + " onCollapse")
    },
    lockedButton: true,
    lockedButtonLabel: "Lock Position",
    onLocked: function() {
      if (myapp_config.debugState)
        console.log($(this).closest(".panel").attr('id') + " onLocked")
    },
    refreshButton: true,
    refreshButtonLabel: "Refrescar",
    onRefresh: function() {
      if (myapp_config.debugState)
        console.log($(this).closest(".panel").attr('id') + " onRefresh")
    },
    colorButton: true,
    colorButtonLabel: "Panel Style",
    onColor: function() {
      if (myapp_config.debugState)
        console.log($(this).closest(".panel").attr('id') + " onColor")
    },
    panelColors: ['bg-primary-700 bg-success-gradient',
            'bg-primary-500 bg-info-gradient',
            'bg-primary-600 bg-primary-gradient',
            'bg-info-600 bg-primray-gradient',                      
            'bg-info-600 bg-info-gradient',
            'bg-info-700 bg-success-gradient',
            'bg-success-900 bg-info-gradient',
            'bg-success-700 bg-primary-gradient', 
            'bg-success-600 bg-success-gradient',                                 
            'bg-danger-900 bg-info-gradient',
            'bg-fusion-400 bg-fusion-gradient', 
            'bg-faded'],
    resetButton: true,
    resetButtonLabel: "Reset Panel",
    onReset: function() {
      if (myapp_config.debugState)
        console.log( $(this).closest(".panel").attr('id') + " " )
    }
});
    
$('#js-page-content').smartPanel();
    
    /* defined datas */
    var dataTargetProfit = [
        [1354586000000, 153],
        [1364587000000, 658],
        [1374588000000, 198],
        [1384589000000, 663],
        [1394590000000, 801],
        [1404591000000, 1080],
        [1414592000000, 353],
        [1424593000000, 749],
        [1434594000000, 523],
        [1444595000000, 258],
        [1454596000000, 688],
        [1464597000000, 364]
    ]
    var dataProfit = [
        [1354586000000, 53],
        [1364587000000, 65],
        [1374588000000, 98],
        [1384589000000, 83],
        [1394590000000, 980],
        [1404591000000, 808],
        [1414592000000, 720],
        [1424593000000, 674],
        [1434594000000, 23],
        [1444595000000, 79],
        [1454596000000, 88],
        [1464597000000, 36]
    ]
    var dataSignups = [
        [1354586000000, 647],
        [1364587000000, 435],
        [1374588000000, 784],
        [1384589000000, 346],
        [1394590000000, 487],
        [1404591000000, 463],
        [1414592000000, 479],
        [1424593000000, 236],
        [1434594000000, 843],
        [1444595000000, 657],
        [1454596000000, 241],
        [1464597000000, 341]
    ]

    var dataSet1 = Array();
    var dataGest = Array();
    var dataEtn = Array();
    var dataDia = Array();
    var dataRen = Array();
    var dataDes = Array();
    var dataTrab = Array();
    var dataEtnCuc = Array();
    var dataEtnTeo = Array();
    var dataEtnCon = Array();
    var dataEtnCar = Array();
    var dataEtnTib = Array();
    var dataEtnTol = Array();


    var dataSet2 = Array();
    var dataFuente = Array();
    var dataEdades = Array();
    var dataIdMuniOcu = Array();
    var dataBarOcu = Array();
    var dataIdBarOcu = Array();

    var property = Array();
    var dataMuniOcu = Array();


    property = JSON.parse(localStorage.getItem('property'));
    dataCoin = JSON.parse(localStorage.getItem('coinTub'));
    dataIdMuniOcu = JSON.parse(localStorage.getItem('dataIdMuniTub'));
    dataMuniOcu = JSON.parse(localStorage.getItem('dataMuniTub'));
    dataIdIndEtn = JSON.parse(localStorage.getItem('etnIdDptoEntIndTub'));
    dataIndEtn = JSON.parse(localStorage.getItem('etnDptoEntIndTub'));
    dataSet1 = JSON.parse(localStorage.getItem('semTub'));
    dataEdades = JSON.parse(localStorage.getItem('edadesTub'));
    dataFuente = JSON.parse(localStorage.getItem('afiliacionTub'));
    dataIdBarOcu = JSON.parse(localStorage.getItem('dataIdBarOcuTub'));
    dataBarOcu = JSON.parse(localStorage.getItem('dataBarOcuTub'));
    dataIdNacTub = JSON.parse(localStorage.getItem('dataIdNacTub'));
    dataNacTub = JSON.parse(localStorage.getItem('dataNacTub'));
    dataCond = JSON.parse(localStorage.getItem('condTub'));
    dataEtn = JSON.parse(localStorage.getItem('etnTub'));
    dataDia = JSON.parse(localStorage.getItem('diaTub'));
    dataRen = JSON.parse(localStorage.getItem('renTub'));
    dataDes = JSON.parse(localStorage.getItem('desTub'));
    dataTrab = JSON.parse(localStorage.getItem('trabTub'));
    dataEtnCuc = JSON.parse(localStorage.getItem('etnCucTub'));
    dataEtnTeo = JSON.parse(localStorage.getItem('etnTeoTub'));
    dataEtnCon = JSON.parse(localStorage.getItem('etnConTub'));
    dataEtnCar = JSON.parse(localStorage.getItem('etnCarTub'));
    dataEtnTib = JSON.parse(localStorage.getItem('etnTibTub'));
    dataEtnTol = JSON.parse(localStorage.getItem('etnTolTub'));

    dataTrans = JSON.parse(localStorage.getItem('transmisionHep'));

    if(localStorage.getItem('mapaTub')){
        dataImagen = JSON.parse(localStorage.getItem('mapaTub'));
        document.getElementById("imagenmapa").src= dataImagen.src;
        document.getElementById("DivMapa").style.display = "block";
    }else{
        document.getElementById("DivMapa").style.display = "none";
    }

    var etndpt = document.getElementById("DivEtnDpt");
    var etnteo = document.getElementById("DivEtnTeo");
    var etncon = document.getElementById("DivEtnCon");
    var etncar = document.getElementById("DivEtnCar");
    var etntib = document.getElementById("DivEtnTib");
    var etntol = document.getElementById("DivEtnTol");
    if(property[1] != 'Norte de Santander'){  
        etndpt.style.display = "none";
        etnteo.style.display = "none";
        etncon.style.display = "none";
        etncar.style.display = "none";
        etntib.style.display = "none";
        etntol.style.display = "none";
    }

    if(dataEtn && dataEtn.length == 0){
        dataEtn = Array(Array(0,0), Array(10,0));
        document.getElementById("flot-etn").style.display = "block";
        document.getElementById("nodatosdpt").style.display = "none";
    }else{
        if(!validarArrayVacio(dataEtn)){
            document.getElementById("flot-etn").style.display = "none";
            document.getElementById("nodatosdpt").style.display = "block";
        }else{
            document.getElementById("flot-etn").style.display = "block";
            document.getElementById("nodatosdpt").style.display = "none";
        }
    }

    if(dataEtnCuc && dataEtnCuc.length == 0){
        dataEtnCuc = Array(Array(0,0), Array(10,0));
        document.getElementById("flot-etn-cuc").style.display = "block";
        document.getElementById("nodatoscuc").style.display = "none";
    }else{
        if(!validarArrayVacio(dataEtnCuc)){
            document.getElementById("flot-etn-cuc").style.display = "none";
            document.getElementById("nodatoscuc").style.display = "block";
        }else{
            document.getElementById("flot-etn-cuc").style.display = "block";
            document.getElementById("nodatoscuc").style.display = "none";
        }
    }

    if(dataEtnTeo && dataEtnTeo.length == 0){
        dataEtnTeo = Array(Array(0,0), Array(10,0));
        document.getElementById("flot-etn-teo").style.display = "block";
        document.getElementById("nodatosteo").style.display = "none";
    }else{
        if(!validarArrayVacio(dataEtnTeo)){
            document.getElementById("flot-etn-teo").style.display = "none";
            document.getElementById("nodatosteo").style.display = "block";
        }else{
            document.getElementById("flot-etn-teo").style.display = "block";
            document.getElementById("nodatosteo").style.display = "none";
        }
    }
    if(dataEtnCon && dataEtnCon.length == 0){
        dataEtnCon = Array(Array(0,0), Array(10,0));
        document.getElementById("flot-etn-con").style.display = "block";
        document.getElementById("nodatoscon").style.display = "none";
    }else{
        if(!validarArrayVacio(dataEtnCon)){
            document.getElementById("flot-etn-con").style.display = "none";
            document.getElementById("nodatoscon").style.display = "block";
        }else{
            document.getElementById("flot-etn-con").style.display = "block";
            document.getElementById("nodatoscon").style.display = "none";
        }
    }

    if(dataEtnCar && dataEtnCar.length == 0){
        dataEtnCar = Array(Array(0,0), Array(10,0));
        document.getElementById("flot-etn-car").style.display = "block";
        document.getElementById("nodatoscar").style.display = "none";
    }else{
        if(!validarArrayVacio(dataEtnCar)){
            document.getElementById("flot-etn-car").style.display = "none";
            document.getElementById("nodatoscar").style.display = "block";
        }else{
            document.getElementById("flot-etn-car").style.display = "block";
            document.getElementById("nodatoscar").style.display = "none";
        }
    }

    if(dataEtnTib && dataEtnTib.length == 0){
        dataEtnTib = Array(Array(0,0), Array(10,0));
        document.getElementById("flot-etn-tib").style.display = "block";
        document.getElementById("nodatostib").style.display = "none";
    }else{
        if(!validarArrayVacio(dataEtnTib)){
            document.getElementById("flot-etn-tib").style.display = "none";
            document.getElementById("nodatostib").style.display = "block";
        }else{
            document.getElementById("flot-etn-tib").style.display = "block";
            document.getElementById("nodatostib").style.display = "none";
        }
    }

    if(dataEtnTol && dataEtnTol.length == 0){
        dataEtnTol = Array(Array(0,0), Array(10,0));
        document.getElementById("flot-etn-tol").style.display = "block";
        document.getElementById("nodatostol").style.display = "none";
    }else{
        if(!validarArrayVacio(dataEtnTol)){
            document.getElementById("flot-etn-tol").style.display = "none";
            document.getElementById("nodatostol").style.display = "block";
        }else{
            document.getElementById("flot-etn-tol").style.display = "block";
            document.getElementById("nodatostol").style.display = "none";
        }
    }
  /*  
    
   
   

    dataEdades = JSON.parse(localStorage.getItem('dataEdades'));
*/
    /*** variables de consulta */
    id_evento = parseInt(localStorage.getItem('id_evento'));
    anyo = parseInt(localStorage.getItem('anyo'));
    dpto_muni = parseInt(localStorage.getItem('dpto_muni'));

    total = JSON.parse(localStorage.getItem('total'));

    console.log(total);

    /*** Variables totales */
    document.getElementById('canMu').innerHTML=total[0].num;
    document.getElementById('canHo').innerHTML=total[1].num;
    document.getElementById('canTo').innerHTML=parseInt(total[0].num)+parseInt(total[1].num);
    document.getElementById('canInd').innerHTML=total[2].enfermedad;
    document.getElementById('canUbi').innerHTML=total[3].ubicacion;
    document.getElementById('canAn').innerHTML=total[4].anyo;
    document.getElementById('canTri').innerHTML=total[5].trimestre;

    var x = document.getElementById("DivBarrios");
    var y = document.getElementById("DivMunicipio");
    if(property[1] != 'Norte de Santander'){   
        x.style.display = "block";
        y.style.display = "none";
        console.log(dataBarOcu);
       // var data="<div id='panel-4' class='panel'><div class='panel-hdr'><h2>Relación <span class='fw-300'><i> Casos vs Barrio de Ocurrencia </i></span></h2><div class='panel-toolbar'></div></div><div class='panel-container show'><div class='panel-content'><div id='flot-bar-ocu' style='width:100%; height:3500px;'></div></div></div></div>";  
      //  document.getElementById('DivBarrios').innerHTML=data;  

      largo = "300px";
      if(dataIdBarOcu.length > 10){
          veces = (dataIdBarOcu.length / 10) * 300;
          largo = veces + "px";
      }
      document.getElementById("flot-bar-ocu").style.width = "100%";
      document.getElementById("flot-bar-ocu").style.height = largo;

        var flotBar3 = $.plot("#flot-bar-ocu", [
            {
                data: dataIdBarOcu,
                color: myapp_get_color.success_400
            }],
            {
                series:
                {
                    bars:
                    {
                        show: true,
                        align: "center",
                        barWidth: 1,
                        horizontal: true,
                        lineWidth: 1,
                        fillColor:
                        {
                            colors: [
                            {
                                opacity: 1
                            },
                            {
                                opacity: 0.8
                            }]
                        }
                    }
                },
                grid:
                {
                    hoverable: true,
                    clickable: true,
                    tickColor: '#f2f2f2',
                    borderWidth: 1,
                    borderColor: '#eee'
                },
                tooltip: true,
                tooltipOpts:
                {
                    cssClass: 'tooltip-inner',
                    defaultTheme: false,
                    content: "Casos: <span class='text-warning fw-500'>%x</span> - Barrio: <span class='text-success fw-500'>%y</span>"
                },
                yaxis:
                {
                    color: "black",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelPadding: 10,
                    tickColor: '#eee',
                    axisLabel: "Barrios",
                    ticks: dataBarOcu,
                    tickLength: 0,
                    font:
                    {
                        color: '#000',
                        size: 10
                    }
                },
                xaxis:
                {
                    color: "black",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelPadding: 10,
                    tickColor: '#eee',
                    axisLabel: "Casos",
                    tickLength: 0,
                    font:
                    {
                        color: '#000',
                        size: 10
                    }
                }
            });  
      
    }else{
        x.style.display = "none";
        y.style.display = "block";

        largo = "300px";
        if(dataIdMuniOcu.length > 10){
            veces = (dataIdMuniOcu.length / 10) * 300;
            largo = veces + "px";
        }
        document.getElementById("flot-mun-ocu").style.width = "100%";
        document.getElementById("flot-mun-ocu").style.height = largo;  

        var flotBar4 = $.plot("#flot-mun-ocu", [
            {
                data: dataIdMuniOcu,
                color: myapp_get_color.danger_500
            }],
            {
                series:
                {
                    bars:
                    {
                        show: true,
                        align: "center",
                        barWidth: 1,
                        horizontal: true,
                        lineWidth: 1,
                        fillColor:
                        {
                            colors: [
                            {
                                opacity: 1
                            },
                            {
                                opacity: 0.8
                            }]
                        }
                    }
                },
                grid:
                {
                    hoverable: true,
                    clickable: true,
                    tickColor: '#f2f2f2',
                    borderWidth: 1,
                    borderColor: '#eee'
                },
                tooltip: true,
                tooltipOpts:
                {
                    cssClass: 'tooltip-inner',
                    defaultTheme: false,
                    content: "Casos: <span class='text-warning fw-500'>%x</span> - Municipio: <span class='text-success fw-500'>%y</span>"
                },
                yaxis:
                {
                    color: "black",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelPadding: 10,
                    tickColor: '#eee',
                    axisLabel: "Municipios",
                    ticks: dataMuniOcu,
                    tickLength: 0,
                    font:
                    {
                        color: '#000',
                        size: 10
                    }
                },
                xaxis:
                {
                    color: "black",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelPadding: 10,
                    tickColor: '#eee',
                    axisLabel: "Casos",
                    tickLength: 0,
                    font:
                    {
                        color: '#000',
                        size: 10
                    }
                }
            });
    }

    
    var et = document.getElementById("DivIndigena");
    if(dataIdIndEtn.length > 0){   
        et.style.display = "block";

        largo = "300px";
        if(dataIdIndEtn.length > 10){
            veces = (dataIdIndEtn.length / 10) * 300;
            largo = veces + "px";
        }
        document.getElementById("flot-ind-etn").style.width = "100%";
        document.getElementById("flot-ind-etn").style.height = largo;  

        var flotBar4 = $.plot("#flot-ind-etn", [
            {
                data: dataIdIndEtn,
                color: '#41d144'
            }],
            {
                series:
                {
                    bars:
                    {
                        show: true,
                        align: "center",
                        barWidth: 1,
                        horizontal: true,
                        lineWidth: 1,
                        fillColor:
                        {
                            colors: [
                            {
                                opacity: 1
                            },
                            {
                                opacity: 0.8
                            }]
                        }
                    }
                },
                grid:
                {
                    hoverable: true,
                    clickable: true,
                    tickColor: '#f2f2f2',
                    borderWidth: 1,
                    borderColor: '#eee'
                },
                tooltip: true,
                tooltipOpts:
                {
                    cssClass: 'tooltip-inner',
                    defaultTheme: false,
                    content: "Casos: <span class='text-warning fw-500'>%x</span> - Grupo Indígenas: <span class='text-success fw-500'>%y</span>"
                },
                yaxis:
                {
                    color: "black",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelPadding: 10,
                    tickColor: '#eee',
                    axisLabel: "Grupo indígenas",
                    ticks: dataIndEtn,
                    tickLength: 0,
                    font:
                    {
                        color: '#000',
                        size: 10
                    }
                },
                xaxis:
                {
                    color: "black",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelPadding: 10,
                    tickColor: '#eee',
                    axisLabel: "Casos",
                    tickLength: 0,
                    font:
                    {
                        color: '#000',
                        size: 10
                    }
                }
            });
    }else{
        et.style.display = "none";
    }



    $(document).ready(function()
    {

        /* init datatables */
        $('#dt-basic-example').dataTable(
        {
            responsive: true,
            dom: "<'row mb-3'<'col-sm-12 col-md-6 d-flex align-items-center justify-content-start'f><'col-sm-12 col-md-6 d-flex align-items-center justify-content-end'B>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    extend: 'colvis',
                    text: 'Column Visibility',
                    titleAttr: 'Col visibility',
                    className: 'btn-outline-default'
                },
                {
                    extend: 'csvHtml5',
                    text: 'CSV',
                    titleAttr: 'Generate CSV',
                    className: 'btn-outline-default'
                },
                {
                    extend: 'copyHtml5',
                    text: 'Copy',
                    titleAttr: 'Copy to clipboard',
                    className: 'btn-outline-default'
                },
                {
                    extend: 'print',
                    text: '<i class="fal fa-print"></i>',
                    titleAttr: 'Print Table',
                    className: 'btn-outline-default'
                }

            ],
            columnDefs: [
                {
                    targets: -1,
                    title: '',
                    orderable: false,
                    render: function(data, type, full, meta)
                    {

                        /*
                        -- ES6
                        -- convert using https://babeljs.io online transpiler
                        return `
                        <a href='javascript:void(0);' class='btn btn-sm btn-icon btn-outline-danger rounded-circle mr-1' title='Delete Record'>
                            <i class="fal fa-times"></i>
                        </a>
                        <div class='dropdown d-inline-block dropleft '>
                            <a href='#'' class='btn btn-sm btn-icon btn-outline-primary rounded-circle shadow-0' data-toggle='dropdown' aria-expanded='true' title='More options'>
                                <i class="fal fa-ellipsis-v"></i>
                            </a>
                            <div class='dropdown-menu'>
                                <a class='dropdown-item' href='javascript:void(0);'>Change Status</a>
                                <a class='dropdown-item' href='javascript:void(0);'>Generate Report</a>
                            </div>
                        </div>`;
                            
                        ES5 example below:	

                        */
                        return "\n\t\t\t\t\t\t<a href='javascript:void(0);' class='btn btn-sm btn-icon btn-outline-danger rounded-circle mr-1' title='Delete Record'>\n\t\t\t\t\t\t\t<i class=\"fal fa-times\"></i>\n\t\t\t\t\t\t</a>\n\t\t\t\t\t\t<div class='dropdown d-inline-block dropleft'>\n\t\t\t\t\t\t\t<a href='#'' class='btn btn-sm btn-icon btn-outline-primary rounded-circle shadow-0' data-toggle='dropdown' aria-expanded='true' title='More options'>\n\t\t\t\t\t\t\t\t<i class=\"fal fa-ellipsis-v\"></i>\n\t\t\t\t\t\t\t</a>\n\t\t\t\t\t\t\t<div class='dropdown-menu'>\n\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Change Status</a>\n\t\t\t\t\t\t\t\t<a class='dropdown-item' href='javascript:void(0);'>Generate Report</a>\n\t\t\t\t\t\t\t</div>\n\t\t\t\t\t\t</div>";
                    },
                },

            ]

        });


        /* flot toggle example */
        var flot_toggle = function()
        {

            var data = [
            {
                label: "Target Profit",
                data: dataTargetProfit,
                color: myapp_get_color.info_400,
                bars:
                {
                    show: true,
                    align: "center",
                    barWidth: 30 * 30 * 60 * 1000 * 80,
                    lineWidth: 0,
                    /*fillColor: {
                        colors: [myapp_get_color.primary_500, myapp_get_color.primary_900]
                    },*/
                    fillColor:
                    {
                        colors: [
                        {
                            opacity: 0.9
                        },
                        {
                            opacity: 0.1
                        }]
                    }
                },
                highlightColor: 'rgba(255,255,255,0.3)',
                shadowSize: 0
            },
            {
                label: "Actual Profit",
                data: dataProfit,
                color: myapp_get_color.warning_500,
                lines:
                {
                    show: true,
                    lineWidth: 2
                },
                shadowSize: 0,
                points:
                {
                    show: true
                }
            },
            {
                label: "User Signups",
                data: dataSignups,
                color: myapp_get_color.success_500,
                lines:
                {
                    show: true,
                    lineWidth: 2
                },
                shadowSize: 0,
                points:
                {
                    show: true
                }
            }]

            var options = {
                grid:
                {
                    hoverable: true,
                    clickable: true,
                    tickColor: '#f2f2f2',
                    borderWidth: 1,
                    borderColor: '#f2f2f2'
                },
                tooltip: true,
                tooltipOpts:
                {
                    cssClass: 'tooltip-inner',
                    defaultTheme: false
                },
                xaxis:
                {
                    mode: "time"
                },
                yaxes:
                {
                    tickFormatter: function(val, axis)
                    {
                        return "$" + val;
                    },
                    max: 1200
                }

            };

            var plot2 = null;

            function plotNow()
            {
                var d = [];
                $("#js-checkbox-toggles").find(':checkbox').each(function()
                {
                    if ($(this).is(':checked'))
                    {
                        d.push(data[$(this).attr("name").substr(4, 1)]);
                    }
                });
                if (d.length > 0)
                {
                    if (plot2)
                    {
                        plot2.setData(d);
                        plot2.draw();
                    }
                    else
                    {
                        plot2 = $.plot($("#flot-toggles"), d, options);
                    }
                }

            };

            $("#js-checkbox-toggles").find(':checkbox').on('change', function()
            {
                plotNow();
            });
            plotNow()
        }

        
        flot_toggle();
        /* flot toggle example -- end*/

          
        /*** Graficos de la Enfermedad */
        /* flot area */

        var flotArea = $.plot($('#flot-area'), [
            {
                data: dataSet1,
                label: '',
                color: myapp_get_color.primary_500
            }],
            {
                series:
                {
                    lines:
                    {
                        show: true,
                        lineWidth: 2,
                        fill: true,
                        fillColor:
                        {
                            colors: [
                            {
                                opacity: 1
                            },
                            {
                                opacity: 0.8
                            }]
                        }
                    },
                    shadowSize: 0
                },
                points:
                {
                    show: true,
                },
                legend:
                {
                    noColumns: 1,
                    position: 'nw'
                },
                tooltip: true,
                tooltipOpts:
                {
                    cssClass: 'tooltip-inner',
                    defaultTheme: false,
                    content: "Semana <span class='text-warning fw-500'>%x</span> - <span class='text-success fw-500'>%y</span> Casos"
                },
                /**fondo */
                grid:
                {
                    hoverable: true,
                    clickable: true,
                    borderColor: '#ddd',
                    borderWidth: 0,
                    labelMargin: 5,
                    backgroundColor: '#fff'
                },
                /***linea Y */
                yaxis:
                {
                       /*********************************************************** maximooooo en linea vertical */
                    color: "black",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelPadding: 10,
                    min: 0,
                    axisLabel: "Casos",
                    tickLength: 0,
                    font:
                    {
                        size: 10,
                        color: '#000'
                    }
                },
                /***linea X */
                xaxis:
                { 
                        /*********************************************************** maximooooo en linea horizontal */
                    color: "black",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelPadding: 10,
                    axisLabel: "Semanas",
                    tickLength: 0,
                    font:
                    {
                        size: 10,
                        color: '#000'
                    }
                }
            });

        /* flot area -- end ******************************************************************************************************************/

           /* flot bar *********************************************************************************************************************/
           var flotBar5 = $.plot("#flot-cond", [
            {
                data: dataCond,
                color: myapp_get_color.info_300
            }],
            {
                series:
                {
                    bars:
                    {
                        show: true,
                        align: "center",
                        lineWidth: 0,
                        fillColor:
                        {
                            colors: [
                            {
                                opacity: 1
                            },
                            {
                                opacity: 0.8
                            }]
                        }
                    }
                },
                grid:
                {
                    hoverable: true,
                    clickable: true,
                    tickColor: '#f2f2f2',
                    borderWidth: 1,
                    borderColor: '#f2f2f2'
                },
                tooltip: true,
                tooltipOpts:
                {
                    cssClass: 'tooltip-inner',
                    defaultTheme: false,
                    content: "Tipo de Condición <span class='text-warning fw-500'>%x</span> - <span class='text-success fw-500'>%y</span> Casos"
                },
                yaxis:
                {
                    color: "black",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelPadding: 10,
                    tickColor: '#eee',
                    axisLabel: "Casos",
                    tickLength: 0,
                    font:
                    {
                        color: '#000',
                        size: 10
                    }
                },
                xaxis:
                {
                    color: "black",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelPadding: 10,
                    tickColor: '#eee',
                    axisLabel: "Tipo de Condición",
                    tickLength: 0,
                    ticks: [
                        [0, 'Sensible'],
                        [1, 'Resistente']
                    ],
                    font:
                    {
                        color: '#000',
                        size: 12
                    }
                }
            });
            if(property[1] == 'Norte de Santander'){
            var flotBar6 = $.plot("#flot-etn", [
                {
                    data: dataEtn,
                    color: myapp_get_color.info_700
                }],
                {
                    series:
                    {
                        bars:
                        {
                            show: true,
                            align: "center",
                            lineWidth: 0,
                            fillColor:
                            {
                                colors: [
                                {
                                    opacity: 1
                                },
                                {
                                    opacity: 0.8
                                }]
                            }
                        }
                    },
                    grid:
                    {
                        hoverable: true,
                        clickable: true,
                        tickColor: '#f2f2f2',
                        borderWidth: 1,
                        borderColor: '#f2f2f2'
                    },
                    tooltip: true,
                    tooltipOpts:
                    {
                        cssClass: 'tooltip-inner',
                        defaultTheme: false,
                        content: "Etnia <span class='text-warning fw-500'>%x</span> - <span class='text-success fw-500'>%y</span> Casos"
                    },
                    yaxis:
                    {
                        color: "black",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelPadding: 10,
                        tickColor: '#eee',
                        axisLabel: "Casos",
                        tickLength: 0,
                        font:
                        {
                            color: '#000',
                            size: 10
                        }
                    },
                    xaxis:
                    {
                        color: "black",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelPadding: 10,
                        tickColor: '#eee',
                        axisLabel: "Etnia",
                        tickLength: 0,
                        ticks: [
                            [0, 'Indígena'],
                            [2, 'Rom. Gitano'],
                            [4, 'Raizal'],
                            [6, 'Palenquero'],
                            [8, 'Negro/Mulato/Afrocolombiano'],
                            [10, 'Otro']
                        ],
                        font:
                        {
                            color: '#000',
                            size: 12
                        }
                    }
                });
            }

                var flotBar6 = $.plot("#flot-etn-cuc", [
                    {
                        data: dataEtnCuc,
                        color: myapp_get_color.info_700
                    }],
                    {
                        series:
                        {
                            bars:
                            {
                                show: true,
                                align: "center",
                                lineWidth: 0,
                                fillColor:
                                {
                                    colors: [
                                    {
                                        opacity: 1
                                    },
                                    {
                                        opacity: 0.8
                                    }]
                                }
                            }
                        },
                        grid:
                        {
                            hoverable: true,
                            clickable: true,
                            tickColor: '#f2f2f2',
                            borderWidth: 1,
                            borderColor: '#f2f2f2'
                        },
                        tooltip: true,
                        tooltipOpts:
                        {
                            cssClass: 'tooltip-inner',
                            defaultTheme: false,
                            content: "Etnia <span class='text-warning fw-500'>%x</span> - <span class='text-success fw-500'>%y</span> Casos"
                        },
                        yaxis:
                        {
                            color: "black",
                            axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 12,
                            axisLabelPadding: 10,
                            tickColor: '#eee',
                            axisLabel: "Casos",
                            tickLength: 0,
                            font:
                            {
                                color: '#000',
                                size: 10
                            }
                        },
                        xaxis:
                        {
                            color: "black",
                            axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 12,
                            axisLabelPadding: 10,
                            tickColor: '#eee',
                            axisLabel: "Etnia",
                            tickLength: 0,
                            ticks: [
                                [0, 'Indígena'],
                                [2, 'Rom. Gitano'],
                                [4, 'Raizal'],
                                [6, 'Palenquero'],
                                [8, 'Negro/Mulato/Afrocolombiano'],
                                [10, 'Otro']
                            ],
                            font:
                            {
                                color: '#000',
                                size: 12
                            }
                        }
                    });
                if(property[1] == 'Norte de Santander'){
                    var flotBar6 = $.plot("#flot-etn-teo", [
                        {
                            data: dataEtnTeo,
                            color: myapp_get_color.info_700
                        }],
                        {
                            series:
                            {
                                bars:
                                {
                                    show: true,
                                    align: "center",
                                    lineWidth: 0,
                                    fillColor:
                                    {
                                        colors: [
                                        {
                                            opacity: 1
                                        },
                                        {
                                            opacity: 0.8
                                        }]
                                    }
                                }
                            },
                            grid:
                            {
                                hoverable: true,
                                clickable: true,
                                tickColor: '#f2f2f2',
                                borderWidth: 1,
                                borderColor: '#f2f2f2'
                            },
                            tooltip: true,
                            tooltipOpts:
                            {
                                cssClass: 'tooltip-inner',
                                defaultTheme: false,
                                content: "Etnia <span class='text-warning fw-500'>%x</span> - <span class='text-success fw-500'>%y</span> Casos"
                            },
                            yaxis:
                            {
                                color: "black",
                                axisLabelUseCanvas: true,
                                axisLabelFontSizePixels: 12,
                                axisLabelPadding: 10,
                                tickColor: '#eee',
                                axisLabel: "Casos",
                                tickLength: 0,
                                font:
                                {
                                    color: '#000',
                                    size: 10
                                }
                            },
                            xaxis:
                            {
                                color: "black",
                                axisLabelUseCanvas: true,
                                axisLabelFontSizePixels: 12,
                                axisLabelPadding: 10,
                                tickColor: '#eee',
                                axisLabel: "Etnia",
                                tickLength: 0,
                                ticks: [
                                    [0, 'Indígena'],
                                    [2, 'Rom. Gitano'],
                                    [4, 'Raizal'],
                                    [6, 'Palenquero'],
                                    [8, 'Negro/Mulato/Afrocolombiano'],
                                    [10, 'Otro']
                                ],
                                font:
                                {
                                    color: '#000',
                                    size: 12
                                }
                            }
                        });
                    }
                    if(property[1] == 'Norte de Santander'){
                        var flotBar6 = $.plot("#flot-etn-con", [
                            {
                                data: dataEtnCon,
                                color: myapp_get_color.info_700
                            }],
                            {
                                series:
                                {
                                    bars:
                                    {
                                        show: true,
                                        align: "center",
                                        lineWidth: 0,
                                        fillColor:
                                        {
                                            colors: [
                                            {
                                                opacity: 1
                                            },
                                            {
                                                opacity: 0.8
                                            }]
                                        }
                                    }
                                },
                                grid:
                                {
                                    hoverable: true,
                                    clickable: true,
                                    tickColor: '#f2f2f2',
                                    borderWidth: 1,
                                    borderColor: '#f2f2f2'
                                },
                                tooltip: true,
                                tooltipOpts:
                                {
                                    cssClass: 'tooltip-inner',
                                    defaultTheme: false,
                                    content: "Etnia <span class='text-warning fw-500'>%x</span> - <span class='text-success fw-500'>%y</span> Casos"
                                },
                                yaxis:
                                {
                                    color: "black",
                                    axisLabelUseCanvas: true,
                                    axisLabelFontSizePixels: 12,
                                    axisLabelPadding: 10,
                                    tickColor: '#eee',
                                    axisLabel: "Casos",
                                    tickLength: 0,
                                    font:
                                    {
                                        color: '#000',
                                        size: 10
                                    }
                                },
                                xaxis:
                                {
                                    color: "black",
                                    axisLabelUseCanvas: true,
                                    axisLabelFontSizePixels: 12,
                                    axisLabelPadding: 10,
                                    tickColor: '#eee',
                                    axisLabel: "Etnia",
                                    tickLength: 0,
                                    ticks: [
                                        [0, 'Indígena'],
                                        [2, 'Rom. Gitano'],
                                        [4, 'Raizal'],
                                        [6, 'Palenquero'],
                                        [8, 'Negro/Mulato/Afrocolombiano'],
                                        [10, 'Otro']
                                    ],
                                    font:
                                    {
                                        color: '#000',
                                        size: 12
                                    }
                                }
                            });
                        }
                        if(property[1] == 'Norte de Santander'){
                            var flotBar6 = $.plot("#flot-etn-car", [
                                {
                                    data: dataEtnCar,
                                    color: myapp_get_color.info_700
                                }],
                                {
                                    series:
                                    {
                                        bars:
                                        {
                                            show: true,
                                            align: "center",
                                            lineWidth: 0,
                                            fillColor:
                                            {
                                                colors: [
                                                {
                                                    opacity: 1
                                                },
                                                {
                                                    opacity: 0.8
                                                }]
                                            }
                                        }
                                    },
                                    grid:
                                    {
                                        hoverable: true,
                                        clickable: true,
                                        tickColor: '#f2f2f2',
                                        borderWidth: 1,
                                        borderColor: '#f2f2f2'
                                    },
                                    tooltip: true,
                                    tooltipOpts:
                                    {
                                        cssClass: 'tooltip-inner',
                                        defaultTheme: false,
                                        content: "Etnia <span class='text-warning fw-500'>%x</span> - <span class='text-success fw-500'>%y</span> Casos"
                                    },
                                    yaxis:
                                    {
                                        color: "black",
                                        axisLabelUseCanvas: true,
                                        axisLabelFontSizePixels: 12,
                                        axisLabelPadding: 10,
                                        tickColor: '#eee',
                                        axisLabel: "Casos",
                                        tickLength: 0,
                                        font:
                                        {
                                            color: '#000',
                                            size: 10
                                        }
                                    },
                                    xaxis:
                                    {
                                        color: "black",
                                        axisLabelUseCanvas: true,
                                        axisLabelFontSizePixels: 12,
                                        axisLabelPadding: 10,
                                        tickColor: '#eee',
                                        axisLabel: "Etnia",
                                        tickLength: 0,
                                        ticks: [
                                            [0, 'Indígena'],
                                            [2, 'Rom. Gitano'],
                                            [4, 'Raizal'],
                                            [6, 'Palenquero'],
                                            [8, 'Negro/Mulato/Afrocolombiano'],
                                            [10, 'Otro']
                                        ],
                                        font:
                                        {
                                            color: '#000',
                                            size: 12
                                        }
                                    }
                                });
                            }
                            if(property[1] == 'Norte de Santander'){
                                var flotBar6 = $.plot("#flot-etn-tib", [
                                    {
                                        data: dataEtnTib,
                                        color: myapp_get_color.info_700
                                    }],
                                    {
                                        series:
                                        {
                                            bars:
                                            {
                                                show: true,
                                                align: "center",
                                                lineWidth: 0,
                                                fillColor:
                                                {
                                                    colors: [
                                                    {
                                                        opacity: 1
                                                    },
                                                    {
                                                        opacity: 0.8
                                                    }]
                                                }
                                            }
                                        },
                                        grid:
                                        {
                                            hoverable: true,
                                            clickable: true,
                                            tickColor: '#f2f2f2',
                                            borderWidth: 1,
                                            borderColor: '#f2f2f2'
                                        },
                                        tooltip: true,
                                        tooltipOpts:
                                        {
                                            cssClass: 'tooltip-inner',
                                            defaultTheme: false,
                                            content: "Etnia <span class='text-warning fw-500'>%x</span> - <span class='text-success fw-500'>%y</span> Casos"
                                        },
                                        yaxis:
                                        {
                                            color: "black",
                                            axisLabelUseCanvas: true,
                                            axisLabelFontSizePixels: 12,
                                            axisLabelPadding: 10,
                                            tickColor: '#eee',
                                            axisLabel: "Casos",
                                            tickLength: 0,
                                            font:
                                            {
                                                color: '#000',
                                                size: 10
                                            }
                                        },
                                        xaxis:
                                        {
                                            color: "black",
                                            axisLabelUseCanvas: true,
                                            axisLabelFontSizePixels: 12,
                                            axisLabelPadding: 10,
                                            tickColor: '#eee',
                                            axisLabel: "Etnia",
                                            tickLength: 0,
                                            ticks: [
                                                [0, 'Indígena'],
                                                [2, 'Rom. Gitano'],
                                                [4, 'Raizal'],
                                                [6, 'Palenquero'],
                                                [8, 'Negro/Mulato/Afrocolombiano'],
                                                [10, 'Otro']
                                            ],
                                            font:
                                            {
                                                color: '#000',
                                                size: 12
                                            }
                                        }
                                    });
                                }
                                if(property[1] == 'Norte de Santander'){
                                    var flotBar6 = $.plot("#flot-etn-tol", [
                                        {
                                            data: dataEtnTol,
                                            color: myapp_get_color.info_700
                                        }],
                                        {
                                            series:
                                            {
                                                bars:
                                                {
                                                    show: true,
                                                    align: "center",
                                                    lineWidth: 0,
                                                    fillColor:
                                                    {
                                                        colors: [
                                                        {
                                                            opacity: 1
                                                        },
                                                        {
                                                            opacity: 0.8
                                                        }]
                                                    }
                                                }
                                            },
                                            grid:
                                            {
                                                hoverable: true,
                                                clickable: true,
                                                tickColor: '#f2f2f2',
                                                borderWidth: 1,
                                                borderColor: '#f2f2f2'
                                            },
                                            tooltip: true,
                                            tooltipOpts:
                                            {
                                                cssClass: 'tooltip-inner',
                                                defaultTheme: false,
                                                content: "Etnia <span class='text-warning fw-500'>%x</span> - <span class='text-success fw-500'>%y</span> Casos"
                                            },
                                            yaxis:
                                            {
                                                color: "black",
                                                axisLabelUseCanvas: true,
                                                axisLabelFontSizePixels: 12,
                                                axisLabelPadding: 10,
                                                tickColor: '#eee',
                                                axisLabel: "Casos",
                                                tickLength: 0,
                                                font:
                                                {
                                                    color: '#000',
                                                    size: 10
                                                }
                                            },
                                            xaxis:
                                            {
                                                color: "black",
                                                axisLabelUseCanvas: true,
                                                axisLabelFontSizePixels: 12,
                                                axisLabelPadding: 10,
                                                tickColor: '#eee',
                                                axisLabel: "Etnia",
                                                tickLength: 0,
                                                ticks: [
                                                    [0, 'Indígena'],
                                                    [2, 'Rom. Gitano'],
                                                    [4, 'Raizal'],
                                                    [6, 'Palenquero'],
                                                    [8, 'Negro/Mulato/Afrocolombiano'],
                                                    [10, 'Otro']
                                                ],
                                                font:
                                                {
                                                    color: '#000',
                                                    size: 12
                                                }
                                            }
                                        });
                                    }

                var flotBar7 = $.plot("#flot-dia", [
                    {
                        data: dataDia,
                        color: '#CD6155'
                    }],
                    {
                        series:
                        {
                            bars:
                            {
                                show: true,
                                align: "center",
                                lineWidth: 0,
                                fillColor:
                                {
                                    colors: [
                                    {
                                        opacity: 1
                                    },
                                    {
                                        opacity: 0.8
                                    }]
                                }
                            }
                        },
                        grid:
                        {
                            hoverable: true,
                            clickable: true,
                            tickColor: '#f2f2f2',
                            borderWidth: 1,
                            borderColor: '#f2f2f2'
                        },
                        tooltip: true,
                        tooltipOpts:
                        {
                            cssClass: 'tooltip-inner',
                            defaultTheme: false,
                            content: "Diabetes <span class='text-warning fw-500'>%x</span> - <span class='text-success fw-500'>%y</span> Casos"
                        },
                        yaxis:
                        {
                            color: "black",
                            axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 12,
                            axisLabelPadding: 10,
                            tickColor: '#eee',
                            axisLabel: "Casos",
                            tickLength: 0,
                            font:
                            {
                                color: '#000',
                                size: 10
                            }
                        },
                        xaxis:
                        {
                            color: "black",
                            axisLabelUseCanvas: true,
                            axisLabelFontSizePixels: 12,
                            axisLabelPadding: 10,
                            tickColor: '#eee',
                            axisLabel: "Diabetes",
                            tickLength: 0,
                            ticks: [
                                [0, 'Si'],
                                [1, 'No']
                            ],
                            font:
                            {
                                color: '#000',
                                size: 12
                            }
                        }
                    });

                    var flotBar8 = $.plot("#flot-ren", [
                        {
                            data: dataRen,
                            color: '#AF7AC5'
                        }],
                        {
                            series:
                            {
                                bars:
                                {
                                    show: true,
                                    align: "center",
                                    lineWidth: 0,
                                    fillColor:
                                    {
                                        colors: [
                                        {
                                            opacity: 1
                                        },
                                        {
                                            opacity: 0.8
                                        }]
                                    }
                                }
                            },
                            grid:
                            {
                                hoverable: true,
                                clickable: true,
                                tickColor: '#f2f2f2',
                                borderWidth: 1,
                                borderColor: '#f2f2f2'
                            },
                            tooltip: true,
                            tooltipOpts:
                            {
                                cssClass: 'tooltip-inner',
                                defaultTheme: false,
                                content: "Enfermedad Renal <span class='text-warning fw-500'>%x</span> - <span class='text-success fw-500'>%y</span> Casos"
                            },
                            yaxis:
                            {
                                color: "black",
                                axisLabelUseCanvas: true,
                                axisLabelFontSizePixels: 12,
                                axisLabelPadding: 10,
                                tickColor: '#eee',
                                axisLabel: "Casos",
                                tickLength: 0,
                                font:
                                {
                                    color: '#000',
                                    size: 10
                                }
                            },
                            xaxis:
                            {
                                color: "black",
                                axisLabelUseCanvas: true,
                                axisLabelFontSizePixels: 12,
                                axisLabelPadding: 10,
                                tickColor: '#eee',
                                axisLabel: "Enfermedad Renal",
                                tickLength: 0,
                                ticks: [
                                    [0, 'Si'],
                                    [1, 'No']
                                ],
                                font:
                                {
                                    color: '#000',
                                    size: 12
                                }
                            }
                        });

                        var flotBar9 = $.plot("#flot-des", [
                            {
                                data: dataDes,
                                color: '#5499C7'
                            }],
                            {
                                series:
                                {
                                    bars:
                                    {
                                        show: true,
                                        align: "center",
                                        lineWidth: 0,
                                        fillColor:
                                        {
                                            colors: [
                                            {
                                                opacity: 1
                                            },
                                            {
                                                opacity: 0.8
                                            }]
                                        }
                                    }
                                },
                                grid:
                                {
                                    hoverable: true,
                                    clickable: true,
                                    tickColor: '#f2f2f2',
                                    borderWidth: 1,
                                    borderColor: '#f2f2f2'
                                },
                                tooltip: true,
                                tooltipOpts:
                                {
                                    cssClass: 'tooltip-inner',
                                    defaultTheme: false,
                                    content: "Desnutrición <span class='text-warning fw-500'>%x</span> - <span class='text-success fw-500'>%y</span> Casos"
                                },
                                yaxis:
                                {
                                    color: "black",
                                    axisLabelUseCanvas: true,
                                    axisLabelFontSizePixels: 12,
                                    axisLabelPadding: 10,
                                    tickColor: '#eee',
                                    axisLabel: "Casos",
                                    tickLength: 0,
                                    font:
                                    {
                                        color: '#000',
                                        size: 10
                                    }
                                },
                                xaxis:
                                {
                                    color: "black",
                                    axisLabelUseCanvas: true,
                                    axisLabelFontSizePixels: 12,
                                    axisLabelPadding: 10,
                                    tickColor: '#eee',
                                    axisLabel: "Desnutrición",
                                    tickLength: 0,
                                    ticks: [
                                        [0, 'Si'],
                                        [1, 'No']
                                    ],
                                    font:
                                    {
                                        color: '#000',
                                        size: 12
                                    }
                                }
                            });

                            var flotBar10 = $.plot("#flot-trab", [
                                {
                                    data: dataTrab,
                                    color: '#48C9B0'
                                }],
                                {
                                    series:
                                    {
                                        bars:
                                        {
                                            show: true,
                                            align: "center",
                                            lineWidth: 0,
                                            fillColor:
                                            {
                                                colors: [
                                                {
                                                    opacity: 1
                                                },
                                                {
                                                    opacity: 0.8
                                                }]
                                            }
                                        }
                                    },
                                    grid:
                                    {
                                        hoverable: true,
                                        clickable: true,
                                        tickColor: '#f2f2f2',
                                        borderWidth: 1,
                                        borderColor: '#f2f2f2'
                                    },
                                    tooltip: true,
                                    tooltipOpts:
                                    {
                                        cssClass: 'tooltip-inner',
                                        defaultTheme: false,
                                        content: "Trabajador de la Salud <span class='text-warning fw-500'>%x</span> - <span class='text-success fw-500'>%y</span> Casos"
                                    },
                                    yaxis:
                                    {
                                        color: "black",
                                        axisLabelUseCanvas: true,
                                        axisLabelFontSizePixels: 12,
                                        axisLabelPadding: 10,
                                        tickColor: '#eee',
                                        axisLabel: "Casos",
                                        tickLength: 0,
                                        font:
                                        {
                                            color: '#000',
                                            size: 10
                                        }
                                    },
                                    xaxis:
                                    {
                                        color: "black",
                                        axisLabelUseCanvas: true,
                                        axisLabelFontSizePixels: 12,
                                        axisLabelPadding: 10,
                                        tickColor: '#eee',
                                        axisLabel: "Trabajador de la Salud",
                                        tickLength: 0,
                                        ticks: [
                                            [0, 'Si'],
                                            [1, 'No']
                                        ],
                                        font:
                                        {
                                            color: '#000',
                                            size: 12
                                        }
                                    }
                                });
            /* flot bar lines -- end */


           /* flot bar *********************************************************************************************************************/
           var flotBar5 = $.plot("#flot-coin", [
            {
                data: dataCoin,
                color: '#505050'
            }],
            {
                series:
                {
                    bars:
                    {
                        show: true,
                        align: "center",
                        lineWidth: 0,
                        fillColor:
                        {
                            colors: [
                            {
                                opacity: 1
                            },
                            {
                                opacity: 0.8
                            }]
                        }
                    }
                },
                grid:
                {
                    hoverable: true,
                    clickable: true,
                    tickColor: '#f2f2f2',
                    borderWidth: 1,
                    borderColor: '#f2f2f2'
                },
                tooltip: true,
                tooltipOpts:
                {
                    cssClass: 'tooltip-inner',
                    defaultTheme: false,
                    content: "Coinfección con VIH <span class='text-warning fw-500'>%x</span> - <span class='text-success fw-500'>%y</span> Casos"
                },
                yaxis:
                {
                    color: "black",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelPadding: 10,
                    tickColor: '#eee',
                    axisLabel: "Casos",
                    tickLength: 0,
                    font:
                    {
                        color: '#000',
                        size: 10
                    }
                },
                xaxis:
                {
                    color: "black",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelPadding: 10,
                    tickColor: '#eee',
                    axisLabel: "Coinfección con VIH",
                    tickLength: 0,
                    ticks: [
                        [0, 'Si'],
                        [1, 'No']
                    ],
                    font:
                    {
                        color: '#000',
                        size: 12
                    }
                }
            });
            /* flot bar lines -- end */



        /* flot bar *********************************************************************************************************************/
        var flotBar = $.plot("#flot-fuente", [
            {
                data: dataFuente,
                color: myapp_get_color.warning_400
            }],
            {
                series:
                {
                    bars:
                    {
                        show: true,
                        align: "center",
                        lineWidth: 0,
                        fillColor:
                        {
                            colors: [
                            {
                                opacity: 1
                            },
                            {
                                opacity: 0.8
                            }]
                        }
                    }
                },
                grid:
                {
                    hoverable: true,
                    clickable: true,
                    tickColor: '#f2f2f2',
                    borderWidth: 1,
                    borderColor: '#f2f2f2'
                },
                tooltip: true,
                tooltipOpts:
                {
                    cssClass: 'tooltip-inner',
                    defaultTheme: false,
                    content: "Tipo de afiliación <span class='text-warning fw-500'>%x</span> - <span class='text-success fw-500'>%y</span> Casos"
                },
                yaxis:
                {
                    color: "black",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelPadding: 10,
                    tickColor: '#eee',
                    axisLabel: "Casos",
                    tickLength: 0,
                    font:
                    {
                        color: '#000',
                        size: 10
                    }
                },
                xaxis:
                {
                    color: "black",
                    axisLabelUseCanvas: true,
                    axisLabelFontSizePixels: 12,
                    axisLabelPadding: 10,
                    tickColor: '#eee',
                    axisLabel: "Tipo de afiliación",
                    tickLength: 0,
                    ticks: [
                        [0, 'Excepción'],
                        [2, 'Especial'],
                        [4, 'Contributivo'],
                        [6, 'Subsidiado'],
                        [8, 'Indeterminado/pendiente'],
                        [10, 'No asegurado']
                    ],
                    font:
                    {
                        color: '#000',
                        size: 10
                    }
                }
            });
            /* flot bar lines -- end */

            var flotBar3 = $.plot("#flot-nacionalidad", [
                {
                    data: dataIdNacTub,
                    color: myapp_get_color.success_400
                }],
                {
                    series:
                    {
                        bars:
                        {
                            show: true,
                            align: "center",
                            barWidth: 1,
                            horizontal: true,
                            lineWidth: 1,
                            fillColor:
                            {
                                colors: [
                                {
                                    opacity: 1
                                },
                                {
                                    opacity: 0.8
                                }]
                            }
                        }
                    },
                    grid:
                    {
                        hoverable: true,
                        clickable: true,
                        tickColor: '#f2f2f2',
                        borderWidth: 1,
                        borderColor: '#eee'
                    },
                    tooltip: true,
                    tooltipOpts:
                    {
                        cssClass: 'tooltip-inner',
                        defaultTheme: false,
                        content: "Casos: <span class='text-warning fw-500'>%x</span> - Nacionalidad (País): <span class='text-success fw-500'>%y</span>"
                    },
                    yaxis:
                    {
                        color: "black",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelPadding: 10,
                        tickColor: '#eee',
                        axisLabel: "Nacionalidad (País)",
                        ticks: dataNacTub,
                        tickLength: 0,
                        font:
                        {
                            color: '#000',
                            size: 10
                        }
                    },
                    xaxis:
                    {
                        color: "black",
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelPadding: 10,
                        tickColor: '#eee',
                        axisLabel: "Casos",
                        tickLength: 0,
                        font:
                        {
                            color: '#000',
                            size: 10
                        }
                    }
                });  

              /* flot bar *********************************************************************************************************************/
     


       /* flot bar *********************************************************************************************************************/
       var flotBar2 = $.plot("#flot-edades", [
        {
            data: dataEdades,
            color: myapp_get_color.success_900
        }],
        {
            series:
            {
                bars:
                {
                    show: true,
                    align: "center",
                    lineWidth: 0,
                    fillColor:
                    {
                        colors: [
                        {
                            opacity: 1
                        },
                        {
                            opacity: 0.8
                        }]
                    }
                }
            },
            grid:
            {
                hoverable: true,
                clickable: true,
                tickColor: '#f2f2f2',
                borderWidth: 1,
                borderColor: '#f2f2f2'
            },
            tooltip: true,
            tooltipOpts:
            {
                cssClass: 'tooltip-inner',
                defaultTheme: false,
                content: "Edades <span class='text-warning fw-500'>%x</span> - <span class='text-success fw-500'>%y</span> Casos"
            },
            yaxis:
            {
                color: "black",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelPadding: 10,
                tickColor: '#eee',
                axisLabel: "Casos",
                tickLength: 0,
                font:
                {
                    color: '#000',
                    size: 10
                }
            },
            xaxis:
            {
                color: "black",
                axisLabelUseCanvas: true,
                axisLabelFontSizePixels: 12,
                axisLabelPadding: 10,
                tickColor: '#eee',
                axisLabel: "Rango de Edades",
                tickLength: 0,
                ticks: [
                    [0, '0-14 Años'],
                    [2, '15-24 Años'],
                    [4, '25-34 Años'],
                    [6, '35-44 Años'],
                    [8, '45-54 Años'],
                    [10, '55-64 Años'],
                    [12, 'Mayor de 65 Años']
                ],
                font:
                {
                    color: '#000',
                    size: 12
                }
            }
        });
        /* flot bar lines -- end */

                 /* flot bar *********************************************************************************************************************/
    
            /* flot bar lines -- end */


                       /* flot bar *********************************************************************************************************************/
                    
                        /* flot bar lines -- end */
            
            
          

              /* flot bar *********************************************************************************************************************/
              var flotBar6 = $.plot("#flot-bar-ocu-cuc", [
                {
                    data: dataIdBarOcu,
                    color: myapp_get_color.success_400
                }],
                {
                    series:
                    {
                        bars:
                        {
                            show: true,
                            align: "center",
                            barWidth: 1,
                            horizontal: true,
                            lineWidth: 1,
                            fillColor:
                            {
                                colors: [
                                {
                                    opacity: 0.9
                                },
                                {
                                    opacity: 0.1
                                }]
                            }
                        }
                    },
                    grid:
                    {
                        hoverable: true,
                        clickable: true,
                        tickColor: '#f2f2f2',
                        borderWidth: 1,
                        borderColor: '#eee'
                    },
                    tooltip: true,
                    tooltipOpts:
                    {
                        cssClass: 'tooltip-inner',
                        defaultTheme: false,
                        content: "Casos: <span class='text-warning fw-500'>%x</span> - Barrio: <span class='text-success fw-500'>%y</span>"
                    },
                    yaxis:
                    {
                        tickColor: '#eee',
                        axisLabel: "Barrios",
                        ticks: dataBarOcu,
                        font:
                        {
                            color: '#000',
                            size: 10
                        }
                    },
                    xaxis:
                    {
                        tickColor: '#eee',
                        axisLabel: "Casos",
                        font:
                        {
                            color: '#000',
                            size: 10
                        }
                    }
                });
                /* flot bar lines -- end */
    

        var flotVisit = $.plot('#flotVisit', [
        {
            data: [
                [3, 0],
                [4, 1],
                [5, 3],
                [6, 3],
                [7, 10],
                [8, 11],
                [9, 12],
                [10, 9],
                [11, 12],
                [12, 8],
                [13, 5]
            ],
            color: myapp_get_color.success_200
        },
        {
            data: [
                [1, 0],
                [2, 0],
                [3, 1],
                [4, 2],
                [5, 2],
                [6, 5],
                [7, 8],
                [8, 12],
                [9, 9],
                [10, 11],
                [11, 5]
            ],
            color: myapp_get_color.info_200
        }],
        {
            series:
            {
                shadowSize: 0,
                lines:
                {
                    show: true,
                    lineWidth: 2,
                    fill: true,
                    fillColor:
                    {
                        colors: [
                        {
                            opacity: 0
                        },
                        {
                            opacity: 0.12
                        }]
                    }
                }
            },
            grid:
            {
                borderWidth: 0
            },
            yaxis:
            {
                min: 0,
                max: 15,
                tickColor: '#ddd',
                ticks: [
                    [0, ''],
                    [5, '100K'],
                    [10, '200K'],
                    [15, '300K']
                ],
                font:
                {
                    color: '#444',
                    size: 10
                }
            },
            xaxis:
            {

                tickColor: '#eee',
                ticks: [
                    [2, '2am'],
                    [3, '3am'],
                    [4, '4am'],
                    [5, '5am'],
                    [6, '6am'],
                    [7, '7am'],
                    [8, '8am'],
                    [9, '9am'],
                    [10, '1pm'],
                    [11, '2pm'],
                    [12, '3pm'],
                    [13, '4pm']
                ],
                font:
                {
                    color: '#999',
                    size: 9
                }
            }
        });


    });

    function validarArrayVacio(listado){
        resp = false;
        listado.forEach(function(row) {    
            if (parseInt(row[1]) != 0) {
                resp = true;
            } 
        });
        return resp;
    }


