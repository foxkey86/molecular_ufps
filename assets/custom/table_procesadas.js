$(document).ready(function()
{

    var datos;
    var ruta = window.location.protocol + "//" + window.location.host;
    // initialize datatable
    $('#dt-basic-example').dataTable(
    {
        responsive:
        {
            details:
            {
                display: $.fn.dataTable.Responsive.display.modal(
                {
                    header: function(row)
                    {
                        var data = row.data();
                        datos = data[0];
                        return 'Detalles para el registro ' + data[0];
                    }
                }),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll(
                {
                    tableClass: 'table table-responsive'
                })
            }
        },
        columnDefs: [
        {
            targets: -1,
            title: 'Admin Control',
            orderable: false,
            render: function(data, type, full, meta)
            {

                /*
                -- ES6
                -- convert using https://babeljs.io online transpiler
                return `
                <div class='d-flex mt-2'>
                    <a href='javascript:void(0);' class='btn btn-sm btn-outline-danger mr-2' title='Delete Record'>
                        <i class="fal fa-times"></i> Delete Record
                    </a>
                    <a href='javascript:void(0);' class='btn btn-sm btn-outline-primary mr-2' title='Edit'>
                        <i class="fal fa-edit"></i> Edit
                    </a>
                    <div class='dropdown d-inline-block'>
                        <a href='#'' class='btn btn-sm btn-outline-primary mr-2' data-toggle='dropdown' aria-expanded='true' title='More options'>
                            <i class="fal fa-plus"></i>
                        </a>
                        <div class='dropdown-menu'>
                            <a class='dropdown-item' href='javascript:void(0);'>Change Status</a>
                            <a class='dropdown-item' href='javascript:void(0);'>Generate Report</a>
                        </div>
                    </div>
                </div>`;
                    
                ES5 example below:	

                */
                return "\n\t\t\t\t\t\t<div class='d-flex mt-2'>\n\t\t\t\t\t\t\t<a href='"+ruta+"/documentacion/editar_documentacion_pro/"+full[0]+"' class='btn btn-sm bg-success-600 mr-2' title='Editar'><i class=\"fal fa-edit\"></i> Editar</a>\n\t\t\t\t\t\t\t\t\t</div>";
            },
        },
        {
            targets: 6,
            /*	The `data` parameter refers to the data for the cell (defined by the
                `data` option, which defaults to the column being worked with, in this case `data: 16`.*/
            render: function(data, type, full, meta)
            { 
                var badge = {
                    1:
                    {
                        'title': 'Administrador',
                        'class': 'bg-primary-600'
                    },
                    2:
                    {
                        'title': 'Coordinador Sistema',
                        'class': 'bg-danger-900'
                    },
                    3:
                    {
                        'title': 'Usuario Particular',
                        'class': 'bg-info-900'
                    },
                    4:
                    {
                        'title': 'Coordinador IDS',
                        'class': 'bg-info-500'
                    },
                    5:
                    {
                        'title': 'Personal IDS',
                        'class': 'bg-info-500'
                    },
                    6:
                    {
                        'title': 'Analista de Laboratorio',
                        'class': 'bg-info-500'
                    },
                    7:
                    {
                        'title': 'Jefe de Laboratorio',
                        'class': 'bg-info-500'
                    },
                    8:
                    {
                        'title': 'Coordinador de Laboratorio',
                        'class': 'bg-info-500'
                    },
                    9:
                    {
                        'title': 'Gestor de Calidad',
                        'class': 'bg-info-500'
                    },
                    10:
                    {
                        'title': 'Responsable de Dimensión',
                        'class': 'bg-fusion-800'
                    },
                };
                if (typeof badge[data] === 'undefined')
                {
                    return data;
                }
                return '<span class="badge ' + badge[data].class + ' badge-pill">' + badge[data].title + '</span>';
            },
        }],
    });
});