$(document).ready(function()
{

    var datos;
    var ruta = window.location.protocol + "//" + window.location.host;
    // initialize datatable
    $('#dt-basic-example').dataTable(
    {
        responsive:
        {
            details:
            {
                display: $.fn.dataTable.Responsive.display.modal(
                {
                    header: function(row)
                    {
                        var data = row.data();
                        datos = data[0];
                        return 'Detalles para el registro ' + data[0];
                    }
                }),
                renderer: $.fn.dataTable.Responsive.renderer.tableAll(
                {
                    tableClass: 'table table-responsive'
                })
            }
        },
        columnDefs: [
        {
            targets: 6,
            /*	The `data` parameter refers to the data for the cell (defined by the
                `data` option, which defaults to the column being worked with, in this case `data: 16`.*/
            render: function(data, type, full, meta)
            { 
                var badge = {
                    1:
                    {
                        'title': 'Administrador',
                        'class': 'bg-primary-600'
                    },
                    2:
                    {
                        'title': 'Coordinador Sistema',
                        'class': 'bg-danger-900'
                    },
                    3:
                    {
                        'title': 'Usuario Particular',
                        'class': 'bg-info-900'
                    },
                    4:
                    {
                        'title': 'Coordinador IDS',
                        'class': 'bg-info-500'
                    },
                    5:
                    {
                        'title': 'Personal IDS',
                        'class': 'bg-info-500'
                    },
                    6:
                    {
                        'title': 'Analista de Laboratorio',
                        'class': 'bg-info-500'
                    },
                    7:
                    {
                        'title': 'Jefe de Laboratorio',
                        'class': 'bg-info-500'
                    },
                    8:
                    {
                        'title': 'Coordinador de Laboratorio',
                        'class': 'bg-info-500'
                    },
                    9:
                    {
                        'title': 'Gestor de Calidad',
                        'class': 'bg-info-500'
                    },
                    10:
                    {
                        'title': 'Responsable de Dimensión',
                        'class': 'bg-fusion-800'
                    },
                };
                if (typeof badge[data] === 'undefined')
                {
                    return data;
                }
                return '<span class="badge ' + badge[data].class + ' badge-pill">' + badge[data].title + '</span>';
            },
        }],
    });
});