<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

$config['template']['default'] = 'default_template';

$config['template']['js'] = array();

$config['template']['css'] = array();

/* End of file template.php */
/* Location: ./application/config/template.php */