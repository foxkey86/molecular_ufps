<?php
defined('BASEPATH') OR exit('No direct script access allowed');
// header('Access-Control-Allow-Origin: http://localhost:8100');
// header("Content-Type: application/json; charset=UTF-8");
// header ("Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS");
// header("Access-Control-Allow-Headers: Origin,X-Requested-With,Content-Type,Accept,Access-Control-Request-Method,Authorization,Cache-Control");

use chriskacerguis\RestServer\RestController;
require APPPATH . '/libraries/RestController.php';
require APPPATH . '/libraries/Format.php';

class Restserver extends RestController {

	function __construct()
	{
		parent::__construct();

	   $this->load->model("PerfilModel");
	   $this->template->set_template('dashboard');
	}

	public function test_get(){
		$array = array("Hola", "Mundo", "CodeIgniter");
		// $this->response($array);
		$this->response( [
			'status' => false,
			'message' => 'No users were found'
		], 404 );
	}

	public function login_post()
	{

        $JSONData = file_get_contents("php://input");
        $dataObject = json_decode($JSONData);
        $usuario = $dataObject->usuario;
        $password = $dataObject->password;
        
        if(!$usuario){
            $this->response( [
                'status' => false,
                'message' => 'No existe usuario de entrada',
                'token' => ''
            ], 404 );
        }
        if(!$password){
            $this->response( [
                'status' => false,
                'message' => 'No existe password de entrada',
                'token' => ''
            ], 404 );
        }
        $this->load->model("UsuariosModel");
		$user = $this->UsuariosModel->getUserLoginApi($usuario, $password);
        if(count($user) == 0){
            $this->response( [
                'status' => false,
                'message' => 'Usuario y/o contraseña incorrecta',
                'token' => ''
            ], 401 );
        }

        $this->response( [
            'data' => $user,
            'status' => true,
            'message' => 'Sesión iniciada correctamente',
            'token' => '2!ld$u3ep*!7PPggz2eNCTnEmSPGwT6J9$geKR6H5fY1E%g*B#'
        ], 200 );
	}

    public function profesional_get()
    {
        $this->load->model("PerfilModel");
        $this->load->model("UsuariosModel");
        $id = $this->get('id');
        $profesional = $this->PerfilModel->getPerfilProfesional($id);
        if(isset($profesional)){
            $this->response( [
                'data' => $profesional,
                'status' => true,
                'message' => 'Profesional'
            ], 200 );
		}

        $this->response( [
            'status' => false,
            'message' => 'No existe el profesional'
        ], 404 );

    }

    public function particular_get()
    {
        $this->load->model("PerfilModel");
        $this->load->model("UsuariosModel");
        $id = $this->get('id');
		$particular = $this->PerfilModel->getPerfilParticular($id);
        if(isset($particular)){
            $this->response( [
                'data' => $particular,
                'status' => true,
                'message' => 'Particular'
            ], 200 );
		}

        $this->response( [
            'status' => false,
            'message' => 'No existe el Particular'
        ], 404 );
    }

    public function preguntasuser_get()
    {
        $this->load->model("ConsultasModel");
        $this->load->model("UsuariosModel");
        $id = $this->get('id');
        $preguntas = $this->ConsultasModel->getPreguntasApi($id);

        foreach($preguntas as $pregunta){
            $userAdm = $this->UsuariosModel->getUserRespApi($pregunta->id_user_r);
            if($pregunta->id != null){
                $pregunta->nombres_user_r = $userAdm->nombres;
                $pregunta->avatar_user_r = $userAdm->avatar;
            }else{
                $pregunta->nombres_user_r = 'Pendiente';
                $pregunta->avatar_user_r = 'default.jpg';
            }
            $pregunta->temas = '';
            $temas_pregunta = $this->ConsultasModel->getTemasPregunta($pregunta->id);
            foreach($temas_pregunta as $tema){
                $pregunta->temas .= $tema->nombre. ' - ' ;
                if($tema->otro_tema){
                    $pregunta->temas .= $tema->otro_tema. ' - ' ;
                }
            }

            $pregunta->sustancia = '';
            $sustancia_pregunta = $this->ConsultasModel->getSustanciaPregunta($pregunta->id_sustancia);
            if($sustancia_pregunta){
                $pregunta->sustancia = $sustancia_pregunta->nombre;
            }

            $pregunta->orientada = '';
            $orientada_pregunta = $this->ConsultasModel->getOrientadaPregunta($pregunta->id_orientacion);
            if($orientada_pregunta){
                $pregunta->orientada = $orientada_pregunta->nombre;
            }
        }
        if(isset($preguntas)){
            $this->response($preguntas,200);
		}

        $this->response( [
            'status' => false,
            'message' => 'No existe preguntas relacionadas'
        ], 404 );
    }

    public function existependientesuser_get()
    {
        $this->load->model("ConsultasModel");
        $id = $this->get('id');
        $pendientes = $this->ConsultasModel->getPendientes($id);

        if(isset($pendientes)){
            $this->response($pendientes,200);
		}

        $this->response( [
            'status' => false,
            'message' => 'No existe pendientes relacionadas'
        ], 404 );
    }

    public function registropregunta_post()
    {

        $this->load->model("ConsultasModel");

        $JSONData = file_get_contents("php://input");
        $dataObject = json_decode($JSONData);
        $id_orientacion = $dataObject->id_orientacion;
        $otro_orientada = $dataObject->otro_orientada;
        $otro_tema = $dataObject->otro_tema;
        $id_sustancia = $dataObject->id_sustancia;
        $nombre_producto = $dataObject->nombre_producto;
        $descripcion = $dataObject->descripcion;
        $id_user = $dataObject->id_user;
        
        if(!$id_orientacion || strlen($id_orientacion) > 2){
            $this->response( [
                'status' => false,
                'message' => 'Orientación no permitida',
                'token' => ''
            ], 404 );
        }        
        
        if(strlen($otro_orientada) > 100){
            $this->response( [
                'status' => false,
                'message' => 'Otro orientada no permitida',
                'token' => ''
            ], 404 );
        }

        if(strlen($otro_tema) > 100){
            $this->response( [
                'status' => false,
                'message' => 'Otro tema no permitido',
                'token' => ''
            ], 404 );
        }

        if(!$id_sustancia || strlen($id_sustancia) > 2){
            $this->response( [
                'status' => false,
                'message' => 'Sustancia no permitida',
                'token' => ''
            ], 404 );
        }

        if(!$nombre_producto || strlen($nombre_producto) > 100){
            $this->response( [
                'status' => false,
                'message' => 'Nombre del producto no permitido',
                'token' => ''
            ], 404 );
        }

        if(!$descripcion || strlen($descripcion) > 4000){
            $this->response( [
                'status' => false,
                'message' => 'Descripción no permitida',
                'token' => ''
            ], 404 );
        }
                   
        $id_pregunta = $this->ConsultasModel->setPregunta($id_orientacion, $otro_orientada, $id_sustancia, $nombre_producto, $descripcion, $id_user);

        if ($id_pregunta > 0) {
            


            $listado_temas = $this->ConsultasModel->getTemas();
            $temas = array();
            array_push($temas,boolval($dataObject->temas0));
            array_push($temas,boolval($dataObject->temas1));
            array_push($temas,boolval($dataObject->temas2));
            array_push($temas,boolval($dataObject->temas3));
            array_push($temas,boolval($dataObject->temas4));
            array_push($temas,boolval($dataObject->temas5));
            array_push($temas,boolval($dataObject->temas6));
            
            for($i = 0; $i < count($listado_temas); $i++){
                if($temas[$i]){
                    if($listado_temas[$i]->id == 7){
                        $aux = $this->ConsultasModel->setTemas($id_pregunta, $listado_temas[$i]->id, $otro_tema);
                    }else{
                        $aux = $this->ConsultasModel->setTemas($id_pregunta, $listado_temas[$i]->id);
                    }
                }
            }

            $this->response( [
                'data' => $temas,
                'status' => true,
                'message' => 'La consulta se envió correctamente, por favor debe esperar a la respuesta.'
            ], 200 );

        } else {
            $this->response( [
                'status' => false,
                'message' => 'La consulta no se registró correctamente, por favor inténtelo de nuevo.',
                'token' => ''
            ], 404 );
        }

        $this->response( [
            'status' => false,
            'message' => 'La consulta no se registró correctamente, por favor inténtelo de nuevo.',
            'token' => ''
        ], 404 );

    }

	public function users_get()
    {
        // Users from a data store e.g. database
        $users = [
            ['id' => 0, 'name' => 'John', 'email' => 'john@example.com'],
            ['id' => 1, 'name' => 'Jim', 'email' => 'jim@example.com'],
        ];

		$this->load->model("UsuariosModel");
		$users = $this->UsuariosModel->getUsuarios();


        $id = $this->get( 'id' );

        if ( $id === null )
        {
            // Check if the users data store contains users
            if ( $users )
            {
                // Set the response and exit
                $this->response( $users, 200 );
            }
            else
            {
                // Set the response and exit
                $this->response( [
                    'status' => false,
                    'message' => 'No users were found'
                ], 404 );
            }
        }
        else
        {
            if ( array_key_exists( $id, $users ) )
            {
                $this->response( $users[$id], 200 );
            }
            else
            {
                $this->response( [
                    'status' => false,
                    'message' => 'No such user found'
                ], 404 );
            }
        }
    }
}