<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Graficas_print extends CMS_Controller {

	 function __construct()
	 {
		 parent::__construct();

	//	 $this->template->add_css('custom/style');

		 $user = array();
		 if ($this->session->userdata(SESSION_NAME)) {
			$user = $this->session->userdata(SESSION_NAME);
		}else{
	//		redirect();
		}
		
		$this->load->model("PerfilModel");
	//	$this->template->set_template('dashboard');
		$this->template->set_template('indicadores_print');
		$this->load->model("GraficasModel");

	 }

 
	public function index()
	{

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'graficas');
		$this->template->set('content_header', 'Graficas');
        $this->template->set('content_sub_header', 'Consulte las graficas realizadas');
        $this->template->render('graficas/index');


		
	}

	
	public function google()
	{

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'graficas_go');
		$this->template->set('content_header', 'Graficas');
        $this->template->set('content_sub_header', 'Consulte las graficas realizadas');
        $this->template->render('graficas/google');


		
	}


	public function propio()
	{

		$this->template->add_js("custom/estadistica");	
		$this->template->add_js("js/statistics/peity/peity.bundle");
		$this->template->add_js("js/statistics/flot/flot.bundle");
		$this->template->add_js("js/statistics/easypiechart/easypiechart.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");

	

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'propio');
		$this->template->set('content_header', 'Graficas');
        $this->template->set('content_sub_header', 'Consulte las graficas realizadas');
        $this->template->render('graficas/propio');


		
	}

	public function mapa()
	{

		$this->template->add_js("custom/estadistica");	
		$this->template->add_js("js/statistics/peity/peity.bundle");
		$this->template->add_js("js/statistics/flot/flot.bundle");
		$this->template->add_js("js/statistics/easypiechart/easypiechart.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");

	

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'mapa');
		$this->template->set('content_header', 'Graficas');
        $this->template->set('content_sub_header', 'Consulte las graficas realizadas');
        $this->template->render('graficas/mapa');


		
	}

	public function periodos()
	{

	//	$this->template->add_js("custom/estadistica");	
		
		$this->template->add_js("custom/indicadores");	
		$this->template->add_js("js/statistics/peity/peity.bundle");
		$this->template->add_js("js/statistics/flot/jquery.flot.axislabels");
		$this->template->add_js("js/statistics/flot/flot.bundle");
		$this->template->add_js("js/statistics/easypiechart/easypiechart.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");

	//	$reporte = $this->GraficasModel->getReporteEvento(1);
	//	echo '<pre>'; print_r($reporte); return;
	//	$mujeres = $this->GraficasModel->getReporteMujeres(1);
	//	echo '<pre>'; print_r($mujeres->num); return;
	//	$hombres = $this->GraficasModel->getReporteHombres(1);
	//	echo '<pre>'; print_r($hombres->num); return;
	//	$porMujeres = ($mujeres->num * 100) / ($mujeres->num + $hombres->num);
	//	$porHombres = ($hombres->num * 100) / ($mujeres->num + $hombres->num);
	//	if($porHombres > $porMujeres){
	//		$dif = $porHombres - $porMujeres;
	//	}else{
	//		$dif = $porMujeres - $porHombres;
	//	}
	/*	$this->template->set('reporte', $reporte);
		$this->template->set('mujeres', $mujeres->num);
		$this->template->set('hombres', $hombres->num);
		$this->template->set('porMujeres', $porMujeres);
		$this->template->set('porHombres', $porHombres);
		$this->template->set('dif', $dif); */

		if($this->input->post('arreglo')){
			$aux = explode("_", $this->input->post('arreglo'));
		//	echo '<pre>'; print_r($aux); return;
		//	$this->dengue();
			if($aux[0] == 1 || $aux[0] == 2){
				redirect('graficas/dengue');
			}
			if($aux[0] == 3 || $aux[0] == 4 || $aux[0] == 5 || $aux[0] == 6 || $aux[0] == 7 || $aux[0] == 8){
				redirect('graficas/hepatitis');
			}
			if($aux[0] == 9 || $aux[0] == 10){
				redirect('graficas/tuberculosis');
			}
			if($aux[0] == 11){
				redirect('graficas/vih');
			}

		//	echo '<pre>'; print_r($aux); return;
		}


		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'periodos');
		$this->template->set('content_header', 'Indicadores');
        $this->template->set('content_sub_header', 'Consulte las graficas enfermedades de norte de santander');
        $this->template->render('graficas/periodos');


		
	}


	public function dengue()
	{

		$this->template->add_js("custom/estadistica");	
		$this->template->add_js("js/statistics/peity/peity.bundle");
		$this->template->add_js("js/statistics/flot/jquery.flot.axislabels");
		$this->template->add_js("js/statistics/flot/flot.bundle");
		$this->template->add_js("js/statistics/easypiechart/easypiechart.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");

		$this->template->add_js("custom/print");	
		$this->template->add_css("css/print");


	//	$reporte = $this->GraficasModel->getReporteEvento(1);
	//	echo '<pre>'; print_r("hola"); return;
	//	$mujeres = $this->GraficasModel->getReporteMujeres(1);
	//	echo '<pre>'; print_r($mujeres->num); return;
	//	$hombres = $this->GraficasModel->getReporteHombres(1);
	//	echo '<pre>'; print_r($hombres->num); return;
	/*	$porHombres = 0;
		$porMujeres = 0;
		if($hombres->num > 0){
			$porHombres = ($hombres->num * 100) / ($mujeres->num + $hombres->num);
		}
		if($mujeres->num > 0){
			$porMujeres = ($mujeres->num * 100) / ($mujeres->num + $hombres->num);
		}

		if($porHombres > $porMujeres){
			$dif = $porHombres - $porMujeres;
		}else{
			$dif = $porMujeres - $porHombres;
		}
		$this->template->set('reporte', $reporte);
		$this->template->set('mujeres', $mujeres->num);
		$this->template->set('hombres', $hombres->num);
		$this->template->set('porMujeres', $porMujeres);
		$this->template->set('porHombres', $porHombres);
		$this->template->set('dif', $dif);
        */

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'dengue');
		$this->template->set('content_header', 'Graficas Dengue 2019');
        $this->template->set('content_sub_header', 'Consulte las graficas enfermedad');
        $this->template->render('graficas/enfermedad_print');
	}
	
	public function jsonSemanasDengue() {
	//	echo json_encode(array('error' => 'No encontro la información.' ));
	//	exit;
   //     $this->validar_ajax(); echo json_encode(array('carga' => true, 'codigo_recibo' => $referencia_pago,'total'=>$total,'url'=>$url_pago,'descripcion'=>$descripcion));
		$this->load->model("GraficasModel");
	//	$this->echo_json($this->GraficasModel->getReporteHombres(1));	
	/////////////////////////////////////////////////////////////////////////////////// implementar indicador convertir a id_evento y clasfinal ///////////////////////////
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

        if ($id_evento) {
			$semanas = $this->GraficasModel->getEnfermedad($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			foreach($semanas as $row):
				array_push($data, array($row->semana, $row->num));
			endforeach;
		//	echo '<pre>'; print_r($data); return;



		//	$dataSet1 = Array(Array('0','0'),Array('1','8'),Array('2','9'),Array('3','10'));
			echo json_encode(array('carga' => true, 'data' => $data));
        }else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
    }

	public function jsonSemanasHep() {
			$this->load->model("GraficasModel");
			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));
	
			if ($id_evento) {
				$semanas = $this->GraficasModel->getEnfermedadHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				foreach($semanas as $row):
					array_push($data, array($row->semana, $row->num));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

	public function jsonSemanasTub() {
		$this->load->model("GraficasModel");
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$semanas = $this->GraficasModel->getEnfermedadTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			foreach($semanas as $row):
				array_push($data, array($row->semana, $row->num));
			endforeach;
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function rangoTri($trimestre){
		switch ($trimestre) {
			case 1:
				return array(1,12);
			case 2:
				return array(13,24);
			case 3:
				return array(25,36);
			case 4:
				return array(37,52);
			case 5:
				return array(1,52);
		}
	}

	public function indicadorAClasFinal($indicador){
		switch ($indicador) {
			case 1:
				return 1;
			case 2:
				return 2;
			case 3:
				return 1;
			case 4:
				return 2;
			case 5:
				return 3;
			case 6:
				return 4;
			case 7:
				return 5;
			case 8:
				return 6;
			case 9:
				return 1;
			case 10:
				return 2;
			case 11:
				return 3;
		}
	}

	public function indicadorAEvento($indicador){
		switch ($indicador) {
			case 1:
				return 1;
			case 2:
				return 1;
			case 3:
				return 2;
			case 4:
				return 2;
			case 5:
				return 2;
			case 6:
				return 2;
			case 7:
				return 2;			
			case 8:
				return 2;
			case 9:
				return 3;
			case 10:
				return 3;
			case 11:
				return 4;
			case 12:
				return 4;
			case 13:
				return 5;
			case 14:
				return 5;
			case 15:
				return 5;
		}
	}

	public function indicadorAEventoNom($indicador){
		switch ($indicador) {
			case 1:
				return 'Dengue sin signos de alarma';
			case 2:
				return 'Dengue con signos de alarma';
			case 3:
				return 'Hepatitis A';
			case 4:
				return 'Hepatitis B Aguda';
			case 5:
				return 'Hepatitis B Crónica';
			case 6:
				return 'Hepatitis B por Transmisión Perinatal';
			case 7:
				return 'Hepatitis Coinfección B-D';			
			case 8:
				return 'Hepatitis C';
			case 9:
				return 'TB Pulmonares';
			case 10:
				return 'TB Extrapulmonares';
			case 11:
				return 'VIH';
			case 12:
				return 'VIH con coinfección TB';
			case 13:
				return 'COVID confirmados';
			case 14:
				return 'COVID Recuperados';
			case 15:
				return 'COVID Fallecidos';
		}
	}

	public function ubicacionDptoMuni($ubicacion){
		if($ubicacion == 'Norte de Santander'){
			return 1;
		}
		if($ubicacion == 'Cúcuta'){
			return 2;
		}
		if($ubicacion == 'Villa del Rosario'){
			return 2;
		}
		if($ubicacion == 'Los Patios'){
			return 2;
		}
		if($ubicacion == 'El Zulia'){
			return 2;
		}
	}

	public function ubicacionMuni($ubicacion){
		if($ubicacion == 'Cúcuta'){
			return 1;
		}
		if($ubicacion == 'Villa del Rosario'){
			return 40;
		}
		if($ubicacion == 'Los Patios'){
			return 22;
		}
		if($ubicacion == 'El Zulia'){
			return 15;
		}
		return 0;
	}

	public function jsonAfiliacionDengue() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getAfiliacion($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->tip_ss) {
						case 1:
							$i = 0;
							break;
						case 2:
							$i = 2;
							break;
						case 3:
							$i = 4;
							break;
						case 4:
							$i = 6;
							break;
						case 5:
							$i = 8;
						case 6:
							$i = 10;
							break;
					}
					array_push($data, array($i, $row->num));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonAfiliacionHep() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getAfiliacionHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->tip_ss) {
						case 1:
							$i = 0;
							break;
						case 2:
							$i = 2;
							break;
						case 3:
							$i = 4;
							break;
						case 4:
							$i = 6;
							break;
						case 5:
							$i = 8;
						case 6:
							$i = 10;
							break;
					}
					array_push($data, array($i, $row->num));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonAfiliacionTub() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getAfiliacionTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->tip_ss) {
						case 1:
							$i = 0;
							break;
						case 2:
							$i = 2;
							break;
						case 3:
							$i = 4;
							break;
						case 4:
							$i = 6;
							break;
						case 5:
							$i = 8;
						case 6:
							$i = 10;
							break;
					}
					array_push($data, array($i, $row->num));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonAfiliacionVih() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getAfiliacionVih($id_evento, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->tip_ss) {
						case 1:
							$i = 0;
							break;
						case 2:
							$i = 2;
							break;
						case 3:
							$i = 4;
							break;
						case 4:
							$i = 6;
							break;
						case 5:
							$i = 8;
						case 6:
							$i = 10;
							break;
					}
					array_push($data, array($i, $row->num));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonTransmisionHep() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getTransmision($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->met_tra) {
						case 1:
							$i = 0;
							break;
						case 2:
							$i = 2;
							break;
						case 3:
							$i = 4;
							break;
						case 4:
							$i = 6;
							break;
						case 5:
							$i = 8;
						case 6:
							$i = 10;
							break;
					}
					array_push($data, array($i, $row->num));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonEdadesDengue() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$data = array();
				$result = $this->GraficasModel->getEdades($id_evento, $anyo, 0, 4, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(0, $result->num));
				$result = $this->GraficasModel->getEdades($id_evento, $anyo, 5, 14, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(2, $result->num));
				$result = $this->GraficasModel->getEdades($id_evento, $anyo, 15, 44, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(4, $result->num));
				$result = $this->GraficasModel->getEdades($id_evento, $anyo, 45, 64, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(6, $result->num));
				$result = $this->GraficasModel->getEdades($id_evento, $anyo, 65, 120, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(8, $result->num));
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonEdadesHep() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$data = array();
				$result = $this->GraficasModel->getEdadesHep($id_evento, $anyo, 0, 5, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(0, $result->num));
				$result = $this->GraficasModel->getEdadesHep($id_evento, $anyo, 6, 14, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(2, $result->num));
				$result = $this->GraficasModel->getEdadesHep($id_evento, $anyo, 15, 45, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(4, $result->num));
				$result = $this->GraficasModel->getEdadesHep($id_evento, $anyo, 46, 120, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(6, $result->num));
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}


		public function jsonHospitalizadoDengue() {

			$this->load->model("GraficasModel");	
			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getHospitalizados($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->pac_hos) {
						case 1:
							$i = 0;
							break;
						case 2:
							$i = 1;
							break;
					}
					array_push($data, array($i, $row->num));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}


	public function jsonBarOcuDengue()
	{
		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getBarOcu($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;

					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->bar_ver));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}

	public function jsonBarOcuTub()
	{
		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getBarOcuTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;

					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->bar_ver));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}

	public function jsonBarOcuVih()
	{
		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getBarOcuVih($id_evento, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;

					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->bar_ver));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}


	public function jsonBarOcuHep()
	{
		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getBarOcuHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;

					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->bar_ver));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}


	public function jsonMunOcuDengue()
	{
		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));

			if ($id_evento) {
				$result = $this->GraficasModel->getMuniOcu($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;
					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->muni));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}

	
	public function jsonNacOcuTub()
	{
		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));

			if ($id_evento) {
				$result = $this->GraficasModel->getNacTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;
					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->pais));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}

	public function jsonNacOcuVih()
	{
		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));

			if ($id_evento) {
				$result = $this->GraficasModel->getNacVih($id_evento, $anyo, $trimestre, $dpto_muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;
					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->pais));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}


	public function jsonMunOcuHep()
	{
		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));

			if ($id_evento) {
				$result = $this->GraficasModel->getMuniOcuHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;
					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->muni));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}

	public function jsonMunOcuTub()
	{
		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));

			if ($id_evento) {
				$result = $this->GraficasModel->getMuniOcuTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;
					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->muni));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}

	public function jsonMunOcuVih()
	{
		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));

			if ($id_evento) {
				$result = $this->GraficasModel->getMuniOcuVih($id_evento, $anyo, $trimestre, $dpto_muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;
					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->muni));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}


	public function jsonGestantesHep() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getGestantesHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			foreach($result as $row):
				$i = 0;
				switch($row->gp_gestan) {
					case 1:
						$i = 0;
						break;
					case 2:
						$i = 1;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonCoinfeccionHep() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getConinfeccionHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			foreach($result as $row):
				$i = 0;
				switch($row->coinf_vih_coinfeccion) {
					case 1:
						$i = 0;
						break;
					case 2:
						$i = 1;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonTotalDengue() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		$result = array();

		if ($id_evento) {
			$result_query = $this->GraficasModel->getGeneroDengue($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			
			if(count($result_query) == 1){
				if($result_query[0]->sexo == 'F'){
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => $result_query[0]->num));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => 0));
				}else{
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => $result_query[0]->num));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => 0));
				}
			}else{
				if(count($result_query) == 0){
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => 0));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => 0));
				}else{
					$result = $result_query;
				}
			}
			$nom_evento = $this->indicadorAEventoNom($this->input->post('indicador'));
			array_push($result, array('enfermedad' => $nom_evento));
			array_push($result, array('ubicacion' => $this->input->post('dpto_muni')));
			array_push($result, array('anyo' => $this->input->post('anyo')));
			if($this->input->post('trimestre') == 5){
				$trime = 'Todos';
			}else{
				$trime = $this->input->post('trimestre');
			}
			array_push($result, array('trimestre' => $trime));
			echo json_encode(array('carga' => true, 'data' => $result));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonTotalHepatitis() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		$result = array();
	//	array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => '0'));
	//	array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => '0'));

		if ($id_evento) {
			$result_query = $this->GraficasModel->getGeneroHepatitis($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			if(count($result_query) == 1){
				if($result_query[0]->sexo == 'F'){
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => $result_query[0]->num));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => 0));
				}else{
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => $result_query[0]->num));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => 0));
				}
			}else{
				if(count($result_query) == 0){
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => 0));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => 0));
				}else{
					$result = $result_query;
				}
			}
			
			$nom_evento = $this->indicadorAEventoNom($this->input->post('indicador'));
			array_push($result, array('enfermedad' => $nom_evento));
			array_push($result, array('ubicacion' => $this->input->post('dpto_muni')));
			array_push($result, array('anyo' => $this->input->post('anyo')));
			if($this->input->post('trimestre') == 5){
				$trime = 'Todos';
			}else{
				$trime = $this->input->post('trimestre');
			}
			array_push($result, array('trimestre' => $trime));
			echo json_encode(array('carga' => true, 'data' => $result));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonTotalTub() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		$result = array();

		if ($id_evento) {
			$result_query = $this->GraficasModel->getGeneroTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			if(count($result_query) == 1){
				if($result_query[0]->sexo == 'F'){
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => $result_query[0]->num));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => 0));
				}else{
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => $result_query[0]->num));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => 0));
				}
			}else{
				if(count($result_query) == 0){
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => 0));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => 0));
				}else{
					$result = $result_query;
				}
			}
			
			$nom_evento = $this->indicadorAEventoNom($this->input->post('indicador'));
			array_push($result, array('enfermedad' => $nom_evento));
			array_push($result, array('ubicacion' => $this->input->post('dpto_muni')));
			array_push($result, array('anyo' => $this->input->post('anyo')));
			if($this->input->post('trimestre') == 5){
				$trime = 'Todos';
			}else{
				$trime = $this->input->post('trimestre');
			}
			array_push($result, array('trimestre' => $trime));
			echo json_encode(array('carga' => true, 'data' => $result));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonTotalVih() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		$result = array();

		if ($id_evento) {
			$result_query = $this->GraficasModel->getGeneroVih($id_evento, $anyo, $trimestre, $dpto_muni, $muni);
			if(count($result_query) == 1){
				if($result_query[0]->sexo == 'F'){
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => $result_query[0]->num));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => 0));
				}else{
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => $result_query[0]->num));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => 0));
				}
			}else{
				if(count($result_query) == 0){
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => 0));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => 0));
				}else{
					$result = $result_query;
				}
			}
			
			$nom_evento = $this->indicadorAEventoNom($this->input->post('indicador'));
			array_push($result, array('enfermedad' => $nom_evento));
			array_push($result, array('ubicacion' => $this->input->post('dpto_muni')));
			array_push($result, array('anyo' => $this->input->post('anyo')));
			if($this->input->post('trimestre') == 5){
				$trime = 'Todos';
			}else{
				$trime = $this->input->post('trimestre');
			}
			array_push($result, array('trimestre' => $trime));
			echo json_encode(array('carga' => true, 'data' => $result));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	
	public function jsonCondicionTub() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getCondicionTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			foreach($result as $row):
				$i = 0;
				switch($row->cond_tuber) {
					case 1:
						$i = 0;
						break;
					case 2:
						$i = 1;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

 	public function jsonLoginPass(){
		$this->load->model("LoginModel");
		$usuario = $this->input->post('usuario');
		$pass = $this->input->post('pass');
		if($usuario && $pass){
			$persona = $this->LoginModel->getUsuarioPerfil($usuario, $pass, 10);
			if($persona){
				if($persona->estado == 1){
					echo json_encode(array('carga' => true, 'data' => 'IJ9*BXTwKKSf@gbDts')); exit;
				}else{
					echo json_encode(array('carga' => true, 'data' => 'T6zqqMoJB8G$LAz%')); exit;
				}
			}
		}
		echo json_encode(array('carga' => false, 'data' =>  'T6zqqMoJB8G$LAz%')); exit;
	 }

	public function jsonCoinfeccionTbc() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getConinfeccionTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			foreach($result as $row):
				$i = 0;
				switch($row->vih_confirmado) {
					case 1:
						$i = 0;
						break;
					case 2:
						$i = 1;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonEdadesTub() {

		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$data = array();
			$result = $this->GraficasModel->getEdadesTub($id_evento, $anyo, 0, 14, $dpto_muni, $clasfinal, $trimestre, $muni);
			array_push($data, array(0, $result->num));
			$result = $this->GraficasModel->getEdadesTub($id_evento, $anyo, 15, 24, $dpto_muni, $clasfinal, $trimestre, $muni);
			array_push($data, array(2, $result->num));
			$result = $this->GraficasModel->getEdadesTub($id_evento, $anyo, 25, 34, $dpto_muni, $clasfinal, $trimestre, $muni);
			array_push($data, array(4, $result->num));
			$result = $this->GraficasModel->getEdadesTub($id_evento, $anyo, 35, 44, $dpto_muni, $clasfinal, $trimestre, $muni);
			array_push($data, array(6, $result->num));
			$result = $this->GraficasModel->getEdadesTub($id_evento, $anyo, 45, 54, $dpto_muni, $clasfinal, $trimestre, $muni);
			array_push($data, array(8, $result->num));
			$result = $this->GraficasModel->getEdadesTub($id_evento, $anyo, 55, 64, $dpto_muni, $clasfinal, $trimestre, $muni);
			array_push($data, array(10, $result->num));
			$result = $this->GraficasModel->getEdadesTub($id_evento, $anyo, 65, 120, $dpto_muni, $clasfinal, $trimestre, $muni);
			array_push($data, array(12, $result->num));
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonEdadesVih() {

		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$data = array();
			$result = $this->GraficasModel->getEdadesVih($id_evento, $anyo, 0, 14, $dpto_muni, $trimestre, $muni);
			array_push($data, array(0, $result->num));
			$result = $this->GraficasModel->getEdadesVih($id_evento, $anyo, 15, 24, $dpto_muni, $trimestre, $muni);
			array_push($data, array(2, $result->num));
			$result = $this->GraficasModel->getEdadesVih($id_evento, $anyo, 25, 34, $dpto_muni, $trimestre, $muni);
			array_push($data, array(4, $result->num));
			$result = $this->GraficasModel->getEdadesVih($id_evento, $anyo, 35, 44, $dpto_muni,  $trimestre, $muni);
			array_push($data, array(6, $result->num));
			$result = $this->GraficasModel->getEdadesVih($id_evento, $anyo, 45, 54, $dpto_muni, $trimestre, $muni);
			array_push($data, array(8, $result->num));
			$result = $this->GraficasModel->getEdadesVih($id_evento, $anyo, 55, 64, $dpto_muni, $trimestre, $muni);
			array_push($data, array(10, $result->num));
			$result = $this->GraficasModel->getEdadesVih($id_evento, $anyo, 65, 120, $dpto_muni, $trimestre, $muni);
			array_push($data, array(12, $result->num));
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonEstadoClinicoVih() {

		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$trimestre = $this->rangoTri($this->input->post('trimestre'));
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getEstadoClinicoVih($id_evento, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			foreach($result as $row):
				$i = 0;
				switch($row->est_cli) {
					case 1:
						$i = 0;
						break;
					case 2:
						$i = 2;
						break;
					case 3:
						$i = 4;
						break;
					case 4:
						$i = 6;
						break;
					case 5:
						$i = 8;
					case 6:
						$i = 10;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function hepatitis()
	{

		$this->template->add_js("custom/estadistica_hep");	
		$this->template->add_js("js/statistics/peity/peity.bundle");
		$this->template->add_js("js/statistics/flot/jquery.flot.axislabels");
		$this->template->add_js("js/statistics/flot/flot.bundle");
		$this->template->add_js("js/statistics/easypiechart/easypiechart.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");

		$this->template->add_js("custom/print");	
		$this->template->add_css("css/print");

	//	$reporte = $this->GraficasModel->getReporteEvento(1);
	//	echo '<pre>'; print_r($reporte); return;
	//	$mujeres = $this->GraficasModel->getReporteMujeres(1);
	//	echo '<pre>'; print_r($mujeres->num); return;
	//	$hombres = $this->GraficasModel->getReporteHombres(1);
	//	echo '<pre>'; print_r($hombres->num); return;
	/*	$porHombres = 0;
		$porMujeres = 0;
		if($hombres->num > 0){
			$porHombres = ($hombres->num * 100) / ($mujeres->num + $hombres->num);
		}
		if($mujeres->num > 0){
			$porMujeres = ($mujeres->num * 100) / ($mujeres->num + $hombres->num);
		}

		if($porHombres > $porMujeres){
			$dif = $porHombres - $porMujeres;
		}else{
			$dif = $porMujeres - $porHombres;
		} */
	//	$this->template->set('reporte', $reporte);
	//	$this->template->set('mujeres', $mujeres->num);
   //		$this->template->set('hombres', $hombres->num);
	//	$this->template->set('porMujeres', $porMujeres);
//		$this->template->set('porHombres', $porHombres);
	//	$this->template->set('dif', $dif);


		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'hepatitis');
		$this->template->set('content_header', 'Graficas Hepatitis 2019');
        $this->template->set('content_sub_header', 'Consulte las graficas enfermedad');
        $this->template->render('graficas/enfermedad_hep_print');
	}

	public function tuberculosis()
	{

		$this->template->add_js("custom/estadistica_tub");	
		$this->template->add_js("js/statistics/peity/peity.bundle");
		$this->template->add_js("js/statistics/flot/jquery.flot.axislabels");
		$this->template->add_js("js/statistics/flot/flot.bundle");
		$this->template->add_js("js/statistics/easypiechart/easypiechart.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");

		$this->template->add_js("custom/print");	
		$this->template->add_css("css/print");

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'tuberculosis');
		$this->template->set('content_header', 'Graficas Tuberculosis 2019');
        $this->template->set('content_sub_header', 'Consulte las graficas enfermedad');
        $this->template->render('graficas/enfermedad_tub_print');
	}


	public function vih()
	{

		$this->template->add_js("custom/estadistica_vih");	
		$this->template->add_js("js/statistics/peity/peity.bundle");
		$this->template->add_js("js/statistics/flot/jquery.flot.axislabels");
		$this->template->add_js("js/statistics/flot/flot.bundle");
		$this->template->add_js("js/statistics/easypiechart/easypiechart.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");

		$this->template->add_js("custom/print");	
		$this->template->add_css("css/print");

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'vih');
		$this->template->set('content_header', 'Graficas VIH 2019');
        $this->template->set('content_sub_header', 'Consulte las graficas enfermedad');
        $this->template->render('graficas/enfermedad_vih_print');
	}
	
	public function covid()
	{

		$this->template->add_js("custom/estadistica_cov");	
		$this->template->add_js("js/statistics/peity/peity.bundle");
		$this->template->add_js("js/statistics/flot/jquery.flot.axislabels");
		$this->template->add_js("js/statistics/flot/flot.bundle");
		$this->template->add_js("js/statistics/easypiechart/easypiechart.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");

		$this->template->add_js("custom/print");	
		$this->template->add_css("css/print");

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'vih');
		$this->template->set('content_header', 'Graficas Covid');
        $this->template->set('content_sub_header', 'Consulte las graficas enfermedad');
        $this->template->render('graficas/enfermedad_cov_print');
	}
	


}
