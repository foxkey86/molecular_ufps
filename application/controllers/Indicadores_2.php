<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Indicadores extends CMS_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 function __construct()
	 {
		 parent::__construct();
         $this->load->model("LoginModel");
		 $this->template->set_template('default_ind');

	 }

 
	public function index()
	{
      
		$this->template->add_css("custom/style");
		$this->template->set('login', true);
        $this->template->render('login/indicadores');

	}

	public function loguearUsuario(){


		if($this->input->post('logueo') == 1){
			$this->load->library('form_validation');
            $rules = [
                [
                    'field' => 'username',
                    'label' => 'Correo Electrónico',
                    'rules' => 'trim|required|max_length[50]'
                ],
                [
                    'field' => 'password',
                    'label' => 'Contraseña',
                    'rules' => 'trim|required|max_length[20]'
                ]
            ];
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() === TRUE) {
				$persona = $this->LoginModel->getUsuario($this->input->post('username'), $this->input->post('password'));
                if ($persona) {
					$this->session->set_userdata(SESSION_NAME, $persona);
					$profesional = $this->LoginModel->getProfesional($persona->id);
					$this->session->set_userdata('profesional', $profesional);
					//	header("Location:".base_url());
					redirect('perfil');
				//	$this->layout->view("index");
                } else {
                 /*   $this->template->add_message([
                        'error' => 'Usuario o Contraseña invalidos'
					]); */
					$this->template->set_flash_message(array('error' => 'El usuario no existe en nuestra base de datos, por favor crea una cuenta.'));
					redirect();
                }
            }
		
		}


	}
	
    
    public function registro()
	{
		$this->template->add_js('custom/validaction');
		$this->template->set('login', false);
		$listado_documento = $this->_tipo_documentos($this->LoginModel->getTipoDocumentos());
		$listado_oficio = $this->_oficios($this->LoginModel->getOficios());
		$this->template->set('listado_documento',$listado_documento);
		$this->template->set('listado_oficio',$listado_oficio);
	//	echo '<pre>'; print_r($listado_documento); return;
		$this->template->render('login/registro');
	}

	private function _tipo_documentos($listado = null)
    {

        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione el tipo de documento";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

	private function _oficios($listado = null)
    {

        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione el tipo de oficio";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id_oficio] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}
	
	public function validar_registro(){

		if($this->input->post('registro') == 1){
		
			$this->load->library('form_validation');
            $rules = [
				[
                    'field' => 'id_tipo_documento',
                    'label' => 'Tipo de Documento',
                    'rules' => 'trim|required|max_length[2]'
				],
				[
                    'field' => 'documento',
                    'label' => 'Número de Documento',
                    'rules' => 'trim|required|max_length[15]'
				],
				[
                    'field' => 'nombres',
                    'label' => 'Nombres',
                    'rules' => 'required|max_length[50]'
				],
				[
                    'field' => 'apellidos',
                    'label' => 'Apellidos',
                    'rules' => 'required|max_length[50]'
				],
				[
                    'field' => 'genero',
                    'label' => 'Género',
                    'rules' => 'trim|required|max_length[1]'
                ],
                [
                    'field' => 'edad',
                    'label' => 'Edad',
                    'rules' => 'trim|required|numeric|max_length[3]'
				],
				[
                    'field' => 'celular',
                    'label' => 'Celular',
                    'rules' => 'trim|required|numeric|max_length[10]'
				],
				[
                    'field' => 'direccion',
                    'label' => 'Dirección',
                    'rules' => 'required|max_length[200]'
				],
				[
                    'field' => 'correo',
                    'label' => 'Correo Electrónico',
                    'rules' => 'required|max_length[200]'
                ],
                [
                    'field' => 'password',
                    'label' => 'Contraseña',
                    'rules' => 'trim|required|min_length[8]|max_length[20]'
                ],
                [
                    'field' => 'password',
                    'label' => 'Contraseña',
                    'rules' => 'trim|required|min_length[8]|max_length[20]'
                ],
                [
                    'field' => 'password',
                    'label' => 'Contraseña',
                    'rules' => 'trim|required|min_length[8]|max_length[20]'
                ],
                [
                    'field' => 'profesion',
                    'label' => 'Profesión',
                    'rules' => 'max_length[30]'
                ],
                [
                    'field' => 'institucion',
                    'label' => 'Institución',
                    'rules' => 'max_length[100]'
                ],
                [
                    'field' => 'oficio',
                    'label' => 'Oficio',
                    'rules' => 'max_length[3]'
                ]
			];
			
		/*	echo '<pre>'; print_r($this->input->post('id_tipo_documento'). ' - ' . $this->input->post('documento'). ' - ' . $this->input->post('nombres')
			. ' - ' . $this->input->post('apellidos'). ' - ' . $this->input->post('genero'). ' - ' .$this->input->post('edad'). ' - ' . $this->input->post('celular')
			. ' - ' . $this->input->post('direccion'). ' - ' . $this->input->post('correo'). ' - ' . $this->input->post('password')); return;
			*/

            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() === TRUE) {
			
				if ($this->LoginModel->setUsuario($this->input->post('id_tipo_documento'), $this->input->post('documento'), $this->input->post('nombres')
				, $this->input->post('apellidos'), $this->input->post('genero'), $this->input->post('edad'), $this->input->post('celular')
				, $this->input->post('direccion'), $this->input->post('correo'), $this->input->post('password'))) {
					$this->template->set_flash_message(array('success' => 'El usuario se registró correctamente, por favor inicie sesión.'));
					$persona = $this->LoginModel->getPersona($this->input->post('documento'));
					if($this->input->post('tipo_usuario') == 1){
						$aux = $this->LoginModel->setProfesional($persona->id, $this->input->post('profesion'), $this->input->post('institucion'));
					}else{
						$aux = $this->LoginModel->setParticular($persona->id, $this->input->post('oficio'));
					}

					redirect();
                } else {
					$this->template->set_flash_message(array('error' => 'El usuario no se registró correctamente, por favor inténtelo de nuevo.'));
					redirect();
                }
            }
		}
	}
}
