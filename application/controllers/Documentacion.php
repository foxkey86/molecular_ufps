<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Documentacion extends CMS_Controller {

	 function __construct()
	 {
		 parent::__construct();

	//	 $this->template->add_css('custom/style');

		 $user = array();
		 if ($this->session->userdata(SESSION_NAME)) {
			$user = $this->session->userdata(SESSION_NAME);
		}else{
			redirect();
		}
		
		$this->load->model("DocumentacionModel");
		$this->template->set_template('dashboard');

	 }


	 private function _validar_documento($name, $required = true, $tipo = "documento_archivo_lab")
	 {
		 switch ($tipo) {
			 case "documento_archivo_lab" :
				 $config['upload_path'] = './' . PATH_ARCHIVO_LAB;
				 $config['max_size'] = MAX_SIZE_ARCHIVO;
				 break;
			 default:
				 $config['upload_path'] = './' . PATH_ARCHIVO_LAB;
				 $config['max_size'] = MAX_SIZE_ARCHIVO;
				 break;
		 }
 
		 $config['allowed_types'] = 'txt|xls|xlsx|doc|docx|pdf|PDF|ppt|pptx';
		 $config['encrypt_name'] = true;
		 $this->load->library('upload');
		 $this->upload->initialize($config);
	//	 echo '<pre>'; print_r($_FILES); return;
	//	 if (!$required && $_FILES[$name]['size'] == 0) return true;
 
		 if (!is_dir($config['upload_path'])) {
			 mkdir($config['upload_path'], 0777);
		 }
 
		 if (!$this->upload->do_upload($name)) {
			 $this->template->set('error_documento', $this->upload->display_errors());
			 return false;
		 } else {
			 return $this->upload->data();
		 }
	 }

 
	public function formatos()
	{

        $this->template->add_css("css/datagrid/datatables/datatables.bundle");
    //    $this->template->add_js("js/vendors.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
        $this->template->add_js("custom/table_documentacion");

        $listado_documentos = $this->DocumentacionModel->getDocumentosPerfil($this->session->userdata(SESSION_NAME)->id_perfil, 'Formato');
	//	echo '<pre>'; print_r($listado_documentos); return;
		$this->template->set('listado_documentos', $listado_documentos);

		
		$this->template->set('item_sidebar_active', 'documentacion');
		$this->template->set('content_header', 'Documentos disponibles - Formatos');
        $this->template->set('content_sub_header', 'La siguiente documentación es el listado para la gestión de procesos de la documentacion necesaria en el registro de laboratorio.');
        $this->template->render('documentacion/index');
		
	}

	public function guias()
	{

        $this->template->add_css("css/datagrid/datatables/datatables.bundle");
    //    $this->template->add_js("js/vendors.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
        $this->template->add_js("custom/table_documentacion");

        $listado_documentos = $this->DocumentacionModel->getDocumentosPerfil($this->session->userdata(SESSION_NAME)->id_perfil, 'Guía');
	//	echo '<pre>'; print_r($listado_documentos); return;
		$this->template->set('listado_documentos', $listado_documentos);

		
		$this->template->set('item_sidebar_active', 'documentacion');
		$this->template->set('content_header', 'Documentos disponibles - Guías');
        $this->template->set('content_sub_header', 'La siguiente documentación es el listado para la gestión de procesos de la documentacion necesaria en el registro de laboratorio.');
        $this->template->render('documentacion/index');
		
	}

	public function instructivos()
	{

        $this->template->add_css("css/datagrid/datatables/datatables.bundle");
    //    $this->template->add_js("js/vendors.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
        $this->template->add_js("custom/table_documentacion");

        $listado_documentos = $this->DocumentacionModel->getDocumentosPerfil($this->session->userdata(SESSION_NAME)->id_perfil, 'Instructivo');
	//	echo '<pre>'; print_r($listado_documentos); return;
		$this->template->set('listado_documentos', $listado_documentos);

		
		$this->template->set('item_sidebar_active', 'documentacion');
		$this->template->set('content_header', 'Documentos disponibles - Instructivos');
        $this->template->set('content_sub_header', 'La siguiente documentación es el listado para la gestión de procesos de la documentacion necesaria en el registro de laboratorio.');
        $this->template->render('documentacion/index');
		
	}

	public function procedimientos()
	{

        $this->template->add_css("css/datagrid/datatables/datatables.bundle");
    //    $this->template->add_js("js/vendors.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
        $this->template->add_js("custom/table_documentacion");

        $listado_documentos = $this->DocumentacionModel->getDocumentosPerfil($this->session->userdata(SESSION_NAME)->id_perfil, 'Procedimiento');
	//	echo '<pre>'; print_r($listado_documentos); return;
		$this->template->set('listado_documentos', $listado_documentos);

		
		$this->template->set('item_sidebar_active', 'documentacion');
		$this->template->set('content_header', 'Documentos disponibles - Procedimientos');
        $this->template->set('content_sub_header', 'La siguiente documentación es el listado para la gestión de procesos de la documentacion necesaria en el registro de laboratorio.');
        $this->template->render('documentacion/index');
		
	}


    /** GESTION DE DOCUMENTACION */
    public function clasificacion_doc()
	{
		$this->template->set('item_sidebar_active', 'documentacion');
		$this->template->set('content_header', 'Documentos disponibles');
        $this->template->set('content_sub_header', 'Módulo de gestión de documentación de laboratorio.');
        $this->template->render('documentacion/panel_tipo_doc');
	}

    public function panel_documentacion()
	{
		$num_pendientes = $this->DocumentacionModel->getTotal($this->session->userdata(SESSION_NAME)->id, 1);
		$this->template->set('num_pendientes', $num_pendientes);
		$num_rechazados = $this->DocumentacionModel->getTotal($this->session->userdata(SESSION_NAME)->id, 3);
		$this->template->set('num_rechazados', $num_rechazados);
		$num_procesados = $this->DocumentacionModel->getTotal($this->session->userdata(SESSION_NAME)->id, 2);
		$this->template->set('num_procesados', $num_procesados);
		$num_historico_rec = $this->DocumentacionModel->getTotalHistorico($this->session->userdata(SESSION_NAME)->id, 3);
		$this->template->set('num_historico_rec', $num_historico_rec);
	
		$this->template->set('item_sidebar_active', 'panel_doc');
		$this->template->set('content_header', 'Gestión de documentación');
        $this->template->set('content_sub_header', 'Módulo de gestión de documentación de laboratorio.');
        $this->template->render('documentacion/panel_documentacion');
	}

    public function pendientes()
	{

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");
		$this->template->add_js("custom/table_pendientes");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
	
        $listado_documentos = $this->DocumentacionModel->getDocumentosPendientes(9);
	//	echo '<pre>'; print_r($listado_documentos); return;
		$this->template->set('listado_documentos', $listado_documentos);

		$this->template->set('item_sidebar_active', 'panel_doc');
		$this->template->set('content_header', 'Gestión de documentación - Documentos pendientes');
        $this->template->set('content_sub_header', 'Módulo de gestión de documentación de laboratorio.');
        $this->template->render('documentacion/pendientes');
		
	}

	public function editar_documentacion($id = null)
	{
		if($id){
			$registro = $this->DocumentacionModel->getUserArchivo($id);
			$this->template->set('registro', $registro);
	
	//		echo '<pre>'; print_r($registro); return;
		}
		//	$this->template->add_js('custom/validaction');
		//	$this->template->set('login', false);

		if($this->input->post('editar') == 1){
				$info = array(
                    'estado' => $this->input->post('estado'),
                    'observaciones' => $this->input->post('observaciones')
                );

				
			//	echo '<pre>'; 
		//		print_r($info2); return;
				$ok = $this->DocumentacionModel->update_documento($this->input->post('id_registro'), $info);
				if ($ok) {
					if($this->input->post('estado') == '3'){
						$registro = $this->DocumentacionModel->getUserArchivo($this->input->post('id_registro'));
						//	echo '<pre>'; print_r($registro); return;
							$info = array(
								'id_user' => $registro->id_user,
								'id_perfil_archivo' => $registro->id_perfil_archivo,
								'estado' => $registro->estado,
								'fecha_carga' => $registro->fecha_carga,
								'fecha_documento' => $registro->fecha_documento,
								'fecha_revision' => $registro->fecha_revision,
								'observaciones' => $registro->observaciones,
								'url_archivo' => $registro->url_archivo,
							);
						//	echo '<pre>'; print_r($info); return;
							$ok = $this->DocumentacionModel->insert_documento_historial($info);
						}
					$this->template->set_flash_message(array('success' => 'El registro se editó correctamente.'));
				}else{
					$this->template->set_flash_message(array('error' => 'El registro no se editó correctamente, por favor inténtelo de nuevo.'));
				}		
				redirect('documentacion/pendientes');

		}else{
			$_POST = array_merge($_POST, (array)$registro);
		}
			
		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "documentacion/pendientes" . '">Pendientes</a></li>';
		$this->template->set('migasdepan', $migas);
		$this->template->set('item_sidebar_active', 'panel_doc');
		$this->template->set('content_header', 'Editar Registro de Documento');
        $this->template->set('content_sub_header', 'El siguiente formulario permite la edición del estado de registro del documento.');
        $this->template->render('documentacion/editar_documentacion');
	}

	public function rechazadas()
	{

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");
		$this->template->add_js("custom/table_rechazadas");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
	
        $listado_documentos = $this->DocumentacionModel->getDocumentosRechazados(9);
	//	echo '<pre>'; print_r($listado_documentos); return;
		$this->template->set('listado_documentos', $listado_documentos);

		$this->template->set('item_sidebar_active', 'panel_doc');
		$this->template->set('content_header', 'Gestión de documentación - Documentos rechazados');
        $this->template->set('content_sub_header', 'Módulo de gestión de documentación de laboratorio.');
        $this->template->render('documentacion/rechazadas');
		
	}

	public function editar_documentacion_rec($id = null)
	{
		if($id){
			$registro = $this->DocumentacionModel->getUserArchivo($id);
			$this->template->set('registro', $registro);
	
		//	echo '<pre>'; print_r($registro); return;
		}
		//	$this->template->add_js('custom/validaction');
		//	$this->template->set('login', false);

		if($this->input->post('editar') == 1){
				$info = array(
                    'estado' => $this->input->post('estado'),
                );
				
			//	echo '<pre>'; 
		//		print_r($info2); return;
				$ok = $this->DocumentacionModel->update_documento($this->input->post('id_registro'), $info);
				if ($ok) {
					$this->template->set_flash_message(array('success' => 'El registro se editó correctamente.'));
				}else{
					$this->template->set_flash_message(array('error' => 'El registro no se editó correctamente, por favor inténtelo de nuevo.'));
				}		
				redirect('documentacion/rechazadas');

		}else{
			$_POST = array_merge($_POST, (array)$registro);
		}
			
		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "documentacion/rechazados" . '">Rechazadas</a></li>';
		$this->template->set('migasdepan', $migas);
		$this->template->set('item_sidebar_active', 'panel_doc');
		$this->template->set('content_header', 'Editar Registro de Documento');
        $this->template->set('content_sub_header', 'El siguiente formulario permite la edición del estado de registro del documento.');
        $this->template->render('documentacion/editar_documentacion_rec');
	}

	public function procesadas()
	{

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");
		$this->template->add_js("custom/table_procesadas");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
	
		$listado_documentos = $this->DocumentacionModel->getDocumentosProcesados(9);
	//	echo '<pre>'; print_r($listado_documentos); return;
		$this->template->set('listado_documentos', $listado_documentos);


		$this->template->set('item_sidebar_active', 'panel_doc');
		$this->template->set('content_header', 'Gestión de documentación - Documentos procesados');
        $this->template->set('content_sub_header', 'Módulo de gestión de documentación de laboratorio.');
        $this->template->render('documentacion/procesadas');
		
	}

	public function editar_documentacion_pro($id = null)
	{
		if($id){
			$registro = $this->DocumentacionModel->getUserArchivo($id);
			$this->template->set('registro', $registro);
	
		//	echo '<pre>'; print_r($registro); return;
		}
		//	$this->template->add_js('custom/validaction');
		//	$this->template->set('login', false);

		if($this->input->post('editar') == 1){
				$info = array(
                    'estado' => $this->input->post('estado'),
                );
				
			//	echo '<pre>'; 
		//		print_r($info2); return;
				$ok = $this->DocumentacionModel->update_documento($this->input->post('id_registro'), $info);
				if ($ok) {
					$this->template->set_flash_message(array('success' => 'El registro se editó correctamente.'));
				}else{
					$this->template->set_flash_message(array('error' => 'El registro no se editó correctamente, por favor inténtelo de nuevo.'));
				}		
				redirect('documentacion/procesadas');

		}else{
			$_POST = array_merge($_POST, (array)$registro);
		}
			
		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "documentacion/rechazados" . '">Procesadas</a></li>';
		$this->template->set('migasdepan', $migas);
		$this->template->set('item_sidebar_active', 'panel_doc');
		$this->template->set('content_header', 'Editar Registro de Documento');
        $this->template->set('content_sub_header', 'El siguiente formulario permite la edición del estado de registro del documento.');
        $this->template->render('documentacion/editar_documentacion_pro');
	}

	public function historial()
	{

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");
		$this->template->add_js("custom/table_historial");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
	
        $listado_documentos = $this->DocumentacionModel->getDocumentosHistoricoRec(9);
	//	echo '<pre>'; print_r($listado_documentos); return;
		$this->template->set('listado_documentos', $listado_documentos);

		$this->template->set('item_sidebar_active', 'panel_doc');
		$this->template->set('content_header', 'Gestión de documentación - Historial rechazados');
        $this->template->set('content_sub_header', 'Módulo de gestión de documentación de laboratorio.');
        $this->template->render('documentacion/historial');
		
	}

	public function nuevo_documento_historico($id = null)
	{
		if($id){
			$registro = $this->DocumentacionModel->getUserArchivo($id);
			$this->template->set('registro', $registro);
	
		//	echo '<pre>'; print_r($registro); return;
		}
		//	$this->template->add_js('custom/validaction');
		//	$this->template->set('login', false);

		if($this->input->post('nuevo') == 1){
				$registro = $this->DocumentacionModel->getUserArchivo($this->input->post('id_registro'));
			//	echo '<pre>'; print_r($registro); return;
				$info = array(
                    'id_user' => $registro->id_user,
                    'id_perfil_archivo' => $registro->id_perfil_archivo,
                    'estado' => $registro->estado,
                    'fecha_carga' => $registro->fecha_carga,
                    'fecha_documento' => $registro->fecha_documento,
					'fecha_revision' => $registro->fecha_revision,
					'observaciones' => $registro->observaciones,
                    'url_archivo' => $registro->url_archivo,
                );
			//	echo '<pre>'; print_r($info); return;
                $ok = $this->DocumentacionModel->insert_documento_historial($info);
				if ($ok) {
					$ok = $this->DocumentacionModel->delete_documento($this->input->post('id_registro'));
                    $this->template->set_flash_message(array('success' => 'Se ha registrado exitosamente el documento en el historial'));
                } else {
                    $this->template->set_flash_message(array('error' => 'Ocurrio un error al registrar el documento en el historial'));
                }	
				redirect('documentacion/rechazadas');

		}else{
			$_POST = array_merge($_POST, (array)$registro);
		}
			
		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "documentacion/rechazados" . '">Historial</a></li>';
		$this->template->set('migasdepan', $migas);
		$this->template->set('item_sidebar_active', 'panel_doc');
		$this->template->set('content_header', 'Editar Registro de Documento');
        $this->template->set('content_sub_header', 'El siguiente formulario permite la edición del estado de registro del documento.');
        $this->template->render('documentacion/nuevo_documento_historico');
	}

    public function panel_documentacion_user()
	{

		$num_rechazados = $this->DocumentacionModel->getTotalUser($this->session->userdata(SESSION_NAME)->id, 3);
		$this->template->set('num_rechazados', $num_rechazados);
		$num_pendientes = $this->DocumentacionModel->getTotalUser($this->session->userdata(SESSION_NAME)->id, 1);
		$this->template->set('num_pendientes', $num_pendientes);
		$num_procesados = $this->DocumentacionModel->getTotalUser($this->session->userdata(SESSION_NAME)->id, 2);
		$this->template->set('num_procesados', $num_procesados);
	
		$this->template->set('item_sidebar_active', 'panel_doc_user');
		$this->template->set('content_header', 'Proceso de Carga');
        $this->template->set('content_sub_header', 'Módulo de gestión de documentación de laboratorio.');
        $this->template->render('documentacion/panel_documentacion_user');
	}

    public function nuevo_documento()
	{
		$this->template->add_js('custom/documentacion');

		if($this->input->post('nuevo')){
            $this->load->library('form_validation');
            $rules = [
                [
                    'field' => 'id_archivo',
                    'label' => 'Documento',
                    'rules' => 'required|numeric|max_length[3]'
                ],
                [
                    'field' => 'fecha_documento',
                    'label' => 'Fecha de documento',
                    'rules' => 'required'
                ]
            ];

            $this->form_validation->set_rules($rules);

			$archivo = $this->_validar_documento('documento', false, "documento_archivo_lab");

			if ($this->form_validation->run() === TRUE && $archivo) {
				$info = array(
                    'id_user' => $this->session->userdata(SESSION_NAME)->id,
                    'id_perfil_archivo' => $this->input->post('id_perfil_archivo'),
                    'fecha_documento' => $this->input->post('fecha_documento'),
                    'url_archivo' => $archivo['file_name'],
                );
			//	echo '<pre>'; print_r($info); return;
                $ok = $this->DocumentacionModel->insert_documento($info);
				if ($ok) {
                    $this->template->set_flash_message(array('success' => 'Se ha registrado exitosamente el nuevo documento'));
                } else {
                    $this->template->set_flash_message(array('error' => 'Ocurrio un error al registrar el nuevo documento'));
                }
				redirect('documentacion/panel_documentacion_user');
			}
		}
		
        $listado_documentos = $this->DocumentacionModel->getDocumentosPerfilAll($this->session->userdata(SESSION_NAME)->id_perfil);
		$this->template->set('listado_documentos', $this->_tipo_documento($listado_documentos));

		$this->load->helper('form');
		$this->template->set('item_sidebar_active', 'panel_doc_user');
		$this->template->set('content_header', 'Gestión de documentación usuario');
        $this->template->set('content_sub_header', 'Módulo de gestión de documentación de laboratorio.');
        $this->template->render('documentacion/nuevo_documento');
		
	}

	public function form_resumen_registro()
	{
		$this->template->add_js('custom/documentacion');

		if($this->input->post('nuevo')){
            $this->load->library('form_validation');
            $rules = [
                [
                    'field' => 'id_archivo',
                    'label' => 'Documento',
                    'rules' => 'required|numeric|max_length[3]'
                ],
                [
                    'field' => 'fecha_documento',
                    'label' => 'Fecha de documento',
                    'rules' => 'required'
                ]
            ];

            $this->form_validation->set_rules($rules);

			$archivo = $this->_validar_documento('documento', false, "documento_archivo_lab");

			if ($this->form_validation->run() === TRUE && $archivo) {
				$info = array(
                    'id_user' => $this->session->userdata(SESSION_NAME)->id,
                    'id_perfil_archivo' => $this->input->post('id_perfil_archivo'),
                    'fecha_documento' => $this->input->post('fecha_documento'),
                    'url_archivo' => $archivo['file_name'],
                );
			//	echo '<pre>'; print_r($info); return;
                $ok = $this->DocumentacionModel->insert_documento($info);
				if ($ok) {
                    $this->template->set_flash_message(array('success' => 'Se ha registrado exitosamente el nuevo documento'));
                } else {
                    $this->template->set_flash_message(array('error' => 'Ocurrio un error al registrar el nuevo documento'));
                }
				redirect('documentacion/panel_documentacion_user');
			}
		}
		
        $listado_documentos = $this->DocumentacionModel->getDocumentosPerfilAll($this->session->userdata(SESSION_NAME)->id_perfil);
		$this->template->set('listado_documentos', $this->_tipo_documento($listado_documentos));

		$this->load->helper('form');
		$this->template->set('item_sidebar_active', 'panel_doc_user');
		$this->template->set('content_header', 'Gestión de documentación usuario');
        $this->template->set('content_sub_header', 'Módulo de gestión de documentación de laboratorio.');
        $this->template->render('documentacion/resumen_cambios_registros');
		
	}

	public function jsonArchivo() {

		$this->load->model("DocumentacionModel");	
		$archivo = $this->DocumentacionModel->getArchivoId($this->input->post('id_archivo'));
		if ($archivo) {
			echo json_encode(array('carga' => true, 'data' => $archivo));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

    public function rechazadas_user()
	{

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");
		$this->template->add_js("custom/table_rechazadas_user");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
	
        $listado_documentos = $this->DocumentacionModel->getDocumentosRechazadosUser($this->session->userdata(SESSION_NAME)->id);
	//	echo '<pre>'; print_r($listado_documentos); return;
		$this->template->set('listado_documentos', $listado_documentos);

		$this->template->set('item_sidebar_active', 'panel_doc_user');
		$this->template->set('content_header', 'Gestión de documentación usuario');
        $this->template->set('content_sub_header', 'Módulo de gestión de documentación de laboratorio.');
        $this->template->render('documentacion/rechazadas_documento');
		
	}

	public function editar_documentacion_user($id = null)
	{
		if($id){
			$registro = $this->DocumentacionModel->getUserArchivo($id);
			$this->template->set('registro', $registro);
	
		//	echo '<pre>'; print_r($registro); return;
		}
		//	$this->template->add_js('custom/validaction');
		//	$this->template->set('login', false);

		if($this->input->post('editar') == 1){
			
			$archivo = $this->_validar_documento('documento', false, "documento_archivo_lab");
			
			if ($archivo) {
				$info = array(
                    'url_archivo' => $archivo['file_name'],
					'estado' => 1
                );
			//	echo '<pre>'; print_r($info); return;
				$ok = $this->DocumentacionModel->update_documento($this->input->post('id_registro'), $info);
				if ($ok) {
                	$this->template->set_flash_message(array('success' => 'El registro se editó correctamente.'));
				}else{
					$this->template->set_flash_message(array('error' => 'El registro no se editó correctamente, por favor inténtelo de nuevo.'));
				}		
				redirect('documentacion/rechazadas_user');
			}
			echo '<pre>'; print_r("no entro"); return;
		}else{
			$_POST = array_merge($_POST, (array)$registro);
		}
			
		$this->load->helper('form');
		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "documentacion/rechazadas_user" . '">Rechazadas</a></li>';
		$this->template->set('migasdepan', $migas);
		$this->template->set('item_sidebar_active', 'panel_doc_user');
		$this->template->set('content_header', 'Editar Registro de Documento');
        $this->template->set('content_sub_header', 'El siguiente formulario permite la edición del estado de registro del documento.');
        $this->template->render('documentacion/editar_documentacion_user');
	}

	public function pendientes_user()
	{

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");
		$this->template->add_js("custom/table_pendientes_user");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
	
        $listado_documentos = $this->DocumentacionModel->getDocumentosPendientesUser($this->session->userdata(SESSION_NAME)->id);
	//	echo '<pre>'; print_r($listado_documentos); return;
		$this->template->set('listado_documentos', $listado_documentos);

		$this->template->set('item_sidebar_active', 'panel_doc_user');
		$this->template->set('content_header', 'Gestión de documentación - Documentos pendientes');
        $this->template->set('content_sub_header', 'Módulo de gestión de documentación de laboratorio.');
        $this->template->render('documentacion/pendientes_user');
		
	}

	public function procesada_user()
	{

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");
		$this->template->add_js("custom/table_procesadas_user");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
	
        $listado_documentos = $this->DocumentacionModel->getDocumentosProcesadosUser($this->session->userdata(SESSION_NAME)->id);
	//	echo '<pre>'; print_r($listado_documentos); return;
		$this->template->set('listado_documentos', $listado_documentos);

		$this->template->set('item_sidebar_active', 'panel_doc_user');
		$this->template->set('content_header', 'Gestión de documentación usuario');
        $this->template->set('content_sub_header', 'Módulo de gestión de documentación de laboratorio.');
        $this->template->render('documentacion/procesadas_documento');
		
	}

	private function _tipo_documento($listado = null)
    {

        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione el tipo de documento";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id_archivo] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

    /** */

    private function  corrige_fecha($fecha)
    {
        $tiene = substr_count($fecha, '/');
        $tiene2 = substr_count($fecha, '-');
        $tip = ($tiene == 2) ? '/' : (($tiene2 == 2) ? '-' : '');
        $fechafinal = '';
        if ($tip) {
            $partesfecha = explode($tip, $fecha);
            $uno = strlen($partesfecha[0]);
            $dos = strlen($partesfecha[1]);
            $tres = strlen($partesfecha[2]);

            if (is_int($uno) && is_int($dos) && is_int($tres)) {
                if ($uno == '4') {
                    if ($partesfecha[1] > 12) {
                        $fechafinal = $partesfecha[0] . '-' . $partesfecha[2] . '-' . $partesfecha[1];
                    } else {
                        $fechafinal = $partesfecha[0] . '-' . $partesfecha[1] . '-' . $partesfecha[2];
                    }
                }
                if ($tres == '4') {
                    if ($partesfecha[1] > 12) {
                        $fechafinal = $partesfecha[2] . '-' . $partesfecha[0] . '-' . $partesfecha[1];
                    } else {
                        $fechafinal = $partesfecha[2] . '-' . $partesfecha[1] . '-' . $partesfecha[0];
                    }

                }
                return $fechafinal;
            }
        }
    }

}
