<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Content-Type: text/html; charset=UTF-8');

class Paginas extends CMS_Controller {

	 function __construct()
	 {
		 parent::__construct();

	//	 $this->template->add_css('custom/style');
		
		$this->load->model("ConsultasModel");
		$this->load->model("PerfilModel");
		$this->template->set_template('paginas');

	 }

 
	public function index()
	{
		$this->template->set('item_sidebar_active', 'consultas');
		$this->template->set('content_header', 'Consultas');
        $this->template->set('content_sub_header', 'Servicios de Consultas para el acceso a información sobre manejo terapéutico, control y seguimiento de enfermedades ocasionadas por agentes biológicos de alto riesgo para la salud humana.');
        $this->template->render('paginas/nosotros');
	}

    public function normatividad()
	{
		$this->template->set('item_sidebar_active', 'consultas');
		$this->template->set('content_header', 'Consultas');
        $this->template->set('content_sub_header', 'Servicios de Consultas para el acceso a información sobre manejo terapéutico, control y seguimiento de enfermedades ocasionadas por agentes biológicos de alto riesgo para la salud humana.');
        $this->template->render('paginas/normatividad');
	}

    public function proyectos()
	{
		$this->template->set('item_sidebar_active', 'consultas');
		$this->template->set('content_header', 'Consultas');
        $this->template->set('content_sub_header', 'Servicios de Consultas para el acceso a información sobre manejo terapéutico, control y seguimiento de enfermedades ocasionadas por agentes biológicos de alto riesgo para la salud humana.');
        $this->template->render('paginas/proyectos');
	}

    public function servicios()
	{
		$this->template->set('item_sidebar_active', 'consultas');
		$this->template->set('content_header', 'Consultas');
        $this->template->set('content_sub_header', 'Servicios de Consultas para el acceso a información sobre manejo terapéutico, control y seguimiento de enfermedades ocasionadas por agentes biológicos de alto riesgo para la salud humana.');
        $this->template->render('paginas/servicios');
	}

    public function contactos()
	{
		$this->template->set('item_sidebar_active', 'consultas');
		$this->template->set('content_header', 'Consultas');
        $this->template->set('content_sub_header', 'Servicios de Consultas para el acceso a información sobre manejo terapéutico, control y seguimiento de enfermedades ocasionadas por agentes biológicos de alto riesgo para la salud humana.');
        $this->template->render('paginas/contactos');
	}

}