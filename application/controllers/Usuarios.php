<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuarios extends CMS_Controller {

	 function __construct()
	 {
		 parent::__construct();

	//	 $this->template->add_css('custom/style');

		 $user = array();
		 if ($this->session->userdata(SESSION_NAME)) {
			$user = $this->session->userdata(SESSION_NAME);
		}else{
			redirect();
		}
	
		$this->load->model("UsuariosModel");
		$this->template->set_template('dashboard');

	 }

 
	public function index()
	{
	
		$this->template->add_css("css/datagrid/datatables/datatables.bundle");
		$this->template->add_js("custom/table_usuario");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
	

		$listado_usuarios = $this->UsuariosModel->getUsuarios();
		$this->template->set('listado_usuarios', $listado_usuarios);

	//	echo '<pre>'; print_r($listado_usuarios); return;

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "usuarios/index" . '">Administracion de Usuarios</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'usuarios');
		$this->template->set('content_header', 'Administración de Usuarios');
        $this->template->set('content_sub_header', 'El siguiente listado son los usuarios que están actualmente registrados en la plataforma donde permite la administración de cada una de las cuentas.');
        $this->template->render('usuarios/todos');
		
	}

	public function editar_usuario($id = null)
	{
		if($id){
			$usuario = $this->UsuariosModel->getUsuario($id);
			$this->template->set('usuario', $usuario);
	
		//	echo '<pre>'; print_r($usuario); return;
		}

		if($this->input->post('editar') == 1){
				$info = array(
                    'id_tipo_documento' => $this->input->post('id_tipo_documento'),
                    'documento' => $this->input->post('documento'),
                    'nombres' => $this->input->post('nombres'),
                    'apellidos' => $this->input->post('apellidos'),
                    'edad' => $this->input->post('edad'),
                    'genero' => $this->input->post('genero'),
                    'celular' => $this->input->post('celular'),
                    'direccion' => $this->input->post('direccion'),
                    'correo' => $this->input->post('correo'),
                    'estado' => $this->input->post('estado'),
                    'id_perfil' => $this->input->post('id_perfil')
                );
				
				$ok = $this->UsuariosModel->update_usuario($this->input->post('id_usuario'), $info);
				if ($ok) {
					$this->template->set_flash_message(array('success' => 'El usuario se editó correctamente.'));
				}else{
					$this->template->set_flash_message(array('error' => 'El usuario no se editó correctamente, por favor inténtelo de nuevo.'));
				}
				redirect('usuarios');

		}else{
			$_POST = array_merge($_POST, (array)$usuario);
		//	echo '<pre>'; print_r($_POST); return;
		}
			
		$listado_documentos = $this->_documentos($this->UsuariosModel->getDocumentos());
		$this->template->set('listado_documentos',$listado_documentos);
		$listado_perfil = $this->_perfil($this->UsuariosModel->getPerfil());
		$this->template->set('listado_perfil',$listado_perfil);
		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "usuarios/editar_usuario" . '">Editar Usuario</a></li>';
		$this->template->set('migasdepan', $migas);
		$this->template->set('item_sidebar_active', 'usuarios');
		$this->template->set('content_header', 'Editar Usuario');
        $this->template->set('content_sub_header', 'El siguiente formulario permite la edición de un usuario.');
        $this->template->render('usuarios/editar_usuario');
	}

	public function borrar_usuario($id = null)
    {
       
		$usuario = $this->UsuariosModel->getUsuario($id);

        if (!$usuario) {
            $this->template->set_flash_message(array('warning' => 'Ocurrió un error al eliminar el usuario seleccionado'));
            redirect('usuarios');
        }

      //  echo '<pre>'; print_r($registro->id_rep); return;
        $ok = $this->UsuariosModel->delete_usuario($usuario->id);

        if ($ok) {
            $this->template->set_flash_message(array('success' => 'Se ha eliminado exitosamente el usuario'));
        } else {
            $this->template->set_flash_message(array('error' => 'Ocurrio un error al eliminar el usuario'));
        }

        redirect('usuarios');
    }

	private function _documentos($listado = null)
    {

        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione el tipo de documentos";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

	private function _perfil($listado = null)
    {

        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione el perfíl";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}



	/*

	public function enviar_pregunta(){


		if($this->input->post('pregunta') == 1){
		
			$this->load->library('form_validation');
            $rules = [
				[
                    'field' => 'id_orientacion',
                    'label' => 'Tipo de orientación',
                    'rules' => 'trim|required|max_length[2]'
				],
				[
                    'field' => 'otro_orientada',
                    'label' => 'Otro orientada',
                    'rules' => 'max_length[100]'
				],
				[
                    'field' => 'otro_tema',
                    'label' => 'Otro tema',
                    'rules' => 'max_length[100]'
				],
				[
                    'field' => 'id_sustancia',
                    'label' => 'Tipo de sustancia',
                    'rules' => 'trim|required|max_length[2]'
				],
				[
                    'field' => 'nombre_producto',
                    'label' => 'Nombre del Producto',
                    'rules' => 'required|max_length[100]'
				],	
				[
                    'field' => 'descripcion',
                    'label' => 'Descripción',
                    'rules' => 'required|max_length[4000]'
				]
			];	

            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() === TRUE) {
			
				$id_pregunta = $this->ConsultasModel->setPregunta($this->input->post('id_orientacion'), $this->input->post('otro_orientada'), $this->input->post('id_sustancia')
				, $this->input->post('nombre_producto'), $this->input->post('descripcion'), $this->session->userdata(SESSION_NAME)->id);

				if ($id_pregunta > 0) {
					$this->template->set_flash_message(array('success' => 'La consulta se envió correctamente, por favor debe esperar a la respuesta.'));

			
					$listado_temas = $this->ConsultasModel->getTemas();
					for($i = 0; $i < count($listado_temas); $i++){
						if($this->input->post('temas_'.$i) == $listado_temas[$i]->id){
							if($listado_temas[$i]->id == 7){
								$aux = $this->ConsultasModel->setTemas($id_pregunta, $listado_temas[$i]->id, $this->input->post('otro_tema'));
							}else{
								$aux = $this->ConsultasModel->setTemas($id_pregunta, $listado_temas[$i]->id);
							}
							
						}
					}

					redirect('consultas');
                } else {
					$this->template->set_flash_message(array('error' => 'La consulta no se registró correctamente, por favor inténtelo de nuevo.'));
					redirect('consultas');
                }
            }
		}

		redirect('consultas');
	}

	public function respuesta($id)
	{


		$respuesta = $this->ConsultasModel->getRespuesta($id);
		$this->template->set('respuesta', $respuesta);

		$migas = '<li class="breadcrumb-item"><a href="' . base_url() . "consultas" . '">Consultas</a></li><li class="breadcrumb-item active">Respuesta Consulta</li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'consultas');
		$this->template->set('content_header', 'Consultas');
        $this->template->set('content_sub_header', 'Servicios de Consultas para el acceso a información sobre manejo terapéutico, control y seguimiento de enfermedades ocasionadas por agentes biológicos de alto riesgo para la salud humana.');
        $this->template->render('consultas/respuesta');

	}

	public function admin()
	{
		




		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "consultas" . '">Consultas</a></li>';
		$this->template->set('migasdepan', $migas);

		$this->template->set('listado_consultas', $this->ConsultasModel->getPendientesAdmin());
		
		$this->template->set('item_sidebar_active', 'consultas');
		$this->template->set('content_header', 'Consultas');
        $this->template->set('content_sub_header', 'Servicios de Consultas para el acceso a información sobre manejo terapéutico, control y seguimiento de enfermedades ocasionadas por agentes biológicos de alto riesgo para la salud humana.');
		$this->template->render('consultas/admin');
	//	$this->template->render('usuarios/todos');
	
	}

	public function contestar($id)
	{


		$pregunta = $this->ConsultasModel->getPregunta($id);
		$this->template->set('pregunta', $pregunta);

		$temas_pregunta = $this->ConsultasModel->getTemasPregunta($id);
		$this->template->set('temas_pregunta', $temas_pregunta);

		$sustancia_pregunta = $this->ConsultasModel->getSustanciaPregunta($pregunta->id_sustancia);
		$this->template->set('sustancia_pregunta', $sustancia_pregunta);

		$orientada_pregunta = $this->ConsultasModel->getOrientadaPregunta($pregunta->id_orientacion);
		$this->template->set('orientada_pregunta', $orientada_pregunta);

		$profesional = $this->PerfilModel->getPerfilProfesional($pregunta->id_user);
		$particular = $this->PerfilModel->getPerfilParticular($pregunta->id_user);
		if(isset($profesional)){
			$this->template->set('profesional', $profesional);
		}else{
			$this->template->set('particular', $particular);
		}

		$migas = '<li class="breadcrumb-item"><a href="' . base_url() . "consultas" . '">Consultas</a></li><li class="breadcrumb-item active">Respuesta Consulta</li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'consultas');
		$this->template->set('content_header', 'Consultas');
        $this->template->set('content_sub_header', 'Servicios de Consultas para el acceso a información sobre manejo terapéutico, control y seguimiento de enfermedades ocasionadas por agentes biológicos de alto riesgo para la salud humana.');
        $this->template->render('consultas/contestar');

	}

	public function enviar_respuesta(){

		if($this->input->post('responder') == 1){
		
			$this->load->library('form_validation');
            $rules = [
				[
                    'field' => 'descripcion',
                    'label' => 'Descripción',
                    'rules' => 'required|max_length[4000]'
				]
			];	

            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() === TRUE) {
			
				$id_respuesta = $this->ConsultasModel->setRespuesta($this->input->post('descripcion'), $this->input->post('id_pregunta'), $this->session->userdata(SESSION_NAME)->id);

				if ($id_respuesta > 0) {
					$id_respuesta = $this->ConsultasModel->setEstadoPregunta($this->input->post('id_pregunta'),'1');
					$this->template->set_flash_message(array('success' => 'La respuesta se enviado correctamente.'));
					redirect('consultas/admin');
                } else {
					$this->template->set_flash_message(array('error' => 'La respuesta no se registró correctamente, por favor inténtelo de nuevo.'));
					redirect('consultas/admin');
                }
            }
		}

		redirect('consultas/admin');
	}
*/



}
