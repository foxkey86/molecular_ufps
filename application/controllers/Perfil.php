<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends CMS_Controller {

	 function __construct()
	 {
		 parent::__construct();

	//	 $this->template->add_css('custom/style');

		 $user = array();
		 if ($this->session->userdata(SESSION_NAME)) {
			$user = $this->session->userdata(SESSION_NAME);
		}else{
			redirect();
		}
		
		$this->load->model("PerfilModel");
		$this->template->set_template('dashboard');

	 }

 
	public function index()
	{

	/*	if ($this->session->userdata(SESSION_NAME)) {
            echo '<pre>'; print_r($this->session->userdata(SESSION_NAME)); return;
        }
*/
		if ($this->session->userdata(SESSION_NAME)) {
			$user = $this->session->userdata(SESSION_NAME);
		}else{
			redirect();
		}

	/*	$this->template->add_message([
			'error' => 'Usuario o Contraseña invalidos'
		]);  */	
		

		$profesional = $this->PerfilModel->getPerfilProfesional($user->id);
		$particular = $this->PerfilModel->getPerfilParticular($user->id);

		if(isset($profesional)){
			$this->template->set('profesional', $profesional);
		}else{
			$this->template->set('particular', $particular);
		}
	//	echo '<pre>'; print_r($particular); return;

		$data = array("ejemplo1", "ejemplo2", "ejemplo3");
		$numeros = "uno dos tres cuatro cinco";

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Perfíl</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'perfil_usuario');
		$this->template->set('content_header', 'Perfíl');
        $this->template->set('content_sub_header', 'Consulte su perfíl de usuario');
        $this->template->render('perfil/index');

	  //  echo '<pre>'; print_r("hola2"); return;
	/*    $this->load->view('template/header');
		$this->load->view('login/index', compact("data","numeros"));
		$this->load->view('template/footer'); */
//		$this->layout->setLayout("login.php");
	//	$this->layout->view("index");
		
//		$this->layout->view("index", compact("personas"));
		
	}

	public function loguearUsuario(){


		if($this->input->post('logueo') == 1){
			$this->load->library('form_validation');
            $rules = [
                [
                    'field' => 'username',
                    'label' => 'Correo Electrónico',
                    'rules' => 'trim|required|max_length[50]'
                ],
                [
                    'field' => 'password',
                    'label' => 'Contraseña',
                    'rules' => 'trim|required|max_length[20]'
                ]
            ];
            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() === TRUE) {
				$persona = $this->LoginModel->getUsuario($this->input->post('username'), $this->input->post('password'));
                if ($persona) {
                    $this->session->set_userdata(SESSION_NAME, $persona);
					
					//	header("Location:".base_url());
					redirect();
				//	$this->layout->view("index");
                } else {
                 /*   $this->template->add_message([
                        'error' => 'Usuario o Contraseña invalidos'
					]); */
					$this->template->set_flash_message(array('error' => 'El usuario no existe en nuestra base de datos, por favor crea una cuenta.'));
					redirect();
                }
            }
		
		}


	}
	
    
    public function hola()
	{
		$this->layout->view("index");
	}
}
