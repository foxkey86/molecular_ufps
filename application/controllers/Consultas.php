<?php
defined('BASEPATH') OR exit('No direct script access allowed');
header('Content-Type: text/html; charset=UTF-8');

class Consultas extends CMS_Controller {

	 function __construct()
	 {
		 parent::__construct();

	//	 $this->template->add_css('custom/style');

		 $user = array();
		 if ($this->session->userdata(SESSION_NAME)) {
			$user = $this->session->userdata(SESSION_NAME);
		}else{
			redirect();
		}
		
		$this->load->model("ConsultasModel");
		$this->load->model("PerfilModel");
		$this->template->set_template('dashboard');

	 }

 
	public function index()
	{

        $this->template->add_css("css/datagrid/datatables/datatables.bundle");
		$this->template->add_js("custom/table_consultas");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
		

		$listado_orientada = $this->_orientadas($this->ConsultasModel->getOrientada());
		$this->template->set('listado_orientada', $listado_orientada);

		$listado_temas = $this->ConsultasModel->getTemas();
		$this->template->set('listado_temas', $listado_temas);

		$listado_sustancia = $this->_sustancias($this->ConsultasModel->getSustancia());
		$this->template->set('listado_sustancia', $listado_sustancia);

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "consultas" . '">Consultas</a></li>';
		$this->template->set('migasdepan', $migas);

		$this->template->set('pendientes', $this->ConsultasModel->getPendientes($this->session->userdata(SESSION_NAME)->id));

		$listado_consultas = $this->ConsultasModel->getPreguntas($this->session->userdata(SESSION_NAME)->id);
		$this->template->set('listado_consultas', $listado_consultas);
	//	echo '<pre>'; print_r($listado_consultas); return;
		$this->template->set('item_sidebar_active', 'consultas');
		$this->template->set('content_header', 'Consultas');
        $this->template->set('content_sub_header', 'Servicios de Consultas para el acceso a información sobre manejo terapéutico, control y seguimiento de enfermedades ocasionadas por agentes biológicos de alto riesgo para la salud humana.');
        $this->template->render('consultas/index');

	  // 
	/*    $this->load->view('template/header');
		$this->load->view('login/index', compact("data","numeros"));
		$this->load->view('template/footer'); */
//		$this->layout->setLayout("login.php");
	//	$this->layout->view("index");
		
//		$this->layout->view("index", compact("personas"));
		
	}

	private function _orientadas($listado = null)
    {

        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione una opción";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

	private function _sustancias($listado = null)
    {

        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione una opción";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

	public function enviar_pregunta(){

		if($this->input->post('pregunta') == 1){
			

		
			$this->load->library('form_validation');
            $rules = [
				[
                    'field' => 'id_orientacion',
                    'label' => 'Tipo de orientación',
                    'rules' => 'trim|required|max_length[2]'
				],
				[
                    'field' => 'otro_orientada',
                    'label' => 'Otro orientada',
                    'rules' => 'max_length[100]'
				],
				[
                    'field' => 'otro_tema',
                    'label' => 'Otro tema',
                    'rules' => 'max_length[100]'
				],
				[
                    'field' => 'id_sustancia',
                    'label' => 'Tipo de sustancia',
                    'rules' => 'trim|required|max_length[2]'
				],
				[
                    'field' => 'nombre_producto',
                    'label' => 'Nombre del Producto',
                    'rules' => 'required|max_length[100]'
				],	
				[
                    'field' => 'descripcion',
                    'label' => 'Descripción',
                    'rules' => 'required|max_length[4000]'
				]
			];	

            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() === TRUE) {
			
				$id_pregunta = $this->ConsultasModel->setPregunta($this->input->post('id_orientacion'), $this->input->post('otro_orientada'), $this->input->post('id_sustancia')
				, $this->input->post('nombre_producto'), $this->input->post('descripcion'), $this->session->userdata(SESSION_NAME)->id);

				if ($id_pregunta > 0) {
					$this->template->set_flash_message(array('success' => 'La consulta se envió correctamente, por favor debe esperar a la respuesta.'));

			
					$listado_temas = $this->ConsultasModel->getTemas();
					for($i = 0; $i < count($listado_temas); $i++){
						if($this->input->post('temas_'.$i) == $listado_temas[$i]->id){
							if($listado_temas[$i]->id == 7){
								$aux = $this->ConsultasModel->setTemas($id_pregunta, $listado_temas[$i]->id, $this->input->post('otro_tema'));
							}else{
								$aux = $this->ConsultasModel->setTemas($id_pregunta, $listado_temas[$i]->id);
							}
							
						}
					}

					redirect('consultas');
                } else {
					$this->template->set_flash_message(array('error' => 'La consulta no se registró correctamente, por favor inténtelo de nuevo.'));
					redirect('consultas');
                }
            }
		}

		redirect('consultas');
	}

	public function respuesta($id)
	{


		$respuesta = $this->ConsultasModel->getRespuesta($id);
		$this->template->set('respuesta', $respuesta);

		$migas = '<li class="breadcrumb-item"><a href="' . base_url() . "consultas" . '">Consultas</a></li><li class="breadcrumb-item active">Respuesta Consulta</li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'consultas');
		$this->template->set('content_header', 'Consultas');
        $this->template->set('content_sub_header', 'Servicios de Consultas para el acceso a información sobre manejo terapéutico, control y seguimiento de enfermedades ocasionadas por agentes biológicos de alto riesgo para la salud humana.');
        $this->template->render('consultas/respuesta');

	}

	public function admin()
	{
        $this->template->add_css("css/datagrid/datatables/datatables.bundle");
		$this->template->add_js("custom/table_consultas");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "consultas" . '">Consultas</a></li>';
		$this->template->set('migasdepan', $migas);

		$this->template->set('listado_consultas', $this->ConsultasModel->getPendientesAdmin());

	//    echo '<pre>'; print_r($this->ConsultasModel->getPendientesAdmin()); return;

//		$listado_consultas = $this->ConsultasModel->getPreguntas($this->session->userdata(SESSION_NAME)->id);
//		$this->template->set('listado_consultas', $listado_consultas);
		
		$this->template->set('item_sidebar_active', 'consultas');
		$this->template->set('content_header', 'Consultas');
        $this->template->set('content_sub_header', 'Servicios de Consultas para el acceso a información sobre manejo terapéutico, control y seguimiento de enfermedades ocasionadas por agentes biológicos de alto riesgo para la salud humana.');
		$this->template->render('consultas/admin');
	//	$this->template->render('usuarios/todos');
	
	}

	public function contestar($id)
	{


		$pregunta = $this->ConsultasModel->getPregunta($id);
		$this->template->set('pregunta', $pregunta);

		$temas_pregunta = $this->ConsultasModel->getTemasPregunta($id);
		$this->template->set('temas_pregunta', $temas_pregunta);

		$sustancia_pregunta = $this->ConsultasModel->getSustanciaPregunta($pregunta->id_sustancia);
		$this->template->set('sustancia_pregunta', $sustancia_pregunta);

		$orientada_pregunta = $this->ConsultasModel->getOrientadaPregunta($pregunta->id_orientacion);
		$this->template->set('orientada_pregunta', $orientada_pregunta);

		$profesional = $this->PerfilModel->getPerfilProfesional($pregunta->id_user);
		$particular = $this->PerfilModel->getPerfilParticular($pregunta->id_user);
		if(isset($profesional)){
			$this->template->set('profesional', $profesional);
		}else{
			$this->template->set('particular', $particular);
		}

        $respuesta = $this->ConsultasModel->getRespuesta($id);
		$this->template->set('respuesta', $respuesta);

	//	echo '<pre>'; print_r($respuesta); return;

		$migas = '<li class="breadcrumb-item"><a href="' . base_url() . "consultas" . '">Consultas</a></li><li class="breadcrumb-item active">Respuesta Consulta</li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'consultas');
		$this->template->set('content_header', 'Consultas');
        $this->template->set('content_sub_header', 'Servicios de Consultas para el acceso a información sobre manejo terapéutico, control y seguimiento de enfermedades ocasionadas por agentes biológicos de alto riesgo para la salud humana.');
        $this->template->render('consultas/contestar');

	}

    public function pregunta($id)
	{


		$pregunta = $this->ConsultasModel->getPregunta($id);
		$this->template->set('pregunta', $pregunta);

		$temas_pregunta = $this->ConsultasModel->getTemasPregunta($id);
		$this->template->set('temas_pregunta', $temas_pregunta);

		$sustancia_pregunta = $this->ConsultasModel->getSustanciaPregunta($pregunta->id_sustancia);
		$this->template->set('sustancia_pregunta', $sustancia_pregunta);

		$orientada_pregunta = $this->ConsultasModel->getOrientadaPregunta($pregunta->id_orientacion);
		$this->template->set('orientada_pregunta', $orientada_pregunta);

		$profesional = $this->PerfilModel->getPerfilProfesional($pregunta->id_user);
		$particular = $this->PerfilModel->getPerfilParticular($pregunta->id_user);
		if(isset($profesional)){
			$this->template->set('profesional', $profesional);
		}else{
			$this->template->set('particular', $particular);
		}

        $respuesta = $this->ConsultasModel->getRespuesta($id);
		$this->template->set('respuesta', $respuesta);

	//	echo '<pre>'; print_r($respuesta); return;

		$migas = '<li class="breadcrumb-item"><a href="' . base_url() . "consultas" . '">Consultas</a></li><li class="breadcrumb-item active">Respuesta Consulta</li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'consultas');
		$this->template->set('content_header', 'Pregunta');
        $this->template->set('content_sub_header', 'Servicios de Consultas para el acceso a información sobre manejo terapéutico, control y seguimiento de enfermedades ocasionadas por agentes biológicos de alto riesgo para la salud humana.');
        $this->template->render('consultas/pregunta');

	}

	public function enviar_respuesta(){

		if($this->input->post('responder') == 1){
		
			$this->load->library('form_validation');
            $rules = [
				[
                    'field' => 'descrip_r',
                    'label' => 'Descripción',
                    'rules' => 'required|max_length[4000]'
				]
			];	

            $this->form_validation->set_rules($rules);
            if ($this->form_validation->run() === TRUE) {
			
				$id_respuesta = $this->ConsultasModel->setRespuesta($this->input->post('descrip_r'), $this->input->post('id_pregunta'), $this->session->userdata(SESSION_NAME)->id);

				if ($id_respuesta > 0) {
					$id_respuesta = $this->ConsultasModel->setEstadoPregunta($this->input->post('id_pregunta'),'1');
					$this->template->set_flash_message(array('success' => 'La respuesta se enviado correctamente.'));
					redirect('consultas/admin');
                } else {
					$this->template->set_flash_message(array('error' => 'La respuesta no se registró correctamente, por favor inténtelo de nuevo.'));
					redirect('consultas/admin');
                }
            }
		}

		redirect('consultas/admin');
	}

	public function carga_den(){
        $error = '';
        $sql = '';
        $mensaje = '';
        $uploads_dir = './public/archivos';
        if( $this->input->post('cargar') && ($_FILES['cargar_resultados'])) {
                move_uploaded_file($_FILES['cargar_resultados']['tmp_name'], $uploads_dir . '/' . $_FILES['cargar_resultados']['name']);
                $students = fopen($uploads_dir . '/' . $_FILES['cargar_resultados']['name'], "r"); //leo el archivo que contiene los datos a cargar
                setlocale(LC_ALL, 'es_ES');
                $posicion = 0;
                $linea = '';
                $sql = '';
                $sql2 = '';

                $delimiter = "";
                while (($datos = fgetcsv($students, 10000, ",")) !== false) //Leo linea por linea del archivo hasta un maximo de 10.000 caracteres por linea leida usando (;) como delimitador
                {

                    if (count($datos) > 1) {
                        $delimiter = ",";
                    }
                }
                fclose($students);
                $students = fopen($uploads_dir . '/' . $_FILES['cargar_resultados']['name'], "r"); //leo el archivo que contiene los datos a cargar
                setlocale(LC_ALL, 'es_ES');

                while (($datos = fgetcsv($students, 10000, ";")) !== false) //Leo linea por linea del archivo hasta un maximo de 10.000 caracteres por linea leida usando (;) como delimitador
                {
                    if (count($datos) > 1) {
                        $delimiter = ";";
                    }
                }

                fclose($students);
                $students = fopen($uploads_dir . '/' . $_FILES['cargar_resultados']['name'], "r"); //leo el archivo que contiene los datos a cargar
                setlocale(LC_ALL, 'es_ES');
                if ($delimiter) {
                    $evento = $this->ConsultasModel->consulta('evento');
                    $pais = $this->ConsultasModel->consulta('pais');
                    $municipio = $this->ConsultasModel->consulta('municipio');
                    $tiposs = $this->ConsultasModel->consulta('tipo_seguridad_social');
                    $conducta = $this->ConsultasModel->consulta('conducta');
                    $clasfinal = $this->ConsultasModel->consulta('clasificacion_dengue');
    
                    while (($datos = fgetcsv($students, 10000, $delimiter)) !== false) //Leo linea por linea del archivo hasta un maximo de 10.000 caracteres por linea leida usando (;) como delimitador
                    {                 
                            $utilizar = array(0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25);
                        if ($posicion != 0) {
                            $aux = "";
                            $aux2= "";
                            $werror = '';
                            $xerror = false;
                            foreach ($utilizar as &$valor) {
                                $info = '';  
                                $info2 = '';
                                switch ($valor) {
                                    case 0:
                                        if(intval($datos[$valor]) == 220){
                                            $datos[$valor] = 210;
                                        }
                                        foreach ($evento as $ev){
                                            if(intval($ev->cod_eve) == intval($datos[$valor])){
                                                $info = $ev->id;
                                            }
                                        }
                                        break;
                                    case 1:
                                        $info = $this->corrige_fecha($datos[$valor]);
                                        break;
                                    case 2:
                                        if(intval($datos[$valor]) > 0 && intval($datos[$valor]) < 54){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 3:
                                        if(intval($datos[$valor]) >= 0 && intval($datos[$valor]) < 120){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 4:
                                        $xxx = (!($datos[$valor])) ? '999' : ($datos[$valor]);
                                        foreach ($pais as $pa){
                                            if(intval($pa->cod_pais) == intval($xxx)){
                                                $info = $pa->id;
                                            }
                                        }
                                        $info = (!$info)?'170':$info;
                                        break;
                                    case 6:
                                        if($datos[$valor] == 'M' || $datos[$valor] == 'F'){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 7:
                                        $xxx2 = (!($datos[$valor])) ? '1' : ($datos[$valor]);
                                        foreach ($municipio as $mun){
                                            if(intval($mun->cod_mun) == intval($xxx2)){
                                                $info = $mun->id;
                                            }
                                        }
                                        $info = (!$info)?'1':$info;
                                        break;
                                    case 8:
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 9:
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 10:
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 11:
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 12:
                                        foreach ($tiposs as $ti){
                                            if($ti->codigo == $datos[$valor]){
                                                $info = $ti->id;
                                            }
                                        }
                                        break;
                                    case 13:
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 14://gp_gestan
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;
                                    case 15: //sem_ges
                                        if($datos[$valor] && intval($datos[$valor]) > 0){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 0;
                                        }
                                        break; 
                                    case 16:
                                            $info = $this->corrige_fecha($datos[$valor]);
                                            break;
                                    case 17:
                                            $info = $this->corrige_fecha($datos[$valor]);
                                            break;
                                    case 18:
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = 2;
                                        }
                                        break;
                                    case 19: //fecha_hos
                                            $info = $this->corrige_fecha($datos[$valor]);
                                          //  echo '<pre>'; print_r($info); return;
                                            break;
                                    case 20: //fecha_nto
                                        $info2 = $this->corrige_fecha($datos[$valor]);
                                        break;
                                    case 21: // clasfinal
                                        $info2 = 3;
                                        foreach ($clasfinal as $c){
                                            if($c->id == $datos[$valor]){
                                                $info2 = $c->id;
                                            }
                                        }
                                        break; 
                                    case 22: //conducta
                                        $info2 = 6;
                                        foreach ($conducta as $c){
                                            if($c->id == $datos[$valor]){
                                                $info2 = $c->id;
                                            }
                                        }
                                        break; 
                                    case 24: //anyo
                                        if(intval($datos[$valor]) > 2018){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 25:
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    default:
                                        $info = '';
                                }
 
                                if($valor == 14 || $valor == 15 || $valor == 20 || $valor == 21 || $valor == 22){
                                    if($info2 != '' || $info2 == 0){
                                        $aux2 = ($aux2)?($aux2.",'".$info2."'"): "'".$info2."'";
                                    }else{
                                        $werror = (intval($posicion)+1);
                                        $xerror = true;
                                    }
                                }else{
                                    if($info != ''){
                                        $aux = ($aux)?($aux.",'".$info."'"): "'".$info."'";
                                        }else{
                                            $werror = (intval($posicion)+1);
                                            $xerror = true;
                                    }
                                }
                            }
                  
                            if(($aux) && (!$xerror)){
                                $sql = ($sql) ? ($sql.",(".$aux.")") : "(".$aux.")";
                                $sql = "INSERT INTO reporte (id_evento,fec_not,semana,edad,id_nacionalidad,sexo,id_muni_ocu,localidad,cen_poblado,vereda,bar_ver,tip_ss,cod_ase,fec_con,fec_sint,pac_hos,fec_hos,anyo,dpto_muni) VALUES ".$sql;
                        //       
                                $bien = $this->ConsultasModel->masivo($sql);
                                if($bien){
                                    $sql = '';
                                    $id_reporte = $this->ConsultasModel->consultaIdUlt('reporte');
                                    $aux2 = ($aux2)?($aux2.",'".$id_reporte->id."'"): "'".$id_reporte->id."'";
                                    $sql2 = ($sql2) ? ($sql2.",(".$aux2.")") : "(".$aux2.")";
                                    $sql2 = "INSERT INTO reporte_dengue (gp_gestan,sem_ges,fecha_nto,clasfinal,conducta,id_reporte) VALUES ".$sql2;
                                    $bien = $this->ConsultasModel->masivo($sql2);
                                    if($bien){
                                        $sql2 = '';
                                    }
                                    $mensaje = 'Finalizo la inserción de los resultados sin ningún contratiempo.';
                                }
                            }
                            if($werror){
                                $error = ($error)? ($error.', '.$werror):$werror;
                            }
                          
                        }
                        
                        $posicion++;
                        if($posicion%50==0) {
                            if($sql){
                          //      $sql = "INSERT INTO reporte (id_evento,fec_not,semana,edad,id_nacionalidad,sexo,id_muni_ocu,localidad,cen_poblado,vereda,bar_ver,tip_ss,cod_ase,fec_con,fec_sint,pac_hos,fec_hos,anyo) VALUES ".$sql;
                          //      $bien = $this->ConsultasModel->masivo($sql);
                                if($bien){
                                    $sql = '';
                                }
                            }
                        }
   
                    }
                    fclose($students); //Cierra el archivo
                } else {
                    $error = " La estructura interna del documento no corresponde al csv inicial.";
                }
        }

   //     if($sql){
       //    $sql = "INSERT INTO reporte (id_evento,fec_not,semana,edad,id_nacionalidad,sexo,id_muni_ocu,localidad,cen_poblado,vereda,bar_ver,tip_ss,cod_ase,fec_con,fec_sint,pac_hos,fec_hos,anyo) VALUES ".$sql;
        //    $bien = $this->ConsultasModel->masivo($sql);
     //       if($bien){
      //         $mensaje = 'Finalizo la inserción de los resultados sin ningún contratiempo.';
      //      }
      //  }

        if($error){
            $mensaje = $mensaje.'<br>Pero las siguientes líneas presentan errores: '.$error.'.';

        }
        if($mensaje){
            $this->template->add_message(array("info" => $mensaje));
            $_POST = '';
        }

        $this->template->set('item_sidebar_active', 'carga_den');
        $this->template->set('content_header', 'Cargar Resultados - Evento Dengue');
        $this->template->set('content_sub_header', '');
        $this->template->render('consultas/carga_den');
	}

    public function carga_hep(){
        $error = '';
        $sql = '';
        $mensaje = '';
        $uploads_dir = './public/archivos';
        if( $this->input->post('cargar') && ($_FILES['cargar_resultados'])) {
                move_uploaded_file($_FILES['cargar_resultados']['tmp_name'], $uploads_dir . '/' . $_FILES['cargar_resultados']['name']);
                $students = fopen($uploads_dir . '/' . $_FILES['cargar_resultados']['name'], "r"); //leo el archivo que contiene los datos a cargar
                setlocale(LC_ALL, 'es_ES');
                $posicion = 0;
                $linea = '';
                $sql = '';
                $sql2 = '';

                $delimiter = "";
                while (($datos = fgetcsv($students, 10000, ",")) !== false) //Leo linea por linea del archivo hasta un maximo de 10.000 caracteres por linea leida usando (;) como delimitador
                {

                    if (count($datos) > 1) {
                        $delimiter = ",";
                    }
                }
                fclose($students);
                $students = fopen($uploads_dir . '/' . $_FILES['cargar_resultados']['name'], "r"); //leo el archivo que contiene los datos a cargar
                setlocale(LC_ALL, 'es_ES');

                while (($datos = fgetcsv($students, 10000, ";")) !== false) //Leo linea por linea del archivo hasta un maximo de 10.000 caracteres por linea leida usando (;) como delimitador
                {
                    if (count($datos) > 1) {
                        $delimiter = ";";
                    }
                }

                fclose($students);
                $students = fopen($uploads_dir . '/' . $_FILES['cargar_resultados']['name'], "r"); //leo el archivo que contiene los datos a cargar
                setlocale(LC_ALL, 'es_ES');
                if ($delimiter) {
                    $evento = $this->ConsultasModel->consulta('evento');
                    $pais = $this->ConsultasModel->consulta('pais');
                    $municipio = $this->ConsultasModel->consulta('municipio');
                    $tiposs = $this->ConsultasModel->consulta('tipo_seguridad_social');
                    $modo_tra = $this->ConsultasModel->consulta('modo_transmision');
                    $clasfinal = $this->ConsultasModel->consulta('clasificacion_hepatitis');
    
                    while (($datos = fgetcsv($students, 10000, $delimiter)) !== false) //Leo linea por linea del archivo hasta un maximo de 10.000 caracteres por linea leida usando (;) como delimitador
                    {
                        $utilizar = array(0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29, 30, 
                        31, 32, 33, 34, 35, 36);
                        if ($posicion != 0) {
                            $aux = "";
                            $aux2= "";
                            $werror = '';
                            $xerror = false;
                            foreach ($utilizar as &$valor) {
                                $info = '';  
                                $info2 = '';
                                switch ($valor) {
                                    case 0: /** cod_eve */
                                        foreach ($evento as $ev){
                                            if(intval($ev->cod_eve) == intval($datos[$valor])){
                                                $info = $ev->id;
                                            }
                                        }
                                        break;
                                    case 1: /** fec_not */
                                        $info = $this->corrige_fecha($datos[$valor]);
                                        break;
                                    case 2: /** semana */
                                        if(intval($datos[$valor]) > 0 && intval($datos[$valor]) < 54){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 3: /** edad_ */
                                        if(intval($datos[$valor]) >= 0 && intval($datos[$valor]) < 120){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 4: /** nacionali_ */
                                        $xxx = (!($datos[$valor])) ? '999' : ($datos[$valor]);
                                        foreach ($pais as $pa){
                                            if(intval($pa->cod_pais) == intval($xxx)){
                                                $info = $pa->id;
                                            }
                                        }
                                        $info = (!$info)?'170':$info;
                                        break;
                                    case 6: /** sexo_ */
                                        if($datos[$valor] == 'M' || $datos[$valor] == 'F'){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 7: /** cod_mun_o */
                                        $xxx2 = (!($datos[$valor])) ? '1' : ($datos[$valor]);
                                        foreach ($municipio as $mun){
                                            if(intval($mun->cod_mun) == intval($xxx2)){
                                                $info = $mun->id;
                                            }
                                        }
                                        $info = (!$info)?'1':$info;
                                        break;
                                    case 8: /** localidad_ */
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 9: /** cen_pobla */
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 10: /** vereda_ */
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 11: /** bar_ver_ */
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 12: /** tip_ss */
                                        foreach ($tiposs as $ti){
                                            if($ti->codigo == $datos[$valor]){
                                                $info = $ti->id;
                                            }
                                        }
                                        break;
                                    case 13: /** cod_ase_ */
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 14:/** gp_gestan */
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;
                                    case 15: /** sem_ges_ */
                                        if($datos[$valor] && intval($datos[$valor]) > 0){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 0;
                                        }
                                        break; 
                                    case 16: /** fec_con_ */
                                            $info = $this->corrige_fecha($datos[$valor]);
                                            break;
                                    case 17: /** ini_sin_ */
                                            $info = $this->corrige_fecha($datos[$valor]);
                                            break;
                                    case 18: /** pac_hos_ */
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = 2;
                                        }
                                        break;
                                    case 19: //fec_hos
                                            $info = $this->corrige_fecha($datos[$valor]);
                                          //  echo '<pre>'; print_r($info); return;
                                            break;
                                    case 20: // clasfinal
                                        $info2 = 7;
                                        foreach ($clasfinal as $c){
                                            if($c->id == $datos[$valor]){
                                                $info2 = $c->id;
                                            }
                                        }
                                        break; 
                                    case 21: // met_tra
                                        $info2 = 5;
                                        foreach ($modo_tra as $c){
                                            if($c->id == $datos[$valor]){
                                                $info2 = $c->id;
                                            }
                                        }
                                        break; 
                                    case 22: // coinf_vih
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;
                                    case 24: // gp_discapa
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;
                                    case 25: // gp_desplaz
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;
                                    case 26: // gp_migrant
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;
                                    case 27: // gp_carcela
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;
                                    case 28: // gp_indigen
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;
                                    case 29: // gp_pobicbf
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;
                                    case 30: // gp_mad_com
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;
                                    case 31: // gp_desmovi
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;   
                                    case 32: // gp_psiquia
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;   
                                    case 33: // gp_vic_vio
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;   
                                    case 34: // gp_otros
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;         
                                    case 35: //anyo
                                        if(intval($datos[$valor]) > 2018){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 36: /** tipo_reporte */
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    default:
                                        $info = '';
                                }
 
                                if($valor == 14 || $valor == 15 || $valor == 20 || $valor == 21 || $valor == 22 || $valor == 24 || $valor == 25 || $valor == 26
                                || $valor == 27 || $valor == 28 || $valor == 29 || $valor == 30 || $valor == 31 || $valor == 32 || $valor == 33 || $valor == 34 ){
                                    if($info2 != '' || $info2 == 0){
                                        $aux2 = ($aux2)?($aux2.",'".$info2."'"): "'".$info2."'";
                                    }else{
                                        $werror = (intval($posicion)+1);
                                        $xerror = true;
                                    }
                                }else{
                                    if($info != ''){
                                        $aux = ($aux)?($aux.",'".$info."'"): "'".$info."'";
                                        }else{
                                            $werror = (intval($posicion)+1);
                                            $xerror = true;
                                    }
                                }
                            }
                  
                            if(($aux) && (!$xerror)){
                                $sql = ($sql) ? ($sql.",(".$aux.")") : "(".$aux.")";
                                $sql = "INSERT INTO reporte (id_evento,fec_not,semana,edad,id_nacionalidad,sexo,id_muni_ocu,localidad,cen_poblado,vereda,bar_ver,tip_ss,cod_ase,fec_con,fec_sint,pac_hos,fec_hos,anyo,dpto_muni) VALUES ".$sql;
                        //       
                                $bien = $this->ConsultasModel->masivo($sql);
                                if($bien){
                                    $sql = '';
                                    $id_reporte = $this->ConsultasModel->consultaIdUlt('reporte');
                                    $aux2 = ($aux2)?($aux2.",'".$id_reporte->id."'"): "'".$id_reporte->id."'";
                                    $sql2 = ($sql2) ? ($sql2.",(".$aux2.")") : "(".$aux2.")";
                                    $sql2 = "INSERT INTO reporte_hepatitis (gp_gestan,sem_ges,clasfinal,met_tra,coinf_vih_coinfeccion,gp_discapa,gp_desplaz,gp_migrant,gp_carcela,gp_indigen,gp_pobicbf,gp_mad_com,gp_desmovi,
                                    gp_psiquia,gp_vic_vio,gp_otros,id_reporte) VALUES ".$sql2;
                                    $bien = $this->ConsultasModel->masivo($sql2);
                                    if($bien){
                                        $sql2 = '';
                                    }
                                    $mensaje = 'Finalizo la inserción de los resultados sin ningún contratiempo.';
                                }
                            }
                            if($werror){
                                $error = ($error)? ($error.', '.$werror):$werror;
                            }
                          
                        }
                        
                        $posicion++;
                        if($posicion%50==0) {
                            if($sql){
                          //      $sql = "INSERT INTO reporte (id_evento,fec_not,semana,edad,id_nacionalidad,sexo,id_muni_ocu,localidad,cen_poblado,vereda,bar_ver,tip_ss,cod_ase,fec_con,fec_sint,pac_hos,fec_hos,anyo) VALUES ".$sql;
                          //      $bien = $this->ConsultasModel->masivo($sql);
                                if($bien){
                                    $sql = '';
                                }
                            }
                        }
   
                    }
                    fclose($students); //Cierra el archivo
                } else {
                    $error = " La estructura interna del documento no corresponde al csv inicial.";
                }
        }

   //     if($sql){
       //    $sql = "INSERT INTO reporte (id_evento,fec_not,semana,edad,id_nacionalidad,sexo,id_muni_ocu,localidad,cen_poblado,vereda,bar_ver,tip_ss,cod_ase,fec_con,fec_sint,pac_hos,fec_hos,anyo) VALUES ".$sql;
        //    $bien = $this->ConsultasModel->masivo($sql);
     //       if($bien){
      //         $mensaje = 'Finalizo la inserción de los resultados sin ningún contratiempo.';
      //      }
      //  }

        if($error){
            $mensaje = $mensaje.'<br>Pero las siguientes líneas presentan errores: '.$error.'.';

        }
        if($mensaje){
            $this->template->add_message(array("info" => $mensaje));
            $_POST = '';
        }

        $this->template->set('item_sidebar_active', 'carga_hep');
        $this->template->set('content_header', 'Cargar Resultados - Evento Hepatitis');
        $this->template->set('content_sub_header', '');
        $this->template->render('consultas/carga_hep');
	}

    public function carga_tbc(){
        $error = '';
        $sql = '';
        $mensaje = '';
        $uploads_dir = './public/archivos';
        if( $this->input->post('cargar') && ($_FILES['cargar_resultados'])) {
                move_uploaded_file($_FILES['cargar_resultados']['tmp_name'], $uploads_dir . '/' . $_FILES['cargar_resultados']['name']);
                $students = fopen($uploads_dir . '/' . $_FILES['cargar_resultados']['name'], "r"); //leo el archivo que contiene los datos a cargar
                setlocale(LC_ALL, 'es_ES');
                $posicion = 0;
                $linea = '';
                $sql = '';
                $sql2 = '';

                $delimiter = "";
                while (($datos = fgetcsv($students, 10000, ",")) !== false) //Leo linea por linea del archivo hasta un maximo de 10.000 caracteres por linea leida usando (;) como delimitador
                {

                    if (count($datos) > 1) {
                        $delimiter = ",";
                    }
                }
                fclose($students);
                $students = fopen($uploads_dir . '/' . $_FILES['cargar_resultados']['name'], "r"); //leo el archivo que contiene los datos a cargar
                setlocale(LC_ALL, 'es_ES');

                while (($datos = fgetcsv($students, 10000, ";")) !== false) //Leo linea por linea del archivo hasta un maximo de 10.000 caracteres por linea leida usando (;) como delimitador
                {
                    if (count($datos) > 1) {
                        $delimiter = ";";
                    }
                }

                fclose($students);
                $students = fopen($uploads_dir . '/' . $_FILES['cargar_resultados']['name'], "r"); //leo el archivo que contiene los datos a cargar
                setlocale(LC_ALL, 'es_ES');
                if ($delimiter) {
                    $evento = $this->ConsultasModel->consulta('evento');
                    $pais = $this->ConsultasModel->consulta('pais');
                    $municipio = $this->ConsultasModel->consulta('municipio');
                    $tiposs = $this->ConsultasModel->consulta('tipo_seguridad_social');
                    $condtbc = $this->ConsultasModel->consulta('condicion_tbc');
                    $clasfinal = $this->ConsultasModel->consulta('clasificacion_tbc');
                    $etnia = $this->ConsultasModel->consulta('etnia');
                   
    
                    while (($datos = fgetcsv($students, 10000, $delimiter)) !== false) //Leo linea por linea del archivo hasta un maximo de 10.000 caracteres por linea leida usando (;) como delimitador
                    {
                            $utilizar = array(0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 27, 28, 29, 30, 31);
                        if ($posicion != 0) {
                            $aux = "";
                            $aux2= "";
                            $werror = '';
                            $xerror = false;
                            foreach ($utilizar as &$valor) {
                                $info = '';  
                                $info2 = '';
                                switch ($valor) {
                                    case 0: /** cod_eve */
                                        foreach ($evento as $ev){
                                            if(intval($ev->cod_eve) == intval($datos[$valor])){
                                                $info = $ev->id;
                                            }
                                        }
                                        break;
                                    case 1: /** fec_not */
                                        $info = $this->corrige_fecha($datos[$valor]);
                                        break;
                                    case 2: /** semana */
                                        if(intval($datos[$valor]) > 0 && intval($datos[$valor]) < 54){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 3: /** edad_ */
                                        if(intval($datos[$valor]) >= 0 && intval($datos[$valor]) < 120){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 4: /** nacionali_ */
                                        $xxx = (!($datos[$valor])) ? '999' : ($datos[$valor]);
                                        foreach ($pais as $pa){
                                            if(intval($pa->cod_pais) == intval($xxx)){
                                                $info = $pa->id;
                                            }
                                        }
                                        $info = (!$info)?'170':$info;
                                        break;
                                    case 6: /** sexo_ */
                                        if($datos[$valor] == 'M' || $datos[$valor] == 'F'){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 7: /** cod_mun_o */
                                        $xxx2 = (!($datos[$valor])) ? '1' : ($datos[$valor]);
                                        foreach ($municipio as $mun){
                                            if(intval($mun->cod_mun) == intval($xxx2)){
                                                $info = $mun->id;
                                            }
                                        }
                                        $info = (!$info)?'1':$info;
                                        break;
                                    case 8: /** localidad_ */
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 9: /** cen_pobla */
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 10: /** vereda_ */
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 11: /** bar_ver_ */
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 12: /** tip_ss */
                                        foreach ($tiposs as $ti){
                                            if($ti->codigo == $datos[$valor]){
                                                $info = $ti->id;
                                            }
                                        }
                                        break;
                                    case 13: /** cod_ase_ */
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 14:/** gp_gestan */
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;
                                    case 15: /** sem_ges_ */
                                        if($datos[$valor] && intval($datos[$valor]) > 0){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 0;
                                        }
                                        break; 
                                    case 16: /** fec_con_ */
                                            $info = $this->corrige_fecha($datos[$valor]);
                                            break;
                                    case 17: /** ini_sin_ */
                                            $info = $this->corrige_fecha($datos[$valor]);
                                            break;
                                    case 18: /** pac_hos_ */
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = 2;
                                        }
                                        break;
                                    case 19: //fec_hos
                                            $info = $this->corrige_fecha($datos[$valor]);
                                          //  echo '<pre>'; print_r($info); return;
                                            break;
                                    case 20: // cond_tuber
                                        $info2 = 3;
                                        foreach ($condtbc as $c){
                                            if($c->id == $datos[$valor]){
                                                $info2 = $c->id;
                                            }
                                        }
                                        break; 
                                    case 21: // tip_tub
                                        $info2 = 3;
                                        foreach ($clasfinal as $c){
                                            if($c->id == $datos[$valor]){
                                                $info2 = $c->id;
                                            }
                                        }
                                        break; 
                                    case 22: // vih_confirmado
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;
                                    case 24: // per_etn
                                        $info2 = 6;
                                        foreach ($etnia as $c){
                                            if($c->id == $datos[$valor]){
                                                $info2 = $c->id;
                                            }
                                        }
                                        break; 
                                    case 25: // nom_grupo
                                        if($datos[$valor]){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = "NO REGISTRA";
                                        }
                                        break;
                                    case 26: // diabetes
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;
                                    case 27: // enfe_renal
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;
                                    case 28: // desnutrici
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;
                                    case 29: // trab_salud
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = 2;
                                        }
                                        break;
                                    case 30: //anyo
                                        if(intval($datos[$valor]) > 2018){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 31: /** tipo_reporte */
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    default:
                                        $info = '';
                                }
 
                                if($valor == 14 || $valor == 15 || $valor == 20 || $valor == 21 || $valor == 22 || $valor == 24 || $valor == 25
                                || $valor == 26 || $valor == 27 || $valor == 28 || $valor == 29){
                                    if($info2 != '' || $info2 == 0){
                                        $aux2 = ($aux2)?($aux2.",'".$info2."'"): "'".$info2."'";
                                    }else{
                                        $werror = (intval($posicion)+1);
                                        $xerror = true;
                                    }
                                }else{
                                    if($info != ''){
                                        $aux = ($aux)?($aux.",'".$info."'"): "'".$info."'";
                                        }else{
                                            $werror = (intval($posicion)+1);
                                            $xerror = true;                                          
                                    }
                                }
                            }
                         
                            if(($aux) && (!$xerror)){
                                $sql = ($sql) ? ($sql.",(".$aux.")") : "(".$aux.")";
                                $sql = "INSERT INTO reporte (id_evento,fec_not,semana,edad,id_nacionalidad,sexo,id_muni_ocu,localidad,cen_poblado,vereda,bar_ver,tip_ss,cod_ase,fec_con,fec_sint,pac_hos,fec_hos,anyo,dpto_muni) VALUES ".$sql;
                        //       
                                $bien = $this->ConsultasModel->masivo($sql);
                                if($bien){
                                    $sql = '';
                                    $id_reporte = $this->ConsultasModel->consultaIdUlt('reporte');
                                    $aux2 = ($aux2)?($aux2.",'".$id_reporte->id."'"): "'".$id_reporte->id."'";
                                    $sql2 = ($sql2) ? ($sql2.",(".$aux2.")") : "(".$aux2.")";
                                    $sql2 = "INSERT INTO reporte_tbc (gp_gestan,sem_ges,cond_tuber,tip_tub,vih_confirmado,per_etn,nombre_grupo,diabetes,enfe_renal,desnutrici,trab_salud,id_reporte) VALUES ".$sql2;
                                    $bien = $this->ConsultasModel->masivo($sql2);
                                    if($bien){
                                        $sql2 = '';
                                    }
                                    $mensaje = 'Finalizo la inserción de los resultados sin ningún contratiempo.';
                                }
                            }
                            if($werror){
                                $error = ($error)? ($error.', '.$werror):$werror;
                            }
                          
                        }
                        
                        $posicion++;
                        if($posicion%50==0) {
                            if($sql){
                          //      $sql = "INSERT INTO reporte (id_evento,fec_not,semana,edad,id_nacionalidad,sexo,id_muni_ocu,localidad,cen_poblado,vereda,bar_ver,tip_ss,cod_ase,fec_con,fec_sint,pac_hos,fec_hos,anyo) VALUES ".$sql;
                          //      $bien = $this->ConsultasModel->masivo($sql);
                                if($bien){
                                    $sql = '';
                                }
                            }
                        }
                    }
                    fclose($students); //Cierra el archivo
                } else {
                    $error = " La estructura interna del documento no corresponde al csv inicial.";
                }
        }

        if($error){
            $mensaje = $mensaje.'<br>Pero las siguientes líneas presentan errores: '.$error.'.';
        }
        if($mensaje){
            $this->template->add_message(array("info" => $mensaje));
            $_POST = '';
        }

        $this->template->set('item_sidebar_active', 'carga_tbc');
        $this->template->set('content_header', 'Cargar Resultados - Evento Tuberculosis');
        $this->template->set('content_sub_header', '');
        $this->template->render('consultas/carga_tbc');
	}

    public function carga_vih(){
        $error = '';
        $sql = '';
        $mensaje = '';
        $uploads_dir = './public/archivos';
        if( $this->input->post('cargar') && ($_FILES['cargar_resultados'])) {
                move_uploaded_file($_FILES['cargar_resultados']['tmp_name'], $uploads_dir . '/' . $_FILES['cargar_resultados']['name']);
                $students = fopen($uploads_dir . '/' . $_FILES['cargar_resultados']['name'], "r"); //leo el archivo que contiene los datos a cargar
                setlocale(LC_ALL, 'es_ES');
                $posicion = 0;
                $linea = '';
                $sql = '';
                $sql2 = '';

                $delimiter = "";
                while (($datos = fgetcsv($students, 10000, ",")) !== false) //Leo linea por linea del archivo hasta un maximo de 10.000 caracteres por linea leida usando (;) como delimitador
                {

                    if (count($datos) > 1) {
                        $delimiter = ",";
                    }
                }
                fclose($students);
                $students = fopen($uploads_dir . '/' . $_FILES['cargar_resultados']['name'], "r"); //leo el archivo que contiene los datos a cargar
                setlocale(LC_ALL, 'es_ES');

                while (($datos = fgetcsv($students, 10000, ";")) !== false) //Leo linea por linea del archivo hasta un maximo de 10.000 caracteres por linea leida usando (;) como delimitador
                {
                    if (count($datos) > 1) {
                        $delimiter = ";";
                    }
                }

                fclose($students);
                $students = fopen($uploads_dir . '/' . $_FILES['cargar_resultados']['name'], "r"); //leo el archivo que contiene los datos a cargar
                setlocale(LC_ALL, 'es_ES');
                if ($delimiter) {
                    $evento = $this->ConsultasModel->consulta('evento');
                    $pais = $this->ConsultasModel->consulta('pais');
                    $municipio = $this->ConsultasModel->consulta('municipio');
                    $tiposs = $this->ConsultasModel->consulta('tipo_seguridad_social');
                    $estadocli = $this->ConsultasModel->consulta('estado_clinico');
    
                    while (($datos = fgetcsv($students, 10000, $delimiter)) !== false) //Leo linea por linea del archivo hasta un maximo de 10.000 caracteres por linea leida usando (;) como delimitador
                    {
                            $utilizar = array(0, 1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 21, 22);
                        if ($posicion != 0) {
                            $aux = "";
                            $aux2= "";
                            $werror = '';
                            $xerror = false;
                            foreach ($utilizar as &$valor) {
                                $info = '';  
                                $info2 = '';
                                switch ($valor) {
                                    case 0: /** cod_eve */
                                        foreach ($evento as $ev){
                                            if(intval($ev->cod_eve) == intval($datos[$valor])){
                                                $info = $ev->id;
                                            }
                                        }
                                        break;
                                    case 1: /** fec_not */
                                        $info = $this->corrige_fecha($datos[$valor]);
                                        break;
                                    case 2: /** semana */
                                        if(intval($datos[$valor]) > 0 && intval($datos[$valor]) < 54){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 3: /** edad_ */
                                        if(intval($datos[$valor]) >= 0 && intval($datos[$valor]) < 120){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 4: /** nacionali_ */
                                        $xxx = (!($datos[$valor])) ? '999' : ($datos[$valor]);
                                        foreach ($pais as $pa){
                                            if(intval($pa->cod_pais) == intval($xxx)){
                                                $info = $pa->id;
                                            }
                                        }
                                        $info = (!$info)?'170':$info;
                                        break;
                                    case 6: /** sexo_ */
                                        if($datos[$valor] == 'M' || $datos[$valor] == 'F'){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 7: /** cod_mun_o */
                                        $xxx2 = (!($datos[$valor])) ? '1' : ($datos[$valor]);
                                        foreach ($municipio as $mun){
                                            if(intval($mun->cod_mun) == intval($xxx2)){
                                                $info = $mun->id;
                                            }
                                        }
                                        $info = (!$info)?'1':$info;
                                        break;
                                    case 8: /** localidad_ */
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 9: /** cen_pobla */
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 10: /** vereda_ */
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 11: /** bar_ver_ */
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 12: /** tip_ss */
                                        foreach ($tiposs as $ti){
                                            if($ti->codigo == $datos[$valor]){
                                                $info = $ti->id;
                                            }
                                        }
                                        break;
                                    case 13: /** cod_ase_ */
                                        if($datos[$valor]){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = "NO REGISTRA";
                                        }
                                        break;
                                    case 14: /** fec_con_ */
                                            $info = $this->corrige_fecha($datos[$valor]);
                                            break;
                                    case 15: /** ini_sin_ */
                                            $info = $this->corrige_fecha($datos[$valor]);
                                            break;
                                    case 16: /** pac_hos_ */
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info = $datos[$valor];
                                        }else{
                                            $info = 2;
                                        }
                                        break;
                                    case 17: //fec_hos
                                            $info = $this->corrige_fecha($datos[$valor]);
                                          //  echo '<pre>'; print_r($info); return;
                                        break;
                                    case 18: /** ide_genero */
                                        if($datos[$valor]){
                                            $info2 = $datos[$valor];
                                        }else{
                                            $info2 = "NO REGISTRA";
                                        }
                                        break;
                                    case 19: // est_cli
                                        $info2 = 4;
                                        foreach ($estadocli as $c){
                                            if($c->id == $datos[$valor]){
                                                $info2 = $c->id;
                                            }
                                        }
                                        break; 
                                    case 21: //anyo
                                        if(intval($datos[$valor]) > 2018){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 22: /** tipo_reporte */
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    default:
                                        $info = '';
                                }
 
                                if($valor == 18 || $valor == 19){
                                    if($info2 != '' || $info2 == 0){
                                        $aux2 = ($aux2)?($aux2.",'".$info2."'"): "'".$info2."'";
                                    }else{
                                        $werror = (intval($posicion)+1);
                                        $xerror = true;
                                    }
                                }else{
                                    if($info != ''){
                                        $aux = ($aux)?($aux.",'".$info."'"): "'".$info."'";
                                        }else{
                                            $werror = (intval($posicion)+1);
                                            $xerror = true;
                                    }
                                }
                            }
                  
                            if(($aux) && (!$xerror)){
                                $sql = ($sql) ? ($sql.",(".$aux.")") : "(".$aux.")";
                                $sql = "INSERT INTO reporte (id_evento,fec_not,semana,edad,id_nacionalidad,sexo,id_muni_ocu,localidad,cen_poblado,vereda,bar_ver,tip_ss,cod_ase,fec_con,fec_sint,pac_hos,fec_hos,anyo,dpto_muni) VALUES ".$sql;
                        //       
                                $bien = $this->ConsultasModel->masivo($sql);
                                if($bien){
                                    $sql = '';
                                    $id_reporte = $this->ConsultasModel->consultaIdUlt('reporte');
                                    $aux2 = ($aux2)?($aux2.",'".$id_reporte->id."'"): "'".$id_reporte->id."'";
                                    $sql2 = ($sql2) ? ($sql2.",(".$aux2.")") : "(".$aux2.")";
                                    $sql2 = "INSERT INTO reporte_vih (ide_genero,est_cli,id_reporte) VALUES ".$sql2;
                                    $bien = $this->ConsultasModel->masivo($sql2);
                                    if($bien){
                                        $sql2 = '';
                                    }
                                    $mensaje = 'Finalizo la inserción de los resultados sin ningún contratiempo.';
                                }
                            }
                            if($werror){
                                $error = ($error)? ($error.', '.$werror):$werror;
                            }
                          
                        }
                        
                        $posicion++;
                        if($posicion%50==0) {
                            if($sql){
                          //      $sql = "INSERT INTO reporte (id_evento,fec_not,semana,edad,id_nacionalidad,sexo,id_muni_ocu,localidad,cen_poblado,vereda,bar_ver,tip_ss,cod_ase,fec_con,fec_sint,pac_hos,fec_hos,anyo) VALUES ".$sql;
                          //      $bien = $this->ConsultasModel->masivo($sql);
                                if($bien){
                                    $sql = '';
                                }
                            }
                        }
                    }
                    fclose($students); //Cierra el archivo
                } else {
                    $error = " La estructura interna del documento no corresponde al csv inicial.";
                }
        }

        if($error){
            $mensaje = $mensaje.'<br>Pero las siguientes líneas presentan errores: '.$error.'.';
        }
        if($mensaje){
            $this->template->add_message(array("info" => $mensaje));
            $_POST = '';
        }

        $this->template->set('item_sidebar_active', 'carga_vih');
        $this->template->set('content_header', 'Cargar Resultados - Evento VIH');
        $this->template->set('content_sub_header', '');
        $this->template->render('consultas/carga_vih');
	}

    public function carga_cov(){
        $error = '';
        $sql = '';
        $mensaje = '';
        $uploads_dir = './public/archivos';
        if( $this->input->post('cargar') && ($_FILES['cargar_resultados'])) {
                move_uploaded_file($_FILES['cargar_resultados']['tmp_name'], $uploads_dir . '/' . $_FILES['cargar_resultados']['name']);
                $students = fopen($uploads_dir . '/' . $_FILES['cargar_resultados']['name'], "r"); //leo el archivo que contiene los datos a cargar
                setlocale(LC_ALL, 'es_ES');
                $posicion = 0;
                $linea = '';
                $sql = '';
                $sql2 = '';

                $delimiter = "";
                while (($datos = fgetcsv($students, 10000, ",")) !== false) //Leo linea por linea del archivo hasta un maximo de 10.000 caracteres por linea leida usando (;) como delimitador
                {

                    if (count($datos) > 1) {
                        $delimiter = ",";
                    }
                }
                fclose($students);
                $students = fopen($uploads_dir . '/' . $_FILES['cargar_resultados']['name'], "r"); //leo el archivo que contiene los datos a cargar
                setlocale(LC_ALL, 'es_ES');

                while (($datos = fgetcsv($students, 10000, ";")) !== false) //Leo linea por linea del archivo hasta un maximo de 10.000 caracteres por linea leida usando (;) como delimitador
                {
                    if (count($datos) > 1) {
                        $delimiter = ";";
                    }
                }

                fclose($students);
                $students = fopen($uploads_dir . '/' . $_FILES['cargar_resultados']['name'], "r"); //leo el archivo que contiene los datos a cargar
                setlocale(LC_ALL, 'es_ES');
                if ($delimiter) {
                    $tipo_prueba = $this->ConsultasModel->consulta('tipo_prueba');
                    $municipio = $this->ConsultasModel->consulta('municipio');
                    $pais = $this->ConsultasModel->consulta('pais');
                    $clasfinal = $this->ConsultasModel->consulta('clasificacion_covid   ');
                    $grupoedad = $this->ConsultasModel->consulta('grupo_edad');
                    $i = 0;

                    while (($datos = fgetcsv($students, 10000, $delimiter)) !== false) //Leo linea por linea del archivo hasta un maximo de 10.000 caracteres por linea leida usando (;) como delimitador
                    { 
                        $datos = array_map("utf8_encode", $datos);

                            $utilizar = array(0, 1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25);
                        if ($posicion != 0) {
                            $aux = "";
                            $werror = '';
                            $xerror = false;
                            foreach ($utilizar as &$valor) {
                                $info = '';  
                                switch ($valor) {
                                    case 0: /** Tipo_prueba */
                                        foreach ($tipo_prueba as $ev){
                                            if(strtolower($ev->nombre) == strtolower($datos[$valor])){
                                                $info = $ev->id;
                                            }
                                        }
                                        break;
                                    case 1: /** fec_not */
                                        $info = $this->corrige_fecha($datos[$valor]);
                                        break;
                                    case 3: /** Ciudad_municipio_nom */
                                        foreach ($municipio as $mun){
                                            if(strtolower($mun->nombre) == strtolower($datos[$valor])){
                                                $info = $mun->id;
                                            }
                                        }
                                        $info = (!$info)?'41':$info;
                                        break;
                                    case 4: /** edad */
                                        if(intval($datos[$valor]) >= 0 && intval($datos[$valor]) < 120){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 5: /** sexo */
                                        if($datos[$valor] == 'M' || $datos[$valor] == 'F'){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 6: /** Fuente_tipo_contagio */
                                        $info = strtoupper($datos[$valor]);
                                        break;
                                    case 7: /** Ubicacion */
                                        $info = strtoupper($datos[$valor]);
                                        break;
                                    case 8: /** Estado */
                                        $info = strtoupper($datos[$valor]);
                                        break;
                                    case 9: /** Nacionalidad_nom */
                                        foreach ($pais as $pa){
                                            if(strtolower($pa->nombre) == strtolower($datos[$valor])){
                                                $info = $pa->id;
                                            }
                                        }
                                        $info = (!$info)?'7':$info;
                                        break;
                                    case 10: /** Fecha_ingreso_pais */
                                        $info = $this->corrige_fecha($datos[$valor]);
                                        break;
                                    case 11: /** Fecha_inicio_sintomas+Fecha_consulta */
                                        $info = $this->corrige_fecha($datos[$valor]);
                                        break;
                                    case 12: /** Fecha_consulta */
                                        $info = $this->corrige_fecha($datos[$valor]);
                                        break;
                                    case 13: /** Fecha_diagnostico   */
                                        $info = $this->corrige_fecha($datos[$valor]);
                                        break;
                                    case 14: /** Fecha_hospitalizacion */
                                        $info = $this->corrige_fecha($datos[$valor]);
                                        break;
                                    case 15: /** Fecha_alta */
                                        $info = $this->corrige_fecha($datos[$valor]);
                                        break;
                                    case 16: /** Fecha_muerte */
                                        $info = $this->corrige_fecha($datos[$valor]);
                                        break;
                                    case 17: /** Fecha_recuperado */
                                        $info = $this->corrige_fecha($datos[$valor]);
                                        break;  
                                    case 18: /** Recuperado */
                                        foreach ($clasfinal as $ti){
                                            if(strtolower($ti->nombre) == strtolower($datos[$valor])){
                                                $info = $ti->id;
                                            }
                                        }
                                        $info = (!$info)?'3':$info;
                                        break;
                                    case 19: /** Tipo_recuperacion */
                                        if($datos[$valor]){
                                            $info = strtoupper($datos[$valor]);
                                        }else{
                                            $info = 'NO REGISTRA';
                                        }
                                        break;
                                    case 20: /** Grupo_edad */
                                        foreach ($grupoedad as $ti){
                                            if(strtolower($ti->nombre) == strtolower($datos[$valor])){
                                                $info = $ti->id;
                                            }
                                        }
                                        $info = (!$info)?'13':$info;
                                        break;
                                    case 21: /** Quinquenio */
                                        $info = strtoupper($datos[$valor]);
                                        break;
                                    case 22: /** SE_Fecha_inicio_sintomas+Fecha_consulta */
                                        if(intval($datos[$valor]) > 0 && intval($datos[$valor]) < 54){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 23: /** Barrio */
                                        $info = strtoupper($datos[$valor]);
                                        break;
                                    case 24: //anyo
                                        if(intval($datos[$valor]) > 2018){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    case 25: /** tipo_reporte */
                                        if(intval($datos[$valor]) == 1 || intval($datos[$valor]) == 2){
                                            $info = $datos[$valor];
                                        }
                                        break;
                                    default:
                                        $info = '';
                                }
 

                                if($info != ''){
                                    $i++;
                                    $aux = ($aux)?($aux.",'".$info."'"): "'".$info."'";
                                }else{
                                    $werror = (intval($posicion)+1);
                                    $xerror = true;
                                }
                            }

                            if(($aux) && (!$xerror)){
                                $sql = ($sql) ? ($sql.",(".$aux.")") : "(".$aux.")";
                                $sql = "INSERT INTO reporte_covid (id_tipo_prueba,fec_not,id_muni,edad,sexo,fuente_tip_con,ubicacion,estado,id_nacionalidad,fec_ing_pais,fec_sint,fec_con,fec_dia,fec_hos,fec_alta,fec_mue,fec_rec,clasfinal,tipo_rec,id_grupo_edad,quinquenio,se_fec_sint,barrio,anyo,dpto_muni) VALUES ".$sql;
                                $bien = $this->ConsultasModel->masivo($sql);
                                if($bien){
                                    $sql = '';
                                    $mensaje = 'Finalizo la inserción de los resultados sin ningún contratiempo.';
                                }
                            }
                            if($werror){
                                $error = ($error)? ($error.', '.$werror):$werror;
                            }
                        }
                        
                        $posicion++;
                        if($posicion%50==0) {
                            if($sql){
                          //      $sql = "INSERT INTO reporte (id_evento,fec_not,semana,edad,id_nacionalidad,sexo,id_muni_ocu,localidad,cen_poblado,vereda,bar_ver,tip_ss,cod_ase,fec_con,fec_sint,pac_hos,fec_hos,anyo) VALUES ".$sql;
                          //      $bien = $this->ConsultasModel->masivo($sql);
                                if($bien){
                                    $sql = '';
                                }
                            }
                        }
                    }
                    fclose($students); //Cierra el archivo
                } else {
                    $error = " La estructura interna del documento no corresponde al csv inicial.";
                }
        }

        if($error){
            $mensaje = $mensaje.'<br>Pero las siguientes líneas presentan errores: '.$error.'.';
        }
        if($mensaje){
            $this->template->add_message(array("info" => $mensaje));
            $_POST = '';
        }

        $this->template->set('item_sidebar_active', 'carga_cov');
        $this->template->set('content_header', 'Cargar Resultados - Evento COVID');
        $this->template->set('content_sub_header', '');
        $this->template->render('consultas/carga_covid');
	}

    private function  corrige_fecha($fecha)
    {
        $tiene = substr_count($fecha, '/');
        $tiene2 = substr_count($fecha, '-');
        $tip = ($tiene == 2) ? '/' : (($tiene2 == 2) ? '-' : '');
        $fechafinal = '';
        if ($tip) {
            $partesfecha = explode($tip, $fecha);
            $uno = strlen($partesfecha[0]);
            $dos = strlen($partesfecha[1]);
            $tres = strlen($partesfecha[2]);

            if (is_int($uno) && is_int($dos) && is_int($tres)) {
                if ($uno == '4') {
                    if ($partesfecha[1] > 12) {
                        $fechafinal = $partesfecha[0] . '-' . $partesfecha[2] . '-' . $partesfecha[1];
                    } else {
                        $fechafinal = $partesfecha[0] . '-' . $partesfecha[1] . '-' . $partesfecha[2];
                    }
                }
                if ($tres == '4') {
                    if ($partesfecha[1] > 12) {
                        $fechafinal = $partesfecha[2] . '-' . $partesfecha[0] . '-' . $partesfecha[1];
                    } else {
                        $fechafinal = $partesfecha[2] . '-' . $partesfecha[1] . '-' . $partesfecha[0];
                    }

                }
                if($fechafinal){
                    return $fechafinal;
                }
                return '0000-00-00';
            }
        }
        return '0000-00-00';
    }

}
