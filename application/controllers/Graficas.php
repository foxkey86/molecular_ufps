<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Graficas extends CMS_Controller {

	 function __construct()
	 {
		 parent::__construct();

	//	 $this->template->add_css('custom/style');

		 $user = array();
		 if ($this->session->userdata(SESSION_NAME)) {
			$user = $this->session->userdata(SESSION_NAME);
		}else{
	//		redirect();
		}
		
		$this->load->model("PerfilModel");
	//	$this->template->set_template('dashboard');
		$this->template->set_template('indicadores');
		$this->load->model("GraficasModel");

	 }

 
	public function index()
	{

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'graficas');
		$this->template->set('content_header', 'Graficas');
        $this->template->set('content_sub_header', 'Consulte las graficas realizadas');
        $this->template->render('graficas/index');


		
	}

	
	public function google()
	{

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'graficas_go');
		$this->template->set('content_header', 'Graficas');
        $this->template->set('content_sub_header', 'Consulte las graficas realizadas');
        $this->template->render('graficas/google');


		
	}


	public function propio()
	{

		$this->template->add_js("custom/estadistica");	
		$this->template->add_js("js/statistics/peity/peity.bundle");
		$this->template->add_js("js/statistics/flot/flot.bundle");
		$this->template->add_js("js/statistics/easypiechart/easypiechart.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");

	

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'propio');
		$this->template->set('content_header', 'Graficas');
        $this->template->set('content_sub_header', 'Consulte las graficas realizadas');
        $this->template->render('graficas/propio');


		
	}

	public function mapa()
	{

		$this->template->add_js("custom/estadistica");	
		$this->template->add_js("js/statistics/peity/peity.bundle");
		$this->template->add_js("js/statistics/flot/flot.bundle");
		$this->template->add_js("js/statistics/easypiechart/easypiechart.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");

	

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'mapa');
		$this->template->set('content_header', 'Graficas');
        $this->template->set('content_sub_header', 'Consulte las graficas realizadas');
        $this->template->render('graficas/mapa');


		
	}

	public function periodos()
	{

	//	$this->template->add_js("custom/estadistica");	
		
		$this->template->add_js("custom/indicadores");	
		$this->template->add_js("js/statistics/peity/peity.bundle");
		$this->template->add_js("js/statistics/flot/jquery.flot.axislabels");
		$this->template->add_js("js/statistics/flot/flot.bundle");
		$this->template->add_js("js/statistics/easypiechart/easypiechart.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");

		if($this->input->post('arreglo')){
			$aux = explode("_", $this->input->post('arreglo'));
				
			if($aux[0] == 1 || $aux[0] == 2){
				redirect('graficas/dengue');
			}
			if($aux[0] == 3 || $aux[0] == 4 || $aux[0] == 5 || $aux[0] == 6 || $aux[0] == 7 || $aux[0] == 8){
				redirect('graficas/hepatitis');
			}
			if($aux[0] == 9 || $aux[0] == 10){
				redirect('graficas/tuberculosis');
			}
			if($aux[0] == 11){
				redirect('graficas/vih');
			}
			if($aux[0] == 13 || $aux[0] == 14 || $aux[0] == 15){
				redirect('graficas/covid');
			}

		}

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'periodos');
		$this->template->set('content_header', 'Indicadores');
        $this->template->set('content_sub_header', 'Consulte las graficas enfermedades de norte de santander');
        $this->template->render('graficas/periodos');
		
	}


	public function dengue()
	{

		$this->template->add_js("custom/estadistica");	
		$this->template->add_js("js/statistics/peity/peity.bundle");
		$this->template->add_js("js/statistics/flot/jquery.flot.axislabels");
		$this->template->add_js("js/statistics/flot/flot.bundle");
		$this->template->add_js("js/statistics/easypiechart/easypiechart.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'dengue');
		$this->template->set('content_header', 'Graficas Dengue 2019');
        $this->template->set('content_sub_header', 'Consulte las graficas enfermedad');
        $this->template->render('graficas/enfermedad');
	}
	
	public function jsonSemanasDengue() {
	//	echo json_encode(array('error' => 'No encontro la información.' ));
	//	exit;
   //     $this->validar_ajax(); echo json_encode(array('carga' => true, 'codigo_recibo' => $referencia_pago,'total'=>$total,'url'=>$url_pago,'descripcion'=>$descripcion));
		$this->load->model("GraficasModel");

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

        if ($id_evento) {
			$semanas = $this->GraficasModel->getEnfermedad($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			array_push($data, array(0, 0));
			foreach($semanas as $row):
				array_push($data, array($row->semana, $row->num));
			endforeach;
			echo json_encode(array('carga' => true, 'data' => $data));
        }else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
    }

	public function jsonSemanasHep() {
			$this->load->model("GraficasModel");
			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));
	
			if ($id_evento) {
				$semanas = $this->GraficasModel->getEnfermedadHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				array_push($data, array(0, 0));
				foreach($semanas as $row):
					array_push($data, array($row->semana, $row->num));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

	public function jsonSemanasTub() {
		$this->load->model("GraficasModel");
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$semanas = $this->GraficasModel->getEnfermedadTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			array_push($data, array(0, 0));
			foreach($semanas as $row):
				array_push($data, array($row->semana, $row->num));
			endforeach;
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function rangoTri($trimestre){
		switch ($trimestre) {
			case 1:
				return array(1,12);
			case 2:
				return array(13,24);
			case 3:
				return array(25,36);
			case 4:
				return array(37,53);
			case 5:
				return array(1,53);
		}
	}

	public function rangoTri2020($trimestre){
		switch ($trimestre) {
			case 1:
				return array(1,14);
			case 2:
				return array(15,28);
			case 3:
				return array(29,40);
			case 4:
				return array(41,53);
			case 5:
				return array(1,53);
		}
	}

	public function rangoTriCov2020($trimestre){
		switch ($trimestre) {
			case 1:
				return array(1,10);
			case 2:
				return array(11,28);
			case 3:
				return array(29,40);
			case 4:
				return array(41,53);
			case 5:
				return array(1,53);
		}
	}

	public function indicadorAClasFinal($indicador){
		switch ($indicador) {
			case 1:
				return 1;
			case 2:
				return 2;
			case 3:
				return 1;
			case 4:
				return 2;
			case 5:
				return 3;
			case 6:
				return 4;
			case 7:
				return 5;
			case 8:
				return 6;
			case 9:
				return 1;
			case 10:
				return 2;
			case 11:
				return 3;
			case 13:
				return 1;
			case 14:
				return 2;
			case 15:
				return 3;
		}
	}

	public function indicadorAEvento($indicador){
		switch ($indicador) {
			case 1:
				return 1;
			case 2:
				return 1;
			case 3:
				return 2;
			case 4:
				return 2;
			case 5:
				return 2;
			case 6:
				return 2;
			case 7:
				return 2;			
			case 8:
				return 2;
			case 9:
				return 3;
			case 10:
				return 3;
			case 11:
				return 4;
			case 12:
				return 4;
			case 13:
				return 5;
			case 14:
				return 5;
			case 15:
				return 5;
		}
	}

	public function indicadorAEventoNom($indicador){
		switch ($indicador) {
			case 1:
				return 'Dengue sin signos de alarma';
			case 2:
				return 'Dengue con signos de alarma';
			case 3:
				return 'Hepatitis A';
			case 4:
				return 'Hepatitis B Aguda';
			case 5:
				return 'Hepatitis B Crónica';
			case 6:
				return 'Hepatitis B por Transmisión Perinatal';
			case 7:
				return 'Hepatitis Coinfección B-D';			
			case 8:
				return 'Hepatitis C';
			case 9:
				return 'TB Pulmonares';
			case 10:
				return 'TB Extrapulmonares';
			case 11:
				return 'VIH';
			case 12:
				return 'VIH con coinfección TB';
			case 13:
				return 'COVID confirmados';
			case 14:
				return 'COVID Recuperados';
			case 15:
				return 'COVID Fallecidos';
		}
	}

	public function ubicacionDptoMuni($ubicacion){
		if($ubicacion == 'Norte de Santander'){
			return 1;
		}
		if($ubicacion == 'Cúcuta'){
			return 2;
		}
		if($ubicacion == 'Villa del Rosario'){
			return 2;
		}
		if($ubicacion == 'Los Patios'){
			return 2;
		}
		if($ubicacion == 'El Zulia'){
			return 2;
		}
	}

	public function ubicacionMuni($ubicacion){
		if($ubicacion == 'Cúcuta'){
			return 1;
		}
		if($ubicacion == 'Villa del Rosario'){
			return 40;
		}
		if($ubicacion == 'Los Patios'){
			return 22;
		}
		if($ubicacion == 'El Zulia'){
			return 15;
		}
		if($ubicacion == 'Teorama'){
			return 36;
		}
		if($ubicacion == 'Convencion'){
			return 10;
		}
		if($ubicacion == 'El Carmen'){
			return 13;
		}
		if($ubicacion == 'Tibu'){
			return 37;
		}
		if($ubicacion == 'Toledo'){
			return 38;
		}
		return 0;
	}

	public function jsonAfiliacionDengue() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getAfiliacion($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$a_0 = array(0,0);
				$a_2 = array(2,0);
				$a_4 = array(4,0);
				$a_6 = array(6,0);
				$a_8 = array(8,0);
				$a_10 = array(10,0);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->tip_ss) {
						case 1:
							$a_0 = array(0,$row->num);
							break;
						case 2:
							$a_2 = array(2,$row->num);
							break;
						case 3:
							$a_4 = array(4,$row->num);
							break;
						case 4:
							$a_6 = array(6,$row->num);
							break;
						case 5:
							$a_8 = array(8,$row->num);
							break;
						case 6:
							$a_10 = array(10,$row->num);
							break;
					}
				endforeach;
				array_push($data, $a_0);
				array_push($data, $a_2);
				array_push($data, $a_4);
				array_push($data, $a_6);
				array_push($data, $a_8);
				array_push($data, $a_10);
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonAfiliacionHep() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}	
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getAfiliacionHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$a_0 = array(0,0);
				$a_2 = array(2,0);
				$a_4 = array(4,0);
				$a_6 = array(6,0);
				$a_8 = array(8,0);
				$a_10 = array(10,0);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->tip_ss) {
						case 1:
							$a_0 = array(0,$row->num);
							break;
						case 2:
							$a_2 = array(2,$row->num);
							break;
						case 3:
							$a_4 = array(4,$row->num);
							break;
						case 4:
							$a_6 = array(6,$row->num);
							break;
						case 5:
							$a_8 = array(8,$row->num);
							break;
						case 6:
							$a_10 = array(10,$row->num);
							break;
					}
				endforeach;
				array_push($data, $a_0);
				array_push($data, $a_2);
				array_push($data, $a_4);
				array_push($data, $a_6);
				array_push($data, $a_8);
				array_push($data, $a_10);
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonAfiliacionTub() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
	
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getAfiliacionTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->tip_ss) {
						case 1:
							$i = 0;
							break;
						case 2:
							$i = 2;
							break;
						case 3:
							$i = 4;
							break;
						case 4:
							$i = 6;
							break;
						case 5:
							$i = 8;
							break;
						case 6:
							$i = 10;
							break;
					}
					array_push($data, array($i, $row->num));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonDptoEntIndTub()
		{
			$this->load->model("GraficasModel");	
	
			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
	
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

				if ($id_evento) {
					$result = $this->GraficasModel->getDptoEntIndTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
					$data = array();
					$data2 = array();
					$i = 0;
					foreach($result as $row):
						if($row->nombre_grupo !=null){
							$i++;
							array_push($data, array($row->num, $i));
							array_push($data2, array($i, $row->nombre_grupo));
						}
					endforeach;
					echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
				}else{
					echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
				}
				exit;
		}

		public function jsonEtniaTub() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
	
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getEtniaTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$a_0 = array(0,0);
				$a_2 = array(2,0);
				$a_4 = array(4,0);
				$a_6 = array(6,0);
				$a_8 = array(8,0);
				$a_10 = array(10,0);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->per_etn) {
						case 1:
							$a_0 = array(0,$row->num);
							break;
						case 2:
							$a_2 = array(2,$row->num);
							break;
						case 3:
							$a_4 = array(4,$row->num);
							break;
						case 4:
							$a_6 = array(6,$row->num);
							break;
						case 5:
							$a_8 = array(8,$row->num);
							break;
						case 6:
							$a_10 = array(10,$row->num);
							break;
					}
				endforeach;
				array_push($data, $a_0);
				array_push($data, $a_2);
				array_push($data, $a_4);
				array_push($data, $a_6);
				array_push($data, $a_8);
				array_push($data, $a_10);
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonEtniaTodos() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
	
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				echo json_encode(array('carga' => true, 'data' => $dpto_muni)); exit;
				$result = $this->GraficasModel->getEtniaMuniTodos($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$a_0 = array(0,0);
				$a_2 = array(2,0);
				$a_4 = array(4,0);
				$a_6 = array(6,0);
				$a_8 = array(8,0);
				$a_10 = array(10,0);
				$data = array();
				foreach($result as $row):
					switch($row->per_etn) {
						case 1:
							$a_0 = array(0,$row->num);
							break;
						case 2:
							$a_2 = array(2,$row->num);
							break;
						case 3:
							$a_4 = array(4,$row->num);
							break;
						case 4:
							$a_6 = array(6,$row->num);
							break;
						case 5:
							$a_8 = array(8,$row->num);
							break;
						case 6:
							$a_10 = array(10,$row->num);
							break;
					}
				endforeach;
				array_push($data, $a_0);
				array_push($data, $a_2);
				array_push($data, $a_4);
				array_push($data, $a_6);
				array_push($data, $a_8);
				array_push($data, $a_10);
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonEtniaCucTub() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
	
			if($this->input->post('dpto_muni') == 'Norte de Santander'){
				$muni = $this->ubicacionMuni('Cúcuta');
			}else{
				$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));
			}
			if ($id_evento) {
				$result = $this->GraficasModel->getEtniaMuniTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$a_0 = array(0,0);
				$a_2 = array(2,0);
				$a_4 = array(4,0);
				$a_6 = array(6,0);
				$a_8 = array(8,0);
				$a_10 = array(10,0);
				$data = array();
				foreach($result as $row):
					switch($row->per_etn) {
						case 1:
							$a_0 = array(0,$row->num);
							break;
						case 2:
							$a_2 = array(2,$row->num);
							break;
						case 3:
							$a_4 = array(4,$row->num);
							break;
						case 4:
							$a_6 = array(6,$row->num);
							break;
						case 5:
							$a_8 = array(8,$row->num);
							break;
						case 6:
							$a_10 = array(10,$row->num);
							break;
					}
				endforeach;
				array_push($data, $a_0);
				array_push($data, $a_2);
				array_push($data, $a_4);
				array_push($data, $a_6);
				array_push($data, $a_8);
				array_push($data, $a_10);
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonEtniaTeoTub() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
	
			$muni = $this->ubicacionMuni('Teorama');

			if ($id_evento) {
				$result = $this->GraficasModel->getEtniaMuniTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$a_0 = array(0,0);
				$a_2 = array(2,0);
				$a_4 = array(4,0);
				$a_6 = array(6,0);
				$a_8 = array(8,0);
				$a_10 = array(10,0);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->per_etn) {
						case 1:
							$a_0 = array(0,$row->num);
							break;
						case 2:
							$a_2 = array(2,$row->num);
							break;
						case 3:
							$a_4 = array(4,$row->num);
							break;
						case 4:
							$a_6 = array(6,$row->num);
							break;
						case 5:
							$a_8 = array(8,$row->num);
							break;
						case 6:
							$a_10 = array(10,$row->num);
							break;
					}
				endforeach;
				array_push($data, $a_0);
				array_push($data, $a_2);
				array_push($data, $a_4);
				array_push($data, $a_6);
				array_push($data, $a_8);
				array_push($data, $a_10);
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonEtniaConTub() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
	
			$muni = $this->ubicacionMuni('Convencion');

			if ($id_evento) {
				$result = $this->GraficasModel->getEtniaMuniTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$a_0 = array(0,0);
				$a_2 = array(2,0);
				$a_4 = array(4,0);
				$a_6 = array(6,0);
				$a_8 = array(8,0);
				$a_10 = array(10,0);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->per_etn) {
						case 1:
							$a_0 = array(0,$row->num);
							break;
						case 2:
							$a_2 = array(2,$row->num);
							break;
						case 3:
							$a_4 = array(4,$row->num);
							break;
						case 4:
							$a_6 = array(6,$row->num);
							break;
						case 5:
							$a_8 = array(8,$row->num);
							break;
						case 6:
							$a_10 = array(10,$row->num);
							break;
					}
				endforeach;
				array_push($data, $a_0);
				array_push($data, $a_2);
				array_push($data, $a_4);
				array_push($data, $a_6);
				array_push($data, $a_8);
				array_push($data, $a_10);
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonEtniaCarTub() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
	
			$muni = $this->ubicacionMuni('El Carmen');

			if ($id_evento) {
				$result = $this->GraficasModel->getEtniaMuniTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$a_0 = array(0,0);
				$a_2 = array(2,0);
				$a_4 = array(4,0);
				$a_6 = array(6,0);
				$a_8 = array(8,0);
				$a_10 = array(10,0);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->per_etn) {
						case 1:
							$a_0 = array(0,$row->num);
							break;
						case 2:
							$a_2 = array(2,$row->num);
							break;
						case 3:
							$a_4 = array(4,$row->num);
							break;
						case 4:
							$a_6 = array(6,$row->num);
							break;
						case 5:
							$a_8 = array(8,$row->num);
							break;
						case 6:
							$a_10 = array(10,$row->num);
							break;
					}
				endforeach;
				array_push($data, $a_0);
				array_push($data, $a_2);
				array_push($data, $a_4);
				array_push($data, $a_6);
				array_push($data, $a_8);
				array_push($data, $a_10);
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonEtniaTibTub() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
	
			$muni = $this->ubicacionMuni('Tibu');

			if ($id_evento) {
				$result = $this->GraficasModel->getEtniaMuniTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$a_0 = array(0,0);
				$a_2 = array(2,0);
				$a_4 = array(4,0);
				$a_6 = array(6,0);
				$a_8 = array(8,0);
				$a_10 = array(10,0);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->per_etn) {
						case 1:
							$a_0 = array(0,$row->num);
							break;
						case 2:
							$a_2 = array(2,$row->num);
							break;
						case 3:
							$a_4 = array(4,$row->num);
							break;
						case 4:
							$a_6 = array(6,$row->num);
							break;
						case 5:
							$a_8 = array(8,$row->num);
							break;
						case 6:
							$a_10 = array(10,$row->num);
							break;
					}
				endforeach;
				array_push($data, $a_0);
				array_push($data, $a_2);
				array_push($data, $a_4);
				array_push($data, $a_6);
				array_push($data, $a_8);
				array_push($data, $a_10);
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonEtniaTolTub() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
	
			$muni = $this->ubicacionMuni('Toledo');

			if ($id_evento) {
				$result = $this->GraficasModel->getEtniaMuniTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$a_0 = array(0,0);
				$a_2 = array(2,0);
				$a_4 = array(4,0);
				$a_6 = array(6,0);
				$a_8 = array(8,0);
				$a_10 = array(10,0);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->per_etn) {
						case 1:
							$a_0 = array(0,$row->num);
							break;
						case 2:
							$a_2 = array(2,$row->num);
							break;
						case 3:
							$a_4 = array(4,$row->num);
							break;
						case 4:
							$a_6 = array(6,$row->num);
							break;
						case 5:
							$a_8 = array(8,$row->num);
							break;
						case 6:
							$a_10 = array(10,$row->num);
							break;
					}
				endforeach;
				array_push($data, $a_0);
				array_push($data, $a_2);
				array_push($data, $a_4);
				array_push($data, $a_6);
				array_push($data, $a_8);
				array_push($data, $a_10);
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonDiaTub(){

			$this->load->model("GraficasModel");	
			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
	
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));
	
			if ($id_evento) {
				$result = $this->GraficasModel->getDiaTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				$si = false;
				$no = false;
				foreach($result as $row):
					$i = 0;
					switch($row->diabetes) {
						case 1:
							$i = 0;
							$si = true;
							break;
						case 2:
							$i = 1;
							$no = true;
							break;
					}
					array_push($data, array($i, $row->num));
				endforeach;
				if($si && !$no){
					array_push($data, array(1, 0));	
				}
				if(!$si && $no){
					$aux = $data; $data = array();
					array_push($data, array(0, 0));	
					array_push($data, $aux[0]);	
				}
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
	
		}

		public function jsonRenTub(){

			$this->load->model("GraficasModel");	
			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
	
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));
	
			if ($id_evento) {
				$result = $this->GraficasModel->getRenTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				$si = false;
				$no = false;
				foreach($result as $row):
					$i = 0;
					switch($row->enfe_renal) {
						case 1:
							$i = 0;
							$si = true;
							break;
						case 2:
							$i = 1;
							$no = true;
							break;
					}
					array_push($data, array($i, $row->num));
				endforeach;
				if($si && !$no){
					array_push($data, array(1, 0));	
				}
				if(!$si && $no){
					$aux = $data; $data = array();
					array_push($data, array(0, 0));	
					array_push($data, $aux[0]);	
				}
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
	
		}

		public function jsonDesTub(){

			$this->load->model("GraficasModel");	
			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
	
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));
	
			if ($id_evento) {
				$result = $this->GraficasModel->getDesTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				$si = false;
				$no = false;
				foreach($result as $row):
					$i = 0;
					switch($row->desnutrici) {
						case 1:
							$i = 0;
							$si = true;
							break;
						case 2:
							$i = 1;
							$no = true;
							break;
					}
					array_push($data, array($i, $row->num));
				endforeach;
				if($si && !$no){
					array_push($data, array(1, 0));	
				}
				if(!$si && $no){
					$aux = $data; $data = array();
					array_push($data, array(0, 0));	
					array_push($data, $aux[0]);	
				}
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
	
		}

		public function jsonTrabTub(){

			$this->load->model("GraficasModel");	
			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
	
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));
	
			if ($id_evento) {
				$result = $this->GraficasModel->getTrabTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				$si = false;
				$no = false;
				foreach($result as $row):
					$i = 0;
					switch($row->trab_salud) {
						case 1:
							$i = 0;
							$si = true;
							break;
						case 2:
							$i = 1;
							$no = true;
							break;
					}
					array_push($data, array($i, $row->num));
				endforeach;
				if($si && !$no){
					array_push($data, array(1, 0));	
				}
				if(!$si && $no){
					$aux = $data; $data = array();
					array_push($data, array(0, 0));	
					array_push($data, $aux[0]);	
				}
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
	
		}

		public function jsonAfiliacionVih() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
	
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getAfiliacionVih($id_evento, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->tip_ss) {
						case 1:
							$i = 0;
							break;
						case 2:
							$i = 2;
							break;
						case 3:
							$i = 4;
							break;
						case 4:
							$i = 6;
							break;
						case 5:
							$i = 8;
							break;
						case 6:
							$i = 10;
							break;
					}
					array_push($data, array($i, $row->num));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonTransmisionHep() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getTransmision($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$a_0 = array(0,0);
				$a_2 = array(2,0);
				$a_4 = array(4,0);
				$a_6 = array(6,0);
				$a_8 = array(8,0);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->met_tra) {
						case 1:
							$a_0 = array(0,$row->num);
							break;
						case 2:
							$a_2 = array(2,$row->num);
							break;
						case 3:
							$a_4 = array(4,$row->num);
							break;
						case 4:
							$a_6 = array(6,$row->num);
							break;
						case 5:
							$a_8 = array(8,$row->num);
							break;
					}
				endforeach;
				array_push($data, $a_0);
				array_push($data, $a_2);
				array_push($data, $a_4);
				array_push($data, $a_6);
				array_push($data, $a_8);
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonEdadesDengue() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$data = array();
				$result = $this->GraficasModel->getEdades($id_evento, $anyo, 0, 4, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(0, $result->num));
				$result = $this->GraficasModel->getEdades($id_evento, $anyo, 5, 14, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(2, $result->num));
				$result = $this->GraficasModel->getEdades($id_evento, $anyo, 15, 44, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(4, $result->num));
				$result = $this->GraficasModel->getEdades($id_evento, $anyo, 45, 64, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(6, $result->num));
				$result = $this->GraficasModel->getEdades($id_evento, $anyo, 65, 120, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(8, $result->num));
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}

		public function jsonEdadesHep() {

			$this->load->model("GraficasModel");	

			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$data = array();
				$result = $this->GraficasModel->getEdadesHep($id_evento, $anyo, 0, 5, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(0, $result->num));
				$result = $this->GraficasModel->getEdadesHep($id_evento, $anyo, 6, 14, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(2, $result->num));
				$result = $this->GraficasModel->getEdadesHep($id_evento, $anyo, 15, 45, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(4, $result->num));
				$result = $this->GraficasModel->getEdadesHep($id_evento, $anyo, 46, 120, $dpto_muni, $clasfinal, $trimestre, $muni);
				array_push($data, array(6, $result->num));
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}


		public function jsonHospitalizadoDengue() {

			$this->load->model("GraficasModel");	
			$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
			$anyo = $this->input->post('anyo');
			$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
			$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
			if($anyo == 2020){
				$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
			}else{
				$trimestre = $this->rangoTri($this->input->post('trimestre'));
			}
			$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getHospitalizados($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				foreach($result as $row):
					$i = 0;
					switch($row->pac_hos) {
						case 1:
							$i = 0;
							break;
						case 2:
							$i = 1;
							break;
					}
					array_push($data, array($i, $row->num));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			exit;
		}


	public function jsonBarOcuDengue()
	{
		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getBarOcu($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;

					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->bar_ver));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}

	public function jsonBarOcuTub()
	{
		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getBarOcuTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;

					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->bar_ver));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}

	public function jsonBarOcuVih()
	{
		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getBarOcuVih($id_evento, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;

					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->bar_ver));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}


	public function jsonBarOcuHep()
	{
		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getBarOcuHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;

					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->bar_ver));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}


	public function jsonMunOcuDengue()
	{
		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

			if ($id_evento) {
				$result = $this->GraficasModel->getMuniOcu($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;
					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->muni));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}

	
	public function jsonNacOcuTub()
	{
		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getNacTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;
					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->pais));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}

	public function jsonNacOcuVih()
	{
		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

			if ($id_evento) {
				$result = $this->GraficasModel->getNacVih($id_evento, $anyo, $trimestre, $dpto_muni, $muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;
					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->pais));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}


	public function jsonMunOcuHep()
	{
		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

			if ($id_evento) {
				$result = $this->GraficasModel->getMuniOcuHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;
					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->muni));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}

	public function jsonMunOcuTub()
	{
		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}


			if ($id_evento) {
				$result = $this->GraficasModel->getMuniOcuTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;
					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->muni));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}

	public function jsonMunOcuVih()
	{
		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}


			if ($id_evento) {
				$result = $this->GraficasModel->getMuniOcuVih($id_evento, $anyo, $trimestre, $dpto_muni);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;
					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->muni));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}


	public function jsonGestantesHep() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getGestantesHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$a_0 = array(0,0);
			$a_1 = array(1,0);
			$data = array();
			foreach($result as $row):
				$i = 0;
				switch($row->gp_gestan) {
					case 1:
						$a_0 = array(0,$row->num);
						break;
					case 2:
						$a_1 = array(1,$row->num);
						break;
				}
			endforeach;
			array_push($data, $a_0);
			array_push($data, $a_1);
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonDiscapaHep() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getDiscapaHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			$si = false;
			$no = false;
			foreach($result as $row):
				$i = 0;
				switch($row->gp_discapa) {
					case 1:
						$i = 0;
						$si = true;
						break;
					case 2:
						$i = 1;
						$no = true;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			if($si && !$no){
				array_push($data, array(1, 0));	
			}
			if(!$si && $no){
				$aux = $data; $data = array();
				array_push($data, array(0, '0'));	
				array_push($data, $aux[0]);	
			}
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonDesplaHep() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getDesplaHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			$si = false;
			$no = false;
			foreach($result as $row):
				$i = 0;
				switch($row->gp_desplaz) {
					case 1:
						$i = 0;
						$si = true;
						break;
					case 2:
						$i = 1;
						$no = true;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			if($si && !$no){
				array_push($data, array(1, 0));	
			}
			if(!$si && $no){
				$aux = $data; $data = array();
				array_push($data, array(0, 0));	
				array_push($data, $aux[0]);	
			}
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonMigraHep() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getMigraHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			$si = false;
			$no = false;
			foreach($result as $row):
				$i = 0;
				switch($row->gp_migrant) {
					case 1:
						$i = 0;
						$si = true;
						break;
					case 2:
						$i = 1;
						$no = true;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			if($si && !$no){
				array_push($data, array(1, 0));	
			}
			if(!$si && $no){
				$aux = $data; $data = array();
				array_push($data, array(0, 0));	
				array_push($data, $aux[0]);	
			}
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonCarceHep() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getCarceHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			$si = false;
			$no = false;
			foreach($result as $row):
				$i = 0;
				switch($row->gp_carcela) {
					case 1:
						$i = 0;
						$si = true;
						break;
					case 2:
						$i = 1;
						$no = true;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			if($si && !$no){
				array_push($data, array(1, 0));	
			}
			if(!$si && $no){
				$aux = $data; $data = array();
				array_push($data, array(0, 0));	
				array_push($data, $aux[0]);	
			}
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonIndiHep() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getIndiHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			$si = false;
			$no = false;
			foreach($result as $row):
				$i = 0;
				switch($row->gp_indigen) {
					case 1:
						$i = 0;
						$si = true;
						break;
					case 2:
						$i = 1;
						$no = true;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			if($si && !$no){
				array_push($data, array(1, 0));	
			}
			if(!$si && $no){
				$aux = $data; $data = array();
				array_push($data, array(0, 0));	
				array_push($data, $aux[0]);	
			}
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonIcbfHep() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getIcbfHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			$si = false;
			$no = false;
			foreach($result as $row):
				$i = 0;
				switch($row->gp_pobicbf) {
					case 1:
						$i = 0;
						$si = true;
						break;
					case 2:
						$i = 1;
						$no = true;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			if($si && !$no){
				array_push($data, array(1, 0));	
			}
			if(!$si && $no){
				$aux = $data; $data = array();
				array_push($data, array(0, 0));	
				array_push($data, $aux[0]);	
			}
			if(count($data) > 0){
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonMadComHep(){

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getMadComHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			$si = false;
			$no = false;
			foreach($result as $row):
				$i = 0;
				switch($row->gp_mad_com) {
					case 1:
						$i = 0;
						$si = true;
						break;
					case 2:
						$i = 1;
						$no = true;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			if($si && !$no){
				array_push($data, array(1, 0));	
			}
			if(!$si && $no){
				$aux = $data; $data = array();
				array_push($data, array(0, 0));	
				array_push($data, $aux[0]);	
			}
			if(count($data) > 0){
				echo json_encode(array('carga' => true, 'data' => $data));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;

	}

	public function jsonDesmoHep(){

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getDesmoHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			$si = false;
			$no = false;
			foreach($result as $row):
				$i = 0;
				switch($row->gp_desmovi) {
					case 1:
						$i = 0;
						$si = true;
						break;
					case 2:
						$i = 1;
						$no = true;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			if($si && !$no){
				array_push($data, array(1, 0));	
			}
			if(!$si && $no){
				$aux = $data; $data = array();
				array_push($data, array(0, 0));	
				array_push($data, $aux[0]);	
			}
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;

	}

	public function jsonPsiquiHep(){

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getPsiquiHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			$si = false;
			$no = false;
			foreach($result as $row):
				$i = 0;
				switch($row->gp_psiquia) {
					case 1:
						$i = 0;
						$si = true;
						break;
					case 2:
						$i = 1;
						$no = true;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			if($si && !$no){
				array_push($data, array(1, 0));	
			}
			if(!$si && $no){
				$aux = $data; $data = array();
				array_push($data, array(0, 0));	
				array_push($data, $aux[0]);	
			}
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;

	}

	public function jsonVicHep(){

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getVicHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			$si = false;
			$no = false;
			foreach($result as $row):
				$i = 0;
				switch($row->gp_vic_vio) {
					case 1:
						$i = 0;
						$si = true;
						break;
					case 2:
						$i = 1;
						$no = true;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			if($si && !$no){
				array_push($data, array(1, 0));	
			}
			if(!$si && $no){
				$aux = $data; $data = array();
				array_push($data, array(0, 0));	
				array_push($data, $aux[0]);	
			}
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;

	}

	public function jsonOtrosHep(){

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getOtrosHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			$si = false;
			$no = false;
			foreach($result as $row):
				$i = 0;
				switch($row->gp_otros) {
					case 1:
						$i = 0;
						$si = true;
						break;
					case 2:
						$i = 1;
						$no = true;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			if($si && !$no){
				array_push($data, array(1, 0));	
			}
			if(!$si && $no){
				$aux = $data; $data = array();
				array_push($data, array(0, 0));	
				array_push($data, $aux[0]);	
			}
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;

	}

	public function jsonCoinfeccionHep() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getConinfeccionHep($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$a_0 = array(0,0);
			$a_1 = array(1,0);
			$data = array();
			foreach($result as $row):
				$i = 0;
				switch($row->coinf_vih_coinfeccion) {
					case 1:
						$a_0 = array(0,$row->num);
						break;
					case 2:
						$a_1 = array(1,$row->num);
						break;
				}
			endforeach;
			array_push($data, $a_0);
			array_push($data, $a_1);
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonTotalDengue() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}
		
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		$result = array();

		if ($id_evento) {
			$result_query = $this->GraficasModel->getGeneroDengue($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			
			if(count($result_query) == 1){
				if($result_query[0]->sexo == 'F'){
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => $result_query[0]->num));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => 0));
				}else{
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => $result_query[0]->num));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => 0));
				}
			}else{
				if(count($result_query) == 0){
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => 0));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => 0));
				}else{
					$result = $result_query;
				}
			}
			$nom_evento = $this->indicadorAEventoNom($this->input->post('indicador'));
			array_push($result, array('enfermedad' => $nom_evento));
			array_push($result, array('ubicacion' => $this->input->post('dpto_muni')));
			array_push($result, array('anyo' => $this->input->post('anyo')));
			if($this->input->post('trimestre') == 5){
				$trime = 'Todos';
			}else{
				$trime = $this->input->post('trimestre');
			}
			array_push($result, array('trimestre' => $trime));
			echo json_encode(array('carga' => true, 'data' => $result));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonTotalHepatitis() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}
		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		$result = array();
	//	array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => '0'));
	//	array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => '0'));

		if ($id_evento) {
			$result_query = $this->GraficasModel->getGeneroHepatitis($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			if(count($result_query) == 1){
				if($result_query[0]->sexo == 'F'){
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => $result_query[0]->num));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => 0));
				}else{
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => $result_query[0]->num));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => 0));
				}
			}else{
				if(count($result_query) == 0){
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => 0));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => 0));
				}else{
					$result = $result_query;
				}
			}
			
			$nom_evento = $this->indicadorAEventoNom($this->input->post('indicador'));
			array_push($result, array('enfermedad' => $nom_evento));
			array_push($result, array('ubicacion' => $this->input->post('dpto_muni')));
			array_push($result, array('anyo' => $this->input->post('anyo')));
			if($this->input->post('trimestre') == 5){
				$trime = 'Todos';
			}else{
				$trime = $this->input->post('trimestre');
			}
			array_push($result, array('trimestre' => $trime));
			echo json_encode(array('carga' => true, 'data' => $result));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonTotalTub() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		$result = array();

		if ($id_evento) {
			$result_query = $this->GraficasModel->getGeneroTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			if(count($result_query) == 1){
				if($result_query[0]->sexo == 'F'){
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => $result_query[0]->num));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => 0));
				}else{
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => $result_query[0]->num));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => 0));
				}
			}else{
				if(count($result_query) == 0){
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => 0));
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => 0));
				}else{
					$result = $result_query;
				}
			}
			
			$nom_evento = $this->indicadorAEventoNom($this->input->post('indicador'));
			array_push($result, array('enfermedad' => $nom_evento));
			array_push($result, array('ubicacion' => $this->input->post('dpto_muni')));
			array_push($result, array('anyo' => $this->input->post('anyo')));
			if($this->input->post('trimestre') == 5){
				$trime = 'Todos';
			}else{
				$trime = $this->input->post('trimestre');
			}
			array_push($result, array('trimestre' => $trime));
			echo json_encode(array('carga' => true, 'data' => $result));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonTotalVih() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		$result = array();

		if ($id_evento) {
			$result_query = $this->GraficasModel->getGeneroVih($id_evento, $anyo, $trimestre, $dpto_muni, $muni);
			
			if(count($result_query) > 0){
				$pos = $this->_extraerGenero($result_query, 'F');
				if($pos !== null){
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => $result_query[$pos]->num));
				}else{
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => 0));
				}
				$pos = $this->_extraerGenero($result_query, 'M');
				if($pos !== null){
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => $result_query[$pos]->num));
				}else{
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => 0));
				}
				$pos = $this->_extraerGenero($result_query, 'T');
				if($pos !== null){
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'T', 'num' => $result_query[$pos]->num));
				}else{
					array_push($result, array('id_evento' => $id_evento, 'sexo' => 'T', 'num' => 0));
				}
			}else{

				array_push($result, array('id_evento' => $id_evento, 'sexo' => 'F', 'num' => 0));
				array_push($result, array('id_evento' => $id_evento, 'sexo' => 'M', 'num' => 0));
				array_push($result, array('id_evento' => $id_evento, 'sexo' => 'T', 'num' => 0));
			}
		
			
			$nom_evento = $this->indicadorAEventoNom($this->input->post('indicador'));
			array_push($result, array('enfermedad' => $nom_evento));
			array_push($result, array('ubicacion' => $this->input->post('dpto_muni')));
			array_push($result, array('anyo' => $this->input->post('anyo')));
			if($this->input->post('trimestre') == 5){
				$trime = 'Todos';
			}else{
				$trime = $this->input->post('trimestre');
			}
			array_push($result, array('trimestre' => $trime));
			echo json_encode(array('carga' => true, 'data' => $result));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	private function _extraerGenero($listado = null, $se)
    {

        $sexo = null;
        if (count($listado) > 0) {
            for ($i = 0; $i < count($listado); $i++) {
				if($listado[$i]->ide_genero === $se){
					$sexo = $i;
				}
            }
        }

        return $sexo;
	}

	
	public function jsonCondicionTub() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getCondicionTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			foreach($result as $row):
				$i = 0;
				switch($row->cond_tuber) {
					case 1:
						$i = 0;
						break;
					case 2:
						$i = 1;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

 	public function jsonLoginPass(){
		$this->load->model("LoginModel");
		$usuario = $this->input->post('usuario');
		$pass = $this->input->post('pass');
		if($usuario && $pass){
			$persona = $this->LoginModel->getUsuarioPerfil($usuario, $pass, 10);
			if($persona){
				if($persona->estado == 1){
					echo json_encode(array('carga' => true, 'data' => 'IJ9*BXTwKKSf@gbDts')); exit;
				}else{
					echo json_encode(array('carga' => true, 'data' => 'T6zqqMoJB8G$LAz%')); exit;
				}
			}
		}
		echo json_encode(array('carga' => false, 'data' =>  'T6zqqMoJB8G$LAz%')); exit;
	 }

	public function jsonCoinfeccionTbc() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getConinfeccionTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			foreach($result as $row):
				$i = 0;
				switch($row->vih_confirmado) {
					case 1:
						$i = 0;
						break;
					case 2:
						$i = 1;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonMapaTbc() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		$trimestre = $this->input->post('trimestre');


		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));
		if ($id_evento) {
			$result = $this->GraficasModel->getMapaTub($id_evento, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni);
			if(count($result)){
				echo json_encode(array('carga' => true, 'data' => $result[0]));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  []));
			}
			
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonEdadesTub() {

		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		$clasfinal = $this->indicadorAClasFinal($this->input->post('indicador'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$data = array();
			$result = $this->GraficasModel->getEdadesTub($id_evento, $anyo, 0, 14, $dpto_muni, $clasfinal, $trimestre, $muni);
			array_push($data, array(0, $result->num));
			$result = $this->GraficasModel->getEdadesTub($id_evento, $anyo, 15, 24, $dpto_muni, $clasfinal, $trimestre, $muni);
			array_push($data, array(2, $result->num));
			$result = $this->GraficasModel->getEdadesTub($id_evento, $anyo, 25, 34, $dpto_muni, $clasfinal, $trimestre, $muni);
			array_push($data, array(4, $result->num));
			$result = $this->GraficasModel->getEdadesTub($id_evento, $anyo, 35, 44, $dpto_muni, $clasfinal, $trimestre, $muni);
			array_push($data, array(6, $result->num));
			$result = $this->GraficasModel->getEdadesTub($id_evento, $anyo, 45, 54, $dpto_muni, $clasfinal, $trimestre, $muni);
			array_push($data, array(8, $result->num));
			$result = $this->GraficasModel->getEdadesTub($id_evento, $anyo, 55, 64, $dpto_muni, $clasfinal, $trimestre, $muni);
			array_push($data, array(10, $result->num));
			$result = $this->GraficasModel->getEdadesTub($id_evento, $anyo, 65, 120, $dpto_muni, $clasfinal, $trimestre, $muni);
			array_push($data, array(12, $result->num));
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonEdadesVih() {

		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$data = array();
			$result = $this->GraficasModel->getEdadesVih($id_evento, $anyo, 0, 14, $dpto_muni, $trimestre, $muni);
			array_push($data, array(0, $result->num));
			$result = $this->GraficasModel->getEdadesVih($id_evento, $anyo, 15, 24, $dpto_muni, $trimestre, $muni);
			array_push($data, array(2, $result->num));
			$result = $this->GraficasModel->getEdadesVih($id_evento, $anyo, 25, 34, $dpto_muni, $trimestre, $muni);
			array_push($data, array(4, $result->num));
			$result = $this->GraficasModel->getEdadesVih($id_evento, $anyo, 35, 44, $dpto_muni,  $trimestre, $muni);
			array_push($data, array(6, $result->num));
			$result = $this->GraficasModel->getEdadesVih($id_evento, $anyo, 45, 54, $dpto_muni, $trimestre, $muni);
			array_push($data, array(8, $result->num));
			$result = $this->GraficasModel->getEdadesVih($id_evento, $anyo, 55, 64, $dpto_muni, $trimestre, $muni);
			array_push($data, array(10, $result->num));
			$result = $this->GraficasModel->getEdadesVih($id_evento, $anyo, 65, 120, $dpto_muni, $trimestre, $muni);
			array_push($data, array(12, $result->num));
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonEdadesCov() {

		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		if($anyo == 2020){
			$trimestre = $this->rangoTriCov2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));
		$tipo = $this->input->post('tipo');

		if ($id_evento) {
			$data = array();
			$result = $this->GraficasModel->getEdadesCov($anyo, 1, $dpto_muni, $trimestre, $muni, $tipo);
			array_push($data, array(0, $result->num));
			$result = $this->GraficasModel->getEdadesCov($anyo, 2, $dpto_muni, $trimestre, $muni, $tipo);
			array_push($data, array(2, $result->num));
			$result = $this->GraficasModel->getEdadesCov($anyo, 3, $dpto_muni, $trimestre, $muni, $tipo);
			array_push($data, array(4, $result->num));
			$result = $this->GraficasModel->getEdadesCov($anyo, 4, $dpto_muni,  $trimestre, $muni, $tipo);
			array_push($data, array(6, $result->num));
			$result = $this->GraficasModel->getEdadesCov($anyo, 5, $dpto_muni, $trimestre, $muni, $tipo);
			array_push($data, array(8, $result->num));
			$result = $this->GraficasModel->getEdadesCov($anyo, 6, $dpto_muni, $trimestre, $muni, $tipo);
			array_push($data, array(10, $result->num));
			$result = $this->GraficasModel->getEdadesCov($anyo, 7, $dpto_muni, $trimestre, $muni, $tipo);
			array_push($data, array(12, $result->num));
			$result = $this->GraficasModel->getEdadesCov($anyo, 8, $dpto_muni, $trimestre, $muni, $tipo);
			array_push($data, array(14, $result->num));
			$result = $this->GraficasModel->getEdadesCov($anyo, 9, $dpto_muni, $trimestre, $muni, $tipo);
			array_push($data, array(16, $result->num));
			$result = $this->GraficasModel->getEdadesCov($anyo, 10, $dpto_muni, $trimestre, $muni, $tipo);
			array_push($data, array(18, $result->num));
			$result = $this->GraficasModel->getEdadesCov($anyo, 11, $dpto_muni, $trimestre, $muni, $tipo);
			array_push($data, array(20, $result->num));
			$result = $this->GraficasModel->getEdadesCov($anyo, 12, $dpto_muni, $trimestre, $muni, $tipo);
			array_push($data, array(22, $result->num));
			$result = $this->GraficasModel->getEdadesCov($anyo, 13, $dpto_muni, $trimestre, $muni, $tipo);
			array_push($data, array(24, $result->num));
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonTotalCov() {

		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		if($anyo == 2020){
			$trimestre = $this->rangoTriCov2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));
		$count = 0;
		$tipo = $this->input->post('tipo');

		$result = array();

		if ($id_evento) {
			$result_query = $this->GraficasModel->getGeneroCov($anyo, $trimestre, $dpto_muni, $muni, $tipo);
			if(count($result_query) == 1){
				if($result_query[0]->sexo == 'F'){
					array_push($result, array('sexo' => 'F', 'num' => $result_query[0]->num));
					array_push($result, array('sexo' => 'M', 'num' => 0));
					$count += intval($result_query[0]->num);
				}else{
					array_push($result, array('sexo' => 'M', 'num' => $result_query[0]->num));
					array_push($result, array('sexo' => 'F', 'num' => 0));
					$count += intval($result_query[0]->num);
				}
			}else{
				if(count($result_query) == 0){
					array_push($result, array('sexo' => 'F', 'num' => 0));
					array_push($result, array('sexo' => 'M', 'num' => 0));
				}else{
					$result = $result_query;
					$count += intval($result_query[0]->num);
					$count += intval($result_query[1]->num);
				}
			}
			
			$nom_evento = $this->indicadorAEventoNom($this->input->post('indicador'));
			array_push($result, array('enfermedad' => $nom_evento));
			array_push($result, array('ubicacion' => $this->input->post('dpto_muni')));
			array_push($result, array('anyo' => $this->input->post('anyo')));
			array_push($result, array('total' => $count));
			if($this->input->post('trimestre') == 5){
				$trime = 'Todos';
			}else{
				$trime = $this->input->post('trimestre');
			}
			array_push($result, array('trimestre' => $trime));
			echo json_encode(array('carga' => true, 'data' => $result));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function jsonBarOcuCov()
	{
		$this->load->model("GraficasModel");	
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		if($anyo == 2020){
			$trimestre = $this->rangoTriCov2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));
		$tipo = $this->input->post('tipo');


			if ($id_evento) {
				$result = $this->GraficasModel->getBarOcuCov($anyo, $trimestre, $dpto_muni, $muni, $tipo);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;

					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->barrio));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}

	public function jsonMunOcuCov()
	{
		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		if($anyo == 2020){
			$trimestre = $this->rangoTriCov2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$tipo = $this->input->post('tipo');

			if ($id_evento) {
				$result = $this->GraficasModel->getMuniOcuCov($anyo, $trimestre, $dpto_muni, $tipo);
				$data = array();
				$data2 = array();
				$i = 0;
				foreach($result as $row):
					$i++;
					array_push($data, array($row->num, $i));
					array_push($data2, array($i, $row->muni));
				endforeach;
				echo json_encode(array('carga' => true, 'data' => $data, 'data2' => $data2));
			}else{
				echo json_encode(array('carga' => false, 'data' =>  [], 'data2' =>  []));
			}
			exit;
	}

	public function jsonSemanasCov() {
		$this->load->model("GraficasModel");
		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		if($anyo == 2020){
			$trimestre = $this->rangoTriCov2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));
		$tipo = $this->input->post('tipo');

		if ($id_evento) {
			$semanas = $this->GraficasModel->getEnfermedadCov($anyo, $trimestre, $dpto_muni, $muni, $tipo);
			$data = array();
			if($this->input->post('trimestre') == 1){
				array_push($data, array(0, 0));
			}
			foreach($semanas as $row):
				array_push($data, array($row->se_fec_sint, $row->num));
			endforeach;
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}


	public function jsonEstadoClinicoVih() {

		$this->load->model("GraficasModel");	

		$id_evento = $this->indicadorAEvento($this->input->post('indicador'));
		$anyo = $this->input->post('anyo');
		$dpto_muni = $this->ubicacionDptoMuni($this->input->post('dpto_muni'));
		if($anyo == 2020){
			$trimestre = $this->rangoTri2020($this->input->post('trimestre'));
		}else{
			$trimestre = $this->rangoTri($this->input->post('trimestre'));
		}

		$muni = $this->ubicacionMuni($this->input->post('dpto_muni'));

		if ($id_evento) {
			$result = $this->GraficasModel->getEstadoClinicoVih($id_evento, $anyo, $trimestre, $dpto_muni, $muni);
			$data = array();
			foreach($result as $row):
				$i = 0;
				switch($row->est_cli) {
					case 1:
						$i = 0;
						break;
					case 2:
						$i = 2;
						break;
					case 3:
						$i = 4;
						break;
					case 4:
						$i = 6;
						break;
					case 5:
						$i = 8;
						break;
					case 6:
						$i = 10;
						break;
				}
				array_push($data, array($i, $row->num));
			endforeach;
			echo json_encode(array('carga' => true, 'data' => $data));
		}else{
			echo json_encode(array('carga' => false, 'data' =>  []));
		}
		exit;
	}

	public function hepatitis()
	{

		$this->template->add_js("custom/estadistica_hep");	
		$this->template->add_js("js/statistics/peity/peity.bundle");
		$this->template->add_js("js/statistics/flot/jquery.flot.axislabels");
		$this->template->add_js("js/statistics/flot/flot.bundle");
		$this->template->add_js("js/statistics/easypiechart/easypiechart.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'hepatitis');
		$this->template->set('content_header', 'Graficas Hepatitis 2019');
        $this->template->set('content_sub_header', 'Consulte las graficas enfermedad');
        $this->template->render('graficas/enfermedad_hep');
	}

	public function tuberculosis()
	{

		$this->template->add_js("custom/estadistica_tub");	
		$this->template->add_js("js/statistics/peity/peity.bundle");
		$this->template->add_js("js/statistics/flot/jquery.flot.axislabels");
		$this->template->add_js("js/statistics/flot/flot.bundle");
		$this->template->add_js("js/statistics/easypiechart/easypiechart.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'tuberculosis');
		$this->template->set('content_header', 'Graficas Tuberculosis 2019');
        $this->template->set('content_sub_header', 'Consulte las graficas enfermedad');
        $this->template->render('graficas/enfermedad_tub');
	}


	public function vih()
	{

		$this->template->add_js("custom/estadistica_vih");	
		$this->template->add_js("js/statistics/peity/peity.bundle");
		$this->template->add_js("js/statistics/flot/jquery.flot.axislabels");
		$this->template->add_js("js/statistics/flot/flot.bundle");
		$this->template->add_js("js/statistics/easypiechart/easypiechart.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'vih');
		$this->template->set('content_header', 'Graficas VIH 2019');
        $this->template->set('content_sub_header', 'Consulte las graficas enfermedad');
        $this->template->render('graficas/enfermedad_vih');
	}

	public function covid()
	{

		$this->template->add_js("custom/estadistica_cov");	
		$this->template->add_js("js/statistics/peity/peity.bundle");
		$this->template->add_js("js/statistics/flot/jquery.flot.axislabels");
		$this->template->add_js("js/statistics/flot/flot.bundle");
		$this->template->add_js("js/statistics/easypiechart/easypiechart.bundle");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");

		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "dashboard" . '">Graficas</a></li>';
		$this->template->set('migasdepan', $migas);
		
		$this->template->set('item_sidebar_active', 'dengue');
		$this->template->set('content_header', 'Graficas Covid');
        $this->template->set('content_sub_header', 'Consulte las graficas enfermedad');
        $this->template->render('graficas/enfermedad_cov');
	}
	


}
