<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CMS_Controller {

	 function __construct()
	 {
		 parent::__construct();

	//	 $this->template->add_css('custom/style');

		 $user = array();
		 if ($this->session->userdata(SESSION_NAME)) {
			$user = $this->session->userdata(SESSION_NAME);
		}else{
			redirect();
		}
	
		$this->load->model("ReportesModel");
		$this->template->set_template('dashboard');

	 }

	 public function filtro_dengue()
	{
	
		$this->template->set('item_sidebar_active', 'reporte_den');
		$this->template->set('content_header', 'Reporte de Dengue');
        $this->template->set('content_sub_header', 'Filtrar para busqueda de registros de dengue.');
        $this->template->render('reportes/filtrar_dengue');
	}

	public function filtro_hep()
	{
	
		$this->template->set('item_sidebar_active', 'reporte_hep');
		$this->template->set('content_header', 'Reporte de Hepatitis');
        $this->template->set('content_sub_header', 'Filtrar para busqueda de registros de hepatitis.');
        $this->template->render('reportes/filtrar_hep');
	}

	public function filtro_tbc()
	{
	
		$this->template->set('item_sidebar_active', 'reporte_tbc');
		$this->template->set('content_header', 'Reporte de Tuberculosis');
        $this->template->set('content_sub_header', 'Filtrar para busqueda de registros de tuberculosis.');
        $this->template->render('reportes/filtrar_tbc');
	}

	public function filtro_vih()
	{
	
		$this->template->set('item_sidebar_active', 'reporte_vih');
		$this->template->set('content_header', 'Reporte de VIH');
        $this->template->set('content_sub_header', 'Filtrar para busqueda de registros de VIH.');
        $this->template->render('reportes/filtrar_vih');
	}

	public function filtro_cov()
	{
	
		$this->template->set('item_sidebar_active', 'reporte_cov');
		$this->template->set('content_header', 'Reporte de Covid');
        $this->template->set('content_sub_header', 'Filtrar para busqueda de registros de Covid.');
        $this->template->render('reportes/filtrar_cov');
	}
 
	public function reporte_den()
	{
		

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");
		$this->template->add_js("custom/table_reporte_dengue");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
	
		if($this->input->post('filtrar') == 1){
			if($this->input->post('anyo') && $this->input->post('dptomuni')){
				$listado = $this->ReportesModel->getReporteDengue($this->input->post('anyo'), $this->input->post('dptomuni'), 1);
				$this->template->set('listado', $listado);
				$this->session->set_flashdata('filtrar', $this->input->post('filtrar'));
				$this->session->set_flashdata('anyo', $this->input->post('anyo'));
				$this->session->set_flashdata('dptomuni', $this->input->post('dptomuni'));
			//	echo '<pre>'; print_r($listado); return;
			}
		}else{
			if($this->session->flashdata('filtrar') == 1){
				if($this->session->flashdata('anyo') && $this->session->flashdata('dptomuni')){
					$listado = $this->ReportesModel->getReporteDengue($this->session->flashdata('anyo'), $this->session->flashdata('dptomuni'), 1);
					$this->template->set('listado', $listado);
					$this->session->set_flashdata('filtrar', $this->session->flashdata('filtrar'));
					$this->session->set_flashdata('anyo', $this->session->flashdata('anyo'));
					$this->session->set_flashdata('dptomuni', $this->session->flashdata('dptomuni'));
				}
			}
		}

		$this->template->set('item_sidebar_active', 'reporte_den');
		$this->template->set('content_header', 'Reporte de Dengue');
        $this->template->set('content_sub_header', 'El siguiente listado son los datos cargados en el reporte del indicador Dengue.');
        $this->template->render('reportes/indicadores');
		
	}

	public function reporte_hep()
	{
		

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");
		$this->template->add_js("custom/table_reporte_hepatitis");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
	
		if($this->input->post('filtrar') == 1){
			if($this->input->post('anyo') && $this->input->post('dptomuni')){
				$listado = $this->ReportesModel->getReporteHep($this->input->post('anyo'), $this->input->post('dptomuni'), 2);
				$this->template->set('listado', $listado);
				$this->session->set_flashdata('filtrar', $this->input->post('filtrar'));
				$this->session->set_flashdata('anyo', $this->input->post('anyo'));
				$this->session->set_flashdata('dptomuni', $this->input->post('dptomuni'));
			}
		}else{
			if($this->session->flashdata('filtrar') == 1){
				if($this->session->flashdata('anyo') && $this->session->flashdata('dptomuni')){
					$listado = $this->ReportesModel->getReporteHep($this->session->flashdata('anyo'), $this->session->flashdata('dptomuni'), 2);
					$this->template->set('listado', $listado);
					$this->session->set_flashdata('filtrar', $this->session->flashdata('filtrar'));
					$this->session->set_flashdata('anyo', $this->session->flashdata('anyo'));
					$this->session->set_flashdata('dptomuni', $this->session->flashdata('dptomuni'));
			//		echo '<pre>'; print_r($listado); return;
				}
			}
		}
	//	echo '<pre>'; print_r($listado); return;
		
		$this->template->set('item_sidebar_active', 'reporte_hep');
		$this->template->set('content_header', 'Reporte de Hepatitis');
        $this->template->set('content_sub_header', 'El siguiente listado son los datos cargados en el reporte del indicador Hepatitis.');
        $this->template->render('reportes/indicadores_hep');
		
	}

	public function reporte_tbc()
	{
		

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");
		$this->template->add_js("custom/table_reporte_tbc");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
	
		if($this->input->post('filtrar') == 1){
			if($this->input->post('anyo') && $this->input->post('dptomuni')){
				$listado = $this->ReportesModel->getReporteTbc($this->input->post('anyo'), $this->input->post('dptomuni'), 3);
				$this->template->set('listado', $listado);
				$this->session->set_flashdata('filtrar', $this->input->post('filtrar'));
				$this->session->set_flashdata('anyo', $this->input->post('anyo'));
				$this->session->set_flashdata('dptomuni', $this->input->post('dptomuni'));
			}
		}else{
			if($this->session->flashdata('filtrar') == 1){
				if($this->session->flashdata('anyo') && $this->session->flashdata('dptomuni')){
					$listado = $this->ReportesModel->getReporteTbc($this->session->flashdata('anyo'), $this->session->flashdata('dptomuni'), 3);
					$this->template->set('listado', $listado);
					$this->session->set_flashdata('filtrar', $this->session->flashdata('filtrar'));
					$this->session->set_flashdata('anyo', $this->session->flashdata('anyo'));
					$this->session->set_flashdata('dptomuni', $this->session->flashdata('dptomuni'));
			//		echo '<pre>'; print_r($listado); return;
				}
			}
		}
	//	echo '<pre>'; print_r($listado); return;
		
		$this->template->set('item_sidebar_active', 'reporte_tbc');
		$this->template->set('content_header', 'Reporte de Tuberculosis');
        $this->template->set('content_sub_header', 'El siguiente listado son los datos cargados en el reporte del indicador Tuberculosis.');
        $this->template->render('reportes/indicadores_tbc');
		
	}

	public function reporte_vih()
	{
		

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");
		$this->template->add_js("custom/table_reporte_vih");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
	
		if($this->input->post('filtrar') == 1){
			if($this->input->post('anyo') && $this->input->post('dptomuni')){
				$listado = $this->ReportesModel->getReporteVih($this->input->post('anyo'), $this->input->post('dptomuni'), 4);
				$this->template->set('listado', $listado);
				$this->session->set_flashdata('filtrar', $this->input->post('filtrar'));
				$this->session->set_flashdata('anyo', $this->input->post('anyo'));
				$this->session->set_flashdata('dptomuni', $this->input->post('dptomuni'));
			}
		}else{
			if($this->session->flashdata('filtrar') == 1){
				if($this->session->flashdata('anyo') && $this->session->flashdata('dptomuni')){
					$listado = $this->ReportesModel->getReporteVih($this->session->flashdata('anyo'), $this->session->flashdata('dptomuni'), 4);
					$this->template->set('listado', $listado);
					$this->session->set_flashdata('filtrar', $this->session->flashdata('filtrar'));
					$this->session->set_flashdata('anyo', $this->session->flashdata('anyo'));
					$this->session->set_flashdata('dptomuni', $this->session->flashdata('dptomuni'));
			//		echo '<pre>'; print_r($listado); return;
				}
			}
		}
	//	echo '<pre>'; print_r($listado); return;
		
		$this->template->set('item_sidebar_active', 'reporte_vih');
		$this->template->set('content_header', 'Reporte de VIH');
        $this->template->set('content_sub_header', 'El siguiente listado son los datos cargados en el reporte del indicador VIH.');
        $this->template->render('reportes/indicadores_vih');
		
	}

	public function reporte_cov()
	{
		

		$this->template->add_css("css/datagrid/datatables/datatables.bundle");
		$this->template->add_js("custom/table_reporte_cov");
		$this->template->add_js("js/datagrid/datatables/datatables.bundle");
	
		if($this->input->post('filtrar') == 1){
			if($this->input->post('anyo') && $this->input->post('dptomuni')){
				$listado = $this->ReportesModel->getReporteCov($this->input->post('anyo'), $this->input->post('dptomuni'));
			//	echo '<pre>'; print_r($listado); return;
				$this->template->set('listado', $listado);
				$this->session->set_flashdata('filtrar', $this->input->post('filtrar'));
				$this->session->set_flashdata('anyo', $this->input->post('anyo'));
				$this->session->set_flashdata('dptomuni', $this->input->post('dptomuni'));
			}
		}else{
			if($this->session->flashdata('filtrar') == 1){
				if($this->session->flashdata('anyo') && $this->session->flashdata('dptomuni')){
					$listado = $this->ReportesModel->getReporteCov($this->session->flashdata('anyo'), $this->session->flashdata('dptomuni'));
				//	echo '<pre>'; print_r($listado); return;
					$this->template->set('listado', $listado);
					$this->session->set_flashdata('filtrar', $this->session->flashdata('filtrar'));
					$this->session->set_flashdata('anyo', $this->session->flashdata('anyo'));
					$this->session->set_flashdata('dptomuni', $this->session->flashdata('dptomuni'));
			//		echo '<pre>'; print_r($listado); return;
				}
			}
		}
	//	echo '<pre>'; print_r($listado); return;
		
		$this->template->set('item_sidebar_active', 'reporte_cov');
		$this->template->set('content_header', 'Reporte de Covid');
        $this->template->set('content_sub_header', 'El siguiente listado son los datos cargados en el reporte del indicador Covid.');
        $this->template->render('reportes/indicadores_cov');
		
	}

	public function editar_dengue($id = null)
	{
		if($id){
			$registro = $this->ReportesModel->getRegistroDengue($id);
			$this->template->set('registro', $registro);
	
	//		echo '<pre>'; print_r($registro); return;
		}
		//	$this->template->add_js('custom/validaction');
		//	$this->template->set('login', false);
		$this->session->set_flashdata('filtrar', $this->session->flashdata('filtrar'));
		$this->session->set_flashdata('anyo', $this->session->flashdata('anyo'));
		$this->session->set_flashdata('dptomuni', $this->session->flashdata('dptomuni'));

		if($this->input->post('editar') == 1){
				$info = array(
                    'id_evento' => $this->input->post('id_evento'),
                    'fec_not' => $this->input->post('fec_not'),
                    'semana' => $this->input->post('semana'),
                    'edad' => $this->input->post('edad'),
                    'id_nacionalidad' => $this->input->post('id_nacionalidad'),
                    'sexo' => $this->input->post('sexo'),
                    'id_muni_ocu' => $this->input->post('id_muni_ocu'),
                    'localidad' => $this->input->post('localidad'),
                    'cen_poblado' => $this->input->post('cen_poblado'),
                    'vereda' => $this->input->post('vereda'),
                    'bar_ver' => $this->input->post('bar_ver'),
                    'tip_ss' => $this->input->post('tip_ss'),
                    'cod_ase' => $this->input->post('cod_ase'),
                    'fec_con' => $this->input->post('fec_con'),
                    'fec_sint' => $this->input->post('fec_sint'),
                    'pac_hos' => $this->input->post('pac_hos'),
                    'fec_hos' => $this->input->post('fec_hos'),
                    'anyo' => $this->input->post('anyo'),
                    'dpto_muni' => $this->input->post('dpto_muni')
                );
				$info2 = array(
                    'gp_gestan' => $this->input->post('gp_gestan'),
                    'sem_ges' => $this->input->post('sem_ges'),
                    'fecha_nto' => $this->input->post('fecha_nto'),
                    'clasfinal' => $this->input->post('clasfinal'),
                    'conducta' => $this->input->post('conducta')
                );
				
			//	echo '<pre>'; 
		//		print_r($info2); return;
				$ok = $this->ReportesModel->update_reporte($this->input->post('id_registro'), $info);
				if ($ok) {
					$ok2 = $this->ReportesModel->update_reporte_dengue($this->input->post('id_registro'), $info2);
					if ($ok && $ok2) {
						$this->template->set_flash_message(array('success' => 'El registro se editó correctamente.'));
					}
				}else{
					$this->template->set_flash_message(array('error' => 'El registro no se editó correctamente, por favor inténtelo de nuevo.'));
				}		
				redirect('reportes/reporte_den');

		}else{
			$_POST = array_merge($_POST, (array)$registro);
		}
			
		$listado_eventos = $this->_eventos($this->ReportesModel->getEventos());
		$this->template->set('listado_eventos',$listado_eventos);
		$listado_nacionalidad = $this->_nacionalidad($this->ReportesModel->getPais());
		$this->template->set('listado_nacionalidad',$listado_nacionalidad);
		$listado_municipio = $this->_municipio($this->ReportesModel->getMunicipio());
		$this->template->set('listado_municipio',$listado_municipio);
		$listado_tip_ss = $this->_tiposs($this->ReportesModel->getTipSS());
		$this->template->set('listado_tip_ss',$listado_tip_ss);
		$listado_opciones = $this->_opciones($this->ReportesModel->getOpciones());
		$this->template->set('listado_opciones',$listado_opciones);
		$listado_tipo_reporte = $this->_tipo_reporte();
		$this->template->set('listado_tipo_reporte',$listado_tipo_reporte);
		$clasificacion_final = $this->_clasificacionFinal($this->ReportesModel->getClasificacionFinal());
		$this->template->set('clasificacion_final',$clasificacion_final);
		$listado_conducta = $this->_conducta($this->ReportesModel->getConducta());
		$this->template->set('listado_conducta',$listado_conducta);
		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "reportes/reporte_den" . '">Reporte Dengue</a></li>';
		$this->template->set('migasdepan', $migas);
		$this->template->set('item_sidebar_active', 'reporte_den');
		$this->template->set('content_header', 'Editar Registro de Dengue');
        $this->template->set('content_sub_header', 'El siguiente formulario permite la edición de un registro de dengue.');
        $this->template->render('reportes/editar_dengue');
	}

	public function borrar_dengue($id = null)
    {
       
		$registro = $this->ReportesModel->getRegistroDengue($id);

		$this->session->set_flashdata('filtrar', $this->session->flashdata('filtrar'));
		$this->session->set_flashdata('anyo', $this->session->flashdata('anyo'));
		$this->session->set_flashdata('dptomuni', $this->session->flashdata('dptomuni'));

        if (!$registro) {
            $this->template->set_flash_message(array('warning' => 'Ocurrió un error al eliminar el registro seleccionado'));
            redirect('reportes/reporte_den');
        }

      //  echo '<pre>'; print_r($registro->id_rep); return;
        $ok = $this->ReportesModel->delete_dengue($registro->id_rep);

        if ($ok) {
            $this->template->set_flash_message(array('success' => 'Se ha eliminado exitosamente el registro'));
        } else {
            $this->template->set_flash_message(array('error' => 'Ocurrio un error al eliminar el registro'));
        }

        redirect('reportes/reporte_den');
    }

	public function editar_hepatitis($id = null)
	{
		if($id){
			$registro = $this->ReportesModel->getRegistroHepatitis($id);
			$this->template->set('registro', $registro);
	
	//		echo '<pre>'; print_r($registro); return;
		}

		$this->session->set_flashdata('filtrar', $this->session->flashdata('filtrar'));
		$this->session->set_flashdata('anyo', $this->session->flashdata('anyo'));
		$this->session->set_flashdata('dptomuni', $this->session->flashdata('dptomuni'));

		if($this->input->post('editar') == 1){
				$info = array(
                    'id_evento' => $this->input->post('id_evento'),
                    'fec_not' => $this->input->post('fec_not'),
                    'semana' => $this->input->post('semana'),
                    'edad' => $this->input->post('edad'),
                    'id_nacionalidad' => $this->input->post('id_nacionalidad'),
                    'sexo' => $this->input->post('sexo'),
                    'id_muni_ocu' => $this->input->post('id_muni_ocu'),
                    'localidad' => $this->input->post('localidad'),
                    'cen_poblado' => $this->input->post('cen_poblado'),
                    'vereda' => $this->input->post('vereda'),
                    'bar_ver' => $this->input->post('bar_ver'),
                    'tip_ss' => $this->input->post('tip_ss'),
                    'cod_ase' => $this->input->post('cod_ase'),
                    'fec_con' => $this->input->post('fec_con'),
                    'fec_sint' => $this->input->post('fec_sint'),
                    'pac_hos' => $this->input->post('pac_hos'),
                    'fec_hos' => $this->input->post('fec_hos'),
                    'anyo' => $this->input->post('anyo'),
                    'dpto_muni' => $this->input->post('dpto_muni')
                );
				$info2 = array(
                    'gp_gestan' => $this->input->post('gp_gestan'),
                    'sem_ges' => $this->input->post('sem_ges'),
                    'clasfinal' => $this->input->post('clasfinal'),
                    'met_tra' => $this->input->post('met_tra'),
					'coinf_vih_coinfeccion' => $this->input->post('coinf_vih_coinfeccion')
                );
				
			//	echo '<pre>'; 
		//		print_r($info2); return;
				$ok = $this->ReportesModel->update_reporte($this->input->post('id_registro'), $info);
				if ($ok) {
					$ok2 = $this->ReportesModel->update_reporte_hepatitis($this->input->post('id_registro'), $info2);
					if ($ok && $ok2) {
						$this->template->set_flash_message(array('success' => 'El registro se editó correctamente.'));
					}

				}else{
					$this->template->set_flash_message(array('error' => 'El registro no se editó correctamente, por favor inténtelo de nuevo.'));
				}
				redirect('reportes/reporte_hep');

		}else{
			$_POST = array_merge($_POST, (array)$registro);
		//	echo '<pre>'; print_r($_POST); return;
		}
			
		$listado_eventos = $this->_eventos($this->ReportesModel->getEventos());
		$this->template->set('listado_eventos',$listado_eventos);
		$listado_nacionalidad = $this->_nacionalidad($this->ReportesModel->getPais());
		$this->template->set('listado_nacionalidad',$listado_nacionalidad);
		$listado_municipio = $this->_municipio($this->ReportesModel->getMunicipio());
		$this->template->set('listado_municipio',$listado_municipio);
		$listado_tip_ss = $this->_tiposs($this->ReportesModel->getTipSS());
		$this->template->set('listado_tip_ss',$listado_tip_ss);
		$listado_opciones = $this->_opciones($this->ReportesModel->getOpciones());
		$this->template->set('listado_opciones',$listado_opciones);
		$listado_tipo_reporte = $this->_tipo_reporte();
		$this->template->set('listado_tipo_reporte',$listado_tipo_reporte);
		$clasificacion_final = $this->_clasificacionFinal($this->ReportesModel->getClasificacionFinalHep());
		$this->template->set('clasificacion_final',$clasificacion_final);
		$listado_met_tra = $this->_modotransmision($this->ReportesModel->getModoTransmision());
		$this->template->set('listado_met_tra',$listado_met_tra);
		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "reportes/reporte_hep" . '">Reporte Hepatitis</a></li>';
		$this->template->set('migasdepan', $migas);
		$this->template->set('item_sidebar_active', 'reporte_hep');
		$this->template->set('content_header', 'Editar Registro de Hepatitis');
        $this->template->set('content_sub_header', 'El siguiente formulario permite la edición de un registro de hepatitis.');
        $this->template->render('reportes/editar_hepatitis');
	}

	public function borrar_hepatitis($id = null)
    {
       
		$registro = $this->ReportesModel->getRegistroHepatitis($id);

		$this->session->set_flashdata('filtrar', $this->session->flashdata('filtrar'));
		$this->session->set_flashdata('anyo', $this->session->flashdata('anyo'));
		$this->session->set_flashdata('dptomuni', $this->session->flashdata('dptomuni'));

        if (!$registro) {
            $this->template->set_flash_message(array('warning' => 'Ocurrió un error al eliminar el registro seleccionado'));
            redirect('reportes/reporte_hep');
        }

      //  echo '<pre>'; print_r($registro->id_rep); return;
        $ok = $this->ReportesModel->delete_hepatitis($registro->id_rep);

        if ($ok) {
            $this->template->set_flash_message(array('success' => 'Se ha eliminado exitosamente el registro'));
        } else {
            $this->template->set_flash_message(array('error' => 'Ocurrio un error al eliminar el registro'));
        }

        redirect('reportes/reporte_hep');
    }

	public function editar_tbc($id = null)
	{
		if($id){
			$registro = $this->ReportesModel->getRegistroTuberculosis($id);
			$this->template->set('registro', $registro);
		}

		$this->session->set_flashdata('filtrar', $this->session->flashdata('filtrar'));
		$this->session->set_flashdata('anyo', $this->session->flashdata('anyo'));
		$this->session->set_flashdata('dptomuni', $this->session->flashdata('dptomuni'));

		if($this->input->post('editar') == 1){
				$info = array(
                    'id_evento' => $this->input->post('id_evento'),
                    'fec_not' => $this->input->post('fec_not'),
                    'semana' => $this->input->post('semana'),
                    'edad' => $this->input->post('edad'),
                    'id_nacionalidad' => $this->input->post('id_nacionalidad'),
                    'sexo' => $this->input->post('sexo'),
                    'id_muni_ocu' => $this->input->post('id_muni_ocu'),
                    'localidad' => $this->input->post('localidad'),
                    'cen_poblado' => $this->input->post('cen_poblado'),
                    'vereda' => $this->input->post('vereda'),
                    'bar_ver' => $this->input->post('bar_ver'),
                    'tip_ss' => $this->input->post('tip_ss'),
                    'cod_ase' => $this->input->post('cod_ase'),
                    'fec_con' => $this->input->post('fec_con'),
                    'fec_sint' => $this->input->post('fec_sint'),
                    'pac_hos' => $this->input->post('pac_hos'),
                    'fec_hos' => $this->input->post('fec_hos'),
                    'anyo' => $this->input->post('anyo'),
                    'dpto_muni' => $this->input->post('dpto_muni')
                );
				$info2 = array(
                    'gp_gestan' => $this->input->post('gp_gestan'),
                    'sem_ges' => $this->input->post('sem_ges'),
                    'cond_tuber' => $this->input->post('cond_tuber'),
                    'tip_tub' => $this->input->post('tip_tub'),
					'vih_confirmado' => $this->input->post('vih_confirmado')
                );
				
			//	echo '<pre>'; 
			//	print_r($info2); return;
				$ok = $this->ReportesModel->update_reporte($this->input->post('id_registro'), $info);
				if ($ok) {
					$ok2 = $this->ReportesModel->update_reporte_tbc($this->input->post('id_registro'), $info2);
					if ($ok && $ok2) {
						$this->template->set_flash_message(array('success' => 'El registro se editó correctamente.'));
					}

				}else{
					$this->template->set_flash_message(array('error' => 'El registro no se editó correctamente, por favor inténtelo de nuevo.'));
				}
				redirect('reportes/reporte_tbc');

		}else{
			$_POST = array_merge($_POST, (array)$registro);
		//	echo '<pre>'; print_r($_POST); return;
		}
			
		$listado_eventos = $this->_eventos($this->ReportesModel->getEventos());
		$this->template->set('listado_eventos',$listado_eventos);
		$listado_nacionalidad = $this->_nacionalidad($this->ReportesModel->getPais());
		$this->template->set('listado_nacionalidad',$listado_nacionalidad);
		$listado_municipio = $this->_municipio($this->ReportesModel->getMunicipio());
		$this->template->set('listado_municipio',$listado_municipio);
		$listado_tip_ss = $this->_tiposs($this->ReportesModel->getTipSS());
		$this->template->set('listado_tip_ss',$listado_tip_ss);
		$listado_opciones = $this->_opciones($this->ReportesModel->getOpciones());
		$this->template->set('listado_opciones',$listado_opciones);
		$listado_tipo_reporte = $this->_tipo_reporte();
		$this->template->set('listado_tipo_reporte',$listado_tipo_reporte);
		$condicion_tbc = $this->_condicionTbc($this->ReportesModel->getCondicionTbc());
		$this->template->set('condicion_tbc',$condicion_tbc);
		$listado_tip_tbc = $this->_tipoTbc($this->ReportesModel->getTipoTbc());
		$this->template->set('listado_tip_tbc',$listado_tip_tbc);
		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "reportes/reporte_tbc" . '">Reporte Tuberculosis</a></li>';
		$this->template->set('migasdepan', $migas);
		$this->template->set('item_sidebar_active', 'reporte_tbc');
		$this->template->set('content_header', 'Editar Registro de Tuberculosis');
        $this->template->set('content_sub_header', 'El siguiente formulario permite la edición de un registro de tuberculosis.');
        $this->template->render('reportes/editar_tuberculosis');
	}

	public function borrar_tbc($id = null)
    {
       
		$registro = $this->ReportesModel->getRegistroTuberculosis($id);

		$this->session->set_flashdata('filtrar', $this->session->flashdata('filtrar'));
		$this->session->set_flashdata('anyo', $this->session->flashdata('anyo'));
		$this->session->set_flashdata('dptomuni', $this->session->flashdata('dptomuni'));

        if (!$registro) {
            $this->template->set_flash_message(array('warning' => 'Ocurrió un error al eliminar el registro seleccionado'));
            redirect('reportes/reporte_tbc');
        }

      //  echo '<pre>'; print_r($registro->id_rep); return;
        $ok = $this->ReportesModel->delete_tbc($registro->id_rep);

        if ($ok) {
            $this->template->set_flash_message(array('success' => 'Se ha eliminado exitosamente el registro'));
        } else {
            $this->template->set_flash_message(array('error' => 'Ocurrio un error al eliminar el registro'));
        }

        redirect('reportes/reporte_tbc');
    }

	public function editar_vih($id = null)
	{
		if($id){
			$registro = $this->ReportesModel->getRegistroVih($id);
			$this->template->set('registro', $registro);
	
	//		echo '<pre>'; print_r($registro); return;
		}

		$this->session->set_flashdata('filtrar', $this->session->flashdata('filtrar'));
		$this->session->set_flashdata('anyo', $this->session->flashdata('anyo'));
		$this->session->set_flashdata('dptomuni', $this->session->flashdata('dptomuni'));

		if($this->input->post('editar') == 1){
				$info = array(
                    'id_evento' => $this->input->post('id_evento'),
                    'fec_not' => $this->input->post('fec_not'),
                    'semana' => $this->input->post('semana'),
                    'edad' => $this->input->post('edad'),
                    'id_nacionalidad' => $this->input->post('id_nacionalidad'),
                    'sexo' => $this->input->post('sexo'),
                    'id_muni_ocu' => $this->input->post('id_muni_ocu'),
                    'localidad' => $this->input->post('localidad'),
                    'cen_poblado' => $this->input->post('cen_poblado'),
                    'vereda' => $this->input->post('vereda'),
                    'bar_ver' => $this->input->post('bar_ver'),
                    'tip_ss' => $this->input->post('tip_ss'),
                    'cod_ase' => $this->input->post('cod_ase'),
                    'fec_con' => $this->input->post('fec_con'),
                    'fec_sint' => $this->input->post('fec_sint'),
                    'pac_hos' => $this->input->post('pac_hos'),
                    'fec_hos' => $this->input->post('fec_hos'),
                    'anyo' => $this->input->post('anyo'),
                    'dpto_muni' => $this->input->post('dpto_muni')
                );
				$info2 = array(
                    'ide_genero' => $this->input->post('ide_genero'),
                    'est_cli' => $this->input->post('est_cli')
                );
				
			//	echo '<pre>'; 
			//	print_r($info2); return;
				$ok = $this->ReportesModel->update_reporte($this->input->post('id_registro'), $info);
				if ($ok) {
					$ok2 = $this->ReportesModel->update_reporte_vih($this->input->post('id_registro'), $info2);
					if ($ok && $ok2) {
						$this->template->set_flash_message(array('success' => 'El registro se editó correctamente.'));
					}

				}else{
					$this->template->set_flash_message(array('error' => 'El registro no se editó correctamente, por favor inténtelo de nuevo.'));
				}
				redirect('reportes/reporte_vih');

		}else{
			$_POST = array_merge($_POST, (array)$registro);
		//	echo '<pre>'; print_r($_POST); return;
		}
			
		$listado_eventos = $this->_eventos($this->ReportesModel->getEventos());
		$this->template->set('listado_eventos',$listado_eventos);
		$listado_nacionalidad = $this->_nacionalidad($this->ReportesModel->getPais());
		$this->template->set('listado_nacionalidad',$listado_nacionalidad);
		$listado_municipio = $this->_municipio($this->ReportesModel->getMunicipio());
		$this->template->set('listado_municipio',$listado_municipio);
		$listado_tip_ss = $this->_tiposs($this->ReportesModel->getTipSS());
		$this->template->set('listado_tip_ss',$listado_tip_ss);
		$listado_opciones = $this->_opciones($this->ReportesModel->getOpciones());
		$this->template->set('listado_opciones',$listado_opciones);
		$listado_tipo_reporte = $this->_tipo_reporte();
		$this->template->set('listado_tipo_reporte',$listado_tipo_reporte);
		$estado_clinico = $this->_estadoClinico($this->ReportesModel->getEstadoClinico());
		$this->template->set('estado_clinico',$estado_clinico);
		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "reportes/reporte_vih" . '">Reporte VIH</a></li>';
		$this->template->set('migasdepan', $migas);
		$this->template->set('item_sidebar_active', 'reporte_vih');
		$this->template->set('content_header', 'Editar Registro de VIH');
        $this->template->set('content_sub_header', 'El siguiente formulario permite la edición de un registro de VIH.');
        $this->template->render('reportes/editar_vih');
	}

	public function borrar_vih($id = null)
    {
       
		$registro = $this->ReportesModel->getRegistroVih($id);

		$this->session->set_flashdata('filtrar', $this->session->flashdata('filtrar'));
		$this->session->set_flashdata('anyo', $this->session->flashdata('anyo'));
		$this->session->set_flashdata('dptomuni', $this->session->flashdata('dptomuni'));

        if (!$registro) {
            $this->template->set_flash_message(array('warning' => 'Ocurrió un error al eliminar el registro seleccionado'));
            redirect('reportes/reporte_vih');
        }

      //  echo '<pre>'; print_r($registro->id_rep); return;
        $ok = $this->ReportesModel->delete_vih($registro->id_rep);

        if ($ok) {
            $this->template->set_flash_message(array('success' => 'Se ha eliminado exitosamente el registro'));
        } else {
            $this->template->set_flash_message(array('error' => 'Ocurrio un error al eliminar el registro'));
        }

        redirect('reportes/reporte_vih');
    }


	public function editar_cov($id = null)
	{
		if($id){
			$registro = $this->ReportesModel->getRegistroCov($id);
			$this->template->set('registro', $registro);
		}

		$this->session->set_flashdata('filtrar', $this->session->flashdata('filtrar'));
		$this->session->set_flashdata('anyo', $this->session->flashdata('anyo'));
		$this->session->set_flashdata('dptomuni', $this->session->flashdata('dptomuni'));

		if($this->input->post('editar') == 1){
				$info = array(
                    'id_tipo_prueba' => $this->input->post('id_tipo_prueba'),
                    'fec_not' => $this->input->post('fec_not'),
					'id_muni' => $this->input->post('id_muni'),
                    'edad' => $this->input->post('edad'),
                    'sexo' => $this->input->post('sexo'),
                    'fuente_tip_con' => $this->input->post('fuente_tip_con'),
                    'ubicacion' => $this->input->post('ubicacion'),
                    'estado' => $this->input->post('estado'),
                    'id_nacionalidad' => $this->input->post('id_nacionalidad'),
                    'fec_ing_pais' => $this->input->post('fec_ing_pais'),
                    'fec_sint' => $this->input->post('fec_sint'),
                    'fec_con' => $this->input->post('fec_con'),
                    'fec_dia' => $this->input->post('fec_dia'),
                    'fec_hos' => $this->input->post('fec_hos'),
                    'fec_alta' => $this->input->post('fec_alta'),
                    'fec_mue' => $this->input->post('fec_mue'),
                    'fec_rec' => $this->input->post('fec_rec'),
                    'clasfinal' => $this->input->post('clasfinal'),
                    'tipo_rec' => $this->input->post('tipo_rec'),
                    'id_grupo_edad' => $this->input->post('id_grupo_edad'),
                    'quinquenio' => $this->input->post('quinquenio'),
                    'se_fec_sint' => $this->input->post('se_fec_sint'),
                    'barrio' => $this->input->post('barrio'),
                    'anyo' => $this->input->post('anyo'),
                    'dpto_muni' => $this->input->post('dpto_muni')
                );

				$ok = $this->ReportesModel->update_reporte_cov($this->input->post('id_registro'), $info);
				if ($ok) {
					$this->template->set_flash_message(array('success' => 'El registro se editó correctamente.'));
				}else{
					$this->template->set_flash_message(array('error' => 'El registro no se editó correctamente, por favor inténtelo de nuevo.'));
				}
				redirect('reportes/reporte_cov');

		}else{
			$_POST = array_merge($_POST, (array)$registro);
		}
			
		$listado_prueba = $this->_tipo_prueba($this->ReportesModel->getTipoPrueba());
		$this->template->set('listado_prueba',$listado_prueba);
		$listado_municipio = $this->_municipio($this->ReportesModel->getMunicipio());
		$this->template->set('listado_municipio',$listado_municipio);
		$listado_nacionalidad = $this->_nacionalidad($this->ReportesModel->getPais());
		$this->template->set('listado_nacionalidad',$listado_nacionalidad);
		$listado_tipo_recuperacion = $this->_tipo_recuperacion($this->ReportesModel->getTipoRecuperacion());
		$this->template->set('listado_tipo_recuperacion',$listado_tipo_recuperacion);		
		$listado_tipo_reporte = $this->_tipo_reporte();
		$this->template->set('listado_tipo_reporte',$listado_tipo_reporte);
		$listado_grupo_edad = $this->_grupo_edad($this->ReportesModel->getGrupoEdad());
		$this->template->set('listado_grupo_edad',$listado_grupo_edad);	
		$migas = '<li class="breadcrumb-item active"><a href="' . base_url() . "reportes/reporte_cov" . '">Reporte Covid</a></li>';
		$this->template->set('migasdepan', $migas);
		$this->template->set('item_sidebar_active', 'reporte_cov');
		$this->template->set('content_header', 'Editar Registro de Covid');
        $this->template->set('content_sub_header', 'El siguiente formulario permite la edición de un registro de Covid.');
        $this->template->render('reportes/editar_cov');
	}

	public function borrar_cov($id = null)
    {
       
		$registro = $this->ReportesModel->getRegistroCov($id);

		$this->session->set_flashdata('filtrar', $this->session->flashdata('filtrar'));
		$this->session->set_flashdata('anyo', $this->session->flashdata('anyo'));
		$this->session->set_flashdata('dptomuni', $this->session->flashdata('dptomuni'));

        if (!$registro) {
            $this->template->set_flash_message(array('warning' => 'Ocurrió un error al eliminar el registro seleccionado'));
            redirect('reportes/reporte_cov');
        }
		
        $ok = $this->ReportesModel->delete_cov($registro->id);

        if ($ok) {
            $this->template->set_flash_message(array('success' => 'Se ha eliminado exitosamente el registro'));
        } else {
            $this->template->set_flash_message(array('error' => 'Ocurrio un error al eliminar el registro'));
        }

        redirect('reportes/reporte_cov');
    }

	private function _eventos($listado = null)
    {

        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione el tipo de evento";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

	private function _nacionalidad($listado = null)
    {

        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione la nacionalidad";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

	private function _municipio($listado = null)
    {

        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione el municipio";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

	private function _tiposs($listado = null)
    {

        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione el tipo de seguridad social";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

	private function _opciones($listado = null)
    {

        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione la opción";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

	private function _tipo_reporte()
    {

        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione el tipo de reporte";
		$arrayTemp[1] = 'Por Departamento';
		$arrayTemp[2] = 'Por Municipio';
    
        return $arrayTemp;
	}

	private function _clasificacionFinal($listado = null)
	{
        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione la clasificacion final";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

	private function _condicionTbc($listado = null)
	{
		$arrayTemp = array();
        $arrayTemp[''] = "Seleccione la condición de tuberculosis";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

	private function _conducta($listado = null)
	{
        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione la conducta";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

	private function _modotransmision($listado = null)
	{
		$arrayTemp = array();
        $arrayTemp[''] = "Seleccione el modo de transmisión";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

	private function _tipoTbc($listado = null)
	{
		$arrayTemp = array();
        $arrayTemp[''] = "Seleccione el tipo de tuberculosis";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

	private function _estadoClinico($listado = null)
	{
		$arrayTemp = array();
        $arrayTemp[''] = "Seleccione el estado clínico";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

	private function _tipo_prueba($listado = null)
    {

        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione el tipo de prueba";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

	private function _tipo_recuperacion($listado = null)
    {

        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione el tipo de recuperación";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}

	private function _grupo_edad($listado = null)
    {

        $arrayTemp = array();
        $arrayTemp[''] = "Seleccione el grupo de edad";
        if (count($listado) > 0) {
            $numFilas = count($listado);
            for ($i = 0; $i < $numFilas; $i++) {
                $arrayTemp[$listado[$i]->id] = $listado[$i]->nombre;
            }
        }

        return $arrayTemp;
	}
}
