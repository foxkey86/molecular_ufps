<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class PerfilModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getPerfilProfesional($id_usuario)
    {
        return $this->db->query("SELECT * FROM profesional WHERE id_user = '{$id_usuario}'")->row();
    }

    public function getPerfilParticular($id_usuario)
    {
        return $this->db->query("SELECT * FROM particular p, oficio o WHERE p.id_user = '{$id_usuario}' AND p.id_oficio = o.id_oficio")->row();
    }
}
