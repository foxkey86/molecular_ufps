<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class UsuariosModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getUsuarios()
    {
        return $this->db->query("SELECT p.*, (select nombre FROM tipo_documento r WHERE p.id_tipo_documento = r.id) nom_documento
        FROM user p")->result();
    }

    public function getUsuario($id)
    {
        return $this->db->query("SELECT * FROM user 
        WHERE id = '{$id}'")->row();
    }

    public function getDocumentos()
    {
        return $this->db->query("SELECT * FROM tipo_documento")->result();
    }

    public function getPerfil()
    {
        return $this->db->query("SELECT * FROM perfil")->result();
    }
    
    public function update_usuario($id_usuario, $info)
    {
        $this->db->where('id', $id_usuario);
        return $this->db->update('user', $info);
    }

    public function delete_usuario($id)
    {
        $this->db->where('id_user', $id);
        $this->db->delete('particular');
        $this->db->where('id_user', $id);
        $this->db->delete('profesional');
        $this->db->where('id', $id);
        return $this->db->delete('user');
    }

    public function getUserLoginApi($usuario, $password)
    {
        $query = $this->db->query("SELECT *
                FROM user WHERE  UPPER(correo) = UPPER('{$usuario}') AND password = '{$password}'");
        return $query->row();
    }

    public function getUserRespApi($id)
    {

        $query = $this->db->query("SELECT nombres, avatar
                                    FROM user WHERE id = '{$id}'");
        return $query->row();
    }

    public function setPregunta($id_orientacion, $otro_orientada, $id_sustancia, $nombre_producto, $descripcion, $id_persona)
    {

     /*  return $this->db->query("INSERT INTO pregunta (id_orientacion, otro_orientada, id_sustancia, nombre_producto, descripcion, id_persona) 
       values ('{$id_orientacion}', '{$otro_orientada}', '{$id_sustancia}', '{$nombre_producto}', '{$descripcion}', '{$id_persona}')"); */

       $this->db->insert("pregunta", ["id_orientacion" => $id_orientacion, "otro_orientada" => $otro_orientada, "id_sustancia" => $id_sustancia, "nombre_producto" => $nombre_producto,
        "descripcion" => $descripcion, "id_user" => $id_persona]);
        $insert_id = $this->db->insert_id();

        return  $insert_id;
   }

   public function setTemas($id_pregunta, $id_tema, $otro = null)
   {

    /*  return $this->db->query("INSERT INTO pregunta (id_orientacion, otro_orientada, id_sustancia, nombre_producto, descripcion, id_persona) 
      values ('{$id_orientacion}', '{$otro_orientada}', '{$id_sustancia}', '{$nombre_producto}', '{$descripcion}', '{$id_persona}')"); */

      $this->db->insert("pregunta_tema", ["id_pregunta" => $id_pregunta, "id_tema" => $id_tema, "otro_tema" => $otro]);
       $insert_id = $this->db->insert_id();

       return  $insert_id;
  }

  public function setRespuesta($descripcion, $id_pregunta, $id_persona)
  {

     $this->db->insert("respuesta", ["r_descrip" => $descripcion, "id_pregunta" => $id_pregunta, "id_user" => $id_persona]);
      $insert_id = $this->db->insert_id();

      return  $insert_id;
 }

 public function setEstadoPregunta($id_pregunta, $estado)
  {

  return $this->db->query("UPDATE pregunta SET estado = '{$estado}' WHERE id = '{$id_pregunta}'");


      
 }

}
