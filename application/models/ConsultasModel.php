<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class ConsultasModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    public function getOrientada()
    {
        return $this->db->query("SELECT * FROM orientacion")->result();
    }
    
    public function getTemas()
    {
        return $this->db->query("SELECT * FROM tema")->result();
    }

    public function getSustancia()
    {
        return $this->db->query("SELECT * FROM sustancia")->result();
    }

    public function getPendientes($id_persona)
    {
        $sql = $this->db->query("SELECT count(*) num FROM pregunta WHERE id_user = '{$id_persona}' AND estado = '0'")->row();
        if($sql->num > 0){
            return true;
        }else{
            return false;
        }
    }

    public function getPendientesAdmin()
    {
        return $this->db->query("SELECT *, p.id as id_p
        FROM pregunta p
        LEFT JOIN respuesta r ON p.id = r.id_pregunta
        ")->result();
    }

    public function getPreguntas($id_persona)
    {
        return $this->db->query("SELECT *, p.id as id_p, p.created_at fe
        FROM (select * from pregunta where id_user = '{$id_persona}') p
        LEFT JOIN respuesta r ON p.id = r.id_pregunta 
        ")->result();
    }

    public function getPreguntasApi($id_persona)
    {
        return $this->db->query("SELECT *, p.id as id_p, p.created_at fe
        FROM (select * from pregunta where id_user = '{$id_persona}') p
        LEFT JOIN respuesta r ON p.id = r.id_pregunta ORDER BY p.id desc
        ")->result();
    }

    public function getRespuesta($id)
    {
        return $this->db->query("SELECT r.*, p.*,a.* FROM respuesta r, pregunta p, user a 
        WHERE r.id_pregunta = '{$id}' AND r.id_pregunta = p.id AND a.id = p.id_user")->row();
    }

    public function getPregunta($id)
    {
        return $this->db->query("SELECT p.*, a.id id_p, a.nombres, a.correo, a.avatar FROM pregunta p, user a 
        WHERE p.id = '{$id}' AND a.id = p.id_user")->row();
    }

    public function getTemasPregunta($id)
    {
        return $this->db->query("SELECT * FROM pregunta_tema p, tema t WHERE p.id_pregunta = '{$id}' AND p.id_tema = t.id")->result();
    }

    public function getSustanciaPregunta($id)
    {
        return $this->db->query("SELECT * FROM sustancia 
        WHERE id = '{$id}'")->row();
    }

    public function getOrientadaPregunta($id)
    {
        return $this->db->query("SELECT * FROM orientacion 
        WHERE id = '{$id}'")->row();
    }

    public function setPregunta($id_orientacion, $otro_orientada, $id_sustancia, $nombre_producto, $descripcion, $id_persona)
    {
       $this->db->insert("pregunta", ["id_orientacion" => $id_orientacion, "otro_orientada" => $otro_orientada, "id_sustancia" => $id_sustancia, "nombre_producto" => $nombre_producto,
        "descripcion" => $descripcion, "id_user" => $id_persona]);
        $insert_id = $this->db->insert_id();

        return  $insert_id;
   }

   public function setTemas($id_pregunta, $id_tema, $otro = null)
   {
      $this->db->insert("pregunta_tema", ["id_pregunta" => $id_pregunta, "id_tema" => $id_tema, "otro_tema" => $otro]);
       $insert_id = $this->db->insert_id();

       return  $insert_id;
  }

  public function setRespuesta($descripcion, $id_pregunta, $id_persona)
  {
     $this->db->insert("respuesta", ["descrip_r" => $descripcion, "id_pregunta" => $id_pregunta, "id_user_r" => $id_persona]);
      $insert_id = $this->db->insert_id();

      return  $insert_id;
 }

 public function setEstadoPregunta($id_pregunta, $estado)
  {

  return $this->db->query("UPDATE pregunta SET estado = '{$estado}' WHERE id = '{$id_pregunta}'");
 }

    public function masivo($sql)
    {
        return $this->db->query($sql);
    }

    public function consulta($id)
    {
        return $this->db->query("select * from ".$id)->result();
    }

    public function consultaIdUlt($id)
    {
        return $this->db->query("select MAX(id) AS id from ".$id)->row();
    }

}
