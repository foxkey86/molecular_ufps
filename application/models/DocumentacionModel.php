<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class DocumentacionModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getDocumentos()
    {
        return $this->db->query("SELECT *
        FROM archivo")->result();
    }

    public function getDocumentosPerfil($id_perfil, $tipo)
    {
        return $this->db->query("SELECT *
        FROM archivo a, perfil_archivo p 
        WHERE p.id_perfil = '{$id_perfil}' AND
        a.id = p.id_archivo AND p.estado = 1
        AND a.tipo = '{$tipo}'")->result();
    }

    public function getDocumentosPerfilAll($id_perfil)
    {
        return $this->db->query("SELECT a.*, p.*
        FROM archivo a, perfil_archivo p 
        WHERE p.id_perfil = '{$id_perfil}' AND
        a.id = p.id_archivo AND p.estado = 1 
        order by a.nombre")->result();
    }

    public function getArchivoId($id)
    {
        return $this->db->query("SELECT a.*, p.*, e.nombre periodo, p.id id_perfil_archivo FROM archivo a, periodo e, perfil_archivo p
        WHERE a.id = '{$id}' AND a.id = p.id_archivo AND a.id_periodo = e.id")->row();
    }

    public function getTotal($id_perfil, $estado)
    {
        return $this->db->query("SELECT count(*) num
        FROM user_archivo u, perfil_archivo p, archivo a, periodo e, user x 
        WHERE p.id_supervisor = '{$id_perfil}' AND
        u.id_perfil_archivo = p.id AND u.estado = '{$estado}' AND
        p.id_archivo = a.id AND a.id_periodo = e.id AND
        u.id_user = x.id
        order by fecha_carga")->row();
    }

    public function getTotalHistorico($id_perfil, $estado)
    {
        return $this->db->query("SELECT count(*) num
        FROM historico_solicitudes u, perfil_archivo p, archivo a, periodo e, user x 
        WHERE p.id_supervisor = '{$id_perfil}' AND
        u.id_perfil_archivo = p.id AND u.estado = '{$estado}' AND
        p.id_archivo = a.id AND a.id_periodo = e.id AND
        u.id_user = x.id
        order by fecha_carga")->row();
    }

    public function getDocumentosPendientes($id_perfil)
    {
        return $this->db->query("SELECT u.*, p.*, a.*, x.*, u.id id_doc, e.nombre nom_per
        FROM user_archivo u, perfil_archivo p, archivo a, periodo e, user x 
        WHERE p.id_supervisor = '{$id_perfil}' AND
        u.id_perfil_archivo = p.id AND u.estado = 1 AND
        p.id_archivo = a.id AND a.id_periodo = e.id AND
        u.id_user = x.id
        order by fecha_carga")->result();
    }

    public function getDocumentosRechazadosUser($id_user)
    {
        return $this->db->query("SELECT u.*, p.*, a.*, x.*, u.id id_doc, e.nombre nom_per
        FROM user_archivo u, perfil_archivo p, archivo a, periodo e, user x 
        WHERE u.id_user = '{$id_user}' AND u.id_perfil_archivo = p.id AND u.estado = 3 AND
        p.id_archivo = a.id AND a.id_periodo = e.id AND
        u.id_user = x.id
        order by fecha_carga")->result();
    }

    public function getDocumentosPendientesUser($id_user)
    {
        return $this->db->query("SELECT u.*, p.*, a.*, x.*, u.id id_doc, e.nombre nom_per
        FROM user_archivo u, perfil_archivo p, archivo a, periodo e, user x 
        WHERE u.id_user = '{$id_user}' AND u.id_perfil_archivo = p.id AND u.estado = 1 AND
        p.id_archivo = a.id AND a.id_periodo = e.id AND
        u.id_user = x.id
        order by fecha_carga")->result();
    }

    public function getDocumentosProcesadosUser($id_user)
    {
        return $this->db->query("SELECT u.*, p.*, a.*, x.*, u.id id_doc, e.nombre nom_per
        FROM user_archivo u, perfil_archivo p, archivo a, periodo e, user x 
        WHERE u.id_user = '{$id_user}' AND u.id_perfil_archivo = p.id AND u.estado = 2 AND
        p.id_archivo = a.id AND a.id_periodo = e.id AND
        u.id_user = x.id
        order by fecha_carga")->result();
    }


    public function getTotalUser($id_user, $estado)
    {
        return $this->db->query("SELECT count(*) num
        FROM user_archivo u, perfil_archivo p, archivo a, periodo e, user x 
        WHERE u.id_user = '{$id_user}' AND
        u.id_perfil_archivo = p.id AND u.estado = '{$estado}' AND
        p.id_archivo = a.id AND a.id_periodo = e.id AND
        u.id_user = x.id
        order by fecha_carga")->row();
    }

    public function getDocumentosRechazados($id_perfil)
    {
        return $this->db->query("SELECT u.*, p.*, a.*, x.*, u.id id_doc, e.nombre nom_per
        FROM user_archivo u, perfil_archivo p, archivo a, periodo e, user x 
        WHERE p.id_supervisor = '{$id_perfil}' AND
        u.id_perfil_archivo = p.id AND u.estado = 3 AND
        p.id_archivo = a.id AND a.id_periodo = e.id AND
        u.id_user = x.id
        order by fecha_carga")->result();
    }

    public function getDocumentosProcesados($id_perfil)
    {
        return $this->db->query("SELECT u.*, p.*, a.*, x.*, u.id id_doc, e.nombre nom_per
        FROM user_archivo u, perfil_archivo p, archivo a, periodo e, user x 
        WHERE p.id_supervisor = '{$id_perfil}' AND
        u.id_perfil_archivo = p.id AND u.estado = 2 AND
        p.id_archivo = a.id AND a.id_periodo = e.id AND
        u.id_user = x.id
        order by fecha_carga")->result();
    }

    public function getDocumentosHistoricoRec($id_perfil)
    {
        return $this->db->query("SELECT u.*, p.*, a.*, x.*, u.id id_doc, e.nombre nom_per
        FROM historico_solicitudes u, perfil_archivo p, archivo a, periodo e, user x 
        WHERE p.id_supervisor = '{$id_perfil}' AND
        u.id_perfil_archivo = p.id AND u.estado = 3 AND
        p.id_archivo = a.id AND a.id_periodo = e.id AND
        u.id_user = x.id
        order by fecha_carga")->result();
    }

    public function getUserArchivo($id)
    {
        return $this->db->query("SELECT u.*, p.*, a.*, x.*, u.id id_doc, e.nombre nom_per, u.estado estado
        FROM user_archivo u, perfil_archivo p, archivo a, periodo e, user x 
        WHERE u.id = '{$id}' AND u.id_perfil_archivo = p.id AND a.id_periodo = e.id AND
        p.id_archivo = a.id AND u.id_user = x.id")->row();
    }

    public function insert_documento($info)
    {
        return $this->db->insert('user_archivo', $info);
    }

    public function update_documento($id_registro, $info)
    {
        $this->db->where('id', $id_registro);
        return $this->db->update('user_archivo', $info);
    }

    public function insert_documento_historial($info)
    {
        return $this->db->insert('historico_solicitudes', $info);
    }

    public function delete_documento($id)
    {
        $this->db->where('id', $id);
        $this->db->delete('user_archivo');
    }
}
