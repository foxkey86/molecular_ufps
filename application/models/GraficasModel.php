<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class GraficasModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getReporteEvento($id)
    {
        return $this->db->query("SELECT * FROM reporte r where id_evento = '{$id}'")->result();
    }

    public function getReporteMujeres($id)
    {
        return $this->db->query("SELECT count(*) num FROM reporte r where id_evento = '{$id}' and sexo = 'F'")->row();
    }

    public function getReporteHombres($id)
    {
        return $this->db->query("SELECT count(*) num FROM reporte r where id_evento = '{$id}' and sexo = 'M'")->row();
    }

    public function getEnfermedad($id,  $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, r.semana, COUNT(*) as num FROM reporte r, reporte_dengue d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' and r.semana Between '{$trimestre[0]}' And '{$trimestre[1]}' 
        and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' $complete GROUP BY r.id_evento, r.semana ORDER BY r.semana")->result();
    }

    public function getEnfermedadHep($id,  $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, r.semana, COUNT(*) as num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' and r.semana Between '{$trimestre[0]}' And '{$trimestre[1]}' 
        and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' $complete GROUP BY r.id_evento, r.semana ORDER BY r.semana")->result();
    }

    public function getAfiliacion($id,  $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, r.tip_ss, COUNT(*) as num FROM reporte r, reporte_dengue d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' And '{$trimestre[1]}'
        GROUP BY r.id_evento, r.tip_ss ORDER BY r.tip_ss")->result();
    }

    public function getEdades($id, $anyo, $ini, $fin, $dpto_muni, $clasfinal, $trimestre, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT count(*) num FROM reporte r, reporte_dengue d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and 
        r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}' and
        r.edad Between '{$ini}' and '{$fin}'")->row();
    }

    public function getEdadesHep($id, $anyo, $ini, $fin, $dpto_muni, $clasfinal, $trimestre, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT count(*) num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and 
        r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}' and
        r.edad Between '{$ini}' and '{$fin}'")->row();
    }

    public function getHospitalizados($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, r.pac_hos, COUNT(*) as num FROM reporte r, reporte_dengue d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, r.pac_hos ORDER BY r.pac_hos")->result();
    }

    public function getBarOcu($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, r.id_muni_ocu, (SELECT nombre FROM municipio WHERE id = r.id_muni_ocu) muni, 
        COUNT(*) as num, r.bar_ver FROM reporte r, reporte_dengue d WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete
        and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, r.id_muni_ocu, r.bar_ver 
        ORDER BY r.bar_ver desc")->result();
    }

    public function getMuniOcu($id, $clasfinal, $anyo, $trimestre, $dpto_muni)
    {
        return $this->db->query("SELECT r.id_evento, r.id_muni_ocu, (SELECT nombre FROM municipio WHERE id = r.id_muni_ocu) muni, 
        COUNT(*) as num FROM reporte r, reporte_dengue d WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' and 
        r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}' and
        r.dpto_muni = '{$dpto_muni}' GROUP BY r.id_evento, r.id_muni_ocu 
        ORDER BY r.id_muni_ocu desc")->result();
    }

    public function getMuniOcuHep($id, $clasfinal, $anyo, $trimestre, $dpto_muni)
    {
        return $this->db->query("SELECT r.id_evento, r.id_muni_ocu, (SELECT nombre FROM municipio WHERE id = r.id_muni_ocu) muni, 
        COUNT(*) as num FROM reporte r, reporte_hepatitis d WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' and 
        r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}' and
        r.dpto_muni = '{$dpto_muni}' GROUP BY r.id_evento, r.id_muni_ocu 
        ORDER BY r.id_muni_ocu desc")->result();
    }

    public function getGestantesHep($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.gp_gestan, COUNT(*) as num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.gp_gestan ORDER BY d.gp_gestan")->result();
    }

    public function getDiscapaHep($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.gp_discapa, COUNT(*) as num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.gp_discapa ORDER BY d.gp_discapa")->result();
    }

    public function getDesplaHep($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.gp_desplaz, COUNT(*) as num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.gp_desplaz ORDER BY d.gp_desplaz")->result();
    }

    public function getMigraHep($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.gp_migrant, COUNT(*) as num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.gp_migrant ORDER BY d.gp_migrant")->result();
    }

    public function getCarceHep($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.gp_carcela, COUNT(*) as num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.gp_carcela ORDER BY d.gp_carcela")->result();
    }

    public function getIndiHep($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.gp_indigen, COUNT(*) as num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.gp_indigen ORDER BY d.gp_indigen")->result();
    }

    public function getIcbfHep($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.gp_pobicbf, COUNT(*) as num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.gp_pobicbf ORDER BY d.gp_pobicbf")->result();
    }

    public function getMadComHep($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.gp_mad_com, COUNT(*) as num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.gp_mad_com ORDER BY d.gp_mad_com")->result();
    }

    public function getDesmoHep($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.gp_desmovi, COUNT(*) as num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.gp_desmovi ORDER BY d.gp_desmovi")->result();
    }

    public function getPsiquiHep($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.gp_psiquia, COUNT(*) as num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.gp_psiquia ORDER BY d.gp_psiquia")->result();
    }

    public function getVicHep($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.gp_vic_vio, COUNT(*) as num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.gp_vic_vio ORDER BY d.gp_vic_vio")->result();
    }

    public function getOtrosHep($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.gp_otros, COUNT(*) as num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.gp_otros ORDER BY d.gp_otros")->result();
    }

    public function getConinfeccionHep($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.coinf_vih_coinfeccion, COUNT(*) as num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.coinf_vih_coinfeccion ORDER BY d.coinf_vih_coinfeccion")->result();
    }

    public function getTransmision($id,  $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.met_tra, COUNT(*) as num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' And '{$trimestre[1]}'
        GROUP BY r.id_evento, d.met_tra ORDER BY d.met_tra")->result();
    }

    public function getAfiliacionHep($id,  $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, r.tip_ss, COUNT(*) as num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' And '{$trimestre[1]}'
        GROUP BY r.id_evento, r.tip_ss ORDER BY r.tip_ss")->result();
    }

    public function getGeneroDengue($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, r.sexo, COUNT(*) as num FROM reporte r, reporte_dengue d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, r.sexo ORDER BY r.sexo")->result();
    }

    public function getGeneroHepatitis($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, r.sexo, COUNT(*) as num FROM reporte r, reporte_hepatitis d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, r.sexo ORDER BY r.sexo")->result();
    }

    public function getBarOcuHep($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, r.id_muni_ocu, (SELECT nombre FROM municipio WHERE id = r.id_muni_ocu) muni, 
        COUNT(*) as num, r.bar_ver FROM reporte r, reporte_hepatitis d WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete
        and r.id = d.id_reporte and d.clasfinal = '{$clasfinal}' and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, r.id_muni_ocu, r.bar_ver 
        ORDER BY r.bar_ver desc")->result();
    }

    public function getCondicionTub($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.cond_tuber, COUNT(*) as num FROM reporte r, reporte_tbc d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.tip_tub = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.cond_tuber ORDER BY d.cond_tuber")->result();
    }

    public function getConinfeccionTub($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.vih_confirmado, COUNT(*) as num FROM reporte r, reporte_tbc d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.tip_tub = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.vih_confirmado ORDER BY d.vih_confirmado")->result();
    }

    public function getMapaTub($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT * FROM mapa_evento WHERE id_evento = '{$id}' 
        and anyo = '{$anyo}' $complete and 
        clasificacion = '{$clasfinal}' and trimestre = '{$trimestre}'")->result();
    }


    public function getMuniOcuTub($id, $clasfinal, $anyo, $trimestre, $dpto_muni)
    {
        return $this->db->query("SELECT r.id_evento, r.id_muni_ocu, (SELECT nombre FROM municipio WHERE id = r.id_muni_ocu) muni, 
        COUNT(*) as num FROM reporte r, reporte_tbc d WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' and 
        r.id = d.id_reporte and d.tip_tub = '{$clasfinal}' and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}' and
        r.dpto_muni = '{$dpto_muni}' GROUP BY r.id_evento, r.id_muni_ocu 
        ORDER BY r.id_muni_ocu desc")->result();
    }

    public function getDptoEntIndTub($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.nombre_grupo, COUNT(*) as num 
        FROM reporte r, reporte_tbc d WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' and per_etn = 1 and
                r.id = d.id_reporte and d.tip_tub = '{$clasfinal}' and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}' and
                r.dpto_muni = '{$dpto_muni}' $complete GROUP BY r.id_evento, d.nombre_grupo
                ORDER BY d.nombre_grupo desc")->result();
    }

    public function getEnfermedadTub($id,  $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, r.semana, COUNT(*) as num FROM reporte r, reporte_tbc d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' and r.semana Between '{$trimestre[0]}' And '{$trimestre[1]}' 
        and r.id = d.id_reporte and d.tip_tub = '{$clasfinal}' $complete GROUP BY r.id_evento, r.semana ORDER BY r.semana")->result();
    }

    public function getEdadesTub($id, $anyo, $ini, $fin, $dpto_muni, $clasfinal, $trimestre, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT count(*) num FROM reporte r, reporte_tbc d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and 
        r.id = d.id_reporte and d.tip_tub = '{$clasfinal}' and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}' and
        r.edad Between '{$ini}' and '{$fin}'")->row();
    }

    public function getAfiliacionTub($id,  $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, r.tip_ss, COUNT(*) as num FROM reporte r, reporte_tbc d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.tip_tub = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' And '{$trimestre[1]}'
        GROUP BY r.id_evento, r.tip_ss ORDER BY r.tip_ss")->result();
    }

    public function getEtniaTub($id,  $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.per_etn, COUNT(*) as num FROM reporte r, reporte_tbc d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.tip_tub = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.per_etn ORDER BY d.per_etn")->result();
    }

    public function getEtniaMuniTub($id,  $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {

        $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
    
        return $this->db->query("SELECT r.id_evento, d.per_etn, COUNT(*) as num FROM reporte r, reporte_tbc d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.tip_tub = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.per_etn ORDER BY d.per_etn")->result();
    }

    public function getEtniaMuniTodos($id,  $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {

        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }
    
        return $this->db->query("SELECT r.id_evento, d.per_etn, COUNT(*) as num FROM reporte r, reporte_tbc d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.tip_tub = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.per_etn ORDER BY d.per_etn")->result();
    }

    public function getDiaTub($id,  $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.diabetes, COUNT(*) as num FROM reporte r, reporte_tbc d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.tip_tub = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.diabetes ORDER BY d.diabetes")->result();
    }

    public function getRenTub($id,  $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.enfe_renal, COUNT(*) as num FROM reporte r, reporte_tbc d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.tip_tub = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.enfe_renal ORDER BY d.enfe_renal")->result();
    }

    public function getDesTub($id,  $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.desnutrici, COUNT(*) as num FROM reporte r, reporte_tbc d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.tip_tub = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.desnutrici ORDER BY d.desnutrici")->result();
    }

    public function getTrabTub($id,  $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.trab_salud, COUNT(*) as num FROM reporte r, reporte_tbc d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.tip_tub = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, d.trab_salud ORDER BY d.trab_salud")->result();
    }

    public function getBarOcuTub($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, r.id_muni_ocu, (SELECT nombre FROM municipio WHERE id = r.id_muni_ocu) muni, 
        COUNT(*) as num, r.bar_ver FROM reporte r, reporte_tbc d WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete
        and r.id = d.id_reporte and d.tip_tub = '{$clasfinal}' and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, r.id_muni_ocu, r.bar_ver 
        ORDER BY r.bar_ver desc")->result();
    }

    public function getNacTub($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, r.id_nacionalidad, (SELECT nombre FROM pais WHERE id = r.id_nacionalidad) pais, 
        COUNT(*) as num FROM reporte r, reporte_tbc d WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and
        r.id = d.id_reporte and d.tip_tub = '{$clasfinal}' and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}' and
        r.dpto_muni = '{$dpto_muni}' GROUP BY r.id_evento, r.id_nacionalidad 
        ORDER BY r.id_nacionalidad desc")->result();
    }

    public function getGeneroTub($id, $clasfinal, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, r.sexo, COUNT(*) as num FROM reporte r, reporte_tbc d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte and d.tip_tub = '{$clasfinal}' 
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, r.sexo ORDER BY r.sexo")->result();
    }
    
    public function getEdadesVih($id, $anyo, $ini, $fin, $dpto_muni, $trimestre, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT count(*) num FROM reporte r
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and 
        r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}' and
        r.edad Between '{$ini}' and '{$fin}'")->row();
    }

    public function getNacVih($id, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, r.id_nacionalidad, (SELECT nombre FROM pais WHERE id = r.id_nacionalidad) pais, 
        COUNT(*) as num FROM reporte r WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' and 
        r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}' $complete and
        r.dpto_muni = '{$dpto_muni}' GROUP BY r.id_evento, r.id_nacionalidad 
        ORDER BY r.id_nacionalidad desc")->result();
    }

    public function getEstadoClinicoVih($id, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.est_cli, COUNT(*) as num FROM reporte r, reporte_vih d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte
        and r.semana Between '{$trimestre[0]}' And '{$trimestre[1]}'
        GROUP BY r.id_evento, d.est_cli ORDER BY d.est_cli")->result();
    }

    public function getAfiliacionVih($id, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, r.tip_ss, COUNT(*) as num FROM reporte r, reporte_vih d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete and r.id = d.id_reporte
        and r.semana Between '{$trimestre[0]}' And '{$trimestre[1]}'
        GROUP BY r.id_evento, r.tip_ss ORDER BY r.tip_ss")->result();
    }

    public function getMuniOcuVih($id, $anyo, $trimestre, $dpto_muni)
    {
        return $this->db->query("SELECT r.id_evento, r.id_muni_ocu, (SELECT nombre FROM municipio WHERE id = r.id_muni_ocu) muni, 
        COUNT(*) as num FROM reporte r, reporte_vih d WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' and 
        r.id = d.id_reporte and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}' and
        r.dpto_muni = '{$dpto_muni}' GROUP BY r.id_evento, r.id_muni_ocu 
        ORDER BY r.id_muni_ocu desc")->result();
    }

    public function getBarOcuVih($id, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, r.id_muni_ocu, (SELECT nombre FROM municipio WHERE id = r.id_muni_ocu) muni, 
        COUNT(*) as num, r.bar_ver FROM reporte r, reporte_vih d WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete
        and r.id = d.id_reporte and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}'
        GROUP BY r.id_evento, r.id_muni_ocu, r.bar_ver 
        ORDER BY r.bar_ver desc")->result();
    }

    public function getGeneroVih($id, $anyo, $trimestre, $dpto_muni, $muni)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni_ocu = '{$muni}'";
        }

        return $this->db->query("SELECT r.id_evento, d.ide_genero, COUNT(*) as num FROM reporte r, reporte_vih d
        WHERE r.id_evento = '{$id}' and r.anyo = '{$anyo}' $complete
        and r.semana Between '{$trimestre[0]}' and '{$trimestre[1]}' and r.id = d.id_reporte
        GROUP BY r.id_evento, d.ide_genero ORDER BY d.ide_genero")->result();
    }

    public function getEdadesCov($anyo, $ini, $dpto_muni, $trimestre, $muni, $tipo)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni = '{$muni}'";
        }

        $clas = "";
        if($tipo == 2){
            $clas = "and r.clasfinal = 1";
        }elseif($tipo == 3){
            $clas = "and r.clasfinal = 2";
        }elseif($tipo == 4){
            $clas = "and r.clasfinal = 3";
        }

        if($anyo == '2020' && $trimestre[1] == '53'){
            return $this->db->query("SELECT count(*) as num FROM reporte_covid r
            WHERE r.anyo IN (2020,2021) $complete and r.se_fec_sint Between '{$trimestre[0]}' and '{$trimestre[1]}' $clas
            and r.id_grupo_edad = '{$ini}'")->row();
        }

        return $this->db->query("SELECT count(*) as num FROM reporte_covid r
        WHERE r.anyo = '{$anyo}' $complete and r.se_fec_sint Between '{$trimestre[0]}' and '{$trimestre[1]}' $clas
        and r.id_grupo_edad = '{$ini}'")->row();
    }

    public function getGeneroCov($anyo, $trimestre, $dpto_muni, $muni, $tipo)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni = '{$muni}'";
        }

        $clas = "";
        if($tipo == 2){
            $clas = "and r.clasfinal = 1";
        }elseif($tipo == 3){
            $clas = "and r.clasfinal = 2";
        }elseif($tipo == 4){
            $clas = "and r.clasfinal = 3";
        }

        if($anyo == '2020' && $trimestre[1] == '53'){
            return $this->db->query("SELECT r.sexo, COUNT(*) as num FROM reporte_covid r
                                    WHERE r.anyo in (2020,2021) $complete and r.se_fec_sint Between '{$trimestre[0]}' and '{$trimestre[1]}' $clas
                                    GROUP BY r.sexo ORDER BY r.sexo")->result();
        }

        return $this->db->query("SELECT r.sexo, COUNT(*) as num FROM reporte_covid r
        WHERE r.anyo = '{$anyo}' $complete and r.se_fec_sint Between '{$trimestre[0]}' and '{$trimestre[1]}' $clas
        GROUP BY r.sexo ORDER BY r.sexo")->result();
    }

    public function getBarOcuCov($anyo, $trimestre, $dpto_muni, $muni, $tipo)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni = '{$muni}'";
        }

        $clas = "";
        if($tipo == 2){
            $clas = "and r.clasfinal = 1";
        }elseif($tipo == 3){
            $clas = "and r.clasfinal = 2";
        }elseif($tipo == 4){
            $clas = "and r.clasfinal = 3";
        }

        if($anyo == '2020' && $trimestre[1] == '53'){
            return $this->db->query("SELECT r.id_muni, (SELECT nombre FROM municipio WHERE id = r.id_muni) muni, 
            COUNT(*) as num, r.barrio FROM reporte_covid r WHERE r.anyo IN (2020,2021) $complete 
            and r.se_fec_sint Between '{$trimestre[0]}' and '{$trimestre[1]}' $clas
            GROUP BY r.id_muni, r.barrio 
            ORDER BY r.barrio desc")->result();
        }

        return $this->db->query("SELECT r.id_muni, (SELECT nombre FROM municipio WHERE id = r.id_muni) muni, 
        COUNT(*) as num, r.barrio FROM reporte_covid r WHERE r.anyo = '{$anyo}' $complete 
        and r.se_fec_sint Between '{$trimestre[0]}' and '{$trimestre[1]}' $clas
        GROUP BY r.id_muni, r.barrio 
        ORDER BY r.barrio desc")->result();
    }

    public function getMuniOcuCov($anyo, $trimestre, $dpto_muni, $tipo)
    {
        $clas = "";
        if($tipo == 2){
            $clas = "and r.clasfinal = 1";
        }elseif($tipo == 3){
            $clas = "and r.clasfinal = 2";
        }elseif($tipo == 4){
            $clas = "and r.clasfinal = 3";
        }

        if($anyo == '2020' && $trimestre[1] == '53'){
            return $this->db->query("SELECT r.id_muni, (SELECT nombre FROM municipio WHERE id = r.id_muni) muni, 
            COUNT(*) as num FROM reporte_covid r WHERE r.anyo IN (2020,2021) and 
            r.se_fec_sint Between '{$trimestre[0]}' and '{$trimestre[1]}' $clas and
            r.dpto_muni = '{$dpto_muni}' GROUP BY r.id_muni
            ORDER BY r.id_muni desc")->result();
        }

        return $this->db->query("SELECT r.id_muni, (SELECT nombre FROM municipio WHERE id = r.id_muni) muni, 
        COUNT(*) as num FROM reporte_covid r WHERE r.anyo = '{$anyo}' and 
        r.se_fec_sint Between '{$trimestre[0]}' and '{$trimestre[1]}' $clas and
        r.dpto_muni = '{$dpto_muni}' GROUP BY r.id_muni
        ORDER BY r.id_muni desc")->result();
    }

    public function getEnfermedadCov($anyo, $trimestre, $dpto_muni, $muni, $tipo)
    {
        $complete = "";
        if($dpto_muni == 1){
            $complete = "and r.dpto_muni = '{$dpto_muni}'";
        }else{
            $complete = "and r.dpto_muni = '{$dpto_muni}' and id_muni = '{$muni}'";
        }

        $clas = "";
        if($tipo == 2){
            $clas = "and r.clasfinal = 1";
        }elseif($tipo == 3){
            $clas = "and r.clasfinal = 2";
        }elseif($tipo == 4){
            $clas = "and r.clasfinal = 3";
        }

        if($anyo == '2020' && $trimestre[1] == '53'){
            return $this->db->query("SELECT r.se_fec_sint, COUNT(*) as num FROM reporte_covid r
            WHERE r.anyo IN (2020,2021) $clas and r.se_fec_sint Between '{$trimestre[0]}' And '{$trimestre[1]}' 
            $complete GROUP BY r.se_fec_sint ORDER BY r.se_fec_sint")->result();
        }

        return $this->db->query("SELECT r.se_fec_sint, COUNT(*) as num FROM reporte_covid r
        WHERE r.anyo = '{$anyo}' $clas and r.se_fec_sint Between '{$trimestre[0]}' And '{$trimestre[1]}' 
        $complete GROUP BY r.se_fec_sint ORDER BY r.se_fec_sint")->result();
    }


}
