<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class ReportesModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getReporteDengue($anyo, $dptomuni, $id_evento)
    {
        return $this->db->query("SELECT r.*, d.*, e.*, p.*, t.*, r.id id_rep
        FROM reporte r, reporte_dengue d, evento e, pais p, tipo_seguridad_social t
        WHERE r.id = d.id_reporte AND r.anyo = '{$anyo}'
        AND r.dpto_muni = '{$dptomuni}' AND r.id_evento = $id_evento
        AND r.id_evento = e.id AND r.id_nacionalidad = p.id
        AND r.tip_ss = t.id
        order by r.id")->result();
    }

    public function getReporteHep($anyo, $dptomuni, $id_evento)
    {
        return $this->db->query("SELECT r.*, d.*, e.*, p.*, t.*, r.id id_rep
        FROM reporte r, reporte_hepatitis d, evento e, pais p, tipo_seguridad_social t
        WHERE r.id = d.id_reporte AND r.anyo = '{$anyo}'
        AND r.dpto_muni = '{$dptomuni}' AND r.id_evento = $id_evento
        AND r.id_evento = e.id AND r.id_nacionalidad = p.id
        AND r.tip_ss = t.id
        order by r.id")->result();
    }

    public function getReporteTbc($anyo, $dptomuni, $id_evento)
    {
        return $this->db->query("SELECT r.*, d.*, e.*, p.*, t.*, r.id id_rep
        FROM reporte r, reporte_tbc d, evento e, pais p, tipo_seguridad_social t
        WHERE r.id = d.id_reporte AND r.anyo = '{$anyo}'
        AND r.dpto_muni = '{$dptomuni}' AND r.id_evento = $id_evento
        AND r.id_evento = e.id AND r.id_nacionalidad = p.id
        AND r.tip_ss = t.id
        order by r.id")->result();
    }

    public function getReporteVih($anyo, $dptomuni, $id_evento)
    {
        return $this->db->query("SELECT r.*, d.*, e.*, p.*, t.*, r.id id_rep
        FROM reporte r, reporte_vih d, evento e, pais p, tipo_seguridad_social t
        WHERE r.id = d.id_reporte AND r.anyo = '{$anyo}'
        AND r.dpto_muni = '{$dptomuni}' AND r.id_evento = $id_evento
        AND r.id_evento = e.id AND r.id_nacionalidad = p.id
        AND r.tip_ss = t.id
        order by r.id")->result();
    }

    public function getReporteCov($anyo, $dptomuni)
    {
        return $this->db->query("SELECT r.*, e.nombre nom_prueba, p.nombre nom_pais, 
        t.nombre nom_rec, g.nombre nom_gru, r.id id_rep
        FROM reporte_covid r, tipo_prueba e, pais p, clasificacion_covid t, grupo_edad g
        WHERE r.anyo = '{$anyo}'
        AND r.dpto_muni = '{$dptomuni}'
        AND r.id_tipo_prueba = e.id AND r.id_nacionalidad = p.id
        AND r.clasfinal = t.id
        AND r.id_grupo_edad = g.id
        order by r.id")->result();
    }



    public function getRegistroDengue($id)
    {
        return $this->db->query("SELECT r.*, d.*, r.id id_rep FROM reporte r, reporte_dengue d 
        WHERE r.id = '{$id}' AND d.id_reporte = '{$id}'")->row();
    }

    public function getRegistroHepatitis($id)
    {
        return $this->db->query("SELECT r.*, d.*, r.id id_rep FROM reporte r, reporte_hepatitis d 
        WHERE r.id = '{$id}' AND d.id_reporte = '{$id}'")->row();
    }

    public function getRegistroTuberculosis($id)
    {
        return $this->db->query("SELECT r.*, d.*, r.id id_rep FROM reporte r, reporte_tbc d 
        WHERE r.id = '{$id}' AND d.id_reporte = '{$id}'")->row();
    }

    public function getRegistroVih($id)
    {
        return $this->db->query("SELECT r.*, d.*, r.id id_rep FROM reporte r, reporte_vih d 
        WHERE r.id = '{$id}' AND d.id_reporte = '{$id}'")->row();
    }

    public function getRegistroCov($id)
    {
        return $this->db->query("SELECT * FROM reporte_covid r 
        WHERE r.id = '{$id}'")->row();
    }


    public function getEventos()
    {
        return $this->db->query("SELECT * FROM evento")->result();
    }

    public function getPais()
    {
        return $this->db->query("SELECT * FROM pais")->result();
    }

    public function getMunicipio()
    {
        return $this->db->query("SELECT * FROM municipio")->result();
    }

    public function getTipSS()
    {
        return $this->db->query("SELECT * FROM tipo_seguridad_social")->result();
    }

    public function getOpciones()
    {
        return $this->db->query("SELECT * FROM opcion")->result();
    }
  
    public function getClasificacionFinal()
    {
        return $this->db->query("SELECT * FROM clasificacion_dengue")->result();
    }

    public function getClasificacionFinalHep()
    {
        return $this->db->query("SELECT * FROM clasificacion_hepatitis")->result();
    }

    public function getCondicionTbc()
    {
        return $this->db->query("SELECT * FROM condicion_tbc")->result();
    }

    public function getEstadoClinico()
    {
        return $this->db->query("SELECT * FROM estado_clinico")->result();
    }


    public function getConducta()
    {
        return $this->db->query("SELECT * FROM conducta")->result();
    }

    public function getModoTransmision()
    {
        return $this->db->query("SELECT * FROM modo_transmision")->result();
    }

    public function getTipoTbc()
    {
        return $this->db->query("SELECT * FROM clasificacion_tbc")->result();
    }

    public function getTipoPrueba()
    {
        return $this->db->query("SELECT * FROM tipo_prueba")->result();
    }

    public function getTipoRecuperacion()
    {
        return $this->db->query("SELECT * FROM clasificacion_covid")->result();
    }

    public function getGrupoEdad()
    {
        return $this->db->query("SELECT * FROM grupo_edad")->result();
    }

    public function update_reporte($id_registro, $info)
    {
        $this->db->where('id', $id_registro);
        return $this->db->update('reporte', $info);
    }

    public function update_reporte_dengue($id_registro, $info)
    {
        $this->db->where('id_reporte', $id_registro);
        return $this->db->update('reporte_dengue', $info);
    }

    public function update_reporte_hepatitis($id_registro, $info)
    {
        $this->db->where('id_reporte', $id_registro);
        return $this->db->update('reporte_hepatitis', $info);
    }

    public function update_reporte_tbc($id_registro, $info)
    {
        $this->db->where('id_reporte', $id_registro);
        return $this->db->update('reporte_tbc', $info);
    }

    public function update_reporte_vih($id_registro, $info)
    {
        $this->db->where('id_reporte', $id_registro);
        return $this->db->update('reporte_vih', $info);
    }

    public function update_reporte_cov($id_registro, $info)
    {
        $this->db->where('id', $id_registro);
        return $this->db->update('reporte_covid', $info);
    }

    public function delete_dengue($id)
    {
        $this->db->where('id_reporte', $id);
        $this->db->delete('reporte_dengue');

        $this->db->where('id', $id);
        return $this->db->delete('reporte');
    }

    public function delete_hepatitis($id)
    {
        $this->db->where('id_reporte', $id);
        $this->db->delete('reporte_hepatitis');

        $this->db->where('id', $id);
        return $this->db->delete('reporte');
    }

    public function delete_tbc($id)
    {
        $this->db->where('id_reporte', $id);
        $this->db->delete('reporte_tbc');

        $this->db->where('id', $id);
        return $this->db->delete('reporte');
    }

    public function delete_vih($id)
    {
        $this->db->where('id_reporte', $id);
        $this->db->delete('reporte_vih');

        $this->db->where('id', $id);
        return $this->db->delete('reporte');
    }

    public function delete_cov($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('reporte_covid');
    }
}
