<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class LoginModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function getPersonas()
    {
        return $this->db->query("SELECT * FROM user")->result();
    }

    public function getUsuario($usuario, $password)
    {
        return $this->db->query("SELECT * FROM user WHERE correo = '{$usuario}' AND password = '{$password}' AND estado = 1")->row();
    }

    public function getTipoDocumentos()
    {
        return $this->db->query("SELECT * FROM tipo_documento")->result();
    }

    public function getOficios()
    {
        return $this->db->query("SELECT * FROM oficio")->result();
    }

    public function setUsuario($tipo, $documento, $nombres, $apellidos, $genero, $edad, $celular, $direccion, $correo, $password, $id_perfil)
     {

        return $this->db->query("INSERT INTO user (id_tipo_documento, documento, nombres, apellidos, genero, edad, celular, direccion, correo, password, id_perfil) 
        values ('{$tipo}', '{$documento}', '{$nombres}', '{$apellidos}', '{$genero}', '{$edad}','{$celular}','{$direccion}','{$correo}','{$password}','{$id_perfil}')");
    }

    public function getPersona($documento)
    {
        return $this->db->query("SELECT * FROM user WHERE documento = '{$documento}'")->row();
    }

    public function setProfesional($id_persona, $profesion, $institucion)
    {

       return $this->db->query("INSERT INTO profesional (id_user, profesion, institucion) 
       values ('{$id_persona}', '{$profesion}', '{$institucion}')");
   }

   public function setParticular($id_persona, $oficio)
    {

       return $this->db->query("INSERT INTO particular (id_user, id_oficio) 
       values ('{$id_persona}', '{$oficio}')");
   }

   public function getProfesional($id_persona)
   {
       return $this->db->query("SELECT * FROM profesional WHERE id_user = '{$id_persona}'")->row();
   }

   public function getUsuarioPerfil($usuario, $password, $perfil)
   {
       return $this->db->query("SELECT * FROM user WHERE correo = '{$usuario}' AND password = '{$password}' AND id_perfil = '{$perfil}'")->row();
   }

   public function getUsuarioPerfilLab($usuario, $password)
   {
       return $this->db->query("SELECT * FROM user WHERE correo = '{$usuario}' 
       AND password = '{$password}' 
       AND id_perfil in ('6','7','8','9')")->row();
   }

}