<div class="row"> 
    <div class="col-xl-12">
        <div class="card border mb-g">
            <div class="card-body pl-4 pt-4 pr-4 pb-0">
                <div class="d-flex flex-column">
                <?php echo form_open('usuarios/editar_usuario', ['class' => '', 'id' => 'form', 'role' => 'form'], ['editar' => 1, 'id_usuario' => $usuario->id]); ?>
                    <div class="form-group row">
                        <div class="col-3 pr-1">
                            <?php $label = 'Tipo de Documento'; ?>
                            <?php $name = 'id_tipo_documento'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown($name, $listado_documentos, $this->input->post($name), "class='form-control $invalid' id='$name' placeholder='Seleccione Tipo de Documento' required"); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Documento'; ?>
                            <?php $name = 'documento'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 15, 'placeholder' => 'Escriba el número de documento', 'class' => 'form-control '.$invalid], set_value($name), 'required'); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Nombres'; ?>
                            <?php $name = 'nombres'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 50, 'placeholder' => 'Escriba los nombres', 'class' => 'form-control '.$invalid], set_value($name), 'required'); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Apellidos'; ?>
                            <?php $name = 'apellidos'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 50, 'placeholder' => 'Escriba los apellidos', 'class' => 'form-control '.$invalid], set_value($name), 'required'); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Edad'; ?>
                            <?php $name = 'edad'; ?>
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'number', 'maxlength' => 3, 'placeholder' => $label, 'class' => 'form-control'], set_value($name), 'required'); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Género'; ?>
                            <?php $name = 'genero'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 1, 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Celular'; ?>
                            <?php $name = 'celular'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Dirección'; ?>
                            <?php $name = 'direccion'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 200, 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Correo'; ?>
                            <?php $name = 'correo'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'email', 'maxlength' => 50, 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Estado'; ?>
                            <?php $name = 'estado'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown($name, array('' => 'Seleccionar el estado','0' => 'Inactivo', '1' => 'Activo', '2' => 'Pendiente'), $this->input->post($name), "class='form-control $invalid' id='$name' placeholder='Seleccione el estado' required"); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Perfíl'; ?>
                            <?php $name = 'id_perfil'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown($name, $listado_perfil, $this->input->post($name), "class='form-control $invalid' id='$name' placeholder='Seleccione el Perfíl' required"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="height-8 d-flex flex-row align-items-center flex-wrap flex-shrink-0">
                        <button class="btn btn-info shadow-0 ml-auto" type="submit" ><i class="fas fa-save"></i> Editar Registro</button>
                    </div>
                <?php echo form_close(); ?>
                </div>
            </div>
        </div>                          
    </div>
    <div class="col-xl-12">
                                <div class="col-md-12 ml-auto text-left" style="margin-bottom:20px;">
                                    <a href="<?php echo base_url("usuarios/index"); ?>" class="btn btn-primary waves-effect waves-themed" type="button"><i class="fal fa-chevron-circle-left"></i> Atrás</a>
                                </div> 
                            </div>
</div>
                      