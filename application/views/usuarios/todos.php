
                            <div class="col-xl-12">
                                <div id="panel-1" class="panel">
                                  
                                    <div class="panel-container show">
                                        <div class="panel-content">
                                            <!-- datatable start -->
                                            <table id="dt-basic-example" class="table table-bordered table-hover table-striped w-100">
                                                <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Documento</th>
                                                        <th>Tipo Documento</th>
                                                        <th>Nombres</th>
                                                        <th>Apellidos</th>
                                                        <th>Edad</th>
                                                        <th>Género</th>
                                                        <th>Celular</th>
                                                        <th>Estado</th>
                                                        <th>Dirección</th>
                                                        <th>Correo</th>
                                                        <th>Perfíl</th>
                                                        <th>Admin Controls</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach($listado_usuarios as $row): ?>
                                                    <tr>
                                                        <td><?php echo $row->id; ?></td>
                                                        <td><?php echo $row->documento; ?></td>
                                                        <td><?php echo $row->nom_documento; ?></td>
                                                        <td><?php echo $row->nombres; ?></td>
                                                        <td><?php echo $row->apellidos; ?></td>
                                                        <td><?php echo $row->edad; ?></td>
                                                        <td><?php echo $row->genero; ?></td>
                                                        <td><?php echo $row->celular; ?></td>
                                                        <td><?php echo $row->estado; ?></td>
                                                        <td><?php echo $row->direccion; ?></td>
                                                        <td><?php echo $row->correo; ?></td>
                                                        <td><?php echo $row->id_perfil; ?></td>
                                                        <td></td>
                                                    </tr>
                                                <?php endforeach;?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Documento</th>
                                                        <th>Tipo Documento</th>
                                                        <th>Nombres</th>
                                                        <th>Apellidos</th>
                                                        <th>Edad</th>
                                                        <th>Género</th>
                                                        <th>Celular</th>
                                                        <th>Estado</th>
                                                        <th>Dirección</th>
                                                        <th>Correo</th>
                                                        <th>Perfíl</th>
                                                        <th>Admin Controls</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <!-- datatable end -->
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <!-- Modal center -->
<div class="modal fade" id="default-example-modal-center" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">
                    Basic Modals
                    <small class="m-0 text-muted">
                        Below is a static modal example
                    </small>
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true"><i class="fal fa-times"></i></span>
                </button>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>

      
                      