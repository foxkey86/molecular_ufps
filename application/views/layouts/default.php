<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome to CodeIgniter con layout</title>
</head>
<body>

<div id="container">
	<h1>Welcome to CodeIgniter! layout</h1>

	<div id="body">
	
		<?php 
			echo $content_for_layout;
			?>

	</div>

	<p class="footer">Page rendered in <strong>{elapsed_time}</strong> seconds. <?php echo  (ENVIRONMENT === 'development') ?  'CodeIgniter Version <strong>' . CI_VERSION . '</strong>' : '' ?></p>
</div>

</body>
</html>