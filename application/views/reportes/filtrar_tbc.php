<div class="row"> 
    <div class="col-xl-12">
        <div class="card border mb-g">
            <div class="card-body pl-4 pt-4 pr-4 pb-0">
                <div class="d-flex flex-column">
                <?php echo form_open('reportes/reporte_tbc', ['class' => '', 'id' => 'form', 'role' => 'form'], ['filtrar' => 1]); ?>
                    <div class="form-group row">
                        <div class="col-6 pr-1">
                            <?php $label = 'Tipo de Reporte'; ?>
                            <?php $name = 'dptomuni'; ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown('dptomuni', array('' => 'Seleccionar una opción','1' => 'Por Departamento', '2' => 'Por Municipio'), $this->input->post('dptomuni'), "class='form-control' id='dptomuni' placeholder='Seleccione la opción' required"); ?>
                            </div>
                        </div>
                        <div class="col-6 pr-1">
                            <?php $label = 'Año'; ?>
                            <?php $name = 'anyo'; ?>
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'number', 'maxlength' => 4, 'placeholder' => $label, 'class' => 'form-control'], set_value($name), 'required'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row no-gutters">
                                                <div class="col-md-4 ml-auto text-right">
                                                    <button id="js-login-btn" type="submit" class="btn btn-block btn-success btn-lg mt-3"><i class="fas fa-check-circle"></i> Consultar</button>
                                                </div>
                    </div>  
                <?php echo form_close(); ?>
                </div>
            </div>
        </div>                          
    </div>
</div>
                      