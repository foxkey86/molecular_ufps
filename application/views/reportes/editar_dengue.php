<div class="row"> 
    <div class="col-xl-12">
        <div class="card border mb-g">
            <div class="card-body pl-4 pt-4 pr-4 pb-0">
                <div class="d-flex flex-column">
                <?php echo form_open('reportes/editar_dengue', ['class' => '', 'id' => 'form', 'role' => 'form'], ['editar' => 1, 'id_registro' => $registro->id_rep]); ?>
                    <div class="form-group row">
                        <div class="col-3 pr-1">
                            <?php $label = 'Tipo de Evento (cod_eve)'; ?>
                            <?php $name = 'id_evento'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown('id_evento', $listado_eventos, $this->input->post('id_evento'), "class='form-control $invalid' id='id_evento' placeholder='Seleccione Tipo de Evento' required"); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Fecha de Notificación (fec_not)'; ?>
                            <?php $name = 'fec_not'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => 'Ej:2019-01-30', 'class' => 'form-control '.$invalid], set_value($name), 'required'); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Semana (semana)'; ?>
                            <?php $name = 'semana'; ?>
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'number', 'maxlength' => 2, 'placeholder' => $label, 'class' => 'form-control'], set_value($name), 'required'); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Edad (edad_)'; ?>
                            <?php $name = 'edad'; ?>
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'number', 'maxlength' => 3, 'placeholder' => $label, 'class' => 'form-control'], set_value($name), 'required'); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Nacionalidad (nacionali_)'; ?>
                            <?php $name = 'id_nacionalidad'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown('id_nacionalidad', $listado_nacionalidad, $this->input->post('id_nacionalidad'), "class='form-control $invalid' id='id_nacionalidad' placeholder='Seleccione la nacionalidad' required"); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Sexo (sexo_)'; ?>
                            <?php $name = 'sexo'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown('sexo', array('' => 'Seleccionar una opción','M' => 'Masculino', 'F' => 'Femenino'), $this->input->post('sexo'), "class='form-control $invalid' id='sexo' placeholder='Seleccione el Género' required"); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Municipio de Ocurrencia (cod_mun_o)'; ?>
                            <?php $name = 'id_muni_ocu'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown('id_muni_ocu', $listado_municipio, $this->input->post('id_muni_ocu'), "class='form-control $invalid' id='id_muni_ocu' placeholder='Seleccione el municipio de ocurrencia' required"); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Localidad (localidad_)'; ?>
                            <?php $name = 'localidad'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 30, 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Centro poblado (cen_pobla_)'; ?>
                            <?php $name = 'cen_poblado'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 30, 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Vereda (vereda_)'; ?>
                            <?php $name = 'vereda'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 30, 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Barrio (bar_ver_)'; ?>
                            <?php $name = 'bar_ver'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 30, 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Tipo de seguridad social (cod_mun_o)'; ?>
                            <?php $name = 'tip_ss'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown('tip_ss', $listado_tip_ss, $this->input->post('tip_ss'), "class='form-control $invalid' id='tip_ss' placeholder='Seleccione el tipo de seguridad social' required"); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Código de la entidad de seguridad social (cod_ase_)'; ?>
                            <?php $name = 'cod_ase'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Fecha de contagio (fec_con_)'; ?>
                            <?php $name = 'fec_con'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => 'Ej:2019-01-30', 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Fecha de inicio de sintomas (ini_sin_)'; ?>
                            <?php $name = 'fec_sint'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => 'Ej:2019-01-30', 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Paciente fué hospitalizado? (pac_hos_)'; ?>
                            <?php $name = 'pac_hos'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown($name, $listado_opciones, $this->input->post($name), "class='form-control $invalid' id='$name' placeholder='Seleccione una opción' required"); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Fecha de hospitalizado (fec_hos_)'; ?>
                            <?php $name = 'fec_hos'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => 'Ej:2019-01-30', 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Año (anyo)'; ?>
                            <?php $name = 'anyo'; ?>
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'number', 'maxlength' => 4, 'placeholder' => $label, 'class' => 'form-control'], set_value($name), 'required'); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Tipo de reporte a cargar (tipo_reporte)'; ?>
                            <?php $name = 'dpto_muni'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown($name, $listado_tipo_reporte, $this->input->post($name), "class='form-control $invalid' id='$name' placeholder='Seleccione el tipo de reporte' required"); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Paciente está en embarazo? (gp_gestan)'; ?>
                            <?php $name = 'gp_gestan'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown($name, $listado_opciones, $this->input->post($name), "class='form-control $invalid' id='$name' placeholder='Seleccione una opción' required"); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Semanas de gestación (sem_ges_)'; ?>
                            <?php $name = 'sem_ges'; ?>
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'number', 'maxlength' => 2, 'placeholder' => $label, 'class' => 'form-control'], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Fecha de nacimiento (fecha_nto_)'; ?>
                            <?php $name = 'fecha_nto'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => 'Ej:2019-01-30', 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Clasificación final (clasfinal)'; ?>
                            <?php $name = 'clasfinal'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown($name, $clasificacion_final, $this->input->post($name), "class='form-control $invalid' id='$name' placeholder='Seleccione la clasificación final' required"); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Conducta (conducta)'; ?>
                            <?php $name = 'conducta'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown($name, $listado_conducta, $this->input->post($name), "class='form-control $invalid' id='$name' placeholder='Seleccione la conducta' required"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="height-8 d-flex flex-row align-items-center flex-wrap flex-shrink-0">
                        <button class="btn btn-info shadow-0 ml-auto" type="submit" ><i class="fas fa-save"></i> Editar Registro</button>
                    </div>
                <?php echo form_close(); ?>
                </div>
            </div>
        </div>                          
    </div>
    <div class="col-xl-12">
        <div class="col-md-12 ml-auto text-left" style="margin-bottom:20px;">
            <a href="<?php echo base_url("reportes/reporte_den"); ?>" class="btn btn-primary waves-effect waves-themed" type="button"><i class="fal fa-chevron-circle-left"></i> Atrás</a>
        </div> 
    </div>
</div>
                      