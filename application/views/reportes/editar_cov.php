<div class="row"> 
    <div class="col-xl-12">
        <div class="card border mb-g">
            <div class="card-body pl-4 pt-4 pr-4 pb-0">
                <div class="d-flex flex-column">
                <?php echo form_open('reportes/editar_cov', ['class' => '', 'id' => 'form', 'role' => 'form'], ['editar' => 1, 'id_registro' => $registro->id]); ?>
                    <div class="form-group row">
                        <div class="col-3 pr-1">
                            <?php $label = 'Tipo de Prueba (Tipo de prueba)'; ?>
                            <?php $name = 'id_tipo_prueba'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown($name, $listado_prueba, $this->input->post($name), "class='form-control $invalid' id='$name' placeholder='Seleccione Tipo de Prueba' required"); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Fecha de Notificación (fec_not)'; ?>
                            <?php $name = 'fec_not'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => 'Ej:2019-01-30', 'class' => 'form-control '.$invalid], set_value($name), 'required'); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Municipio de Ocurrencia (Ciudad_municipio_nom)'; ?>
                            <?php $name = 'id_muni'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown($name, $listado_municipio, $this->input->post($name), "class='form-control $invalid' id='$name' placeholder='Seleccione el municipio de ocurrencia' required"); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Edad (edad)'; ?>
                            <?php $name = 'edad'; ?>
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'number', 'maxlength' => 3, 'placeholder' => $label, 'class' => 'form-control'], set_value($name), 'required'); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Sexo (sexo)'; ?>
                            <?php $name = 'sexo'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown('sexo', array('' => 'Seleccionar una opción','M' => 'Masculino', 'F' => 'Femenino'), $this->input->post('sexo'), "class='form-control $invalid' id='sexo' placeholder='Seleccione el Género' required"); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Fuente de tipo de contagio (Fuente_tipo_contagio)'; ?>
                            <?php $name = 'fuente_tip_con'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 20, 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Ubicación (ubicacion)'; ?>
                            <?php $name = 'ubicacion'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 20, 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Estado (estado)'; ?>
                            <?php $name = 'estado'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 20, 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Nacionalidad (Nacionalidad_nom)'; ?>
                            <?php $name = 'id_nacionalidad'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown('id_nacionalidad', $listado_nacionalidad, $this->input->post('id_nacionalidad'), "class='form-control $invalid' id='id_nacionalidad' placeholder='Seleccione la nacionalidad' required"); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Fecha Ingreso Pais (Fecha_ingreso_pais)'; ?>
                            <?php $name = 'fec_ing_pais'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => 'Ej:2019-01-30', 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Fecha de inicio de sintomas (Fecha_inicio_sintomas)'; ?>
                            <?php $name = 'fec_sint'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => 'Ej:2019-01-30', 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Fecha de la consulta (Fecha_consulta)'; ?>
                            <?php $name = 'fec_con'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => 'Ej:2019-01-30', 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Fecha del diagnóstico (Fecha_diagnostico)'; ?>
                            <?php $name = 'fec_dia'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => 'Ej:2019-01-30', 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Fecha de hospitalizado (Fecha_hospitalizacion)'; ?>
                            <?php $name = 'fec_hos'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => 'Ej:2019-01-30', 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Fecha de alta (Fecha_alta)'; ?>
                            <?php $name = 'fec_alta'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => 'Ej:2019-01-30', 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Fecha de muerte (Fecha_muerte)'; ?>
                            <?php $name = 'fec_mue'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => 'Ej:2019-01-30', 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Fecha de recuperación (Fecha_recuperado)'; ?>
                            <?php $name = 'fec_rec'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => 'Ej:2019-01-30', 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Recuperado (Tipo_recuperacion)'; ?>
                            <?php $name = 'clasfinal'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown($name, $listado_tipo_recuperacion, $this->input->post($name), "class='form-control $invalid' id='$name' placeholder='Seleccione el tipo de recuperación' required"); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Tipo de recuperación (Tipo_recuperacion)'; ?>
                            <?php $name = 'tipo_rec'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 20, 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Grupo de edad (Grupo_edad)'; ?>
                            <?php $name = 'id_grupo_edad'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown($name, $listado_grupo_edad, $this->input->post($name), "class='form-control $invalid' id='$name' placeholder='Seleccione el grupo de edad' required"); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Grupo etario por quinquenio (Quinquenio)'; ?>
                            <?php $name = 'quinquenio'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Semanas epidemológicas (SE_Fecha_inicio_sintomas+Fecha_consulta)'; ?>
                            <?php $name = 'se_fec_sint'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Barrio (barrio)'; ?>
                            <?php $name = 'barrio'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 30, 'class' => 'form-control '.$invalid], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Año (anyo)'; ?>
                            <?php $name = 'anyo'; ?>
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'number', 'maxlength' => 4, 'placeholder' => $label, 'class' => 'form-control'], set_value($name), 'required'); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Tipo de reporte a cargar (tipo_reporte)'; ?>
                            <?php $name = 'dpto_muni'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_dropdown($name, $listado_tipo_reporte, $this->input->post($name), "class='form-control $invalid' id='$name' placeholder='Seleccione el tipo de reporte' required"); ?>
                            </div>
                        </div>

                    </div>
                    <div class="height-8 d-flex flex-row align-items-center flex-wrap flex-shrink-0">
                        <button class="btn btn-info shadow-0 ml-auto" type="submit" ><i class="fas fa-save"></i> Editar Registro</button>
                    </div>
                <?php echo form_close(); ?>
                </div>
            </div>
        </div>                          
    </div>
    <div class="col-xl-12">
        <div class="col-md-12 ml-auto text-left" style="margin-bottom:20px;">
            <a href="<?php echo base_url("reportes/reporte_cov"); ?>" class="btn btn-primary waves-effect waves-themed" type="button"><i class="fal fa-chevron-circle-left"></i> Atrás</a>
        </div> 
    </div>
</div>
                      