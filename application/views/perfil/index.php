<div class="row">
    <div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
        <!-- profile summary -->
        <div class="card mb-g rounded-top">
            <div class="row no-gutters row-grid">
                <div class="col-12">
                    <div class="d-flex flex-column align-items-center justify-content-center p-4">
                        <img src="<?php echo base_url(); ?>assets/img/demo/avatars/<?php echo $informacion_usuario->avatarlg; ?>" class="rounded-circle shadow-2 img-thumbnail" alt="">
                        <h5 class="mb-0 fw-700 text-center mt-3">
                        <?php if (isset($informacion_usuario->nombres)):
                            echo $informacion_usuario->nombres;
                            ?>
                        <?php else: ?>
                            Usuario
                            <?php endif; ?>
                            <small class="text-muted mb-0"> <?php if (isset($informacion_usuario->apellidos)):
                                echo $informacion_usuario->apellidos; 
                                ?>
                            <?php else: ?>
                                    
                            <?php endif; ?></small>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- contacts -->
    <div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
    <div class="card mb-g">
            <div class="row row-grid no-gutters">
                <div class="col-12">
                    <div class="p-3">
                        <h2 class="mb-0 fs-xl">
                            Contactos
                        </h2>
                    </div>
                </div>
                <div class="col-4">
                    <a href="javascript:void(0);" class="text-center p-3 d-flex flex-column hover-highlight">
                        <span class="profile-image rounded-circle d-block m-auto" style="background-image:url('<?php echo base_url(); ?>assets/img/demo/avatars/default_1.jpg'); background-size: cover;"></span>
                        <span class="d-block text-truncate text-muted fs-xs mt-1">Dr. Juan Camilo Tórres</span>
                    </a>
                </div>
                <div class="col-4">
                    <a href="javascript:void(0);" class="text-center p-3 d-flex flex-column hover-highlight">
                        <span class="profile-image rounded-circle d-block m-auto" style="background-image:url('<?php echo base_url(); ?>assets/img/demo/avatars/default_2.jpg'); background-size: cover;"></span>
                        <span class="d-block text-truncate text-muted fs-xs mt-1">Jefe Daniela Ortíz</span>
                    </a>
                </div>
                <div class="col-4">
                    <a href="javascript:void(0);" class="text-center p-3 d-flex flex-column hover-highlight">
                        <span class="profile-image rounded-circle d-block m-auto" style="background-image:url('<?php echo base_url(); ?>assets/img/demo/avatars/default_2.jpg'); background-size: cover;"></span>
                        <span class="d-block text-truncate text-muted fs-xs mt-1">Dra. María Sierra</span>
                    </a>
                </div>
                <div class="col-4">
                    <a href="javascript:void(0);" class="text-center p-3 d-flex flex-column hover-highlight">
                        <span class="profile-image rounded-circle d-block m-auto" style="background-image:url('<?php echo base_url(); ?>assets/img/demo/avatars/default_1.jpg'); background-size: cover;"></span>
                        <span class="d-block text-truncate text-muted fs-xs mt-1">Dr. John Cook PhD</span>
                    </a>
                </div>
                <div class="col-4">
                    <a href="javascript:void(0);" class="text-center p-3 d-flex flex-column hover-highlight">
                        <span class="profile-image rounded-circle d-block m-auto" style="background-image:url('<?php echo base_url(); ?>assets/img/demo/avatars/default_2.jpg'); background-size: cover;"></span>
                        <span class="d-block text-truncate text-muted fs-xs mt-1">Auxiliar Sandra Pérez</span>
                    </a>
                </div>
                <div class="col-4">
                    <a href="javascript:void(0);" class="text-center p-3 d-flex flex-column hover-highlight">
                        <span class="profile-image rounded-circle d-block m-auto" style="background-image:url('<?php echo base_url(); ?>assets/img/demo/avatars/default_1.jpg'); background-size: cover;"></span>
                        <span class="d-block text-truncate text-muted fs-xs mt-1">Bacteriólogo Jimmy Fellan</span>
                    </a>
                </div>
                <div class="col-4">
                    <a href="javascript:void(0);" class="text-center p-3 d-flex flex-column hover-highlight">
                        <span class="profile-image rounded-circle d-block m-auto" style="background-image:url('<?php echo base_url(); ?>assets/img/demo/avatars/default_2.jpg'); background-size: cover;"></span>
                        <span class="d-block text-truncate text-muted fs-xs mt-1">Dra. Arica Grace</span>
                    </a>
                </div>
                <div class="col-4">
                    <a href="javascript:void(0);" class="text-center p-3 d-flex flex-column hover-highlight">
                        <span class="profile-image rounded-circle d-block m-auto" style="background-image:url('<?php echo base_url(); ?>assets/img/demo/avatars/default_2.jpg'); background-size: cover;"></span>
                        <span class="d-block text-truncate text-muted fs-xs mt-1">Dr. Jim Ketty</span>
                    </a>
                </div>
                <div class="col-4">
                    <a href="javascript:void(0);" class="text-center p-3 d-flex flex-column hover-highlight">
                        <span class="profile-image rounded-circle d-block m-auto" style="background-image:url('<?php echo base_url(); ?>assets/img/demo/avatars/default_2.jpg'); background-size: cover;"></span>
                        <span class="d-block text-truncate text-muted fs-xs mt-1">Jefe. Ali Grey</span>
                    </a>
                </div>
                <div class="col-12">
                    <div class="p-3 text-center">
                        <a href="javascript:void(0);" class="btn-link font-weight-bold">Ver todos</a>
                    </div>
                </div>
            </div>
        </div> 
    </div> 
    <!-- fin contactos -->
    <!-- datos profesional -->
    <div class="col-lg-6 col-xl-6 order-lg-2 order-xl-3">
        <!-- add : -->
        <div class="card mb-2">
            <div class="card-body">
                <a href="javascript:void(0);" class="d-flex flex-row align-items-center">
                    <div class='icon-stack display-3 flex-shrink-0'>
                        <i class="fal fa-circle icon-stack-3x opacity-100 color-primary-400"></i>
                        <i class="fas fa-address-card icon-stack-1x opacity-100 color-primary-500"></i>
                    </div>
                    <div class="ml-3">
                    <strong>
                            Tipo de Usuario
                        </strong>
                        <?php if (isset($profesional)): ?>
                        <br>
                            Profesional
                        <br>
                            <?php echo $profesional->profesion; ?>
                        <?php else: ?>
                        <br>
                        Invitado
                        <br>
                            <?php if (isset($particular)): ?>
                                <?php echo $particular->nombre; ?>
                            <?php endif; ?>
                        <?php endif; ?>
                    </div>
                </a>
            </div>
        </div>
        <div class="card mb-g">
            <div class="card-body">
                <a href="javascript:void(0);" class="d-flex flex-row align-items-center">
                    <div class='icon-stack display-3 flex-shrink-0'>
                        <i class="fal fa-circle icon-stack-3x opacity-100 color-warning-400"></i>
                        <i class="fas fa-building icon-stack-1x opacity-100 color-warning-500"></i>
                    </div>
                    <div class="ml-3">
                        <strong>
                        <?php if (isset($profesional)):?>
                            Institución
                        <?php else: ?>
                            Sitio
                            <?php endif; ?>
                        </strong>
                        <br>
                        <?php if (isset($profesional->institucion)):
                            echo $profesional->institucion; ?>
                        <?php elseif (isset($profesional->institucion)):
                            echo $particular->lugar; ?>
                            <?php else: ?>
                                No registra
                            <?php endif; ?>
                    </div>
                </a>
            </div>
        </div>                          
    </div>
    <!-- fin datos profesional -->
</div>