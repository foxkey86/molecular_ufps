<section class="container g-py-100">
      <div class="row g-mb-20">
        <div class="col-lg-6 g-mb-50">
          <!-- Heading -->
          <h2 class="h1 g-color-black g-font-weight-700 mb-4">Contactos</h2>
          <p class="g-font-size-18 mb-0">M. Sc. GERMAN LUCIANO LOPEZ BARRERA<br>
            Jefe de Laboratorio.
            </p>
          <!-- End Heading -->
        </div>
        <div class="col-lg-3 align-self-end ml-auto g-mb-50">
          <div class="media">
            <div class="d-flex align-self-center">
              <span class="u-icon-v2 u-icon-size--sm g-color-white g-bg-primary rounded-circle mr-3">
                  <i class="g-font-size-16 icon-communication-033 u-line-icon-pro"></i>
                </span>
            </div>
            <div class="media-body align-self-center">
              <h3 class="h6 g-color-black g-font-weight-700 text-uppercase mb-0">Email</h3>
              <p class="mb-0">labdiagmolecular@ufps.edu.co</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- End Contact Form -->