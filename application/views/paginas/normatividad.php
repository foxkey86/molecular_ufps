<!-- Section 1 -->
<section class="g-py-100">
    <div class="container">
    <div class="row">
        <div class="col-md-12 align-self-center g-mb-50--md g-mb-0--md">
        <div class="mb-2">
            <div class="d-inline-block g-width-30 g-height-2 g-pos-rel g-top-minus-4 g-bg-primary mr-2"></div>
            <span class="g-color-gray-dark-v3 g-font-weight-600 g-font-size-12 text-uppercase">Normatividad</span>
        </div>
        <div class="u-heading-v2-3--bottom g-brd-primary g-mb-30">
            <h2 class="h1 g-color-black g-font-weight-600 g-mb-35"><span class="g-color-primary">Normatividad</span></h2>
        </div>

        <div class="g-font-size-18 g-line-height-2 g-mb-30">
            <p>La creación del CENTRO DE INVESTIGACIÓN EN BIOTECNOLOGÍA Y MEDIO AMBIENTE — CIBIM está enmarcado en:</p>
            <p>El Acuerdo No.056 de Septiembre 07 de 2012 en el Capítulo I, La organización del sistema de investigación de la 
                UFPS en el Artículo 2, literal d, El sistema de investigaciones de la UFPS está conformado por los siguientes 
                componentes: Semilleros, grupos y centros de Investigación como unidades básicas sobre las cuales se fundamenta 
                el ejercicio investigativo.</p>
            <p>El Acuerdo No.056 de Septiembre 07 de 2012, en el Capítulo III sobre los procesos institucionales para la 
                investigación en la UFPS en el Artículo 11, los centros de investigación estarán adscritos a una unidad académica.</p>
            <p>El capítulo IV, artículo 20 del Acuerdo No.056 de Septiembre 07 de 2012 y en el Artículo 3 del Capítulo 1 de la 
                Resolución No.123 de Mayo 24 de 2011 del Consejo Académico de la UFPS, define un centro de investigación como 
                la conformación de dos o más grupos de investigación, donde como mínimo uno de sus grupos debe estar reconocido por 
                Colciencias en categorías A1, A o B. Su objetivo y actividad principal es la investigación científica o tecnológica 
                pero también realiza otras actividades relacionadas con ciencia y tecnología, tales como: cualificación del talento 
                humano, transferencia de tecnología, difusión y divulgación científica y gestión, seguimiento y evaluación de procesos 
                de ciencia y tecnología.</p>
            <p>El Artículo 8 del Capítulo II de la Resolución No.123 de Mayo 24 de 2011 del Consejo Académico de la UFPS, sobre 
                la creación y funcionamiento de grupos, centros e institutos de investigación, indica que los centros e institutos 
                de investigación adquieren vida institucional dentro de la UFPS mediante acuerdo del Consejo Superior Universitario, 
                con previa recomendación del Consejo Académico y del Comité Central de Investigaciones.</p>
            <p>La norma ISO/IEC 17025 de 2017 “Requisitos generales para las competencias de los laboratorios de prueba y calibración”</p>

        </div>
        </div>
    </div>
    </div>
</section>
<!-- End Section 1 -->