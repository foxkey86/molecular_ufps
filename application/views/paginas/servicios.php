<!-- Section 1 -->
<section class="g-py-100">
    <div class="container">
    <div class="row">
        <div class="col-md-12 align-self-center g-mb-50--md g-mb-0--md">
        <div class="mb-2">
            <div class="d-inline-block g-width-30 g-height-2 g-pos-rel g-top-minus-4 g-bg-primary mr-2"></div>
            <span class="g-color-gray-dark-v3 g-font-weight-600 g-font-size-12 text-uppercase">Servicios</span>
        </div>
        <div class="u-heading-v2-3--bottom g-brd-primary g-mb-30">
        
            <h2 class="h1 g-color-black g-font-weight-600 g-mb-35"><span class="g-color-primary">Servicios</span></h2>
        </div>

        <div class="g-font-size-18 g-line-height-2 g-mb-30">
        <p>Líneas de investigación: EL CENTRO DE INVESTIGACIÓN EN BIOTECNOLOGÍA Y MEDIO AMBIENTE — 
            CIBIM de la Universidad Francisco de Paula Santander brindará asesoría en la formulación 
            y/o diseño, ejecución y seguimiento de proyectos en las siguientes líneas de trabajo:</p>
<p>a)	Biotecnología Industrial.</p>
<p>b)	Bioprocesos Ambientales.</p>
<p>c)	Diagnóstico molecular enfocado en salud.</p>
<p>d)	Diagnóstico molecular enfocado a industria.</p>
<p>e)	Salud pública y epidemiología.</p><br>

<p>Son funciones del Centro de Investigación en Biotecnología y Medio Ambiente — CIBIM:</p>
<p>a)	Formular, ejecutar y evaluar proyectos de investigación, innovación y de desarrollo tecnológico enmarcados dentro de las líneas de investigación definidas por los grupos de investigación que forman parte del CIBIM.</p>
<p>b)	Fomentar la participación de docentes, estudiantes, graduados y administrativos de la UFPS, así como, personal externo en las actividades del CIBIM.</p>
<p>c)	Garantizar el mantenimiento, actualización y mejora de los sistemas de gestión institucionales y de los laboratorios del CIBIM que tengan implementados una norma específica.</p>
<p>d)	Garantizar la confidencialidad e imparcialidad de los servicios prestados.</p>
<p>e)	Generar espacios para socializar y divulgar actividades y proyectos de investigación y extensión.</p>
<p>f)	Propiciar la formación de personal calificado.</p>
<p>g)	Participar en diferentes convocatorias del orden regional, nacional e internacional que permitan obtener recursos para el cumplimiento de los objetivos del centro.</p>
<p>h)	Ofertar servicios de ensayos de laboratorio, conforme a las líneas de investigación, a los diferentes sectores productivos y de servicios.</p>


        </div>
        </div>
    </div>
    </div>
</section>
<!-- End Section 1 -->