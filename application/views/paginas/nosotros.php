<!-- Section 1 -->
<section class="g-py-100">
    <div class="container">
    <div class="row">
        <div class="col-md-6 align-self-center g-mb-50--md g-mb-0--md">
        <div class="mb-2">
            <div class="d-inline-block g-width-30 g-height-2 g-pos-rel g-top-minus-4 g-bg-primary mr-2"></div>
            <span class="g-color-gray-dark-v3 g-font-weight-600 g-font-size-12 text-uppercase">Visión</span>
        </div>
        <div class="u-heading-v2-3--bottom g-brd-primary g-mb-30">
        
            <h2 class="h1 g-color-black g-font-weight-600 g-mb-35"><span class="g-color-primary">Centro de</span> Investigación En Biotecnología <span class="g-color-primary">y Medio Ambiente — CIBIM</span></h2>
        </div>

        <div class="g-font-size-18 g-line-height-2 g-mb-30">
            <p>El CIBIM de la UFPS será reconocido como un centro líder en actividades de generación de conocimiento, 
                transferencia de tecnología, formación de talento humano y realización de ensayos técnicamente válidos 
                enfocados a fortalecer la competitividad y productividad de los sectores productivos y de servicios 
                contribuyendo al desarrollo sostenible y el favorecimiento de la innovación para el desarrollo tecnológico de los mismos.</p>
        </div>
        </div>

        <div class="col-md-6 align-self-center text-center g-overflow-hidden">
        <img class="img-fluid w-100 mb-4" src="<?php echo base_url(); ?>assets/img/molecular/vision_img.jpg" alt="Image Description" data-animation="fadeInUp" data-animation-delay="0" data-animation-duration="1000">
        </div>
    </div>
    </div>
</section>
<!-- End Section 1 -->

<!-- Section 2 -->
<section class="g-bg-purple g-color-white g-py-100">
    <div class="container">
    <div class="row">
        <div class="col-md-6 align-self-center text-center g-overflow-hidden g-mb-50--md g-mb-0--md">
        <img class="img-fluid w-100 mb-4" src="<?php echo base_url(); ?>assets/img/molecular/mision_img.jpg" alt="Image Description" data-animation="fadeInUp" data-animation-delay="0" data-animation-duration="1000">
        </div>

        <div class="col-md-6 align-self-center">
        <div class="mb-2">
            <div class="d-inline-block g-width-30 g-height-2 g-pos-rel g-top-minus-4 g-bg-white mr-2"></div>
            <span class="g-color-white g-font-weight-600 g-font-size-12 text-uppercase">Misión</span>
        </div>
        <div class="u-heading-v2-3--bottom g-brd-white g-mb-30">
            <h2 class="h1 u-heading-v2__title text-uppercase g-font-weight-700">Centro De Investigación En Biotecnología y Medio Ambiente — CIBIM</h2>
        </div>


        <div class="g-font-size-18 g-line-height-2 g-mb-30">
            <p>El CIBIM de la UFPS, se encuentra orientado a generar, desarrollar y ofrecer conocimiento científico 
                y tecnológico mediante la prestación de servicios de extensión, análisis y ensayos de laboratorio y 
                herramientas biotecnológicas, enfocadas al mejoramiento continuo de la competitividad, productividad 
                y el desarrollo sostenible a nivel departamental, nacional e internacional.</p>
        </div>
        </div>
    </div>
    </div>
</section>
<!-- End Section 2 -->