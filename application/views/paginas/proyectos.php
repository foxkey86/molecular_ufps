<!-- Section 1 -->
<section class="g-py-100">
    <div class="container">
    <div class="row">
        <div class="col-md-12 align-self-center g-mb-50--md g-mb-0--md">
        <div class="mb-2">
            <div class="d-inline-block g-width-30 g-height-2 g-pos-rel g-top-minus-4 g-bg-primary mr-2"></div>
            <span class="g-color-gray-dark-v3 g-font-weight-600 g-font-size-12 text-uppercase">Proyectos</span>
        </div>
        <div class="u-heading-v2-3--bottom g-brd-primary g-mb-30">
        
            <h2 class="h1 g-color-black g-font-weight-600 g-mb-35"><span class="g-color-primary">Proyectos</span></h2>
        </div>

        <div class="g-font-size-18 g-line-height-2 g-mb-30">
            <p>Producto de la pandemia por el  SARS-CoV-2 (COVID-19) se realizó la convocatoria 009-2020 del Fondo de 
                Ciencia y Tecnología del Sistema General de Regalías para el Fortalecimiento de Laboratorios Regionales, 
                donde la Universidad participó con la propuesta FORTALECIMIENTO DE LAS CAPACIDADES CIENTÍFICO-TECNOLÓGICAS 
                DEL LABORATORIO DE BIOLOGÍA MOLECULAR-UFPS COMO UNA HERRAMIENTA PARA EL DIAGNÓSTICO DE AGENTES BIOLÓGICOS 
                DE ALTO RIESGO PARA LA SALUD HUMANA CÚCUTA, la cual fue aprobada y financiada con código BPIN 2020000100123. 
                Este proyecto tiene como objetivo fortalecer las capacidades científico-tecnológicas del laboratorio de 
                Biología Molecular de la UFPS como una herramienta para el diagnóstico de agentes biológicos de alto 
                riesgo para la salud humana, que permita prestar servicios científicos y tecnológicos para atender 
                problemáticas asociadas con agentes biológicos de alto riesgo para la salud humana. Para este fin, es 
                necesario la creación del Centro de Investigación en Biotecnología y Medio Ambiente, donde se articule 
                el laboratorio y se cumpla con los requerimientos establecidos por la norma NTC-ISO 17025 de 2017 para 
                poder obtener el permiso y prestar los servicios que conlleven al fortalecimiento científico tecnológico 
                del Departamento para prestar servicios relacionados con el diagnóstico de agentes biológicos con riesgo 
                a la salud humana y permita el mejoramiento continuo de la competitividad, productividad y el desarrollo 
                sostenible a nivel departamental y nacional.</p>
        </div>
        </div>
    </div>
    </div>
</section>
<!-- End Section 1 -->