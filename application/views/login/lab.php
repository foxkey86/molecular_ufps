<?php echo validation_errors('<div class="alert alert-danger alert-dismissable text-justify">', '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>'); ?>
<div class="page-logo m-0 w-100 align-items-center justify-content-center rounded border-bottom-left-radius-0 border-bottom-right-radius-0 px-4">
                <a href="javascript:void(0)" class="page-logo-link press-scale-down d-flex align-items-center">
                    <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin WebApp" aria-roledescription="logo">
                    <span class="page-logo-text mr-1">CIBIM</span>
                </a>
            </div>
            <?php echo form_open('lab/loguearUsuarioLab', ['class' => '', 'id' => 'form', 'role' => 'form'], ['logueo' => 1]); ?>
            <div class="card p-4 border-top-left-radius-0 border-top-right-radius-0">
            <div class="form-group">
                <?php $label = 'Correo Electrónico'; ?>
                <?php $name = 'username'; ?>
                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                <?php echo form_label($label, $name, ['class' => 'form-label']); ?>
                <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'email', 'maxlength' => 50, 'placeholder' => $label, 'class' => 'form-control form-control-lg '.$invalid], set_value($name), 'required'); ?>
        </div>
        <div class="form-group">
                <?php $label = 'Contraseña'; ?>
                <?php $name = 'password'; ?>
                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                <?php echo form_label($label, $name, ['class' => 'form-label']); ?>
                <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'password', 'maxlength' => 20, 'placeholder' => $label, 'class' => 'form-control form-control-lg '.$invalid], set_value($name), 'required'); ?>
        </div>

        <input type="hidden" name="google-response-token" id="google-response-token">

            <div class="row no-gutters">
            
                <div class="col-lg-12 pl-lg-1 my-2">
                    <button id="js-login-btn" type="submit" class="btn btn-success btn-block btn-lg"><i class="fa fa-unlock" aria-hidden="true"></i> Iniciar Sesión</button>
                </div>
            </div>
            </div>
            <div class="blankpage-footer text-center">
                <a href="#"><strong style="color: #9c282f;">Para restablecer contraseña debe ponerse en contacto con el administrador</strong></a>
            </div>
            <?php echo form_close(); ?>
