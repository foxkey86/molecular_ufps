<div class="col-xl-12">
<?php echo validation_errors('<div class="alert alert-danger alert-dismissable text-justify">', '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>'); ?>
</div>
<div class="col-xl-12">
                                    <h2 class="fs-xxl fw-500 mt-4 text-secondary text-center">
                                       Formulario de Registro
                                        <small class="h3 fw-300 mt-3 mb-5 text-secondary hidden-sm-down">
                                            Para utilizar los diferentes servicios de Sistema Molecular-UFPS el interesado debe estar registrado en nuestra base de datos.
                                            <br>Por favor diligencie el siguiente formulario!
                                        </small>
                                    </h2>
                                </div>
                                <div class="col-xl-8 ml-auto mr-auto">
                                    <div class="card p-4 rounded-plus bg-faded">
                                     <!--   <div class="alert alert-primary text-dark" role="alert">
                                            <strong>Heads Up!</strong> Due to server maintenance from 9:30GTA to 12GTA, the verification emails could be delayed by up to 10 minutes.
                                        </div>  -->

                                        <?php echo form_open_multipart('', ['class' => '', 'id' => 'form', 'role' => 'form'], ['registro' => 1]); ?>

                                        <div class="form-group row">
                                            <div class="col-6 pr-1">
                                                <?php $label = 'Tipo de Documento'; ?>
                                                <?php $name = 'id_tipo_documento'; ?>
                                                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                                <div class="form-group col-md-12">
                                                    <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                                    <?php echo form_dropdown('id_tipo_documento', $listado_documento, $this->input->post('id_tipo_documento'), "class='form-control $invalid' id='id_tipo_documento' placeholder='Seleccione Tipo de Documento' required"); ?>
                                                </div>
                                            </div>

                                            <div class="col-6 pr-1">
                                                <?php $label = 'Número de Documento'; ?>
                                                <?php $name = 'documento'; ?>
                                                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                                <div class="form-group col-md-12">
                                                    <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                                    <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 15, 'placeholder' => $label, 'class' => 'form-control '.$invalid], set_value($name), 'required'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-6 pr-1">
                                                <?php $label = 'Nombres'; ?>
                                                <?php $name = 'nombres'; ?>
                                                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                                <div class="form-group col-md-12">
                                                    <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                                    <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 50, 'placeholder' => $label, 'class' => 'form-control '.$invalid], set_value($name), 'required'); ?>
                                                </div>
                                            </div>

                                            <div class="col-6 pr-1">
                                                <?php $label = 'Apellidos'; ?>
                                                <?php $name = 'apellidos'; ?>
                                                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                                <div class="form-group col-md-12">
                                                    <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                                    <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 50, 'placeholder' => $label, 'class' => 'form-control '.$invalid], set_value($name), 'required'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-6 pr-1">
                                                <?php $label = 'Género'; ?>
                                                <?php $name = 'genero'; ?>
                                                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                                <div class="form-group col-md-12">
                                                    <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                                    <?php echo form_dropdown('genero', array('' => 'Seleccionar una opción','M' => 'Masculino', 'F' => 'Femenino'), $this->input->post('genero'), "class='form-control $invalid' id='genero' placeholder='Seleccione el Género' required"); ?>
                                                </div>
                                            </div>

                                            <div class="col-6 pr-1">
                                                <?php $label = 'Edad'; ?>
                                                <?php $name = 'edad'; ?>
                                                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                                <div class="form-group col-md-12">
                                                    <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                                    <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'number', 'maxlength' => 3, 'placeholder' => $label, 'class' => 'form-control '.$invalid], set_value($name), 'required'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-6 pr-1">
                                                <?php $label = 'Celular'; ?>
                                                <?php $name = 'celular'; ?>
                                                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                                <div class="form-group col-md-12">
                                                    <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                                    <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => $label, 'class' => 'form-control '.$invalid], set_value($name), 'required'); ?>
                                                </div>
                                            </div>

                                            <div class="col-6 pr-1">
                                                <?php $label = 'Dirección'; ?>
                                                <?php $name = 'direccion'; ?>
                                                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                                <div class="form-group col-md-12">
                                                    <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                                    <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 200, 'placeholder' => $label, 'class' => 'form-control '.$invalid], set_value($name), 'required'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12 pr-1">
                                                <?php $label = 'Correo Electrónico'; ?>
                                                <?php $name = 'correo'; ?>
                                                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                                <div class="form-group col-md-12">
                                                    <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                                    <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'email', 'maxlength' => 50, 'placeholder' => $label, 'class' => 'form-control '.$invalid], set_value($name), 'required'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-6 pr-1">
                                                <?php $label = 'Contraseña'; ?>
                                                <?php $name = 'password'; ?>
                                                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                                <div class="form-group col-md-12">
                                                    <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                                    <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'password', 'minlength' => 8, 'maxlength' => 20, 'placeholder' => $label, 'class' => 'form-control '.$invalid], set_value($name), 'required'); ?>
                                                </div>
                                            </div>
                                            <div class="col-6 pr-1">
                                                <?php $label = 'Confirmar Contraseña'; ?>
                                                <?php $name = 'confirm_password'; ?>
                                                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                                <div class="form-group col-md-12">
                                                    <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                                    <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'password', 'minlength' => 8, 'maxlength' => 20, 'placeholder' => $label, 'class' => 'form-control '.$invalid], set_value($name), 'required'); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-12 pr-1">
                                                <?php $label = 'Tipo de Usuario'; ?>
                                                <?php $name = 'tipo_usuario'; ?>
                                                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                                <div class="form-group col-md-12">
                                                    <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                                    <?php echo form_dropdown('tipo_usuario', array('' => 'Seleccionar una opción','1' => 'Profesional', '2' => 'Particular'), $this->input->post('tipo_usuario'), "class='form-control $invalid' id='tipo_usuario' placeholder='Seleccione el Tipo de Usuario' required onchange=\"cambia_tipo_usuario(this.value)\""); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-6 pr-1">
                                                <?php $label = 'Profesión'; ?>
                                                <?php $name = 'profesion'; ?>
                                                <div class="form-group col-md-12 <?= (form_error($name) ? 'has-error has-feedback' : '') ?>">
                                                    <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 30, 'placeholder' => $label, 'class' => 'form-control'], set_value($name)); ?>
                                                </div>
                                            </div>
                                            <div class="col-6 pr-1">
                                                <?php $label = 'Institución'; ?>
                                                <?php $name = 'institucion'; ?>
                                                <div class="form-group col-md-12 <?= (form_error($name) ? 'has-error has-feedback' : '') ?>">
                                                    <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 100, 'placeholder' => $label, 'class' => 'form-control'], set_value($name)); ?>
                                                </div>
                                            </div>
                                            <div class="col-6 pr-1">
                                                <?php $label = 'Oficio'; ?>
                                                <?php $name = 'oficio'; ?>
                                                <div class="form-group col-md-12 <?= (form_error($name) ? 'has-error has-feedback' : '') ?>">
                                                    <?php echo form_dropdown('oficio', $listado_oficio, $this->input->post('oficio'), "class='form-control' id='oficio' placeholder='Seleccione el Oficio'"); ?>
                                                </div>
                                            </div>
                                        </div>
 


                                        <?php // echo form_open('', ['class' => '', 'id' => 'form', 'role' => 'form'], ['registro' => 1]); ?>

                                    <!--      
                                            <div class="form-group">
                                                <label class="form-label" for="fcorreo">Tipo de Usuario</label>
                                                <?php // echo form_dropdown('tipo_usuario', array('' => 'Seleccionar una opción','1' => 'Profesional', '2' => 'Particular'), $this->input->post('tipo_usuario'), "class='form-control' id='tipo_usuario' placeholder='Seleccione el Tipo de Usuario' required onchange=\"cambia_tipo_usuario(this.value)\""); ?>
                                               <div class="invalid-feedback">Opción del Tipo de Usuario Inválido.</div>     
                                               <br>
                                               <?php // echo form_input(['id' => 'profesion', 'name' => 'profesion', 'maxlength' => 30, 'placeholder' => "Profesión", 'class' => 'form-control'], set_value('profesion'));?>
                                               <?php // echo form_dropdown('oficio', $listado_oficio, $this->input->post('oficio'), "class='form-control' id='oficio' placeholder='Seleccione el Oficio'"); ?>
                                               <br>
                                               <?php // echo form_input(['id' => 'institucion', 'name' => 'institucion', 'maxlength' => 100, 'placeholder' => "Empresa o Institución donde labora", 'class' => 'form-control'], set_value('institucion'));?>
                                            </div>
                                            <div class="form-group demo">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="terms" required>
                                                    <label class="custom-control-label" for="terms"> Acepto términos y condiciones</label>
                                                    <div class="invalid-feedback">Debes estar de acuerdo antes de continuar</div>
                                                </div>
                                            </div>
                                            <div class="row no-gutters">
                                                <div class="col-md-4 ml-auto text-right">
                                                    <button id="js-login-btn" type="submit" class="btn btn-block btn-success btn-lg mt-3">Regístrate</button>
                                                </div>
                                            </div>  

                                            -->
                                            <div class="form-group demo">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" class="custom-control-input" id="terms" required>
                                                    <label class="custom-control-label" for="terms"> Acepto términos y condiciones</label>
                                                    <div class="invalid-feedback">Debes estar de acuerdo antes de continuar</div>
                                                </div>
                                            </div>
                                            <div class="row no-gutters">
                                                <div class="col-md-4 ml-auto text-right">
                                                    <button id="js-login-btn" type="submit" class="btn btn-block btn-success btn-lg mt-3"><i class="fa fa-save"></i> Regístrate</button>
                                                </div>
                                            </div>  
                                        <?php echo form_close(); ?>
                                    </div>
                                </div>

