<div class="page-logo m-0 w-100 align-items-center justify-content-center rounded border-bottom-left-radius-0 border-bottom-right-radius-0 px-4">
                <a href="javascript:void(0)" class="page-logo-link press-scale-down d-flex align-items-center">
                    <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin WebApp" aria-roledescription="logo">
                    <span class="page-logo-text mr-1">INDICADORES-UFPS</span>
                    <i class="fal fa-angle-down d-inline-block ml-1 fs-lg color-primary-300"></i>
                </a>
            </div>
            <?php echo form_open('login/loguearUsuario', ['class' => '', 'id' => 'form', 'role' => 'form'], ['logueo' => 1]); ?>
            <div class="card p-4 border-top-left-radius-0 border-top-right-radius-0">
                <form action="intel_introduction.html">
                    <div class="form-group">
                        <label class="form-label" for="username">Correo Electrónico</label>
                        <input type="email" id="username" class="form-control" placeholder="Correo Electrónico" required>
                        <div class="invalid-feedback">Correo Electrónico del Usuario Inválido</div>
                                                <div class="help-block">Por favor digite el Correo Electrónico</div>
                    </div>
                    <div class="form-group">
                        <label class="form-label" for="password">Contraseña</label>
                        <input type="password" id="password" class="form-control" placeholder="Contraseña" required>
                        <div class="invalid-feedback">Contraseña o Correo Electrónico Invalida.</div>
                        <div class="help-block">Por favor digite la Contraseña</div>
                    </div>
                    <div class="form-group text-left">
                        <div class="custom-control custom-checkbox">
                            <input type="checkbox" class="custom-control-input" id="rememberme">
                            <label class="custom-control-label" for="rememberme"> Recordarme</label>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-default float-right">Iniciar Sesión</button>
                </form>
            </div>
            <div class="blankpage-footer text-center">
                <a href="#"><strong>Para restablecer contraseña debe ponerse en contacto con el administrador</strong></a>
            </div>
            <?php echo form_close(); ?>
