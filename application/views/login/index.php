<div class="col col-md-6 col-lg-7 hidden-sm-down">
                                    <h2 class="fs-xxl fw-500 mt-4 text-secondary ">
                                        Módulo de consulta de literatura científica
                                        <small class="h3 fw-300 mt-3 mb-5 text-secondary">
										A través de un registro con un usuario y contraseña, los usuarios externos que quieran mayor información sobre tratamiento farmacológico, publicaciones de último nivel de evidencia o más recientes sobre patologías infecciosas pueden realizar consultas a través de esta plataforma.
                                        </small>
                                    </h2>
                                   
                                <!--    <div class="d-sm-flex flex-column align-items-center justify-content-center d-md-block">
                                        <div class="px-0 py-1 mt-5 text-white fs-nano opacity-50">
                                            Encuéntrenos en las redes sociales
                                        </div>
                                        <div class="d-flex flex-row opacity-70">
                                            <a href="#" class="mr-2 fs-xxl text-white">
                                                <i class="fab fa-facebook-square"></i>
                                            </a>
                                            <a href="#" class="mr-2 fs-xxl text-white">
                                                <i class="fab fa-twitter-square"></i>
                                            </a>
                                            <a href="#" class="mr-2 fs-xxl text-white">
                                                <i class="fab fa-google-plus-square"></i>
                                            </a>
                                            <a href="#" class="mr-2 fs-xxl text-white">
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </div>
                                    </div>  -->
                                </div>

								<div class="col-sm-12 col-md-6 col-lg-5 col-xl-4 ml-auto">
                                    <?php echo validation_errors('<div class="alert alert-danger alert-dismissable text-justify">', '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button></div>'); ?>
                                    <h1 class="fs-xxl fw-500 mt-4 text-secondary">
                                        Inicio de Sesión
                                    </h1>
                                    <div class="card p-4 rounded-plus bg-faded">
                            
										<?php echo form_open('login/loguearUsuario', ['class' => '', 'id' => 'form', 'role' => 'form'], ['logueo' => 1]); ?>

                                        <div class="form-group">
                                                <?php $label = 'Correo Electrónico'; ?>
                                                <?php $name = 'username'; ?>
                                                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                                <?php echo form_label($label, $name, ['class' => 'form-label']); ?>
                                                <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'email', 'maxlength' => 50, 'placeholder' => $label, 'class' => 'form-control form-control-lg '.$invalid], set_value($name), 'required'); ?>
                                        </div>
                                        <div class="form-group">
                                                <?php $label = 'Contraseña'; ?>
                                                <?php $name = 'password'; ?>
                                                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                                <?php echo form_label($label, $name, ['class' => 'form-label']); ?>
                                                <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'password', 'maxlength' => 20, 'placeholder' => $label, 'class' => 'form-control form-control-lg '.$invalid], set_value($name), 'required'); ?>
                                        </div>

                                        <input type="hidden" name="google-response-token" id="google-response-token">
                                            <div class="row no-gutters">
                                           
                                                <div class="col-lg-12 pl-lg-1 my-2">
                                                    <button id="js-login-btn" type="submit" class="btn btn-success btn-block btn-lg"><i class="fa fa-unlock" aria-hidden="true"></i> Iniciar Sesión</button>
                                                </div>
                                            </div>
                                  
										<?php echo form_close(); ?>
                                    </div>
                                </div>