<main id="js-page-content" role="main" class="page-content">
                       
                                            <span class="title">
                                            Indicadores de Salud Pública del Departamento de Norte de Santander.
                                            </span>
                   
                        <div class="row">
                            <div class="col-sm-12 margin_top_40">
                                <span class="ind" id="canInd">     
                                </span>
                            </div>
                            <div class="col-sm-12">
                                <span class="title_ind">
                                    Indicador
                                </span>
                            </div>
                            <div class="col-sm-12 margin_top_10">
                                <span class="ind" id="canUbi">     
                                </span>
                            </div>
                            <div class="col-sm-12">
                                <span class="title_ind">
                                    Ubicación
                                </span>
                            </div>
                            <div class="col-sm-12 margin_top_10">
                                <span class="ind" id="canAn">     
                                </span>
                            </div>
                            <div class="col-sm-12">
                                <span class="title_ind">
                                Año
                                </span>
                            </div>
                            <div class="col-sm-12 margin_top_10">
                                <span class="ind" id="canTri">     
                                </span>
                            </div>
                            <div class="col-sm-12">
                                <span class="title_ind">
                                Trimestre
                                </span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12 margin_top_10">
                                <div class="panel-content p-0">
                                    <div class="row row-grid no-gutters">
                                        <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
                                            <div class="px-3 py-2 d-flex align-items-center">
                                                <div class="js-easy-pie-chart color-primary-300 position-relative d-inline-flex align-items-center justify-content-center" data-percent="100" data-piesize="50" data-linewidth="5" data-linecap="butt" data-scalelength="0">
                                                    <div class="d-flex flex-column align-items-center justify-content-center position-absolute pos-left pos-right pos-top pos-bottom fw-300 fs-lg" id="canMu">
                                                        
                                                    </div>
                                                </div>
                                                <span class="d-inline-block ml-2 text-muted title_ind">
                                                    Mujeres
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 col-md-6 col-lg-6 col-xl-3">
                                            <div class="px-3 py-2 d-flex align-items-center">
                                                <div class="js-easy-pie-chart color-success-500 position-relative d-inline-flex align-items-center justify-content-center" data-percent="100" data-piesize="50" data-linewidth="5" data-linecap="butt">
                                                    <div class="d-flex flex-column align-items-center justify-content-center position-absolute pos-left pos-right pos-top pos-bottom fw-300 fs-lg" id="canHo">
                                                        <span class="js-percent d-block text-dark"></span>
                                                    </div>
                                                </div>
                                                <span class="d-inline-block ml-2 text-muted title_ind">
                                                    Hombres
                                                </span>
                                            </div>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                      
                            <div class="col-sm-12 margin_top_40">
             <!--              <button onclick="window.print()">Imprimir pantalla con otro ejemplo</button>  -->
                                <div id="panel-1" class="panel" data-panel-locked data-panel-refresh data-panel-reset data-panel-color data-panel-custombutton>
                                    <div class="panel-hdr bg-info-600 bg-info-gradient">
                                        <h2>
                                            Relación <span class="fw-300"><i> Casos vs Semanas</i></span>
                                        </h2>
                                    </div>
                                    <div class="panel-container show">
                                        <div class="panel-content poisition-relative">
                                            <div class="p-1 position-absolute pos-right pos-top mt-3 mr-3 z-index-cloud d-flex align-items-center justify-content-center">
                                                <div class="border-faded border-top-0 border-left-0 border-bottom-0 py-2 pr-4 mr-3 hidden-sm-down">
                                                    <div class="text-right fw-500 l-h-n d-flex flex-column">
                                                    </div>
                                                </div>
                                            </div>
                                         
                                            <div id="flot-area" style="width:100%; height:300px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 margin_top_40">
                                <div id="panel-3" class="panel" data-panel-locked data-panel-refresh data-panel-reset data-panel-color data-panel-custombutton>
                                    <div class="panel-hdr bg-info-600 bg-info-gradient">
                                        <h2>
                                            Relación <span class="fw-300"><i> Casos vs Rango de Edades </i></span>
                                        </h2>
                                        <div class="panel-toolbar">
                                     <!--       <button class="btn btn-panel" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Colapso"></button>
                                            <button class="btn btn-panel" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Pantalla Completa"></button>
                                            <button class="btn btn-panel" data-action="panel-close" data-toggle="tooltip" data-offset="0,10" data-original-title="Cerrar"></button>  -->
                                        </div>
                                    </div>
                                    <div class="panel-container show">
                                        <div class="panel-content">
                                            <div id="flot-edades" style="width:100%; height:300px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-12 margin_top_40" id="DivBarrios">
                                    <div id="panel-4" class="panel">
                                    <div class="panel-hdr bg-info-600 bg-info-gradient">
                                            <h2>
                                                Relación <span class="fw-300"><i> Casos vs Barrio de Ocurrencia </i></span>
                                            </h2>
                                            <div class="panel-toolbar">
                                            </div>
                                        </div>
                                        <div class="panel-container show">
                                            <div class="panel-content">
                                                <div id="flot-bar-ocu" style="width:100%; height:3600px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
                                <div class="col-lg-12 margin_top_40" id="DivMunicipio">
                                    <div id="panel-1" class="panel" data-panel-locked data-panel-refresh data-panel-reset data-panel-color data-panel-custombutton>
                                    <div class="panel-hdr bg-info-600 bg-info-gradient">
                                            <h2>
                                                Relación <span class="fw-300"><i> Casos vs Municipios de Ocurrencia </i></span>
                                            </h2>
                                        </div>
                                        <div class="panel-container show">
                                            <div class="panel-content">
                                                <div id="flot-mun-ocu" style="width:100%; height:600px;"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            

                            <div class="col-sm-12 margin_top_40">
                                <div class="card mb-g">
                                    <div class="card-body">
                                        <h4 class="mb-g mt-2 text-center">
                                            <strong>Mapa de Intensidad</strong> <br>
                                        </h4>
                                        <div class="demo-window rounded mb-g shadow-1 m-auto" style="max-width:100%">
                                            <div class="demo-window-content">
                                                <div class="d-flex app-body-demo">
                                                    <div class="app-nav-demo bg-fusion-200">
                                                        <div class="p-1 pt-0 bg-primary-300 pattern-0 app-header-demo"></div>
                                                    </div>
                                                    <div class="d-flex flex-column flex-1">
                                                            <img src="<?php echo base_url(); ?>assets/img/molecular/mapa.jpeg" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 col-xl-12" style="padding-bottom: 40px;">
                            <a href="<?php echo site_url('graficas/periodos'); ?>" class="btn btn-primary btn-block waves-effect waves-themed">                                       
                                <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                    <span class="fal fa-list mr-1"></span> Consultar Otro Indicador       
                                </h3>
                            </a>
                            </div>
                        </div>
                    </main>


                    