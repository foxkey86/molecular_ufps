<main id="js-page-content" role="main" class="page-content"  style="padding-bottom: 60px;">
<div class="text-primary" style="font-size: 480%; line-height: 60px; padding-bottom: 40px;">
                                            <span class="fw-900" style="color:#aa1916;">
                                            Indicadores de Salud Pública del Departamento de Norte de Santander.
              
                                            </span>
                                        </div>
<div class="row" style="padding-bottom: 40px;">
<div class="col-sm-12 col-xl-12">

                                                <div class="alert bg-info-900 text-white fade show" role="alert">
                                                    <div class="d-flex align-items-center">
                                                        <div class="alert-icon">
                                                            <span class="icon-stack icon-stack-md">
           
                                                                <i class="fal fa-chart-bar mb-n1 mr-n1" style="font-size:2rem"></i>
                                                            </span>
                                                        </div>
                                                        <div class="flex-1">
                                                            <span class="h4">Indicador</span>
                                                            <br>
                                                            <span id="text_indicador">Seleccione un indicador.</span>
                                                        </div>
                                                    </div>
                                                </div>
</div>

                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-info-600 rounded overflow-hidden position-relative text-white mb-g" id="ind1" style="cursor:pointer;">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                        Dengue sin signos de alarma
                                            <small class="m-0 l-h-n">Indicador</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-chart-bar position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-info-600 rounded overflow-hidden position-relative text-white mb-g" id="ind2" style="cursor:pointer;">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                        Dengue con signos de alarma
                                            <small class="m-0 l-h-n">Indicador</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-chart-bar position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
                                </div>
                            </div>
                      <!--      <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-info-600 rounded overflow-hidden position-relative text-white mb-g" id="ind3" style="cursor:pointer;">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                        Hepatitis A<br>&nbsp;
                                            <small class="m-0 l-h-n">Indicador</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-chart-bar position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>  -->
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-info-600 rounded overflow-hidden position-relative text-white mb-g" id="ind4" style="cursor:pointer;">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                        Hepatitis B Aguda<br>&nbsp;
                                            <small class="m-0 l-h-n">Indicador</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-chart-bar position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-info-600 rounded overflow-hidden position-relative text-white mb-g" id="ind5" style="cursor:pointer;">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                        Hepatitis B Crónica<br>&nbsp;
                                            <small class="m-0 l-h-n">Indicador</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-chart-bar position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>
                     <!--       <div class="col-sm-6 col-xl-2">
                                <div class="p-3 bg-warning-400 rounded overflow-hidden position-relative text-white mb-g" id="ind6">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                        Hepatitis B por Transmisión Perinatal<br>&nbsp;
                                            <small class="m-0 l-h-n">Indicador</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-chart-bar position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-2">
                                <div class="p-3 bg-warning-400 rounded overflow-hidden position-relative text-white mb-g" id="ind7">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                        Hepatitis Coinfección B-D<br>&nbsp;
                                            <small class="m-0 l-h-n">Indicador</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-chart-bar position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>  -->
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-info-600 rounded overflow-hidden position-relative text-white mb-g" id="ind8" style="cursor:pointer;">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                        Hepatitis C<br>&nbsp;
                                            <small class="m-0 l-h-n">Indicador</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-chart-bar position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-info-600 rounded overflow-hidden position-relative text-white mb-g" id="ind9" style="cursor:pointer;">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                        TB Pulmonares<br>&nbsp;
                                            <small class="m-0 l-h-n">Indicador</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-chart-bar position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-info-600 rounded overflow-hidden position-relative text-white mb-g" id="ind10" style="cursor:pointer;">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                        TB Extrapulmonares<br>&nbsp;
                                            <small class="m-0 l-h-n">Indicador</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-chart-bar position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-info-600 rounded overflow-hidden position-relative text-white mb-g" id="ind11" style="cursor:pointer;">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                        VIH<br>&nbsp;
                                            <small class="m-0 l-h-n">Indicador</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-chart-bar position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>
                      <!--      <div class="col-sm-6 col-xl-2">
                                <div class="p-3 bg-info-200 rounded overflow-hidden position-relative text-white mb-g" id="ind12">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                        VIH con coinfección TB<br>&nbsp;
                                            <small class="m-0 l-h-n">Indicador</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-chart-bar position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>  -->
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-info-600 rounded overflow-hidden position-relative text-white mb-g" id="ind13" style="cursor:pointer;">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                        COVID confirmados<br>&nbsp;
                                            <small class="m-0 l-h-n">Indicador</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-chart-bar position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-info-600 rounded overflow-hidden position-relative text-white mb-g" id="ind14" style="cursor:pointer;">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                        COVID Recuperados<br>&nbsp;
                                            <small class="m-0 l-h-n">Indicador</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-chart-bar position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-info-600 rounded overflow-hidden position-relative text-white mb-g" id="ind15" style="cursor:pointer;">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                        COVID Fallecidos<br>&nbsp;
                                            <small class="m-0 l-h-n">Indicador</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-chart-bar position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
                                </div>
                            </div>
                        </div>

                        <div class="row" style="padding-bottom: 40px;">

    <div class="col-sm-12 col-xl-12">
        <div class="alert bg-warning-900 text-white fade show" role="alert">
            <div class="d-flex align-items-center">
                <div class="alert-icon">
                    <span class="icon-stack icon-stack-md">

                        <i class="fa fa-map mb-n1 mr-n1" style="font-size:2rem"></i>
                    </span>
                </div>
                <div class="flex-1">
                    <span class="h4">Ubicación</span>
                    <br>
                    <span id="text_ubi">Seleccione una ubicación.</span>
                </div>
            </div>
        </div>
    </div>
<div class="col-sm-6 col-xl-3">
    <div class="p-3 bg-warning-600 rounded overflow-hidden position-relative text-white mb-g" id="ubi1" style="cursor:pointer;">
        <div class="">
            <h3 class="display-5 d-block l-h-n m-0 fw-500">
             Norte de Santander
                <small class="m-0 l-h-n">Departamento</small>
            </h3>
        </div>
        <i class="fa fa-map position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
    </div>
</div>
<div class="col-sm-6 col-xl-3">
    <div class="p-3 bg-warning-600 rounded overflow-hidden position-relative text-white mb-g" id="ubi2" style="cursor:pointer;">
        <div class="">
            <h3 class="display-5 d-block l-h-n m-0 fw-500">
             Cúcuta
                <small class="m-0 l-h-n">Municipio</small>
            </h3>
        </div>
        <i class="fa fa-map position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
    </div>
</div> <div class="col-sm-6 col-xl-3">
    <div class="p-3 bg-warning-600 rounded overflow-hidden position-relative text-white mb-g" id="ubi3" style="cursor:pointer;">
        <div class="">
            <h3 class="display-5 d-block l-h-n m-0 fw-500">
             Villa del Rosario
                <small class="m-0 l-h-n">Municipio</small>
            </h3>
        </div>
        <i class="fa fa-map position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
    </div>
</div>
<div class="col-sm-6 col-xl-3">
    <div class="p-3 bg-warning-600 rounded overflow-hidden position-relative text-white mb-g" id="ubi4" style="cursor:pointer;">
        <div class="">
            <h3 class="display-5 d-block l-h-n m-0 fw-500">
             Los Patios
                <small class="m-0 l-h-n">Municipio</small>
            </h3>
        </div>
        <i class="fa fa-map position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
    </div>
</div>
<div class="col-sm-6 col-xl-3">
    <div class="p-3 bg-warning-600 rounded overflow-hidden position-relative text-white mb-g" id="ubi5" style="cursor:pointer;">
        <div class="">
            <h3 class="display-5 d-block l-h-n m-0 fw-500">
             El Zulia
                <small class="m-0 l-h-n">Municipio</small>
            </h3>
        </div>
        <i class="fa fa-map position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
    </div>
</div>
</div>

                        <div class="row" style="padding-bottom: 40px;">
    <div class="col-sm-12 col-xl-12">
        <div class="alert bg-success-900 text-white fade show" role="alert">
            <div class="d-flex align-items-center">
                <div class="alert-icon">
                    <span class="icon-stack icon-stack-md">

                        <i class="fal fa-calendar-alt mb-n1 mr-n1" style="font-size:2rem"></i>
                    </span>
                </div>
                <div class="flex-1">
                    <span class="h4">Año</span>
                    <br>
                    <span id="text_anyo">Seleccione un año.</span>
                </div>
            </div>
        </div>
    </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-success-600 rounded overflow-hidden position-relative text-white mb-g" id="any1" style="cursor:pointer;">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                         2019
                                            <small class="m-0 l-h-n">Año</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-calendar-alt position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
                                </div>
                            </div>
                            <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-success-600 rounded overflow-hidden position-relative text-white mb-g" id="any2" style="cursor:pointer;">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                         2020
                                            <small class="m-0 l-h-n">Año</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-calendar-alt position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
                                </div>
                            </div> <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-success-600 rounded overflow-hidden position-relative text-white mb-g" id="any3" style="cursor:pointer;">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                         2021
                                            <small class="m-0 l-h-n">Año</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-calendar-alt position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
                                </div>
                            </div> <div class="col-sm-6 col-xl-3">
                                <div class="p-3 bg-success-600 rounded overflow-hidden position-relative text-white mb-g" id="any4" style="cursor:pointer;">
                                    <div class="">
                                        <h3 class="display-5 d-block l-h-n m-0 fw-500">
                                         2022
                                            <small class="m-0 l-h-n">Año</small>
                                        </h3>
                                    </div>
                                    <i class="fal fa-calendar-alt position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
                                </div>
                            </div>
                        </div>
                        <div class="row">
    <div class="col-sm-12 col-xl-12">
        <div class="alert bg-primary-900 text-white fade show" role="alert">
            <div class="d-flex align-items-center">
                <div class="alert-icon">
                    <span class="icon-stack icon-stack-md">

                        <i class="fal fa-calendar-alt mb-n1 mr-n1" style="font-size:2rem"></i>
                    </span>
                </div>
                <div class="flex-1">
                    <span class="h4">Trimestre</span>
                    <br>
                    <span id="text_tri">Seleccione un trimestre.</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-xl-3">
        <div class="p-3 bg-primary-600 rounded overflow-hidden position-relative text-white mb-g" id="tri1" style="cursor:pointer;">
            <div class="">
                <h3 class="display-5 d-block l-h-n m-0 fw-500">
                 1
                    <small class="m-0 l-h-n">Trimestre</small>
                </h3>
            </div>
            <i class="fal fa-calendar-alt position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
        </div>
    </div>
    <div class="col-sm-6 col-xl-3">
        <div class="p-3 bg-primary-600 rounded overflow-hidden position-relative text-white mb-g" id="tri2" style="cursor:pointer;">
            <div class="">
                <h3 class="display-5 d-block l-h-n m-0 fw-500">
                 2
                    <small class="m-0 l-h-n">Trimestre</small>
                </h3>
            </div>
            <i class="fal fa-calendar-alt position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
        </div>
    </div> 
    <div class="col-sm-6 col-xl-3">
        <div class="p-3 bg-primary-600 rounded overflow-hidden position-relative text-white mb-g" id="tri3" style="cursor:pointer;">
            <div class="">
                <h3 class="display-5 d-block l-h-n m-0 fw-500">
                 3
                    <small class="m-0 l-h-n">Trimestre</small>
                </h3>
            </div>
            <i class="fal fa-calendar-alt position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
        </div>
    </div> 
    <div class="col-sm-6 col-xl-3">
        <div class="p-3 bg-primary-600 rounded overflow-hidden position-relative text-white mb-g" id="tri4" style="cursor:pointer;">
            <div class="">
                <h3 class="display-5 d-block l-h-n m-0 fw-500">
                 4
                    <small class="m-0 l-h-n">Trimestre</small>
                </h3>
            </div>
            <i class="fal fa-calendar-alt position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
        </div>
    </div>
    <div class="col-sm-6 col-xl-3">
        <div class="p-3 bg-primary-600 rounded overflow-hidden position-relative text-white mb-g" id="tri5" style="cursor:pointer;">
            <div class="">
                <h3 class="display-5 d-block l-h-n m-0 fw-500">
                 Todos
                    <small class="m-0 l-h-n">Trimestre</small>
                </h3>
            </div>
            <i class="fal fa-calendar-alt position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
        </div>
    </div>

    <div class="col-xl-12">
    <div class="card border mb-g">
        <div class="row">
        <div class="col-sm-6 col-xl-6">
 
 <div class="height-8 d-flex flex-row align-items-center flex-wrap flex-shrink-0">
                                             <button class="btn btn-info btn-block btn-lg waves-effect waves-themed font-weight-bolder" type="button" onClick="limpiar()"><span class="fal fa-sync mr-1"></span> Limpiar</button>
                                         </div>
 </div>
                            <div class="col-sm-6 col-xl-6">

                                
                                        <?php echo form_open('', ['class' => '', 'id' => 'form', 'role' => 'form'], ['responder' => 1, 'id_pregunta' => '1']); ?>
                                            <input type="hidden" id="arreglo" name="arreglo">
                                            <div class="height-8 d-flex flex-row align-items-center flex-wrap flex-shrink-0">
                                                <button class="btn btn-success btn-block btn-lg waves-effect waves-themed font-weight-bolder" style="display:none;" id="Bcargando" type="button" disabled><span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Cargando...</button>
                                                <button class="btn btn-success btn-block btn-lg waves-effect waves-themed font-weight-bolder" id="Bconsultar" type="button" onClick="placeOrder(this.form)"><span class="fal fa-check-circle mr-1"></span> Consultar</button>
                                            </div>

                                                                <!-- Modal Center Transparent -->
                                                                <div class="modal fade example-modal-centered-transparent" tabindex="-1" role="dialog" aria-hidden="true" id="loginIndicador">
                                                <div class="modal-dialog modal-dialog-centered modal-transparent" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title text-white">
                                                                Validar Ingreso
                                                                <small class="m-0 text-white opacity-70">
                                                                    Para visualizar las gráficas del indicador es necesario validar el ingreso del usuario, por favor ingresar el usuario y contraseña asignada.
                                                                </small>
                                                            </h4>
                                                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                                <div class="form-group">
                                                                    <label class="form-label text-white" for="simpleinput">Usuario</label>
                                                                    <input type="usuario" id="usuario" class="form-control">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label class="form-label text-white" for="example-password">Contraseña</label>
                                                                    <input type="password" id="password" class="form-control">
                                                                </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                            <button type="button" class="btn btn-success" onClick="validarLogin(this.form)">Ingresar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                        <?php echo form_close(); ?>
                                </div>   
 

    </div>
    </div>
                                                   
                            </div>
</div>

                     





            
                        
                    </main>


                    


                                                                                                                <!-- Modal Center Transparent -->
                                            <div class="modal fade example-modal-centered-transparent" tabindex="-1" role="dialog" aria-hidden="true" id="loginError">
                                                <div class="modal-dialog modal-dialog-centered modal-transparent" role="document">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title text-white">
                                                                Error logueo
                                                            </h4>
                                                            <button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
                                                                <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                               <h3 class="text-white">Usuario o Contraseña inválida, por favor debe ingresar correctamente el usuario y contraseña asignada.</h3>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Aceptar</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>