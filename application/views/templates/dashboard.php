    <html lang="en">
    <head>
        <meta charset="utf-8">
        <title>
        Centro de Investigación de Biotecnología y Medio Ambiente - CIBIM
        </title>

        <meta name="application-name" content="Website Saber Pro UFPS" lang="es">
        <meta name="Author" content="Viarney Alirio Villamizar Moreno" lang="es">
        <meta name="description" content="Dashboard">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
        <!-- Call App Mode on ios devices -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no">
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="<?php echo base_url(); ?>assets/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?php echo base_url(); ?>assets/css/app.bundle.css">
        <!-- Place favicon.ico in the root directory -->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/img/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/img/favicon/favicon-32x32.png">
        <link rel="mask-icon" href="<?php echo base_url(); ?>assets/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <!-- Optional: page related CSS-->
        <link rel="stylesheet" media="screen, print" href="<?php echo base_url(); ?>assets/css/fa-brands.css">
        <link rel="stylesheet" media="screen, print" href="<?php echo base_url(); ?>assets/css/fa-solid.css">
   
        <?php echo $_css ?>
   </head>
    <body class="mod-bg-1">
        <!-- DOC: script to save and load page settings -->
        <script>
            /**
             *	This script should be placed right after the body tag for fast execution 
             *	Note: the script is written in pure javascript and does not depend on thirdparty library
             **/
            'use strict';

            var classHolder = document.getElementsByTagName("BODY")[0],
                /** 
                 * Load from localstorage
                 **/
                themeSettings = (localStorage.getItem('themeSettings')) ? JSON.parse(localStorage.getItem('themeSettings')) :
                {},
                themeURL = themeSettings.themeURL || '',
                themeOptions = themeSettings.themeOptions || '';
            /** 
             * Load theme options
             **/
            if (themeSettings.themeOptions)
            {
                classHolder.className = themeSettings.themeOptions;
                console.log("%c✔ Theme settings loaded", "color: #148f32");
            }
            else
            {
                console.log("Heads up! Theme settings is empty or does not exist, loading default settings...");
            }
            if (themeSettings.themeURL && !document.getElementById('mytheme'))
            {
                var cssfile = document.createElement('link');
                cssfile.id = 'mytheme';
                cssfile.rel = 'stylesheet';
                cssfile.href = themeURL;
                document.getElementsByTagName('head')[0].appendChild(cssfile);
            }
            /** 
             * Save to localstorage 
             **/
            var saveSettings = function()
            {
                themeSettings.themeOptions = String(classHolder.className).split(/[^\w-]+/).filter(function(item)
                {
                    return /^(nav|header|mod|display)-/i.test(item);
                }).join(' ');
                if (document.getElementById('mytheme'))
                {
                    themeSettings.themeURL = document.getElementById('mytheme').getAttribute("href");
                };
                localStorage.setItem('themeSettings', JSON.stringify(themeSettings));
            }
            /** 
             * Reset settings
             **/
            var resetSettings = function()
            {
                localStorage.setItem("themeSettings", "");
            }

        </script>
        <!-- BEGIN Page Wrapper -->
        <div class="page-wrapper">
            <div class="page-inner">
                <!-- BEGIN Left Aside -->
                <?php 
                    include APPPATH . "views/templates/sidebar.php";
                ?>
                   <!-- END Left Aside -->
                <div class="page-content-wrapper">
                    <!-- BEGIN Page Header -->
                    <?php 
                        include APPPATH . "views/templates/header.php";
                    ?>
                    <!-- END Page Header -->
                    <!-- BEGIN Page Content -->
                    <!-- the #js-page-content id is needed for some plugins to initialize -->
                    <main id="js-page-content" role="main" class="page-content">
                        <ol class="breadcrumb page-breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0);">Inicio</a></li>
                            <?php if (isset($migasdepan)) : ?>
                            <?php echo $migasdepan; ?>
                            <?php endif; ?>
                            <li class="position-absolute pos-top pos-right d-none d-sm-block"><span><?php echo $fecha_actual; ?></span></li>
                        </ol>
                        
                        <?php if (isset($content_header)) : ?>
                            <!-- Content Header (Page header) -->
                            <div class="subheader">
                                <h1 class="subheader-title">
                                    <i class='subheader-icon fal fa-plus-circle'></i> <?php echo ((isset($content_header)) ? $content_header : ''); ?>
                                    <small><?php echo ((isset($content_sub_header)) ? $content_sub_header : ''); ?></small>      
                                 </h1>
                            </div>
                        <?php endif; ?>
                         <!-- Alertas de la clase Template -->
                             
                        <?php foreach ($_warning as $_msj): ?>
                            <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                    <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                                                </button>
                                                                <div class="d-flex align-items-center">
                                                                    <div class="alert-icon width-3">
                                                                        <span class="icon-stack icon-stack-sm">
                                                                        <i class="fal fa-shield-check text-warning"></i>
                                                                        </span>
                                                                    </div>
                                                                    <div class="flex-1">
                                                                        <span class="h5 m-0 fw-700">Atención!</span>
                                                                        <?= $_msj ?>
                                                                    </div>
                                                                </div>
                            </div>
                        <?php endforeach; ?>
                        <?php foreach ($_success as $_msj): ?>
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                    <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                                                </button>
                                                                <div class="d-flex align-items-center">
                                                                    <div class="alert-icon width-3">
                                                                        <span class="icon-stack icon-stack-sm">
                                                                        <i class="fal fa-check "></i>
                                                                        </span>
                                                                    </div>
                                                                    <div class="flex-1">
                                                                        <span class="h5 m-0 fw-700">Exitoso!</span>
                                                                        <?= $_msj ?>
                                                                    </div>
                                                                </div>
                            </div>
                        
                        <?php endforeach; ?>
                        <?php foreach ($_error as $_msj): ?>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                    <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                                                </button>
                                                                <div class="d-flex align-items-center">
                                                                    <div class="alert-icon width-3">
                                                                        <span class="icon-stack icon-stack-sm">
                                                                        <i class="fal fa-times"></i>
                                                                        </span>
                                                                    </div>
                                                                    <div class="flex-1">
                                                                        <span class="h5 m-0 fw-700">Error!</span>
                                                                        <?= $_msj ?>
                                                                    </div>
                                                                </div>
                            </div>
                        <?php endforeach; ?>
                        <?php foreach ($_info as $_msj): ?>
                            <div class="alert alert-info alert-dismissible fade show" role="alert">
                                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                                    <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                                                </button>
                                                                <div class="d-flex align-items-center">
                                                                    <div class="alert-icon width-3">
                                                                        <span class="icon-stack icon-stack-sm">
                                                                        <i class="fal fa-info-circle"></i>
                                                                        </span>
                                                                    </div>
                                                                    <div class="flex-1">
                                                                        <span class="h5 m-0 fw-700">Información!</span>
                                                                        <?= $_msj ?>
                                                                    </div>
                                                                </div>
                            </div>
                        <?php endforeach; ?>

                        <?php foreach ($_content as $_view): ?>
                             <?php include $_view; ?>
                        <?php endforeach; ?>

                    </main>
                    <!-- this overlay is activated only when mobile menu is triggered -->
                    <div class="page-content-overlay" data-action="toggle" data-class="mobile-nav-on"></div> <!-- END Page Content -->
                    <!-- BEGIN Page Footer -->
                    <footer class="page-footer" role="contentinfo">
                        <div class="d-flex align-items-center flex-1 text-muted">
                            <span class="hidden-md-down fw-700">2021 © Universidad Francisco de Paula Santander by&nbsp;<a href='https://ww2.ufps.edu.co' class='text-primary fw-500' title='gotbootstrap.com' target='_blank'>ww2.ufps.edu.co</a></span>
                        </div>
                        <div>
                            <ul class="list-table m-0">
                                <li><a href="intel_introduction.html" class="text-secondary fw-700">Acerca de</a></li>
                                <li class="pl-3"><a href="info_app_licensing.html" class="text-secondary fw-700">Terminos y Condiciones</a></li>
                                <li class="pl-3"><a href="info_app_docs.html" class="text-secondary fw-700">Documentación</a></li>
                            </ul>
                        </div>
                    </footer>
                    <!-- END Page Footer -->
                    <!-- BEGIN Shortcuts -->
                </div>
            </div>
        </div>
        <!-- END Page Wrapper -->

    
        <!-- base vendor bundle: 
			 DOC: if you remove pace.js from core please note on Internet Explorer some CSS animations may execute before a page is fully loaded, resulting 'jump' animations 
						+ pace.js (recommended)
						+ jquery.js (core)
						+ jquery-ui-cust.js (core)
						+ popper.js (core)
						+ bootstrap.js (core)
						+ slimscroll.js (extension)
						+ app.navigation.js (core)
						+ ba-throttle-debounce.js (core)
						+ waves.js (extension)
						+ smartpanels.js (extension)
						+ src/../jquery-snippets.js (core) -->
        <script src="<?php echo base_url(); ?>assets/js/vendors.bundle.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/app.bundle.js"></script>
        <?php echo $_js; ?>
    </body>
</html>
