<aside class="page-sidebar">
                    <div class="page-logo">
                        <a href="#" class="page-logo-link press-scale-down d-flex align-items-center position-relative" data-toggle="modal" data-target="#modal-shortcut">
                            <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin WebApp" aria-roledescription="logo">
                            <span class="page-logo-text mr-1">BIOMOL-UFPS</span>
                            <span class="position-absolute text-white opacity-50 small pos-top pos-right mr-2 mt-n2"></span>
                            <i class="fal fa-angle-down d-inline-block ml-1 fs-lg color-primary-300"></i>
                        </a>
                    </div>
                    <!-- BEGIN PRIMARY NAVIGATION -->
                    <nav id="js-primary-nav" class="primary-nav" role="navigation">
                        <div class="nav-filter">
                            <div class="position-relative">
                                <input type="text" id="nav_filter_input" placeholder="Filter menu" class="form-control" tabindex="0">
                                <a href="#" onclick="return false;" class="btn-primary btn-search-close js-waves-off" data-action="toggle" data-class="list-filter-active" data-target=".page-sidebar">
                                    <i class="fal fa-chevron-up"></i>
                                </a>
                            </div>
                        </div>
                        <div class="info-card">
                            <img src="<?php echo base_url(); ?>assets/img/demo/avatars/<?php echo $informacion_usuario->avatar; ?>" class="profile-image rounded-circle" alt="Dr. Codex Lantern">
                            <div class="info-card-text">
                                <a href="#" class="d-flex align-items-center text-white">
                                    <span class="text-truncate text-truncate-sm d-inline-block">
                                    <?php if (isset($informacion_usuario->nombres)):
                                        echo $informacion_usuario->nombres; ?>
                                    <?php else: ?>
                                       Usuario
                                    <?php endif; ?>
                                    </span>
                                </a>
                                <span class="d-inline-block text-truncate text-truncate-sm">
                                <?php if (isset($informacion_usuario->apellidos)):
                                        echo $informacion_usuario->apellidos; ?>
                                    <?php else: ?>
                                       Usuario
                                    <?php endif; ?>
                                </span>
                            </div>
                            <img src="<?php echo base_url(); ?>assets/img/card-backgrounds/fondo_user.jpg" class="cover" alt="cover">
                            <a href="#" onclick="return false;" class="pull-trigger-btn" data-action="toggle" data-class="list-filter-active" data-target=".page-sidebar" data-focus="nav_filter_input">
                                <i class="fal fa-angle-down"></i>
                            </a>
                        </div>
                        <ul id="js-nav-menu" class="nav-menu">
                            <li class="nav-title">Usuario</li>
                            <li class="<?php echo ($item_sidebar_active == "perfil_usuario") ? "active open" : ""; ?>">
                                <a href="<?php echo site_url('perfil'); ?>" title="Perfíl Usuario" data-filter-tags="Perfíl Usuario">
                                    <i class="fal fa-user"></i>
                                    <span class="nav-link-text" data-i18n="nav.application_intel">Perfíl Usuario</span>
                                </a>
                            </li>
                           
                            <?php if(isset($informacion_usuario) && ($informacion_usuario->id_perfil == 1 || $informacion_usuario->id_perfil == 2)): ?>
                            <li class="nav-title">Administración</li>
                            <li class="<?php echo (in_array($item_sidebar_active, array("usuarios", "notas"))) ? "active" : ""; ?>">
                                <a href="#" title="Theme Settings" data-filter-tags="theme settings">
                                    <i class="fal fa-cog"></i>
                                    <span class="nav-link-text" data-i18n="nav.theme_settings">Usuarios</span>
                                </a>
                                <ul>
                                    <li class="<?php echo ($item_sidebar_active == "usuarios") ? "active open" : ""; ?>">
                                        <a href="<?php echo site_url('usuarios/index'); ?>" title="Módulo de Consultas" data-filter-tags="Módulo de Usuarios">
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_how_it_works">Administración de Usuarios</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <?php endif; ?>
                            <?php if(isset($informacion_usuario) && ($informacion_usuario->id_perfil == 1 || $informacion_usuario->id_perfil == 2)): ?>
                            <li class="nav-title">Servicios</li>
                                <li class="<?php echo (in_array($item_sidebar_active, array("consultas", "notas"))) ? "active" : ""; ?>">
                                    <a href="#" title="Theme Settings" data-filter-tags="theme settings">
                                        <i class="fal fa-cog"></i>
                                        <span class="nav-link-text" data-i18n="nav.theme_settings">Consultas Agentes Biológicos</span>
                                    </a>
                                    <ul>
                                        <li class="<?php echo ($item_sidebar_active == "consultas") ? "active open" : ""; ?>">
                                            <a href="<?php echo site_url('consultas/admin'); ?>" title="Módulo de Consultas" data-filter-tags="Módulo de Consultas">
                                                <span class="nav-link-text" data-i18n="nav.theme_settings_how_it_works">Administración de Consultas</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            <?php else:?>
                                <?php if(isset($informacion_usuario) && ($informacion_usuario->id_perfil == 3)): ?>
                                    <li class="<?php echo ($item_sidebar_active == "consultas") ? "active open" : ""; ?>">
                                    <a href="#" title="Theme Settings" data-filter-tags="theme settings">
                                        <i class="fal fa-question-circle"></i>
                                        <span class="nav-link-text" data-i18n="nav.theme_settings">Consultas Agentes Biológicos</span>
                                    </a>
                                    <ul>
                                        <li class="<?php echo ($item_sidebar_active == "consultas") ? "active open" : ""; ?>">
                                            <a href="<?php echo site_url('consultas'); ?>" title="Módulo de Consultas" data-filter-tags="Módulo de Consultas">
                                                <span class="nav-link-text" data-i18n="nav.theme_settings_how_it_works">Consultar</span>
                                            </a>
                                        </li>
                                    </ul>
                                    </li>
                                <?php endif;?>
                            <?php endif;?>

                            <?php if(isset($informacion_usuario) && ($informacion_usuario->id_perfil == 1 || $informacion_usuario->id_perfil == 2)): ?>
                            <li class="nav-title">Indicadores Salud Pública</li>
                            <li>
                                <a href="<?php echo site_url('graficas/periodos'); ?>" title="Indicadores" data-filter-tags="Indicadores">
                                    <i class="fal fa-chart-pie"></i>
                                    <span class="nav-link-text" data-i18n="nav.application_intel">Indicadores</span>
                                </a>
                            </li>
                            <li class="<?php echo (in_array($item_sidebar_active, array("carga_den", "carga_hep", "carga_tbc", "carga_vih", "carga_cov"))) ? "active" : ""; ?>">
                                <a href="#" title="Theme Settings" data-filter-tags="theme settings">
                                    <i class="fal fa-cog"></i>
                                    <span class="nav-link-text" data-i18n="nav.theme_settings">Cargar de Datos</span>
                                </a>
                                <ul>
                                    <li class="<?php echo ($item_sidebar_active == "carga_den") ? "active open" : ""; ?>">
                                        <a href="<?php echo site_url('consultas/carga_den'); ?>" title="Resultados" data-filter-tags="Resultados">
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_how_it_works"> Carga Dengue</span>
                                        </a>
                                    </li>
                                    <li class="<?php echo ($item_sidebar_active == "carga_hep") ? "active open" : ""; ?>">
                                        <a href="<?php echo site_url('consultas/carga_hep'); ?>" title="Resultados" data-filter-tags="Resultados">
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_how_it_works"> Carga Hepatitis</span>
                                        </a>
                                    </li>
                                    <li class="<?php echo ($item_sidebar_active == "carga_tbc") ? "active open" : ""; ?>">
                                        <a href="<?php echo site_url('consultas/carga_tbc'); ?>" title="Resultados" data-filter-tags="Resultados">
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_how_it_works"> Carga Tuberculosis</span>
                                        </a>
                                    </li>
                                    <li class="<?php echo ($item_sidebar_active == "carga_vih") ? "active open" : ""; ?>">
                                        <a href="<?php echo site_url('consultas/carga_vih'); ?>" title="Resultados" data-filter-tags="Resultados">
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_how_it_works"> Carga VIH</span>
                                        </a>
                                    </li>
                                    <li class="<?php echo ($item_sidebar_active == "carga_cov") ? "active open" : ""; ?>">
                                        <a href="<?php echo site_url('consultas/carga_cov'); ?>" title="Resultados" data-filter-tags="Resultados">
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_how_it_works"> Carga Covid</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="<?php echo (in_array($item_sidebar_active, array("reporte_den", "reporte_hep", "reporte_tbc", "reporte_vih", "reporte_cov"))) ? "active" : ""; ?>">
                                <a href="#" title="Theme Settings" data-filter-tags="theme settings">
                                    <i class="fal fa-list-alt"></i>
                                    <span class="nav-link-text" data-i18n="nav.theme_settings">Reporte de Datos</span>
                                </a>
                                <ul>
                                    <li class="<?php echo ($item_sidebar_active == "reporte_den") ? "active open" : ""; ?>">
                                        <a href="<?php echo site_url('reportes/filtro_dengue'); ?>" title="Resultados" data-filter-tags="Resultados">
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_how_it_works"> Reporte Dengue</span>
                                        </a>
                                    </li>
                                    <li class="<?php echo ($item_sidebar_active == "reporte_hep") ? "active open" : ""; ?>">
                                        <a href="<?php echo site_url('reportes/filtro_hep'); ?>" title="Resultados" data-filter-tags="Resultados">
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_how_it_works"> Reporte Hepatitis</span>
                                        </a>
                                    </li>
                                    <li class="<?php echo ($item_sidebar_active == "reporte_tbc") ? "active open" : ""; ?>">
                                        <a href="<?php echo site_url('reportes/filtro_tbc'); ?>" title="Resultados" data-filter-tags="Resultados">
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_how_it_works"> Reporte Tuberculosis</span>
                                        </a>
                                    </li>
                                    <li class="<?php echo ($item_sidebar_active == "reporte_vih") ? "active open" : ""; ?>">
                                        <a href="<?php echo site_url('reportes/filtro_vih'); ?>" title="Resultados" data-filter-tags="Resultados">
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_how_it_works"> Reporte VIH</span>
                                        </a>
                                    </li>
                                    <li class="<?php echo ($item_sidebar_active == "reporte_cov") ? "active open" : ""; ?>">
                                        <a href="<?php echo site_url('reportes/filtro_cov'); ?>" title="Resultados" data-filter-tags="Resultados">
                                            <span class="nav-link-text" data-i18n="nav.theme_settings_how_it_works"> Reporte Covid</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <?php endif;?>
                            <?php if(isset($informacion_usuario) && ($informacion_usuario->id_perfil == 1 || $informacion_usuario->id_perfil == 6
                            || $informacion_usuario->id_perfil == 7 || $informacion_usuario->id_perfil == 8 || $informacion_usuario->id_perfil == 9)): ?>
                                <li class="nav-title">Documentación de Laboratorio</li>
                                <li class="<?php echo ($item_sidebar_active == "documentacion") ? "active open" : ""; ?>">
                                    <a href="<?php echo site_url('documentacion/clasificacion_doc'); ?>" title="Documentos disponibles" data-filter-tags="Resultados">
                                        <i class="fal fa-folder-open"></i>
                                        <span class="nav-link-text" data-i18n="nav.application_intel">Documentos disponibles</span>
                                    </a>
                                </li>
                                <?php if(isset($informacion_usuario) && ($informacion_usuario->id_perfil == 7 || $informacion_usuario->id_perfil == 8 || 
                                            $informacion_usuario->id_perfil == 9)): ?>
                                    <li class="<?php echo ($item_sidebar_active == "panel_doc") ? "active open" : ""; ?>">
                                        <a href="<?php echo site_url('documentacion/panel_documentacion'); ?>" title="Gestión de documentación" data-filter-tags="Resultados">
                                            <i class="fas fa-copy"></i>
                                            <span class="nav-link-text" data-i18n="nav.application_intel">Gestión de documentación</span>
                                        </a>
                                    </li>
                                    <li class="<?php echo ($item_sidebar_active == "panel_doc_user") ? "active open" : ""; ?>">
                                        <a href="<?php echo site_url('documentacion/panel_documentacion_user'); ?>" title="Gestión de documentación" data-filter-tags="Resultados">
                                            <i class="fas fa-file-alt"></i>
                                            <span class="nav-link-text" data-i18n="nav.application_intel">Proceso de carga</span>
                                        </a>
                                    </li>
                                <?php endif;?>
                            <?php endif;?>
                            <li class="nav-title">Cerrar Sesión</li>
                            <li class="active open">
                                <a href="<?php echo site_url('sesion/logout'); ?>" title="Pages" data-filter-tags="pages">
                                    <i class="fal fa-sign-out"></i>
                                    <span class="nav-link-text" data-i18n="nav.pages">Cerrar Sesión</span>
                                </a>
                            </li>
                        </ul>
                        <div class="filter-message js-filter-message bg-success-600"></div>
                    </nav>
                    <!-- END PRIMARY NAVIGATION -->
                    <!-- NAV FOOTER -->
                    <div class="nav-footer shadow-top">
                        <a href="#" onclick="return false;" data-action="toggle" data-class="nav-function-minify" class="hidden-md-down">
                            <i class="ni ni-chevron-right"></i>
                            <i class="ni ni-chevron-right"></i>
                        </a>
                        <ul class="list-table m-auto nav-footer-buttons">
                            <li>
                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Chat">
                                    <i class="fal fa-comments"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Soporte">
                                    <i class="fal fa-life-ring"></i>
                                </a>
                            </li>
                            <li>
                                <a href="javascript:void(0);" data-toggle="tooltip" data-placement="top" title="Llamada">
                                    <i class="fal fa-phone"></i>
                                </a>
                            </li>
                        </ul>
                    </div> <!-- END NAV FOOTER -->
                </aside>
             