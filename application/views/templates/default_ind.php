<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <title>
           Iniciar Sesión - Molecular UFPS
        </title>

        <meta name="application-name" content="Molecular UFPS" lang="es">
        <meta name="Author" content="Viarney Alirio Villamizar Moreno" lang="es">
        <meta name="description" content="Login">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
        <!-- Call App Mode on ios devices -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no">
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="<?php echo base_url(); ?>assets/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?php echo base_url(); ?>assets/css/app.bundle.css">
        <!-- Place favicon.ico in the root directory -->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/img/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/img/favicon/favicon-32x32.png">
        <link rel="mask-icon" href="<?php echo base_url(); ?>assets/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <!-- Optional: page related CSS-->
        <link rel="stylesheet" media="screen, print" href="<?php echo base_url(); ?>assets/css/fa-brands.css">

     
        <link rel="mask-icon" href="<?php echo base_url(); ?>assets/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <!-- Optional: page related CSS-->
        <link rel="stylesheet" media="screen, print" href="<?php echo base_url(); ?>assets/css/page-login.css">


        <?php echo $_css; ?>
    </head>
    <body>
        <div class="blankpage-form-field">
        <?php foreach ($_warning as $_msj): ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                                    </button>
                                                    <div class="d-flex align-items-center">
                                                        <div class="alert-icon width-3">
                                                            <span class="icon-stack icon-stack-sm">
                                                            <i class="fal fa-shield-check text-warning"></i>
                                                            </span>
                                                        </div>
                                                        <div class="flex-1">
                                                            <span class="h5 m-0 fw-700">Atención!</span>
                                                            <?= $_msj ?>
                                                        </div>
                                                    </div>
                                                </div>
            <?php endforeach; ?>
            <?php foreach ($_success as $_msj): ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                                    </button>
                                                    <div class="d-flex align-items-center">
                                                        <div class="alert-icon width-3">
                                                            <span class="icon-stack icon-stack-sm">
                                                            <i class="fal fa-check "></i>
                                                            </span>
                                                        </div>
                                                        <div class="flex-1">
                                                            <span class="h5 m-0 fw-700">Exitoso!</span>
                                                            <?= $_msj ?>
                                                        </div>
                                                    </div>
                                                </div>
               
            <?php endforeach; ?>
            <?php foreach ($_error as $_msj): ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                                    </button>
                                                    <div class="d-flex align-items-center">
                                                        <div class="alert-icon width-3">
                                                            <span class="icon-stack icon-stack-sm">
                                                            <i class="fal fa-times"></i>
                                                            </span>
                                                        </div>
                                                        <div class="flex-1">
                                                            <span class="h5 m-0 fw-700">Error!</span>
                                                            <?= $_msj ?>
                                                        </div>
                                                    </div>
                                                </div>
            <?php endforeach; ?>
            <?php foreach ($_info as $_msj): ?>
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                                    </button>
                                                    <div class="d-flex align-items-center">
                                                        <div class="alert-icon width-3">
                                                            <span class="icon-stack icon-stack-sm">
                                                            <i class="fal fa-info-circle"></i>
                                                            </span>
                                                        </div>
                                                        <div class="flex-1">
                                                            <span class="h5 m-0 fw-700">Información!</span>
                                                            <?= $_msj ?>
                                                        </div>
                                                    </div>
                                                </div>
            <?php endforeach; ?>

            <?php foreach ($_content as $_view): ?>
                <?php include $_view; ?>
            <?php endforeach; ?>

          
        </div>

        <video poster="<?php echo base_url(); ?>assets/img/backgrounds/clouds.png" id="bgvid" playsinline autoplay muted loop>
            <source src="<?php echo base_url(); ?>assets/media/video/ca.webm" type="video/webm">
            <source src="<?php echo base_url(); ?>assets/media/video/ca.mp4" type="video/mp4">
        </video>
        <!-- base vendor bundle: 
			 DOC: if you remove pace.js from core please note on Internet Explorer some CSS animations may execute before a page is fully loaded, resulting 'jump' animations 
						+ pace.js (recommended)
						+ jquery.js (core)
						+ jquery-ui-cust.js (core)
						+ popper.js (core)
						+ bootstrap.js (core)
						+ slimscroll.js (extension)
						+ app.navigation.js (core)
						+ ba-throttle-debounce.js (core)
						+ waves.js (extension)
						+ smartpanels.js (extension)
						+ src/../jquery-snippets.js (core) -->
        <script src="<?php echo base_url(); ?>assets/js/vendors.bundle.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/app.bundle.js"></script>
        <!-- Page related scripts -->
        <?php echo $_js; ?>
    </body>
</html>
