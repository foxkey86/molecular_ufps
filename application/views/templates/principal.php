<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Title -->
  <title>Centro de Investigación de Biotecnología y Medio Ambiente - CIBIM</title>

  <!-- Required Meta Tags Always Come First -->
  <meta charset="utf-8">

  <meta name="application-name" content="Website Saber Pro UFPS" lang="es">
        <meta name="Author" content="Viarney Alirio Villamizar Moreno" lang="es">
        <meta name="description" content="Dashboard">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
        <!-- Call App Mode on ios devices -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no">
        <!-- base css -->

  <!-- Favicon -->
  <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/safari-pinned-tab.svg">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- CSS Global Compulsory -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/bootstrap/bootstrap.min.css">
  <!-- CSS Global Icons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/icon-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/icon-line/css/simple-line-icons.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/icon-etlinefont/style.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/icon-line-pro/style.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/icon-hs/style.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/animate.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/hs-megamenu/src/hs.megamenu.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/hamburgers/hamburgers.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/css/settings.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/css/layers.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/css/navigation.css">

  <!-- CSS Unify -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/css/unify-core.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/css/unify-components.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/css/unify-globals.css">

  <!-- CSS Customization -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/css/custom.css">

  <?php echo $_css ?>

</head>

<body>
  <main>
  <!-- Header -->
  <header id="js-header" class="u-header u-header--static">
      <div class="u-header__section u-header__section--light g-bg-white g-transition-0_3 g-py-10">
        <nav class="js-mega-menu navbar navbar-expand-lg hs-menu-initialized hs-menu-horizontal">
          <div class="container">
            <!-- Responsive Toggle Button -->
            <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-3 g-right-0" type="button" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
              <span class="hamburger hamburger--slider">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
              </span>
              </span>
            </button>
            <!-- End Responsive Toggle Button -->

            <!-- Logo -->
            <a href="/" class="navbar-brand">
              <img src="https://ww2.ufps.edu.co/public/imagenes/template/header/logo_ufps.png" alt="Unify Logo">
            </a>
            <!-- End Logo -->

            <!-- Navigation -->
            <div class="collapse navbar-collapse align-items-center flex-sm-row g-pt-10 g-pt-5--lg g-mr-40--lg" id="navBar">
              <ul class="navbar-nav text-uppercase g-pos-rel g-font-weight-600 ml-auto">
                <!-- Intro -->
                <li class="nav-item  g-mx-10--lg g-mx-15--xl active">
                  <a href="/" class="nav-link g-py-7 g-px-0">Inicio</a>
                </li>
                <!-- End Intro -->

                <!-- Home -->
                <li class="hs-has-mega-menu nav-item g-mx-10--lg g-mx-15--xl" data-animation-in="fadeIn" data-animation-out="fadeOut" data-max-width="60%" data-position="left">
                  <a id="mega-menu-home" class="nav-link g-py-7 g-px-0" href="<?php echo base_url(); ?>paginas" aria-haspopup="true" aria-expanded="false">Nosotros
        </a>
                </li>
                <!-- End Home -->
                                <!-- Home -->
                                <li class="hs-has-mega-menu nav-item g-mx-10--lg g-mx-15--xl" data-animation-in="fadeIn" data-animation-out="fadeOut" data-max-width="60%" data-position="left">
                  <a id="mega-menu-home" class="nav-link g-py-7 g-px-0" href="<?php echo base_url(); ?>paginas/normatividad" aria-haspopup="true" aria-expanded="false">Normatividad
        </a>
                </li>
                <!-- End Home -->
                      <!-- Home -->
                      <li class="hs-has-mega-menu nav-item g-mx-10--lg g-mx-15--xl" data-animation-in="fadeIn" data-animation-out="fadeOut" data-max-width="60%" data-position="left">
                  <a id="mega-menu-home" class="nav-link g-py-7 g-px-0" href="<?php echo base_url(); ?>paginas/proyectos" aria-haspopup="true" aria-expanded="false">Proyectos
        </a>
                </li>
                <!-- End Home -->
                                 <!-- Home -->
                                 <li class="hs-has-mega-menu nav-item g-mx-10--lg g-mx-15--xl" data-animation-in="fadeIn" data-animation-out="fadeOut" data-max-width="60%" data-position="left">
                  <a id="mega-menu-home" class="nav-link g-py-7 g-px-0" href="<?php echo base_url(); ?>paginas/servicios" aria-haspopup="true" aria-expanded="false">Servicios
        </a>
                </li>
                <!-- End Home -->

                <!-- Shortcodes -->
                <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                  <a href="<?php echo base_url(); ?>paginas/contactos" class="nav-link g-py-7 g-px-0">Contactos</a>
                </li>
                <!-- End Shortcodes -->

               


                  </ul>
                </li>
                <!-- End Demos -->
              </ul>
            </div>
            <!-- End Navigation -->

          </div>
        </nav>
      </div>
    </header>
    <!-- End Header -->
    <!-- Revolution Slider -->
    <div class="g-overflow-hidden">
      <div id="rev_slider_26_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="mask-showcase" data-source="gallery" style="background:#aaaaaa;padding:0px;">
        <!-- START REVOLUTION SLIDER 5.4.1 fullscreen mode -->
        <div id="rev_slider_26_1" class="rev_slider fullscreenbanner tiny_bullet_slider" style="display:none;" data-version="5.4.1">
          <ul>





            <!-- SLIDE  -->
            <li data-index="rs-73" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="http://works.themepunch.com/revolution_5_3/wp-content/"
            data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
              <!-- MAIN IMAGE -->
              <img src="<?php echo base_url(); ?>assets/unify/assets/img/bg/transparent2.png" data-bgcolor='linear-gradient(90deg, rgba(118, 201, 210, 0.5) 0%, rgba(0, 190, 214, 0.6) 100%)' style='background:linear-gradient(90deg, rgba(0, 0, 153, 0.5) 0%, rgba(0, 190, 214, 0.6) 100%)' alt="" data-bgposition="center center"
              data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
              <!-- LAYERS -->

              <!-- LAYER NR. 1 -->
              <a href="<?php echo base_url(); ?>lab" target="_blank" class="tp-caption rev-btn  tp-resizeme" id="slide-73-layer-4" data-x="['left','left','left','left']" data-hoffset="['170','120','70','40']" data-y="['middle','middle','middle','middle']" data-voffset="['150','150','150','110']" data-width="none"
              data-height="none" data-whitespace="normal" data-type="button" data-responsive_offset="on" data-frames='[{"delay":500,"speed":1000,"sfxcolor":"#1aaec1","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(0,0,0);bg:rgb(255,255,255);"}]'
              data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[30,30,30,30]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[30,30,30,30]" style="z-index: 5; white-space: normal; font-size: 15px; line-height: 50px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: 2px;border-color:rgb(255,255,255);border-style:solid;border-width:1px 1px 1px 1px;border-radius: 50px; outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Ingresar
              </a>

                   <!-- LAYER NR. 2 -->
              <div class="tp-caption   tp-resizeme rs-parallaxlevel-2" id="slide-73-layer-1" data-x="['center','center','center','center']" data-hoffset="['300','300','200','120']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="none"
              data-height="none" data-whitespace="nowrap" data-type="image" data-responsive_offset="on" data-frames='[{"delay":400,"speed":750,"sfxcolor":"#ffffff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
              data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6;">
                <img src="<?php echo base_url(); ?>assets/img/molecular/o.png" alt="" data-ww="['850px','850px','850px','250px']" data-hh="['700px','700px','700','150']" width="1200" height="675" data-no-retina>
              </div>
                  <!-- LAYER NR. 3 -->
           <!--           <div class="tp-caption   tp-resizeme rs-parallaxlevel-10" id="slide-3238-layer-6" data-x="['left','left','left','left']" data-hoffset="['400','400','450','450']" data-y="['top','top','top','top']" data-voffset="['100','100','130','130']"
              data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-basealign="slide" data-responsive_offset="on" data-frames='[{"from":"x:left;rZ:45deg;","speed":2500,"to":"o:1;","delay":650,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
              data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 7;border-width:0px;">
                <img src="<?php //echo base_url(); ?>assets/img/molecular/n.png" alt="" data-ww="['500px','500px','500px','500px']" data-hh="['550px','550px','550px','550px']" width="200" height="150">
              </div>  -->


              <!-- LAYER NR. 3 -->
          <!--    <div class="tp-caption   tp-resizeme" id="slide-73-layer-3" data-x="['left','left','left','left']" data-hoffset="['150','100','50','20']" data-y="['middle','middle','middle','middle']" data-voffset="['-177','-177','-177','-157']" data-width="none" data-height="none"
              data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":300,"speed":750,"sfxcolor":"#ffff58","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
              data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]" data-paddingright="[20,20,20,20]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[20,20,20,20]" style="z-index: 7; white-space: normal; font-size: 20px; line-height: 20px; font-weight: 400; color: #ffffff; letter-spacing: 10px;">PROYECTO
              </div>  -->

              <!-- LAYER NR. 4 -->
              <div class="tp-caption   tp-resizeme" id="slide-73-layer-2" data-x="['left','left','left','left']" data-hoffset="['150','100','50','20']" data-y="['middle','middle','middle','middle']" data-voffset="['-30','-30','-30','-30']" data-fontsize="['70','70','70','50']"
              data-lineheight="['70','70','70','50']" data-width="['650','650','620','380']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":200,"speed":750,"sfxcolor":"#1aaec1","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
              data-textAlign="['left','left','left','left']" data-paddingtop="[20,20,20,20]" data-paddingright="[20,20,20,20]" data-paddingbottom="[30,30,30,30]" data-paddingleft="[20,20,20,20]" style="z-index: 8; min-width: 650px; max-width: 650px; white-space: normal; font-size: 70px; font-weight: 600; line-height: 70px; color: #ffffff; letter-spacing: -2px;">Módulo de análisis de laboratorio.
              </div>
            </li>















            <!-- SLIDE  -->
            <li data-index="rs-74" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="http://works.themepunch.com/revolution_5_3/wp-content/"
            data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
              <!-- MAIN IMAGE -->
              <img src="<?php echo base_url(); ?>assets/unify/assets/img/bg/transparent2.png" data-bgcolor='linear-gradient(120deg, rgb(113, 189, 140) 0%, rgba(189, 243, 223, 0.7) 100%)' style='background:linear-gradient(120deg, #b7ebf6 0%, rgba(228, 97, 210, 0.7) 100%)' alt="" data-bgposition="center center"
              data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
              <!-- LAYERS -->

              <!-- LAYER NR. 5 -->
              <div class="tp-caption   tp-resizeme rs-parallaxlevel-2" id="slide-74-layer-1" data-x="['center','center','center','center']" data-hoffset="['-150','-150','-150','-20']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
              data-width="none" data-height="none" data-whitespace="nowrap" data-type="image" data-responsive_offset="on" data-frames='[{"delay":200,"speed":750,"sfxcolor":"#ffffff","sfx_effect":"blockfromright","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoright","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
              data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 5;">
                <img src="<?php echo base_url(); ?>assets/img/molecular/w.png" alt="" data-ww="['950px','950px','950px','550px']" data-hh="['700px','700px','700px','5  50px']" width="1200" height="675" data-no-retina>
              </div>

              <!-- LAYER NR. 6 -->
              <a href="<?php echo base_url(); ?>login" target="_blank" class="tp-caption rev-btn  tp-resizeme" id="slide-74-layer-4" data-x="['center','center','center','center']" data-hoffset="['550','550','550','350']" data-y="['middle','middle','middle','middle']" data-voffset="['150','150','150','110']" data-width="none"
              data-height="none" data-whitespace="normal" data-type="button" data-responsive_offset="on" data-frames='[{"delay":500,"speed":750,"sfxcolor":"#17673c","sfx_effect":"blockfromright","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoright","frame":"999","to":"z:0;","ease":"Power4.easeOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(0,0,0);bg:rgb(255,255,255);"}]'
              data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[30,30,30,30]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[30,30,30,30]" style="z-index: 6; white-space: normal; font-size: 15px; line-height: 50px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: 2px;border-color:rgb(255,255,255);border-style:solid;border-width:1px 1px 1px 1px;border-radius: 50px; outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Ingresar
              </a>

              <!-- LAYER NR. 7 -->
          <!--    <div class="tp-caption   tp-resizeme" id="slide-74-layer-3" data-x="['left','left','left','left']" data-hoffset="['820','700','540','270']" data-y="['middle','middle','middle','middle']" data-voffset="['-177','-177','-177','-157']" data-width="none" data-height="none"
              data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":400,"speed":750,"sfxcolor":"#cbbacc","sfx_effect":"blockfromright","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoright","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
              data-textAlign="['left','left','left','left']" data-paddingtop="[10,10,10,10]" data-paddingright="[20,20,20,20]" data-paddingbottom="[10,10,10,10]" data-paddingleft="[20,20,20,20]" style="z-index: 7; white-space: normal; font-size: 20px; line-height: 20px; font-weight: 400; color: #ffffff; letter-spacing: 10px;">PROJECTS
              </div>  -->

              <!-- LAYER NR. 8 -->
              <div class="tp-caption   tp-resizeme" id="slide-74-layer-2" data-x="['left','left','left','left']" data-hoffset="['600','440','310','180']" data-y="['middle','middle','middle','middle']" data-voffset="['-30','-30','-30','-30']" data-fontsize="['70','70','70','50']"
              data-lineheight="['70','70','70','50']" data-width="['700','700','670','430']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":300,"speed":750,"sfxcolor":"#17673c","sfx_effect":"blockfromright","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoright","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
              data-textAlign="['right','right','right','right']" data-paddingtop="[20,20,20,20]" data-paddingright="[20,20,20,20]" data-paddingbottom="[30,30,30,30]" data-paddingleft="[20,20,20,20]" style="z-index: 8; min-width: 650px; max-width: 650px; white-space: normal; font-size: 70px; font-weight: 600; line-height: 70px; color: #ffffff; letter-spacing: -2px;">Módulo de consulta de literatura científica.
              </div>
            </li>

















                <!-- SLIDE  -->
                <li data-index="rs-75" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300" data-thumb="http://works.themepunch.com/revolution_5_3/wp-content/"
            data-rotate="0" data-saveperformance="off" data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
              <!-- MAIN IMAGE -->
              <img src="<?php echo base_url(); ?>assets/unify/assets/img/bg/transparent2.png" data-bgcolor='linear-gradient(90deg, rgba(159, 215, 255, 0.6) 0%, rgba(109, 225, 251, 0.6) 100%)' style='background:linear-gradient(90deg, rgba(0, 0, 153, 0.5) 0%, rgba(0, 190, 214, 0.6) 100%)' alt="" data-bgposition="center center"
              data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
              <!-- LAYERS -->

              <!-- LAYER NR. 1 -->
              <a href="<?php echo site_url('graficas/periodos'); ?>" target="_blank" class="tp-caption rev-btn  tp-resizeme" id="slide-73-layer-4" data-x="['left','left','left','left']" data-hoffset="['70','20','70','40']" data-y="['middle','middle','middle','middle']" data-voffset="['250','250','250','210']" data-width="none"
              data-height="none" data-whitespace="normal" data-type="button" data-responsive_offset="on" data-frames='[{"delay":500,"speed":1000,"sfxcolor":"#5ba9bb","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"},{"frame":"hover","speed":"0","ease":"Linear.easeNone","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgb(0,0,0);bg:rgb(255,255,255);"}]'
              data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[30,30,30,30]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[30,30,30,30]" style="z-index: 5; white-space: normal; font-size: 15px; line-height: 50px; font-weight: 400; color: rgba(255,255,255,1); letter-spacing: 2px;border-color:rgb(255,255,255);border-style:solid;border-width:1px 1px 1px 1px;border-radius: 50px; outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Ingresar
              </a>

              <!-- LAYER NR. 2 -->
              <div class="tp-caption   tp-resizeme rs-parallaxlevel-2" id="slide-73-layer-1" data-x="['center','center','center','center']" data-hoffset="['350','350','350','120']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="none"
              data-height="none" data-whitespace="nowrap" data-type="image" data-responsive_offset="on" data-frames='[{"delay":400,"speed":750,"sfxcolor":"#ffffff","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
              data-textAlign="['inherit','inherit','inherit','inherit']" data-paddingtop="[0,0,0,0]" data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="z-index: 6;">
                <img src="<?php echo base_url(); ?>assets/img/molecular/y.png" alt="" data-ww="['850px','850px','850px','550px']" data-hh="['563px','563px','550','281']" width="1200" height="675" data-no-retina>
              </div>

              <!-- LAYER NR. 4 -->
              <div class="tp-caption   tp-resizeme" id="slide-73-layer-2" data-x="['left','left','left','left']" data-hoffset="['50','50','50','20']" data-y="['middle','middle','middle','middle']" data-voffset="['-30','-30','-30','-30']" data-fontsize="['70','70','70','50']"
              data-lineheight="['70','70','70','50']" data-width="['800','800','800','480']" data-height="none" data-whitespace="normal" data-type="text" data-responsive_offset="on" data-frames='[{"delay":200,"speed":750,"sfxcolor":"#5ba9bb","sfx_effect":"blockfromleft","frame":"0","from":"z:0;","to":"o:1;","ease":"Power3.easeInOut"},{"delay":"wait","speed":500,"sfxcolor":"#ffffff","sfx_effect":"blocktoleft","frame":"999","to":"z:0;","ease":"Power4.easeOut"}]'
              data-textAlign="['left','left','left','left']" data-paddingtop="[20,20,20,20]" data-paddingright="[20,20,20,20]" data-paddingbottom="[30,30,30,30]" data-paddingleft="[20,20,20,20]" style="z-index: 8; min-width: 800px; max-width: 800px; white-space: normal; font-size: 70px; font-weight: 600; line-height: 70px; color: #ffffff; letter-spacing: -2px;">Módulo de indicadores de salud pública del departamento de Norte de Santander.
              </div>
            </li>
            <!-- SLIDE  -->
          </ul>
          <div class="tp-bannertimer" style="height: 10px; background: rgba(0, 0, 0, 0.15);"></div>
        </div>
      </div>
    </div>
    <!-- End Revolution Slider -->

    <!-- Section 1 -->
    <section class="g-py-100">
      <div class="container">
        <div class="row">
          <div class="col-md-6 align-self-center g-mb-50--md g-mb-0--md">
          <div class="mb-2">
              <div class="d-inline-block g-width-30 g-height-2 g-pos-rel g-top-minus-4 g-bg-primary mr-2"></div>
              <span class="g-color-gray-dark-v3 g-font-weight-600 g-font-size-12 text-uppercase">Proyecto</span>
            </div>
            <div class="u-heading-v2-3--bottom g-brd-primary g-mb-30">
          
              <h2 class="h1 g-color-black g-font-weight-600 g-mb-35"><span class="g-color-primary">Módulo de</span> Consulta de Literatura <span class="g-color-primary">Científica</span></h2>
            </div>

            <div class="g-font-size-18 g-line-height-2 g-mb-30">
              <p>A través de un registro con un usuario y contraseña, los usuarios externos que quieran mayor información sobre tratamiento farmacológico, publicaciones de último nivel de evidencia o más recientes sobre patologías infecciosas pueden realizar consultas a través de esta plataforma.</p>
            </div>

            <a href="<?php echo base_url(); ?>login" class="btn u-btn-primary u-shadow-v21 g-font-size-12 text-uppercase g-font-weight-600 g-rounded-50 g-py-15 g-px-25 g-mr-15 g-mb-10 g-mb-0--sm">
              <i class="icon-layers g-pos-rel g-top-1 g-mr-5"></i> Ingresar
            </a>
          </div>

          <div class="col-md-6 align-self-center text-center g-overflow-hidden">
            <img class="img-fluid w-100 mb-4" src="<?php echo base_url(); ?>assets/img/molecular/pc.png" alt="Image Description" data-animation="fadeInUp" data-animation-delay="0" data-animation-duration="1000">
          </div>
        </div>
      </div>
    </section>
    <!-- End Section 1 -->

    <!-- Section 2 -->
    <section class="g-bg-purple g-color-white g-py-100">
      <div class="container">
        <div class="row">
          <div class="col-md-6 align-self-center text-center g-overflow-hidden g-mb-50--md g-mb-0--md">
          <img class="img-fluid w-100 mb-4" src="<?php echo base_url(); ?>assets/img/molecular/pc2.png" alt="Image Description" data-animation="fadeInUp" data-animation-delay="0" data-animation-duration="1000">
          </div>

          <div class="col-md-6 align-self-center">
          <div class="mb-2">
              <div class="d-inline-block g-width-30 g-height-2 g-pos-rel g-top-minus-4 g-bg-white mr-2"></div>
              <span class="g-color-white g-font-weight-600 g-font-size-12 text-uppercase">Proyecto</span>
            </div>
            <div class="u-heading-v2-3--bottom g-brd-white g-mb-30">
              <h2 class="h1 u-heading-v2__title text-uppercase g-font-weight-700">Módulo de análisis de laboratorio.</h2>
            </div>


            <div class="g-font-size-18 g-line-height-2 g-mb-30">
              <p>A través de esta plataforma nuestros contratistas tendrán acceso con un usuario y una clave a los resultados de los análisis que se realicen en laboratorio de biología molecular.</p>
            </div>

            <a href="<?php echo base_url(); ?>lab" class="btn u-shadow-v21 g-bg-white-opacity-0_2 g-color-white g-brd-white--hover g-color-purple--hover g-bg-white--hover g-font-size-12 text-uppercase g-font-weight-600 g-rounded-50 g-py-15 g-px-25 g-mr-15 g-mb-10 g-mb-0--sm">
              <i class="icon-wallet g-pos-rel g-top-1 g-mr-5"></i> Ingresar
            </a>
          </div>
        </div>
      </div>
    </section>
    <!-- End Section 2 -->

    <!-- Section 4 -->
    <section class="g-py-100">
      <div class="container">
        <div class="row">
          <div class="col-md-6 align-self-center g-mb-50--md g-mb-0--md">
          <div class="mb-2">
              <div class="d-inline-block g-width-30 g-height-2 g-pos-rel g-top-minus-4 g-bg-primary mr-2"></div>
              <span class="g-color-gray-dark-v3 g-font-weight-600 g-font-size-12 text-uppercase">Proyecto</span>
            </div>
            <div class="u-heading-v2-3--bottom g-brd-primary g-mb-30">
            <h2 class="h1 g-color-black g-font-weight-600 g-mb-35"><span class="g-color-primary">Módulo de</span> indicadores de salud pública del departamento de <span class="g-color-primary">Norte de Santander</span>.</h2>
            </div>

            <div class="g-font-size-18 g-line-height-2 g-mb-30">
              <p>En este módulo, las entidades territoriales de salud del departamento de Norte de Santander podrán tener acceso a información de indicadores de salud pública de enfermedades infecciosas priorizadas y su distribución por el territorio.</p>
            </div>

            <a href="<?php echo base_url(); ?>indicadores" class="btn u-btn-primary u-shadow-v21 g-font-size-12 text-uppercase g-font-weight-600 g-rounded-50 g-py-15 g-px-25 g-mr-15 g-mb-10 g-mb-0--sm">
              <i class="icon-speedometer g-pos-rel g-top-1 g-mr-5"></i> Ingresar
            </a>
          </div>

          <div class="col-md-6 align-self-center text-center g-overflow-hidden">
            <img class="img-fluid w-100 mb-4" src="<?php echo base_url(); ?>assets/img/molecular/pc3.png" alt="Image Description" data-animation="fadeInUp" data-animation-delay="0" data-animation-duration="1000">    
          </div>
        </div>
      </div>
    </section>
    <!-- End Section 4 -->

    <!-- Section 5 -->
    <section class="g-bg-pink g-color-white g-py-100">
      <div class="container">
        <div class="row">
          <div class="col-md-6 align-self-center text-center g-overflow-hidden g-mb-50 g-mb-0--md">
            <img class="img-fluid w-75" src="<?php echo base_url(); ?>assets/img/molecular/cel.png" alt="Image Description" data-animation="fadeInUp" data-animation-delay="0" data-animation-duration="1000">
          </div>

          <div class="col-md-6 align-self-center">
          <div class="mb-2">
              <div class="d-inline-block g-width-30 g-height-2 g-pos-rel g-top-minus-4 g-bg-white mr-2"></div>
              <span class="g-color-white g-font-weight-600 g-font-size-12 text-uppercase">App Móvil</span>
            </div>
            <div class="u-heading-v2-3--bottom g-brd-white g-mb-30">
              <h2 class="h1 u-heading-v2__title text-uppercase g-font-weight-700">App Móvil Cibim</h2>
            </div>


            <div class="g-font-size-18 g-line-height-2 g-mb-30">
              <p>Aplicación móvil CIBIM, aplicación que permite a los usuarios realizar consultas de medicamentos donde unos profesionales capacitados les responderán sobre la consulta que realiza cada usuario. </div>

            <a href="https://play.google.com/store/apps/details?id=com.cibim_ufps&hl=es_419&gl=US" class="btn u-shadow-v21 g-bg-white-opacity-0_2 g-color-white g-brd-white--hover g-color-pink--hover g-bg-white--hover g-font-size-12 text-uppercase g-font-weight-600 g-rounded-50 g-py-15 g-px-25 g-mr-15 g-mb-10 g-mb-0--sm">
                <i class="fa fa-android"></i> Descargar
            </a>
          </div>
        </div>
      </div>
    </section>
    <!-- End Section 5 -->
    <!-- Footer -->
    <div id="contacts-section" class="g-bg-black-opacity-0_9 g-color-white-opacity-0_8 g-py-60">
      <div class="container">
        <div class="row">
          <!-- Footer Content -->
          <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
            <a href="index.html" data-uw-styling-context="true"><img id="logo-footer" class="img-responsive" src="https://ww2.ufps.edu.co/public/imagenes/template/footer/logoufpsvertical.png" alt="Logo Pie de Página UFPS" data-uw-styling-context="true"></a>
            <a href="https://www.colombia.co" target="_blank" data-uw-styling-context="true"><img id="logo-colombia" class="img-responsive" src="https://ww2.ufps.edu.co/public/imagenes/seccion/cf7c7322fcc9e80a498c8e0cafc9e5c5.png" alt="Marca País Colombia" data-uw-styling-context="true"></a>
            <a href="https://www.gov.co" target="_blank" data-uw-styling-context="true"><img width="120" height="25" id="logo-gobierno" class="img-responsive" src="https://ww2.ufps.edu.co/public/imagenes/seccion/0c7af37828c031da66297184f075cab3.png" alt="Gobierno de Colombia" data-uw-styling-context="true"></a>
          </div>
          <!-- End Footer Content -->

          <!-- Footer Content -->
          <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
            <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
              <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Portales Institucionales</h2>
            </div>

            <ul class="list-unstyled latest-list" data-uw-styling-context="true">
              <li data-uw-styling-context="true">
                  <a href="https://divisist2.ufps.edu.co/" data-uw-styling-context="true">Divisist</a>
              </li>
              <li data-uw-styling-context="true"><a href="https://acceso.ufps.edu.co" data-uw-styling-context="true">Pagos de Egresados y Externos</a></li>

              <li data-uw-styling-context="true">
                  <a href="http://dptosist.ufps.edu.co/piagev1/servlet/piagev" data-uw-styling-context="true">Piagev</a>
              </li>
              <li data-uw-styling-context="true">
                  <a href="/universidad/atencion_ciudadano" data-uw-styling-context="true">PDQRS</a>
              </li>
              <li data-uw-styling-context="true">
                  <a href="http://ugad.ufps.edu.co:8084/datarsoft001/home.ufps" data-uw-styling-context="true">DatarSoft</a>
              </li>
              <li data-uw-styling-context="true">
                  <a href="http://nomina.ufps.edu.co:9191/nominaufps" data-uw-styling-context="true">Sistema de Nómina</a>
              </li>
              <li data-uw-styling-context="true">
                  <a href="http://www.ufps.edu.co/ufps/cread/Presentacion.php" data-uw-styling-context="true">DISERACA</a>
              </li>
          </ul>
          </div>
          <!-- End Footer Content -->

          <!-- Footer Content -->
          <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
            <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
              <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Enlaces de Interés</h2>
            </div>

            <nav class="text-uppercase1">
            <ul class="list-unstyled latest-list" data-uw-styling-context="true">
                        <li data-uw-styling-context="true"><a href="https://ww2.ufps.edu.co/universidad/planeacion/655" style="text-transform: none;" data-uw-styling-context="true">Plan Anticorrupción</a></li>
                        <li data-uw-styling-context="true"><a href="https://ww2.ufps.edu.co/universidad/seleccion" style="text-transform: none;" data-uw-styling-context="true">Proceso de selección</a></li>
                        <li data-uw-styling-context="true"><a href="https://ww2.ufps.edu.co/universidad/contratacion/1122" style="text-transform: none;" data-uw-styling-context="true">Contratación</a></li>
                        <li data-uw-styling-context="true"><a href="https://ww2.ufps.edu.co/informacion/proceso_democratico_2022" style="text-transform: none;" data-uw-styling-context="true">Proceso democrático</a></li>
                        <li data-uw-styling-context="true"><a href="https://ww2.ufps.edu.co/vicerrectoria/vicerrectoria-administrativa/527" style="text-transform: none;" data-uw-styling-context="true">Derechos pecuniarios </a></li>
                        <li data-uw-styling-context="true"><a href="https://mail.google.com/a/ufps.edu.co/" style="text-transform: none;" target="_blank" data-uw-styling-context="true">Correo Electrónico Institucional</a></li>
                        <li data-uw-styling-context="true"><a href="https://ww2.ufps.edu.co/universidad/consultorio-juridico/1156" style="text-transform: none;" data-uw-styling-context="true">Consultorio Jurídico </a></li>
                    </ul>
            </nav>
          </div>
          <!-- End Footer Content -->

          <!-- Footer Content -->
          <div class="col-lg-3 col-md-6">
            <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
              <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Contactos Institucional</h2>
            </div>

            <address class="md-margin-bottom-40" data-uw-styling-context="true">
                        Avenida Gran Colombia No. 12E-96 Barrio Colsag, <br data-uw-styling-context="true">
                        San José de Cúcuta - Colombia <br data-uw-styling-context="true">
                        Teléfono (057)(7) 5776655 <br data-uw-styling-context="true"> <br data-uw-styling-context="true">


                        Solicitudes y correspondencia<br data-uw-styling-context="true">
                        Unidad de Gestión Documental<br data-uw-styling-context="true">
                        <a href="mailto:ugad@ufps.edu.co" class="" data-uw-styling-context="true">ugad@ufps.edu.co </a><br data-uw-styling-context="true"><br data-uw-styling-context="true">

                        Uso único y exclusivo para notificaciones judiciales:<br data-uw-styling-context="true">
                        <a href="mailto:notificacionesjudiciales@ufps.edu.co" class="" data-uw-styling-context="true">notificacionesjudiciales@ufps.edu.co</a><br data-uw-styling-context="true"><br data-uw-styling-context="true">

                    </address>
          </div>
          <!-- End Footer Content -->
        </div>
      </div>
    </div>
    <!-- End Footer -->

    <!-- Copyright Footer -->
    <footer class="g-bg-gray-dark-v1 g-color-white-opacity-0_8 g-py-20">
      <div class="container">
        <div class="row">
          <div class="col-md-8 text-center text-md-left g-mb-10 g-mb-0--md">
            <div class="d-lg-flex">
                        2022 © All Rights Reserved. Desarrollado por:&nbsp;<a href="mailto:viarneyaliriovm@ufps.edu.co" data-uw-styling-context="true">VAVM - Developer</a>
            </div>
          </div>

         
        </div>
      </div>
    </footer>
    <!-- End Copyright Footer -->

    <a class="js-go-to u-go-to-v1" href="#!" data-type="fixed" data-position='{
     "bottom": 15,
     "right": 15
   }' data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn">
      <i class="hs-icon hs-icon-arrow-top"></i>
    </a>
  </main>

  <div class="u-outer-spaces-helper"></div>


  <!-- JS Global Compulsory -->
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/bootstrap/bootstrap.min.js"></script>


  <!-- JS Implementing Plugins -->
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/appear.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>

  <!-- JS Revolution Slider -->
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>

  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution-addons/slicey/js/revolution.addon.slicey.min.js"></script>

  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>

  <!-- JS Unify -->
  <script src="<?php echo base_url(); ?>assets/unify/assets/js/hs.core.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/js/components/hs.header.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/js/helpers/hs.hamburgers.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/js/components/hs.tabs.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/js/components/hs.onscroll-animation.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/js/components/hs.go-to.js"></script>

  <!-- JS Customization -->
  <script src="<?php echo base_url(); ?>assets/unify/assets/js/custom.js"></script>

  <!-- JS Plugins Init. -->
  <script>
    // initialization of slider revolution
      var tpj = jQuery,
        revapi26;

      tpj(document).ready(function () {
        if (tpj("#rev_slider_26_1").revolution == undefined) {
          revslider_showDoubleJqueryError("#rev_slider_26_1");
        } else {
          revapi26 = tpj("#rev_slider_26_1").show().revolution({
            sliderType: "standard",
            jsFileLocation: "revolution/js/",
            sliderLayout: "fullscreen",
            dottedOverlay: "none",
            delay: 9000,
            navigation: {
              keyboardNavigation: "off",
              keyboard_direction: "horizontal",
              mouseScrollNavigation: "off",
              mouseScrollReverse: "default",
              onHoverStop: "off",
              touch: {
                touchenabled: "on",
                touchOnDesktop: "off",
                swipe_threshold: 75,
                swipe_min_touches: 1,
                swipe_direction: "horizontal",
                drag_block_vertical: false
              },
              arrows: {
                style: "uranus",
                enable: true,
                hide_onmobile: true,
                hide_under: 778,
                hide_onleave: false,
                tmp: '',
                left: {
                  h_align: "left",
                  v_align: "center",
                  h_offset: 15,
                  v_offset: 0
                },
                right: {
                  h_align: "right",
                  v_align: "center",
                  h_offset: 15,
                  v_offset: 0
                }
              },
              bullets: {
                enable: true,
                hide_onmobile: false,
                style: "bullet-bar",
                hide_onleave: false,
                direction: "horizontal",
                h_align: "center",
                v_align: "bottom",
                h_offset: 0,
                v_offset: 30,
                space: 5,
                tmp: ''
              }
            },
            responsiveLevels: [1240, 1024, 778, 480],
            visibilityLevels: [1240, 1024, 778, 480],
            gridwidth: [1240, 1024, 778, 480],
            gridheight: [868, 768, 960, 720],
            lazyType: "none",
            parallax: {
              type: "scroll",
              origo: "slidercenter",
              speed: 2000,
              levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 51, 55]
            },
            shadow: 0,
            spinner: "off",
            stopLoop: "off",
            stopAfterLoops: -1,
            stopAtSlide: -1,
            shuffle: "off",
            autoHeight: "off",
            fullScreenAutoWidth: "off",
            fullScreenAlignForce: "off",
            fullScreenOffsetContainer: "",
            fullScreenOffset: "60px",
            hideThumbsOnMobile: "off",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
              simplifyAll: "off",
              nextSlideOnWindowFocus: "off",
              disableFocusListener: false
            }
          });
        }
      });

      $(document).on('ready', function () {
        // initialization of tabs
        $.HSCore.components.HSTabs.init('[role="tablist"]');

        // initialization of scroll animation
        $.HSCore.components.HSOnScrollAnimation.init('[data-animation]');

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');
      });

      $(window).on('load', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
          event: 'hover',
          pageContainer: $('.container'),
          breakpoint: 991
        });
      });

      $(window).on('resize', function () {
        setTimeout(function () {
          $.HSCore.components.HSTabs.init('[role="tablist"]');
        }, 200);
      });
  </script>

 <?php echo $_js ?>





</body>

</html>