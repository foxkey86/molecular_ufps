<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Title -->
  <title>Centro de Investigación de Biotecnología y Medio Ambiente - CIBIM</title>

  <!-- Required Meta Tags Always Come First -->
  <meta charset="utf-8">

  <meta name="application-name" content="Website Saber Pro UFPS" lang="es">
        <meta name="Author" content="Viarney Alirio Villamizar Moreno" lang="es">
        <meta name="description" content="Dashboard">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
        <!-- Call App Mode on ios devices -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no">
        <!-- base css -->

  <!-- Favicon -->
  <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/img/favicon/safari-pinned-tab.svg">
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
  <!-- CSS Global Compulsory -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/bootstrap/bootstrap.min.css">
  <!-- CSS Global Icons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/icon-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/icon-line/css/simple-line-icons.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/icon-etlinefont/style.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/icon-line-pro/style.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/icon-hs/style.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/animate.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/hs-megamenu/src/hs.megamenu.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/hamburgers/hamburgers.min.css">

  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/css/settings.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/css/layers.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/css/navigation.css">

  <!-- CSS Unify -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/css/unify-core.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/css/unify-components.css">
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/css/unify-globals.css">

  <!-- CSS Customization -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/css/custom.css">

  <?php echo $_css ?>

</head>

<body>
  <main>
  <!-- Header -->
  <header id="js-header" class="u-header u-header--static">
      <div class="u-header__section u-header__section--light g-bg-white g-transition-0_3 g-py-10">
        <nav class="js-mega-menu navbar navbar-expand-lg hs-menu-initialized hs-menu-horizontal">
          <div class="container">
            <!-- Responsive Toggle Button -->
            <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-3 g-right-0" type="button" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar" data-toggle="collapse" data-target="#navBar">
              <span class="hamburger hamburger--slider">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
              </span>
              </span>
            </button>
            <!-- End Responsive Toggle Button -->

            <!-- Logo -->
            <a href="/" class="navbar-brand">
              <img src="https://ww2.ufps.edu.co/public/imagenes/template/header/logo_ufps.png" alt="Unify Logo">
            </a>
            <!-- End Logo -->

            <!-- Navigation -->
            <div class="collapse navbar-collapse align-items-center flex-sm-row g-pt-10 g-pt-5--lg g-mr-40--lg" id="navBar">
              <ul class="navbar-nav text-uppercase g-pos-rel g-font-weight-600 ml-auto">
                <!-- Intro -->
                <li class="nav-item  g-mx-10--lg g-mx-15--xl active">
                  <a href="/" class="nav-link g-py-7 g-px-0">Inicio</a>
                </li>
                <!-- End Intro -->

                <!-- Home -->
                <li class="hs-has-mega-menu nav-item g-mx-10--lg g-mx-15--xl" data-animation-in="fadeIn" data-animation-out="fadeOut" data-max-width="60%" data-position="left">
                  <a id="mega-menu-home" class="nav-link g-py-7 g-px-0" href="<?php echo base_url(); ?>paginas" aria-haspopup="true" aria-expanded="false">Nosotros
        </a>
                </li>
                <!-- End Home -->
                                <!-- Home -->
                                <li class="hs-has-mega-menu nav-item g-mx-10--lg g-mx-15--xl" data-animation-in="fadeIn" data-animation-out="fadeOut" data-max-width="60%" data-position="left">
                  <a id="mega-menu-home" class="nav-link g-py-7 g-px-0" href="<?php echo base_url(); ?>paginas/normatividad" aria-haspopup="true" aria-expanded="false">Normatividad
        </a>
                </li>
                <!-- End Home -->
                      <!-- Home -->
                      <li class="hs-has-mega-menu nav-item g-mx-10--lg g-mx-15--xl" data-animation-in="fadeIn" data-animation-out="fadeOut" data-max-width="60%" data-position="left">
                  <a id="mega-menu-home" class="nav-link g-py-7 g-px-0" href="<?php echo base_url(); ?>paginas/proyectos" aria-haspopup="true" aria-expanded="false">Proyectos
        </a>
                </li>
                <!-- End Home -->
                                 <!-- Home -->
                                 <li class="hs-has-mega-menu nav-item g-mx-10--lg g-mx-15--xl" data-animation-in="fadeIn" data-animation-out="fadeOut" data-max-width="60%" data-position="left">
                  <a id="mega-menu-home" class="nav-link g-py-7 g-px-0" href="<?php echo base_url(); ?>paginas/servicios" aria-haspopup="true" aria-expanded="false">Servicios
        </a>
                </li>
                <!-- End Home -->

                <!-- Shortcodes -->
                <li class="nav-item  g-mx-10--lg g-mx-15--xl">
                  <a href="<?php echo base_url(); ?>paginas/contactos" class="nav-link g-py-7 g-px-0">Contactos</a>
                </li>
                <!-- End Shortcodes -->

               


                  </ul>
                </li>
                <!-- End Demos -->
              </ul>
            </div>
            <!-- End Navigation -->

          </div>
        </nav>
      </div>
    </header>
    <!-- End Header -->

    <?php foreach ($_content as $_view): ?>
      <?php include $_view; ?>
    <?php endforeach; ?>  

   <!-- Footer -->
   <div id="contacts-section" class="g-bg-black-opacity-0_9 g-color-white-opacity-0_8 g-py-60">
      <div class="container">
        <div class="row">
          <!-- Footer Content -->
          <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
            <a href="index.html" data-uw-styling-context="true"><img id="logo-footer" class="img-responsive" src="https://ww2.ufps.edu.co/public/imagenes/template/footer/logoufpsvertical.png" alt="Logo Pie de Página UFPS" data-uw-styling-context="true"></a>
            <a href="https://www.colombia.co" target="_blank" data-uw-styling-context="true"><img id="logo-colombia" class="img-responsive" src="https://ww2.ufps.edu.co/public/imagenes/seccion/cf7c7322fcc9e80a498c8e0cafc9e5c5.png" alt="Marca País Colombia" data-uw-styling-context="true"></a>
            <a href="https://www.gov.co" target="_blank" data-uw-styling-context="true"><img width="120" height="25" id="logo-gobierno" class="img-responsive" src="https://ww2.ufps.edu.co/public/imagenes/seccion/0c7af37828c031da66297184f075cab3.png" alt="Gobierno de Colombia" data-uw-styling-context="true"></a>
          </div>
          <!-- End Footer Content -->

          <!-- Footer Content -->
          <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
            <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
              <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Portales Institucionales</h2>
            </div>

            <ul class="list-unstyled latest-list" data-uw-styling-context="true">
              <li data-uw-styling-context="true">
                  <a href="https://divisist2.ufps.edu.co/" data-uw-styling-context="true">Divisist</a>
              </li>
              <li data-uw-styling-context="true"><a href="https://acceso.ufps.edu.co" data-uw-styling-context="true">Pagos de Egresados y Externos</a></li>

              <li data-uw-styling-context="true">
                  <a href="http://dptosist.ufps.edu.co/piagev1/servlet/piagev" data-uw-styling-context="true">Piagev</a>
              </li>
              <li data-uw-styling-context="true">
                  <a href="/universidad/atencion_ciudadano" data-uw-styling-context="true">PDQRS</a>
              </li>
              <li data-uw-styling-context="true">
                  <a href="http://ugad.ufps.edu.co:8084/datarsoft001/home.ufps" data-uw-styling-context="true">DatarSoft</a>
              </li>
              <li data-uw-styling-context="true">
                  <a href="http://nomina.ufps.edu.co:9191/nominaufps" data-uw-styling-context="true">Sistema de Nómina</a>
              </li>
              <li data-uw-styling-context="true">
                  <a href="http://www.ufps.edu.co/ufps/cread/Presentacion.php" data-uw-styling-context="true">DISERACA</a>
              </li>
          </ul>
          </div>
          <!-- End Footer Content -->

          <!-- Footer Content -->
          <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
            <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
              <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Enlaces de Interés</h2>
            </div>

            <nav class="text-uppercase1">
            <ul class="list-unstyled latest-list" data-uw-styling-context="true">
                        <li data-uw-styling-context="true"><a href="https://ww2.ufps.edu.co/universidad/planeacion/655" style="text-transform: none;" data-uw-styling-context="true">Plan Anticorrupción</a></li>
                        <li data-uw-styling-context="true"><a href="https://ww2.ufps.edu.co/universidad/seleccion" style="text-transform: none;" data-uw-styling-context="true">Proceso de selección</a></li>
                        <li data-uw-styling-context="true"><a href="https://ww2.ufps.edu.co/universidad/contratacion/1122" style="text-transform: none;" data-uw-styling-context="true">Contratación</a></li>
                        <li data-uw-styling-context="true"><a href="https://ww2.ufps.edu.co/informacion/proceso_democratico_2022" style="text-transform: none;" data-uw-styling-context="true">Proceso democrático</a></li>
                        <li data-uw-styling-context="true"><a href="https://ww2.ufps.edu.co/vicerrectoria/vicerrectoria-administrativa/527" style="text-transform: none;" data-uw-styling-context="true">Derechos pecuniarios </a></li>
                        <li data-uw-styling-context="true"><a href="https://mail.google.com/a/ufps.edu.co/" style="text-transform: none;" target="_blank" data-uw-styling-context="true">Correo Electrónico Institucional</a></li>
                        <li data-uw-styling-context="true"><a href="https://ww2.ufps.edu.co/universidad/consultorio-juridico/1156" style="text-transform: none;" data-uw-styling-context="true">Consultorio Jurídico </a></li>
                    </ul>
            </nav>
          </div>
          <!-- End Footer Content -->

          <!-- Footer Content -->
          <div class="col-lg-3 col-md-6">
            <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
              <h2 class="u-heading-v2__title h6 text-uppercase mb-0">Contactos Institucional</h2>
            </div>

            <address class="md-margin-bottom-40" data-uw-styling-context="true">
                        Avenida Gran Colombia No. 12E-96 Barrio Colsag, <br data-uw-styling-context="true">
                        San José de Cúcuta - Colombia <br data-uw-styling-context="true">
                        Teléfono (057)(7) 5776655 <br data-uw-styling-context="true"> <br data-uw-styling-context="true">


                        Solicitudes y correspondencia<br data-uw-styling-context="true">
                        Unidad de Gestión Documental<br data-uw-styling-context="true">
                        <a href="mailto:ugad@ufps.edu.co" class="" data-uw-styling-context="true">ugad@ufps.edu.co </a><br data-uw-styling-context="true"><br data-uw-styling-context="true">

                        Uso único y exclusivo para notificaciones judiciales:<br data-uw-styling-context="true">
                        <a href="mailto:notificacionesjudiciales@ufps.edu.co" class="" data-uw-styling-context="true">notificacionesjudiciales@ufps.edu.co</a><br data-uw-styling-context="true"><br data-uw-styling-context="true">

                    </address>
          </div>
          <!-- End Footer Content -->
        </div>
      </div>
    </div>
    <!-- End Footer -->

    <!-- Copyright Footer -->
    <footer class="g-bg-gray-dark-v1 g-color-white-opacity-0_8 g-py-20">
      <div class="container">
        <div class="row">
          <div class="col-md-8 text-center text-md-left g-mb-10 g-mb-0--md">
            <div class="d-lg-flex">
                        2022 © All Rights Reserved. Desarrollado por:&nbsp;<a href="mailto:viarneyaliriovm@ufps.edu.co" data-uw-styling-context="true">VAVM - Developer</a>
            </div>
          </div>

         
        </div>
      </div>
    </footer>
    <!-- End Copyright Footer -->

    <a class="js-go-to u-go-to-v1" href="#!" data-type="fixed" data-position='{
     "bottom": 15,
     "right": 15
   }' data-offset-top="400" data-compensation="#js-header" data-show-effect="zoomIn">
      <i class="hs-icon hs-icon-arrow-top"></i>
    </a>
  </main>

  <div class="u-outer-spaces-helper"></div>


  <!-- JS Global Compulsory -->
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/jquery/jquery.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/popper.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/bootstrap/bootstrap.min.js"></script>


  <!-- JS Implementing Plugins -->
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/appear.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/hs-megamenu/src/hs.megamenu.js"></script>

  <!-- JS Revolution Slider -->
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>

  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution-addons/slicey/js/revolution.addon.slicey.min.js"></script>

  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/vendor/revolution-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>

  <!-- JS Unify -->
  <script src="<?php echo base_url(); ?>assets/unify/assets/js/hs.core.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/js/components/hs.header.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/js/helpers/hs.hamburgers.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/js/components/hs.tabs.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/js/components/hs.onscroll-animation.js"></script>
  <script src="<?php echo base_url(); ?>assets/unify/assets/js/components/hs.go-to.js"></script>

  <!-- JS Customization -->
  <script src="<?php echo base_url(); ?>assets/unify/assets/js/custom.js"></script>

  <!-- JS Plugins Init. -->
  <script>
    // initialization of slider revolution
      var tpj = jQuery,
        revapi26;

      tpj(document).ready(function () {
        if (tpj("#rev_slider_26_1").revolution == undefined) {
          revslider_showDoubleJqueryError("#rev_slider_26_1");
        } else {
          revapi26 = tpj("#rev_slider_26_1").show().revolution({
            sliderType: "standard",
            jsFileLocation: "revolution/js/",
            sliderLayout: "fullscreen",
            dottedOverlay: "none",
            delay: 9000,
            navigation: {
              keyboardNavigation: "off",
              keyboard_direction: "horizontal",
              mouseScrollNavigation: "off",
              mouseScrollReverse: "default",
              onHoverStop: "off",
              touch: {
                touchenabled: "on",
                touchOnDesktop: "off",
                swipe_threshold: 75,
                swipe_min_touches: 1,
                swipe_direction: "horizontal",
                drag_block_vertical: false
              },
              arrows: {
                style: "uranus",
                enable: true,
                hide_onmobile: true,
                hide_under: 778,
                hide_onleave: false,
                tmp: '',
                left: {
                  h_align: "left",
                  v_align: "center",
                  h_offset: 15,
                  v_offset: 0
                },
                right: {
                  h_align: "right",
                  v_align: "center",
                  h_offset: 15,
                  v_offset: 0
                }
              },
              bullets: {
                enable: true,
                hide_onmobile: false,
                style: "bullet-bar",
                hide_onleave: false,
                direction: "horizontal",
                h_align: "center",
                v_align: "bottom",
                h_offset: 0,
                v_offset: 30,
                space: 5,
                tmp: ''
              }
            },
            responsiveLevels: [1240, 1024, 778, 480],
            visibilityLevels: [1240, 1024, 778, 480],
            gridwidth: [1240, 1024, 778, 480],
            gridheight: [868, 768, 960, 720],
            lazyType: "none",
            parallax: {
              type: "scroll",
              origo: "slidercenter",
              speed: 2000,
              levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 46, 47, 48, 49, 50, 51, 55]
            },
            shadow: 0,
            spinner: "off",
            stopLoop: "off",
            stopAfterLoops: -1,
            stopAtSlide: -1,
            shuffle: "off",
            autoHeight: "off",
            fullScreenAutoWidth: "off",
            fullScreenAlignForce: "off",
            fullScreenOffsetContainer: "",
            fullScreenOffset: "60px",
            hideThumbsOnMobile: "off",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
              simplifyAll: "off",
              nextSlideOnWindowFocus: "off",
              disableFocusListener: false
            }
          });
        }
      });

      $(document).on('ready', function () {
        // initialization of tabs
        $.HSCore.components.HSTabs.init('[role="tablist"]');

        // initialization of scroll animation
        $.HSCore.components.HSOnScrollAnimation.init('[data-animation]');

        // initialization of go to
        $.HSCore.components.HSGoTo.init('.js-go-to');
      });

      $(window).on('load', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of HSMegaMenu component
        $('.js-mega-menu').HSMegaMenu({
          event: 'hover',
          pageContainer: $('.container'),
          breakpoint: 991
        });
      });

      $(window).on('resize', function () {
        setTimeout(function () {
          $.HSCore.components.HSTabs.init('[role="tablist"]');
        }, 200);
      });
  </script>

 <?php echo $_js ?>





</body>

</html>