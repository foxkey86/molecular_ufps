<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <title>
           Centro de Investigación de Biotecnología y Medio Ambiente - CIBIM
        </title>

        <meta name="application-name" content="Molecular UFPS" lang="es">
        <meta name="Author" content="Viarney Alirio Villamizar Moreno" lang="es">
        <meta name="description" content="Login">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
        <!-- Call App Mode on ios devices -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no">
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="<?php echo base_url(); ?>assets/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?php echo base_url(); ?>assets/css/app.bundle.css">
        <!-- Place favicon.ico in the root directory -->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/img/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/img/favicon/favicon-32x32.png">
        <link rel="mask-icon" href="<?php echo base_url(); ?>assets/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <!-- Optional: page related CSS-->
        <link rel="stylesheet" media="screen, print" href="<?php echo base_url(); ?>assets/css/fa-brands.css">
        <link rel="stylesheet" media="screen, print" href="<?php echo base_url(); ?>assets/css/fa-solid.css">

        <?php echo $_css; ?>
    </head>
    <body>
	

        <div class="page-wrapper">
            <div class="page-inner bg-brand-gradient">
                <div class="page-content-wrapper bg-transparent m-0">

                    <div class="flex-1" style="background: url(<?php echo base_url(); ?>assets/img/svg/pattern-1.svg) no-repeat center bottom fixed; background-size: cover; background-color: white;">
                        <div class="container py-4 px-4 px-sm-0">
                       
                               <!-- Alertas de la clase Template -->
                             
 
                        <div class="row">
                            <?php foreach ($_content as $_view): ?>
                                <?php include $_view; ?>
                                <?php endforeach; ?>
                        </div>
                        <div class="position-absolute pos-bottom pos-left pos-right p-3 text-center text-white height-10 w-100 shadow-lg px-4 bg-brand-gradient">
                            2021 © Universidad Francisco de Paula Santander by&nbsp;<a href='https://ww2.ufps.edu.co' class='text-white opacity-40 fw-500' title='ww2.ufps.edu.co' target='_blank'>ww2.ufps.edu.co</a>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- base vendor bundle: 
			 DOC: if you remove pace.js from core please note on Internet Explorer some CSS animations may execute before a page is fully loaded, resulting 'jump' animations 
						+ pace.js (recommended)
						+ jquery.js (core)
						+ jquery-ui-cust.js (core)
						+ popper.js (core)
						+ bootstrap.js (core)
						+ slimscroll.js (extension)
						+ app.navigation.js (core)
						+ ba-throttle-debounce.js (core)
						+ waves.js (extension)
						+ smartpanels.js (extension)
						+ src/../jquery-snippets.js (core) -->
        <script src="<?php echo base_url(); ?>assets/js/vendors.bundle.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/app.bundle.js"></script>
        <script>
            $("#js-login-btn").click(function(event)
            {

                // Fetch form to apply custom Bootstrap validation
                var form = $("#js-login")

                if (form[0].checkValidity() === false)
                {
                    event.preventDefault()
                    event.stopPropagation()
                }

                form.addClass('was-validated');
                // Perform ajax submit here...
            });

        </script>
           <?php echo $_js; ?>
    </body>
</html>
