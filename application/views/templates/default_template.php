<!DOCTYPE html>
<html lang="en">
<head>
        <meta charset="utf-8">
        <title>
        Centro de Investigación de Biotecnología y Medio Ambiente - CIBIM
        </title>

        <meta name="application-name" content="Molecular UFPS" lang="es">
        <meta name="Author" content="Viarney Alirio Villamizar Moreno" lang="es">
        <meta name="description" content="Login">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no, minimal-ui">
        <!-- Call App Mode on ios devices -->
        <meta name="apple-mobile-web-app-capable" content="yes" />
        <!-- Remove Tap Highlight on Windows Phone IE -->
        <meta name="msapplication-tap-highlight" content="no">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/unify/assets/vendor/icon-awesome/css/font-awesome.min.css">
        <!-- base css -->
        <link rel="stylesheet" media="screen, print" href="<?php echo base_url(); ?>assets/css/vendors.bundle.css">
        <link rel="stylesheet" media="screen, print" href="<?php echo base_url(); ?>assets/css/app.bundle.css">
        <!-- Place favicon.ico in the root directory -->
        <link rel="apple-touch-icon" sizes="180x180" href="<?php echo base_url(); ?>assets/img/favicon/apple-touch-icon.png">
        <link rel="icon" type="image/png" sizes="32x32" href="<?php echo base_url(); ?>assets/img/favicon/favicon-32x32.png">
        <link rel="mask-icon" href="<?php echo base_url(); ?>assets/img/favicon/safari-pinned-tab.svg" color="#5bbad5">
        <!-- Optional: page related CSS-->
        <link rel="stylesheet" media="screen, print" href="<?php echo base_url(); ?>assets/css/fa-brands.css">
        <script src="https://www.google.com/recaptcha/api.js?render=<?php echo SITE_KEY; ?>"></script>
        <?php echo $_css; ?>
    </head>
    <body>
	

        <div class="page-wrapper">
            <div class="page-inner bg-brand-gradient">
                <div class="page-content-wrapper bg-transparent m-0">
                    <div class="height-10 w-100 shadow-lg px-4 bg-brand-gradient">
                        <div class="d-flex align-items-center container p-0">
                            <div class="page-logo width-mobile-auto m-0 align-items-center justify-content-center p-0 bg-transparent bg-img-none shadow-0 height-9">
                                <a href="javascript:void(0)" class="page-logo-link press-scale-down d-flex align-items-center">
                                    <img src="<?php echo base_url(); ?>assets/img/logo.png" alt="SmartAdmin WebApp" aria-roledescription="logo">
                                    <span class="page-logo-text mr-1">CIBIM</span>
                                </a>
                            </div>
                            <?php if ($login) : ?>
                                <a href="<?php echo site_url('login/registro'); ?>" class="btn-link text-white ml-auto btn btn-primary">
                                <i class="fa fa-user"></i> Crear Cuenta
                                </a>
                            <?php else: ?>
                                <span class="text-white opacity-50 ml-auto mr-2 hidden-sm-down">
                                Ya eres usuario?
                                </span>
                                <a href="<?php echo site_url('login'); ?>" class="btn-link text-white ml-auto ml-sm-0 btn btn-primary">
                                <i class="fa fa-user"></i> Iniciar Sesión
                                </a>
                            <?php endif; ?>
                          
                        </div>
                    </div>
                    <div class="flex-1" style="background: url(<?php echo base_url(); ?>assets/img/svg/pattern-1.svg) no-repeat center bottom fixed; background-size: cover; background-color: white;">
                        <div class="container py-4 py-lg-5 my-lg-5 px-4 px-sm-0">
                       
                               <!-- Alertas de la clase Template -->
                             
            <?php foreach ($_warning as $_msj): ?>
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                                    </button>
                                                    <div class="d-flex align-items-center">
                                                        <div class="alert-icon width-3">
                                                            <span class="icon-stack icon-stack-sm">
                                                            <i class="fal fa-shield-check text-warning"></i>
                                                            </span>
                                                        </div>
                                                        <div class="flex-1">
                                                            <span class="h5 m-0 fw-700">Atención!</span>
                                                            <?= $_msj ?>
                                                        </div>
                                                    </div>
                                                </div>
            <?php endforeach; ?>
            <?php foreach ($_success as $_msj): ?>
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                                    </button>
                                                    <div class="d-flex align-items-center">
                                                        <div class="alert-icon width-3">
                                                            <span class="icon-stack icon-stack-sm">
                                                            <i class="fal fa-check "></i>
                                                            </span>
                                                        </div>
                                                        <div class="flex-1">
                                                            <span class="h5 m-0 fw-700">Exitoso!</span>
                                                            <?= $_msj ?>
                                                        </div>
                                                    </div>
                                                </div>
               
            <?php endforeach; ?>
            <?php foreach ($_error as $_msj): ?>
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                                    </button>
                                                    <div class="d-flex align-items-center">
                                                        <div class="alert-icon width-3">
                                                            <span class="icon-stack icon-stack-sm">
                                                            <i class="fal fa-times"></i>
                                                            </span>
                                                        </div>
                                                        <div class="flex-1">
                                                            <span class="h5 m-0 fw-700">Error!</span>
                                                            <?= $_msj ?>
                                                        </div>
                                                    </div>
                                                </div>
            <?php endforeach; ?>
            <?php foreach ($_info as $_msj): ?>
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                                    </button>
                                                    <div class="d-flex align-items-center">
                                                        <div class="alert-icon width-3">
                                                            <span class="icon-stack icon-stack-sm">
                                                            <i class="fal fa-info-circle"></i>
                                                            </span>
                                                        </div>
                                                        <div class="flex-1">
                                                            <span class="h5 m-0 fw-700">Información!</span>
                                                            <?= $_msj ?>
                                                        </div>
                                                    </div>
                                                </div>
            <?php endforeach; ?>
                            
                            <div class="row">
                            <?php foreach ($_content as $_view): ?>
                <?php include $_view; ?>
            <?php endforeach; ?>
                            </div>
                            <div class="position-absolute pos-bottom pos-left pos-right p-3 text-center text-white">
                                2021 © Universidad Francisco de Paula Santander by&nbsp;<a href='https://ww2.ufps.edu.co' class='text-white opacity-40 fw-500' title='ww2.ufps.edu.co' target='_blank'>ww2.ufps.edu.co</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- base vendor bundle: 
			 DOC: if you remove pace.js from core please note on Internet Explorer some CSS animations may execute before a page is fully loaded, resulting 'jump' animations 
						+ pace.js (recommended)
						+ jquery.js (core)
						+ jquery-ui-cust.js (core)
						+ popper.js (core)
						+ bootstrap.js (core)
						+ slimscroll.js (extension)
						+ app.navigation.js (core)
						+ ba-throttle-debounce.js (core)
						+ waves.js (extension)
						+ smartpanels.js (extension)
						+ src/../jquery-snippets.js (core) -->
        <script src="<?php echo base_url(); ?>assets/js/vendors.bundle.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/app.bundle.js"></script>
        <script>
            $("#js-login-btn").click(function(event)
            {

                // Fetch form to apply custom Bootstrap validation
                var form = $("#js-login")

                if (form[0].checkValidity() === false)
                {
                    event.preventDefault()
                    event.stopPropagation()
                }

                form.addClass('was-validated');
                // Perform ajax submit here...
            });

        </script>
            <script type="text/javascript">

                grecaptcha.ready(function() {
                    grecaptcha.execute('<?php echo SITE_KEY; ?>', {action: 'homepage'})
                    .then(function(token) {
                        $('#google-response-token').val(token);
                    });
                });


            </script>
           <?php echo $_js; ?>
    </body>
</html>
