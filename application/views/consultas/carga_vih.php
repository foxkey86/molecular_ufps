<div class="alert alert-warning alert-dismissible fade show">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                                    </button>
                                                    <div class="d-flex align-items-center">
                                                        <div class="flex-1">
                                                            <span class="h3">Cargue Masivo de la Información de la Dimensión VIH.</span>
                                                            <br>
                                                            <span class="h4">Se debe tener en cuenta las siguientes recomendaciones al momento de la carga del archivo.</span>
                                                            <br><br>
                                                            <strong>- El archivo a cargar debe estar en formato de excel con la extensión .csv (delimitado por comas).</strong><br><br>
                                                            <strong>- El archivo debe contener las columnas siguientes columnas y en orden como se mencionan:</strong>
                                                            <ul>
                                                            <li>COLUMNA A: cod eve (corresponde al código del evento 850 VIH).</li>
                                                            <li>COLUMNA B: fec_not (corresponde a la fecha de notificación).</li>
                                                            <li>COLUMNA C: semana (corresponde a la semana de notificación del calendario epidemiológico el primer trimestres va desde la semana 1 hasta la semana 12, segundo trimestre de la semana 13 hasta la semana 24, tercer trimestre de la semana 25 a la semana 36 y cuarto trimestre de la semana 37 a la semana 52).</li>
                                                            <li>COLUMNA D: edad.</li>
                                                            <li>COLUMNA E: nacionalidad (corresponde a la nacionalidad del paciente).</li>
                                                            <li>COLUMNA F: nombre _ nacionalidad.</li>
                                                            <li>COLUMNA G: sexo.</li>
                                                            <li>COLUMNA H: cod_muni (corresponde al código del municipio).</li>
                                                            <li>COLUMNA I: localidad (corresponde a la localidad del municipio).</li>
                                                            <li>COLUMNA J: cen_poblado (corresponde si pertenece al centro poblado del municipio).</li>
                                                            <li>COLUMNA K: vereda_ (corresponde a la vereda).</li>
                                                            <li>COLUMNA L: bar_ver_ (corresponde al barrio).</li>
                                                            <li>COLUMNA M: tip_ss_ (corresponde al tipo de seguridad social, P. Excepción, E. Especial, C. Contributivo, S. Subsidiado, I. Indeterminado/ pendiente, N. No Asegurado).</li>
                                                            <li>COLUMNA N: cod-ase- (corresponde al código de la entidad de seguridad social).</li>
                                                            <li>COLMNUNA O: fec_con_ (corresponde a la fecha de contagio).</li>
                                                            <li>COLUMNA P: ini_sin_ (corresponde a la fecha de inicio de sintomas).</li>
                                                            <li>COLUMNA Q: pac_hos_ (corresponde si el paciente fué hospitalizado 1: si  2: no).</li>
                                                            <li>COLUMNA R: fec_hos_ (corresponde a la fecha de hospitalizado).</li>
                                                            <li>COLUMNA S: ide_genero (corresponde a la identidad de genero).</li>
                                                            <li>COLUMNA T: est_cli (corresponde al estado clinico 1: VIH 2: sida, 3: Muerte).</li>
                                                            <li>COLUMNA U: nom_eve (corresponde al nombre del evento).</li>
                                                            <li>COLUMNA V: anyo (corresponde al año de la notificación del evento).</li>
                                                            <li>COLUMNA W: tipo_reporte (corresponde al tipo de reporte a cargar 1: Por departamento 2: Por municipio).</li>
                                                            </ul>
                                                            <strong>- Si existe algún error en la carga del archivo se le indicara que fila tuvo el error y debe corregir y volver a subir la fila en un archivo con la misma extensión por aparte.</strong><br>
                                                        </div>
                                                    </div>
</div>

                         <div class="col-xl-12">
                                <div class="card mb-g border shadow-0">
                                    <div class="card-header bg-white">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col">
                                                <span class="h6 font-weight-bold text-uppercase">Carga masiva de Resultados - <b>Evento VIH</b></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body p-0">
                                        <div class="row no-gutters row-grid"><br>
                                            <?php echo form_open_multipart('consultas/carga_vih', ['class' => '', 'id' => 'form', 'role' => 'form'], []); ?>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <input type="file" name="cargar_resultados" required accept=".csv, .CSV" class="form-control">
                                                </div>
                                                </div>
                                             <div class="row">
                                                <div style="margin-left:30px; margin-top:30px;">
                                                    <button type="submit" name="cargar" value="1" class="btn btn-primary"><i class="fa fa-save"></i> Cargar</button>
                                                </div>
                                            </div>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </div>
                                </div>
                          </div>
                      