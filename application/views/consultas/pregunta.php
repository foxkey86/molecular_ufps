<div class="row">
    <div class="col-xl-12">
        <div class="card mb-g border shadow-0">
            <div class="card-header bg-white p-0">
                <div class="p-3 d-flex flex-row">
                    <div class="d-block flex-shrink-0">
                        <img src="<?php echo base_url(); ?>assets/img/demo/avatars/<?php echo $pregunta->avatar; ?>" class="img-fluid img-thumbnail" alt="">
                    </div>
                    <div class="d-block ml-2">
                        <a href="javascript:void(0);" class="fs-sm text-info h6 fw-500 mb-0 d-block"><?php echo $pregunta->nombres; ?><br>
                        <?php echo $pregunta->correo; ?></a>
                        <?php if (isset($profesional)): ?>
                            Profesional: <?php echo $profesional->profesion; ?>
                        <?php else: ?>
                            Invitado: <?php echo $particular->nombre; ?>
                            <?php endif; ?>

                        
                    </div>
                    <a href="javascript:void(0);" class="d-inline-flex align-items-center text-dark ml-auto align-self-start">
                        <i class="fal fa-heart ml-1 text-muted"></i>
                    </a>
                </div>
            </div>
            <div class="card-body ">
                <blockquote class="font-italic fw-sm bg-faded border border-top-0 border-right-0 border-bottom-0 p-3">
                    <b>Temas asociados:</b><br>
                    <?php foreach($temas_pregunta as $row): ?>
                        <?php echo $row->nombre; ?>
                        <?php if($row->otro_tema): ?>
                            <?php echo $row->otro_tema; ?> 
                        <?php endif; ?>
                        <br>
                    <?php endforeach; ?>
                </blockquote>
                <blockquote class="font-italic fw-sm bg-faded border border-top-0 border-right-0 border-bottom-0 p-3">
                    <b>Sustancia específicas:</b><br><?php echo $sustancia_pregunta->nombre; ?>
                </blockquote>
                <blockquote class="font-italic fw-sm bg-faded border border-top-0 border-right-0 border-bottom-0 p-3">
                    <b>Consultada orientada a:</b><br><?php echo $orientada_pregunta->nombre; ?>
                </blockquote>
                <?php if($pregunta->otro_orientada): ?>
                    <blockquote class="font-italic fw-sm bg-faded border border-top-0 border-right-0 border-bottom-0 p-3">
                        <b>Otra orientada a:</b><br><?php echo $pregunta->otro_orientada; ?>
                    </blockquote>
                <?php endif; ?>
                <blockquote class="font-italic fw-sm bg-faded border border-top-0 border-right-0 border-bottom-0 p-3">
                        <b>Nombre(s) del/los producto(s):</b><br><?php echo $pregunta->nombre_producto; ?>
                </blockquote>
                <blockquote class="font-italic fw-sm bg-faded border border-top-0 border-right-0 border-bottom-0 p-3">
                    <b>Pregunta:</b><br><?php echo $pregunta->descripcion; ?>
                </blockquote>
            </div>
            <div class="card-footer">
                <div class="d-flex align-items-center">
                    <span class="text-sm text-muted font-italic"><i class="fal fa-clock mr-1"></i> <?php echo $pregunta->created_at; ?></span>
                </div>
            </div>
        </div>
        <!-- post -end -->
    </div>
</div>
<?php if (isset($respuesta)): ?>
    <div class="row">
        <div class="col-xl-12">
            <div class="card mb-g border shadow-0">
                <div class="card-header bg-white p-0">
                    <div class="p-3 d-flex flex-row">
                        <b>Respuesta:</b><br>
                        <?php echo $respuesta->descrip_r; ?>
                    </div>
                </div>   
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="row">
<div class="col-xl-12">
    <div style="margin-bottom:20px;">
        <a href="<?php echo base_url("consultas"); ?>" class="btn btn-primary waves-effect waves-themed" type="button"><i class="fal fa-chevron-circle-left"></i> Atrás</a>
    </div> 
</div>
</div>