
                        <div class="row">
                            <div class="col-xl-12">
                 
                        
                                        <!-- post -->
                                        <div class="card mb-g border shadow-0">
                                    <div class="card-header bg-white p-0">
                                        <div class="p-3 d-flex flex-row">
                                            <div class="d-block flex-shrink-0">
                                                <img src="<?php echo base_url(); ?>assets/img/demo/avatars/avatar-b.png" class="img-fluid img-thumbnail" alt="">
                                            </div>
                                            <div class="d-block ml-2">
                                                <span class="h6 font-weight-bold text-uppercase d-block m-0">RE: <?php echo $respuesta->nombre_producto; ?></span>
                                                <a href="javascript:void(0);" class="fs-sm text-info h6 fw-500 mb-0 d-block"><?php echo $respuesta->nombres; ?></a>
                                                <div class="d-flex mt-1 text-warning align-items-center">
                                                    <i class="fas fa-star mr-1"></i>
                                                    <i class="fas fa-star mr-1"></i>
                                                    <i class="fas fa-star mr-1"></i>
                                                    <i class="fal fa-star mr-1"></i>
                                                    <i class="fal fa-star mr-1"></i>
                                                    <span class="text-muted fs-xs font-italic">
                                                        (90 votes)
                                                    </span>
                                                </div>
                                            </div>
                                            <a href="javascript:void(0);" class="d-inline-flex align-items-center text-dark ml-auto align-self-start">
                                                <i class="fal fa-heart ml-1 text-muted"></i>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="card-body ">
                                        <blockquote class="font-italic fw-sm bg-faded border border-top-0 border-right-0 border-bottom-0 p-3">
                                        <?php echo $respuesta->descripcion; ?>
                                        </blockquote>
                                        <p>
                                            <?php echo $respuesta->descrip_r; ?>
                                        </p>
                                    </div>
                                    <div class="card-footer">
                                        <div class="d-flex align-items-center">
                                            <span class="text-sm text-muted font-italic"><i class="fal fa-clock mr-1"></i> <?php echo $respuesta->created_at; ?></span>
                                            <a href="<?php echo base_url("consultas"); ?>" class="flex-shrink-0 ml-auto">Volver al listado <i class="fal fa-reply ml-2"></i> </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- post -end -->
                            
                              
                              
                            </div>
                        </div>