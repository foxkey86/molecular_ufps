<div class="row">
    <div class="col-xl-12">
        <div class="card mb-g border shadow-0">
            <div class="card-header bg-white p-0">
                <div class="p-3 d-flex flex-row">
                    <div class="d-block flex-shrink-0">
                        <img src="<?php echo base_url(); ?>assets/img/demo/avatars/<?php echo $pregunta->avatar; ?>" class="img-fluid img-thumbnail" alt="">
                    </div>
                    <div class="d-block ml-2">
                        <a href="javascript:void(0);" class="fs-sm text-info h6 fw-500 mb-0 d-block"><?php echo $pregunta->nombres; ?><br>
                        <?php echo $pregunta->correo; ?></a>
                        <?php if (isset($profesional)): ?>
                            Profesional: <?php echo $profesional->profesion; ?>
                        <?php else: ?>
                            <?php if (isset($particular)): ?>
                                Invitado: <?php echo $particular->nombre; ?>
                            <?php endif; ?>
                        <?php endif; ?>

                        
                    </div>
                    <a href="javascript:void(0);" class="d-inline-flex align-items-center text-dark ml-auto align-self-start">
                        <i class="fal fa-heart ml-1 text-muted"></i>
                    </a>
                </div>
            </div>
            <div class="card-body ">
                <blockquote class="font-italic fw-sm bg-faded border border-top-0 border-right-0 border-bottom-0 p-3">
                    <b>Temas asociados:</b><br>
                    <?php foreach($temas_pregunta as $row): ?>
                        <?php echo $row->nombre; ?>
                        <?php if($row->otro_tema): ?>
                            <?php echo $row->otro_tema; ?> 
                        <?php endif; ?>
                        <br>
                    <?php endforeach; ?>
                </blockquote>
                <blockquote class="font-italic fw-sm bg-faded border border-top-0 border-right-0 border-bottom-0 p-3">
                    <b>Sustancia específicas:</b><br><?php echo $sustancia_pregunta->nombre; ?>
                </blockquote>
                <blockquote class="font-italic fw-sm bg-faded border border-top-0 border-right-0 border-bottom-0 p-3">
                    <b>Consultada orientada a:</b><br><?php echo $orientada_pregunta->nombre; ?>
                </blockquote>
                <?php if($pregunta->otro_orientada): ?>
                    <blockquote class="font-italic fw-sm bg-faded border border-top-0 border-right-0 border-bottom-0 p-3">
                        <b>Otra orientada a:</b><br><?php echo $pregunta->otro_orientada; ?>
                    </blockquote>
                <?php endif; ?>
                <blockquote class="font-italic fw-sm bg-faded border border-top-0 border-right-0 border-bottom-0 p-3">
                        <b>Nombre(s) del/los producto(s):</b><br><?php echo $pregunta->nombre_producto; ?>
                </blockquote>
                <blockquote class="font-italic fw-sm bg-faded border border-top-0 border-right-0 border-bottom-0 p-3">
                    <b>Pregunta:</b><br><?php echo $pregunta->descripcion; ?>
                </blockquote>
            </div>
            <div class="card-footer">
                <div class="d-flex align-items-center">
                    <span class="text-sm text-muted font-italic"><i class="fal fa-clock mr-1"></i> <?php echo $pregunta->created_at; ?></span>
                </div>
            </div>
        </div>
        <!-- post -end -->
    </div>
</div>
<?php if (!isset($respuesta)): ?>
    <div class="row">
        <div class="col-xl-12">
            <div class="card border mb-g">
                <div class="card-body pl-4 pt-4 pr-4 pb-0">
                    <div class="d-flex flex-column">
                            <?php echo form_open('consultas/enviar_respuesta', ['class' => '', 'id' => 'form', 'role' => 'form'], ['responder' => 1, 'id_pregunta' => $pregunta->id]); ?>
                            <div class="border-0 flex-1 position-relative shadow-top">
                                <div class="pt-2 pb-1 pr-0 pl-0 rounded-0 position-relative" tabindex="-1">
                                    <span class="profile-image rounded-circle d-block position-absolute" style="background-image:url('<?php echo base_url(); ?>assets/img/demo/avatars/<?php echo $informacion_usuario->avatar; ?>'); background-size: cover;"></span>
                                    <div class="pl-5 ml-5">     
                                        <div class="form-group">
                                            <textarea class="form-control" name="descrip_r" id="descrip_r" maxlength="4000" rows="4" placeholder="A continuación escriba su pregunta. Máximo 4000 caracteres."><?php echo $this->input->post('descrip_r'); ?></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="height-8 d-flex flex-row align-items-center flex-wrap flex-shrink-0">
                                <button class="btn btn-info shadow-0 ml-auto" type="submit" ><i class="fas fa-save"></i> Enviar Consulta</button>
                            </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>   
        </div>
    </div>
<?php endif; ?>
<?php if (isset($respuesta)): ?>
    <div class="row">
        <div class="col-xl-12">
            <div class="card mb-g border shadow-0">
                <div class="card-header bg-white p-0">
                    <div class="p-3 d-flex flex-row">
                        <b>Respuesta:</b><br>
                        <?php echo $respuesta->descrip_r; ?>
                    </div>
                </div>   
            </div>
        </div>
    </div>
<?php endif; ?>

<div class="row">
<div class="col-xl-12">
    <div style="margin-bottom:20px;">
        <a href="<?php echo base_url("consultas/admin"); ?>" class="btn btn-primary waves-effect waves-themed" type="button"><i class="fal fa-chevron-circle-left"></i> Atrás</a>
    </div> 
</div>
</div>