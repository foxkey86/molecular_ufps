 <div class="col-xl-12">                       
<div class="card mb-g border shadow-0">
    <div class="card-header bg-white">
        <div class="row no-gutters align-items-center">
            <div class="col">
                <span class="h6 font-weight-bold text-uppercase">Consultas Realizadas</span>
            </div>
        </div>
    </div>

    <div class="col-xl-12">
                                <div id="panel-1" class="panel">
                                  
                                    <div class="panel-container show">
                                        <div class="panel-content">

        <table id="dt-basic-example" class="table table-bordered table-hover table-striped w-100">
                                                <thead>
                                                    <tr>
                                                        <th>Consulta</th>
                                                        <th>F. Pregunta</th>
                                                        <th>Estado</th>
                                                        <th>Respuesta</th>
                                                        <th>F. Respuesta</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach($listado_consultas as $row): ?>
                                                
                                                    <tr>
                                                        <td>                                    
                                    <a href="<?php echo base_url("consultas/contestar/" . $row->id_p); ?>" class="fs-lg fw-500 d-block">
                                        <?php echo $row->nombre_producto; ?>  
                                    </a>
                                    <div class="d-block text-muted fs-sm">
                                        <?php echo $row->descripcion; ?>
                                    </div></td>
                                    <td><div class="d-block text-muted fs-sm">
                                <span class="badge bg-primary-400"><?php echo $row->created_at; ?></span>
                                </div>
                                </td>
                                                        <td>                        <?php if($row->estado == 1): ?> 
                                            <span class="badge badge-success">Contestada</span>
                        <?php else: ?>
                                            <span class="badge badge-warning">Pendiente</span>
                        <?php endif; ?></td>
                   
                                                        <td>
                                                        <?php if($row->descrip_r): ?>
                                                            <div class="d-flex align-items-center">
                                                                <div class="d-inline-block align-middle status status-success status-sm mr-2">
                                                                    <span class="profile-image-md rounded-circle d-block" style="background-image:url('<?php echo base_url(); ?>assets/img/demo/avatars/<?php echo $informacion_usuario->avatar; ?>'); background-size: cover;"></span>
                                                                </div>
                                                                <div class="d-block text-muted fs-sm">
                                                                    <?php echo $row->descrip_r; ?>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                        </td>
                                                        <td>
                                                            <div class="d-block text-muted fs-sm">
                                                            <span class="badge bg-info-400"><?php echo $row->fecha_respuesta; ?></span>
                                                            </div>
                                                        </td>
                    
                                                    </tr>
                                                   
                                                <?php endforeach;?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Consulta</th>
                                                        <th>F. Pregunta</th>
                                                        <th>Estado</th>
                                                        <th>Respuesta</th>
                                                        <th>F. Respuesta</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
    </div>
</div>
                      