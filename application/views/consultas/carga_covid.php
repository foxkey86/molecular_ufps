
<div class="alert alert-warning alert-dismissible fade show">
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true"><i class="fal fa-times"></i></span>
                                                    </button>
                                                    <div class="d-flex align-items-center">
                                                        <div class="flex-1">
                                                            <span class="h3">Cargue Masivo de la Información de la Dimensión Covid.</span>
                                                            <br>
                                                            <span class="h4">Se debe tener en cuenta las siguientes recomendaciones al momento de la carga del archivo.</span>
                                                            <br><br>
                                                            <strong>- El archivo a cargar debe estar en formato de excel con la extensión .csv (delimitado por comas).</strong><br><br>
                                                            <strong>- El archivo debe contener las columnas siguientes columnas y en orden como se mencionan:</strong>
                                                            <ul>
                                                            <li>COLUMNA A: Tipo_prueba (corresponde al tipo de prueba, ANTIGENO o PCR).</li>
                                                            <li>COLUMNA B: fec_not (corresponde a la fecha de notificación).</li>
                                                            <li>COLUMNA C: Departamento_nom (corresponde al nombre de departamento).</li>
                                                            <li>COLUMNA D: Ciudad_municipio_nom (corresponde al nombre del municipio).</li>
                                                            <li>COLUMNA E: edad (corresponde a la edad de la paciente).</li>
                                                            <li>COLUMNA F: sexo (corresponde al sexo de la paciente, F o M).</li>
                                                            <li>COLUMNA G: Fuente_tipo_contagio (corresponde a la fuente de contagio, comunitario, relacionado, importado o en estudio).</li>
                                                            <li>COLUMNA H: Ubicación (corresponde a la ubicación, fallecido, casa o N/a).</li>
                                                            <li>COLUMNA I: Estado (corresponde al estado, leve, fallecido o N/a).</li>
                                                            <li>COLUMNA J: Nacionalidad_nom (corresponde al nombre de la nacionalidad del paciente).</li>
                                                            <li>COLUMNA K: Fecha_ingreso_pais (corresponde a la fecha de ingreso al país).</li>
                                                            <li>COLUMNA L: Fecha_inicio_sintomas (corresponde a la fecha de inicio de síntomas).</li>
                                                            <li>COLUMNA M: Fecha_consulta (corresponde a la fecha de la consulta).</li>
                                                            <li>COLUMNA N: Fecha_diagnostico (corresponde a la fecha de diagnóstico).</li>
                                                            <li>COLUMNA O: Fecha_hospitalizacion (corresponde a la fecha de hospitalización).</li>
                                                            <li>COLUMNA P: Fecha_alta (corresponde a la fecha de alta).</li>
                                                            <li>COLMNUNA Q: Fecha_muerte (corresponde a la fecha de muerte).</li>
                                                            <li>COLUMNA R: Fecha_recuperado (corresponde a la fecha de recuperación).</li>
                                                            <li>COLUMNA S: Recuperado (corresponde a recuperado, recuperado, fallecido o N/A).</li>
                                                            <li>COLUMNA T: Tipo_recuperacion (corresponde al tipo de recuperación, Tiempo o PCR).</li>
                                                            <li>COLUMNA U: Grupo_edad (corresponde al grupo de edad).</li>
                                                            <li>COLUMNA V: Quinquenio (corresponde al grupo etario por quinquenio).</li>
                                                            <li>COLUMNA W: SE_Fecha_inicio_sintomas+Fecha_consulta (corresponde a las semanas epidemológicas)</li>
                                                            <li>COLUMNA X: Barrio (corresponde al nombre del barrio).</li>
                                                            <li>COLUMNA Y: anyo (corresponde al año de la notificación del evento).</li>
                                                            <li>COLUMNA Z: tipo_reporte (corresponde al tipo de reporte a cargar 1: Por departamento 2: Por municipio).</li>
                                                            </ul>
                                                            <strong>- Si existe algún error en la carga del archivo se le indicara que fila tuvo el error y debe corregir y volver a subir la fila en un archivo con la misma extensión por aparte.</strong><br>
                                                        </div>
                                                    </div>
</div>
 

                         <div class="col-xl-12">
                                <div class="card mb-g border shadow-0">
                                    <div class="card-header bg-white">
                                        <div class="row no-gutters align-items-center">
                                            <div class="col">
                                                <span class="h6 font-weight-bold text-uppercase">Carga masiva de Resultados - <b>Evento Covid</b></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body p-0">
                                        <div class="row no-gutters row-grid"><br>
                                            <?php echo form_open_multipart('consultas/carga_cov', ['class' => '', 'id' => 'form', 'role' => 'form'], []); ?>
                                            <div class="row">
                                                <div class="form-group col-md-12">
                                                    <input type="file" name="cargar_resultados" required accept=".csv, .CSV" class="form-control">
                                                </div>
                                                </div>
                                             <div class="row">
                                                <div style="margin-left:30px; margin-top:30px;">
                                                    <button type="submit" name="cargar" value="1" class="btn btn-primary"><i class="fa fa-save"></i> Cargar</button>
                                                </div>
                                            </div>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </div>
                                </div>
                          </div>
                      