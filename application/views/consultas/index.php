                          
                          
                          
                          <div class="col-xl-12">
                            <?php if(!$pendientes): ?>
                                <div class="card border mb-g">
                                    <div class="card-body pl-4 pt-4 pr-4 pb-0">
                                        <div class="d-flex flex-column">
                                        <?php echo form_open('consultas/enviar_pregunta', ['class' => '', 'id' => 'form', 'role' => 'form'], ['pregunta' => 1]); ?>
                                            <div class="border-0 flex-1 position-relative shadow-top">
                                                <div class="pt-2 pb-1 pr-0 pl-0 rounded-0 position-relative" tabindex="-1">
                                                    <span class="profile-image rounded-circle d-block position-absolute" style="background-image:url('<?php echo base_url(); ?>assets/img/demo/avatars/<?php echo $informacion_usuario->avatarlg; ?>'); background-size: cover;"></span>
                                                    <div class="pl-5 ml-5">
                                                        <div class="form-group row">
                                                        <label class="col-xl-12 form-label" for="fdocumento">Consulta orientada a:</label>
                                                            <div class="col-6 pr-1">
                                                            <?php echo form_dropdown('id_orientacion', $listado_orientada, $this->input->post('id_orientacion'), "class='form-control' id='id_orientacion' placeholder='Seleccione Tipo de Orientación' required"); ?>
                                                            <div class="invalid-feedback">Tipo de orientación inválido.</div>
                                                            </div>
                                                            <div class="col-6 pr-1">
                                                            <input type="text" id="otro_orientada" name="otro_orientada" class="form-control" maxlength="100" placeholder="Si su respuesta es otro especifique cuál">
                                                            <div class="invalid-feedback">Otro orientada inválido.</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group row">
                                                        <label class="col-xl-12 form-label" for="fdocumento">Temas asociados:</label>
                                                            <div class="col-6 pr-1">
                                                            <div class="frame-wrap">
                                                <div class="demo">
                                                    <?php foreach($listado_temas as $clave => $row): ?>
                                                    <div class="custom-control custom-checkbox custom-control-inline">
                                                        <input type="checkbox" class="custom-control-input" id="temas_<?php echo $clave; ?>" name="temas_<?php echo $clave; ?>" value="<?php echo $row->id; ?>">
                                                        <label class="custom-control-label" for="temas_<?php echo $clave; ?>"><?php echo $row->nombre; ?></label>
                                                    </div>
                                                    <?php endforeach; ?>
                                                </div>
                                            </div>
                                                            </div>
                                                            <div class="col-6 pr-1">
                                                            <input type="text" id="otro_tema" name="otro_tema" class="form-control" maxlength="100" placeholder="Si su respuesta es otro especifique cuál">
                                                            <div class="invalid-feedback">Otro tema inválido.</div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                        <label class="form-label" for="fsustancias">Sustancias específicas:</label>
                                                            <?php echo form_dropdown('id_sustancia', $listado_sustancia, $this->input->post('id_sustancia'), "class='form-control' id='id_sustancia' placeholder='Seleccione Tipo de Sustancia' required"); ?>
                                                            <div class="invalid-feedback">Tipo de sustancia inválida.</div>
                                                        </div>

                                                        <div class="form-group">
                                                        <label class="form-label" for="fproducto">Productos:</label>
                                                            <input type="text" id="nombre_producto" name="nombre_producto" class="form-control" placeholder="Cite el nombre del (los) producto(s) (genérico, comercial, producto natural, etc.)">
                                                            <div class="invalid-feedback">Nombre de producto inválido.</div>
                                                        </div>
                                                        <div class="form-group">
                                                            <textarea class="form-control" name="descripcion" id="descripcion" maxlength="4000" rows="4" placeholder="A continuación escriba su pregunta. Máximo 4000 caracteres."><?php echo $this->input->post('descripcion'); ?></textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="height-8 d-flex flex-row align-items-center flex-wrap flex-shrink-0">
                                                <button class="btn btn-info shadow-0 ml-auto" type="submit" >Enviar Consulta</button>
                                            </div>
                                            <?php echo form_close(); ?>
                                        </div>
                                    </div>
                                </div>
                               <?php endif;?> 
                                                            
                            
                        <div class="col-xl-12">
                                <div id="panel-1" class="panel">
                                  
                                    <div class="panel-container show">
                                        <div class="panel-content">

        <table id="dt-basic-example" class="table table-bordered table-hover table-striped w-100">
                                                <thead>
                                                    <tr>
                                                        <th>Consulta</th>
                                                        <th>F. Pregunta</th>
                                                        <th>Estado</th>
                                                        <th>Respuesta</th>
                                                        <th>F. Respuesta</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                <?php foreach($listado_consultas as $row): ?>
                                                
                                                    <tr>
                                                        <td>                                    
                                    <a href="<?php echo base_url("consultas/pregunta/" . $row->id_p); ?>" class="fs-lg fw-500 d-block">
                                        <?php echo $row->nombre_producto; ?>  
                                    </a>
                                    <div class="d-block text-muted fs-sm">
                                        <?php echo $row->descripcion; ?>
                                    </div></td>
                                    <td><div class="d-block text-muted fs-sm">
                                <span class="badge bg-primary-400"><?php echo $row->fe; ?></span>
                                </div>
                                </td>
                                                        <td>                        <?php if($row->estado == 1): ?> 
                                            <span class="badge badge-success">Contestada</span>
                        <?php else: ?>
                                            <span class="badge badge-warning">Pendiente</span>
                        <?php endif; ?></td>
                   
                                                        <td>
                                                        <?php if($row->descrip_r): ?>
                                                            <div class="d-flex align-items-center">
                                                                <div class="d-block text-muted fs-sm">
                                                                    <?php echo $row->descrip_r; ?>
                                                                </div>
                                                            </div>
                                                        <?php endif; ?>
                                                        </td>
                                                        <td>
                                                            <div class="d-block text-muted fs-sm">
                                                            <span class="badge bg-info-400"><?php echo $row->fecha_respuesta; ?></span>
                                                            </div>
                                                        </td>
                    
                                                    </tr>
                                                   
                                                <?php endforeach;?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Consulta</th>
                                                        <th>F. Pregunta</th>
                                                        <th>Estado</th>
                                                        <th>Respuesta</th>
                                                        <th>F. Respuesta</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      