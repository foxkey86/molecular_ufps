<!--=== Header v4 ===-->
<div class="header-v4 margin-bottom-20">
    <!-- Topbar -->
    <div class="topbar-v1">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <ul class="list-inline top-v1-contacts">
                        <li style="color: #fff;">
                            <i style="color: #fff;" class="fa fa-envelope"></i> Correo: <a style="color: #fff;"
                                                                                           href="<?php echo URL_CORREO; ?>"><?php echo CORREO; ?></a>
                        </li>
                        <!--                        <li>-->
                        <!--                            <i class="fa fa-phone"></i> Línea Nacional: 01800091111 - 0314261111-->
                        <!--                        </li>-->
                    </ul>
                </div>

                <div class="col-md-6">
                    <ul class="list-inline top-v1-data">
                        <li><a href="<?php echo base_url(); ?>"><i class="fa fa-home"></i></a></li>
                        <!--                        <li><a href="#"><i class="fa fa-globe"></i></a></li>-->
                        <!--                        <li><a href="#"><i class="fa fa-shopping-cart"></i></a></li>-->
                        <li><a style="color: #fff;" href="<?php echo URL_INSTITUCION; ?>"><?php echo INSTITUCION; ?></a>
                        </li>

                        <li><a style="color: #fff;"
                               href="<?php echo base_url("administracion"); ?>"><?php echo((!isset($activo)) ? "Iniciar Sesión" : " "); ?></a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- End Topbar -->

    <!-- Navbar -->
    <div class="navbar navbar-default mega-menu" role="navigation">

        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="parallax-quote parallaxBg" style="padding: 50px 0px;">
            <div class="parallax-quote-in">
                <div class="row">
                    <div class="col-md-5 col-sm-4 col-xs-5">
                        <a href="<?php echo base_url(); ?>">
                            <img id="logo-header" class="img-responsive"

                                 src="<?php echo base_url("public/template/" . ((isset($template->ruta_logo)) ? $template->ruta_logo : " ")); ?>"
                                 alt="Logo Programa">
                        </a>
                    </div>

                    <div class="col-md-2 col-ms-2 col-xs-2 pull-right">
                        <a href="http://www.colombia.co/"><img class="header-banner img-responsive"
                                                               src="<?php echo base_url("public/template/" . ((isset($template->ruta_escudo)) ? $template->ruta_escudo : " ")); ?>"
                                                               alt="Escudo de UFPS"></a>
                    </div>
                </div>
            </div>
        </div>
        <button type="button" class="navbar-toggle" data-toggle="collapse"
                data-target=".navbar-responsive-collapse">
            <span class="full-width-menu">Menu Bar</span>
							<span class="icon-toggle">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</span>
        </button>


        <div class="clearfix"></div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-responsive-collapse" style="background: #f8f8f8;">
            <div class="container">
                <ul class="nav navbar-nav">
                    <!-- Inicio -->
                    <li>
                        <a href="<?php echo base_url(); ?>">
                            <b><i class="fa fa-home"></i> Inicio</b>
                        </a>
                    </li>
                    <li class="dropdown">
                        <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                            <b><i class="fa fa-newspaper-o"></i> Noticias</b>
                        </a>
                        <?php if (isset($noticias)): ?>
                            <ul class="dropdown-menu">
                                <?php $numFilas = count($noticias); ?>
                                <?php for ($i = 0; $i < $numFilas; $i++): ?>
                                    <li>
                                        <a href="<?php echo base_url('noticia/' . $noticias[$i]->nombre_corto); ?>">
                                            <i class="fa fa-circle-o"></i> <?php echo $noticias[$i]->nombre; ?>
                                        </a>
                                    </li>
                                <?php endfor; ?>
                            </ul>
                        <?php endif; ?>
                    </li>
                    <?php if (isset($categorias)): ?>
                        <?php $numFilas = count($categorias); ?>
                        <?php for ($i = 0; $i < $numFilas; $i++): ?>
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                                    <b><?php echo $categorias[$i]->icono; ?> <?php echo $categorias[$i]->nombre; ?></b>
                                </a>
                                <?php if (isset($listCat[$i])): ?>
                                    <ul class="dropdown-menu">
                                        <?php $numFilas2 = count($listCat[$i]); ?>
                                        <?php for ($j = 0; $j < $numFilas2; $j++): ?>
                                            <li>
                                                <a href="<?php echo base_url('contenido/' . $listCat[$i][$j]->nombre_corto); ?>">
                                                    <i class="fa fa-circle-o"></i> <?php echo $listCat[$i][$j]->nombre; ?>
                                                </a>
                                            </li>
                                        <?php endfor; ?>
                                    </ul>
                                <?php endif; ?>
                            </li>
                        <?php endfor; ?>
                    <?php endif; ?>
                </ul>

                <!--Search Block-->
                <!--                <ul class="nav navbar-nav navbar-border-bottom navbar-right">-->
                <!--                    <li class="no-border">-->
                <!--                        <i class="search fa fa-search search-btn"></i>-->
                <!--                        <div class="search-open">-->
                <!--                            <div class="input-group animated fadeInDown">-->
                <!--                                <input type="text" class="form-control" placeholder="Search">-->
                <!--										<span class="input-group-btn">-->
                <!--											<button class="btn-u" type="button"> Go</button>-->
                <!--										</span>-->
                <!--                            </div>-->
                <!--                        </div>-->
                <!--                    </li>-->
                <!--                </ul>-->
                <!--End Search Block-->
            </div><!--/end container-->
        </div><!--/navbar - collapse-->
    </div>
    <!--End Navbar-->
</div>
<!--=== End Header v4 === -->