<!--=== Footer Version 1 ===-->
<div class="footer-v1" style="margin-top: 40px;">
    <div class="footer">
        <div class="container">
            <div class="row">
                <!-- About -->
                <div class="col-md-3 md-margin-bottom-40" >

                    <a href="<?php echo URL_INSTITUCION; ?>"><img id="logo-footer" class="footer-logo img-responsive" src="<?php echo base_url("public/template/" . ((isset($template->ruta_logo_footer)) ? $template->ruta_logo_footer : " ")); ?>" alt="Logo UFPS"></a>

                    <p><?php echo ((isset($template->texto_footer)) ? $template->texto_footer : " "); ?></p>
                </div><!--/col-md-3-->
                <!-- End About -->

                <!-- Link List -->
                <div class="col-md-3 col-sm-4 md-margin-bottom-40">
                    <div class="posts">

                        <div class="headline"><h2>Aplicaciones</h2></div>
                        <ul class="list-unstyled latest-list">
                            <li><a href="https://divisist2.ufps.edu.co">Divisist</a></li>
                            <li><a href="http://sqrs.ufps.edu.co:8084/PQRSoftV1a/index2.ufps">P.Q.R.S.D.</a></li>
                            <li><a href="http://ugad.ufps.edu.co:8084/datarsoft001/home.ufps">DatarSoft</a></li>
                            <li><a href="http://dptosist.ufps.edu.co/piagev1/servlet/piagev">Piagev</a></li>

                        </ul>
                    </div>
                </div><!--/col-md-3-->
                
                <!-- Latest -->
                <div class="col-md-3 col-sm-4 md-margin-bottom-40">
                    <div class="posts">
                        <div class="headline"><h2>Enlaces de Interés</h2></div>
                        <ul class="list-unstyled latest-list">
                            <li>
                                <a href="<?php echo URL_SELECCION; ?>">Proceso de Selección</a>
                            </li>
                            <li>
                                <a href="<?php echo URL_CONVOCATORIA; ?>">Convocatorias</a>
                            </li>
                            <li>
                                <a href="<?php echo URL_CALENDARIO; ?>">Calendarios</a>
                            </li>
                            <li>
                                <a href="<?php echo URL_PROCESO; ?>">Proceso democrático</a>
                            </li>

                            <li>
                                <a href="<?php echo URL_DERECHO; ?>">Derechos pecuniarios</a>
                            </li>
                        </ul>
                    </div>
                </div><!--/col-md-3-->
                <!-- End Latest -->


                <!-- End Link List -->
                <div class="col-md-3 col-sm-4  map-img md-margin-bottom-40">
                    <div class="headline" style="border-bottom: #272727;"><h2>Contactos</h2></div>
                    <address class="md-margin-bottom-40">
                        Avenida Gran Colombia No. 12E-96 Barrio Colsag, <br>
                        San José de Cúcuta - Colombia <br>
                        Teléfono (057)(7) 5776655 <br>
                        Correo: <a href="mailto:oficinadeprensa@ufps.edu.co" class="">oficinadeprensa@ufps.edu.co</a>
                    </address>
                </div>

                <!-- Address -->
<!--                <div class="col-md-3 col-ms-4 col-xs-4 " style="margin-top: 30px;">-->
<!--                    <a href="http://www.nortedesantander.gov.co/"><img class="img-responsive"-->
<!--                                                                       src="--><?php //echo base_url("public/template/" . ((isset($template->ruta_escudo_footer)) ? $template->ruta_escudo_footer : " ")); ?><!--"-->
<!---->
<!--                                                                       alt="Escudo de la Gobernación"></a>-->
<!--                </div><!--/col-md-3-->-->
                <!-- End Address -->
            </div>
        </div>
    </div><!--/footer-->

    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <p>
                        2016 &copy; All Rights Reserved.
                        <a href="mailto:henryalxp@gmail.com">Desarrollado por: HAPM - VAVM</a>
                    </p>
                </div>

                <!-- Social Links -->
                <div class="col-md-6">
                    <ul class="footer-socials list-inline">
                        <li>
                            <a href="<?php echo URL_FACEBOOK; ?>" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Facebook">
                                <i class="fa fa-facebook"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo URL_TWITTER; ?>" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Twitter">
                                <i class="fa fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo URL_CORREO; ?>" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Correo">
                                <i class="fa fa-envelope-o"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo URL_YOUTUBE; ?>" class="tooltips" data-toggle="tooltip" data-placement="top" title="" data-original-title="Youtube">
                                <i class="fa fa-youtube"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- End Social Links -->
            </div>
        </div>
    </div><!--/copyright-->
</div>
<!--=== End Footer Version 1 ===-->