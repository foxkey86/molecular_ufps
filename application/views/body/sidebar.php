<div class="container">


    <!--=== Slider ===-->
    <div class="ms-layers-template">
        <!-- masterslider -->
        <div class="master-slider ms-skin-black-2 round-skin" id="masterslider">
            <?php if (isset($noticias)): ?>
                <?php $numFilas = count($noticias); ?>
                <?php for ($i = 0; $i < $numFilas; $i++): ?>

                    <div class="ms-slide" style="z-index: 10" data-delay="8">

                        <img src="<?php echo base_url("public/noticia/" . $noticias[$i]->ruta_imagen); ?>"
                             data-src="<?php echo base_url("public/noticia/" . $noticias[$i]->ruta_imagen); ?>" alt="">

                        <div class="ms-layer ms-promo-info color-light"
                             style="left:15px; top:160px;  background:rgba(0,0,0,0.4) !important;"
                             data-effect="bottom(40)"
                             data-duration="2000"
                             data-delay="700"
                             data-ease="easeOutExpo"

                        ></div>

                        <!--                        <div class="ms-layer ms-promo-info ms-promo-info-in color-light" style="left:15px; top:120px;  background:rgba(0,0,0,0.4) !important;"-->
                        <!---->
                        <!--                             data-effect="bottom(40)"-->
                        <!--                             data-duration="2000"-->
                        <!--                             data-delay="1000"-->
                        <!--                             data-ease="easeOutExpo"-->
                        <!--                        ><span>--><?php //echo WEBSITE; ?><!--</span></div>-->


                        <!--                        <div class="ms-layer ms-promo-sub color-light" style="left:15px; top:260px;  background:rgba(0,0,0,0.4) !important;"-->
                        <!---->
                        <!--                             data-effect="bottom(40)"-->
                        <!--                             data-duration="2000"-->
                        <!--                             data-delay="1300"-->
                        <!--                             data-ease="easeOutExpo"-->
                        <!--                        ><span>--><?php //echo $noticias[$i]->nombre; ?><!--</span></div>-->


                        <!--                        <a class="ms-layer btn-u btn-brd btn-brd-hover btn-u-light rounded" style="top:250px; background:rgba(0,0,0,0.4) !important;" href="-->
                        <?php //echo base_url("noticia/". $noticias[$i]->nombre_corto);?><!--"-->
                        <!--                           data-effect="bottom(40)"-->
                        <!--                           data-duration="2000"-->
                        <!--                           data-delay="1300"-->
                        <!--                           data-ease="easeOutExpo"-->
                        <!--                        >Ver más --><?php //echo $noticias[$i]->nombre; ?><!--</a>-->

                        <!-- linked slide -->
                        <?php if (isset($noticias[$i]->url_opcional)): ?>
                            <a href="<?php echo $noticias[$i]->url_opcional; ?>"><?php echo $noticias[$i]->nombre; ?></a>
                        <?php else: ?>
                            <a href="<?php echo base_url("noticia/" . $noticias[$i]->nombre_corto); ?>"><?php echo $noticias[$i]->nombre; ?></a>
                        <?php endif; ?>

                    </div>
                <?php endfor; ?>
            <?php endif; ?>


        </div>
        <!-- end of masterslider -->
    </div>
    <!--=== End Slider ===-->

</div>