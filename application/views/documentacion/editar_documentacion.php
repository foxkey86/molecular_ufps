<div class="row"> 
    <div class="col-xl-12">
        <div class="card border mb-g">
            <div class="card-body pl-4 pt-4 pr-4 pb-0">
                <div class="d-flex flex-column">
                <?php echo form_open('documentacion/editar_documentacion', ['class' => '', 'id' => 'form', 'role' => 'form'], ['editar' => 1, 'id_registro' => $registro->id_doc]); ?>
                    <div class="form-group row">
                        <div class="col-3 pr-1">
                            <?php $label = 'Código'; ?>
                            <?php $name = 'codigo'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => $name, 'class' => 'form-control' , 'value' => $registro->codigo, 'readonly' => 'readonly']); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Periodo'; ?>
                            <?php $name = 'periodo'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 30, 'placeholder' => $name, 'class' => 'form-control' , 'value' => $registro->nom_per, 'readonly' => 'readonly']); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Fecha del documento'; ?>
                            <?php $name = 'fecha_documento'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => $name, 'class' => 'form-control' , 'value' => $registro->fecha_documento, 'readonly' => 'readonly']); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Encargado'; ?>
                            <?php $name = 'encargado'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 100, 'placeholder' => $name, 'class' => 'form-control' , 'value' => $registro->nombres . ' ' . $registro->apellidos, 'readonly' => 'readonly']); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 pr-1">
                            <?php $label = 'Nombre del documento'; ?>
                            <?php $name = 'nombre_documento'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 100, 'placeholder' => $name, 'class' => 'form-control' , 'value' => $registro->nombre, 'readonly' => 'readonly']); ?>
                            </div>
                        </div>

                    </div>
                    <div class="form-group row">
                        <div class="col-3 pr-1">
                            <?php $label = 'Archivo'; ?>
                            <?php $name = 'archivo'; ?>
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?><br>
                                <a href="<?php echo base_url(); ?>public/archivos/lab/<?php echo $registro->url_archivo; ?>" class="btn btn-sm btn-outline-info waves-effect waves-themed">
                                                                        <i class="fal fa-file-alt"></i> Archivo
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="card border mb-0">
                        <div class="card-header bg-success-500 d-flex pr-2 align-items-center flex-wrap">
                            <div class="card-title">Actualizar estado</div>
                        </div>
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-12 pr-1">
                                    <?php $label = 'Estado'; ?>
                                    <?php $name = 'estado'; ?>
                                    <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                    <div class="form-group col-md-12">
                                        <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                        <?php echo form_dropdown($name, array('' => 'Seleccionar una opción','1' => 'Pendiente', '2' => 'Procesado', '3' => 'Rechazado'), $this->input->post($name), "class='form-control $invalid' id=$name placeholder='Seleccione el estado' required"); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12 pr-1">
                                    <?php $label = 'Observaciones'; ?>
                                    <?php $name = 'observaciones'; ?>
                                    <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                    <div class="form-group col-md-12">
                                        <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                        <?php echo form_textarea(['id' => $name, 'name' => $name, 'maxlength' => 200, 'placeholder' => $name, 'class' => 'form-control'], set_value($name), 'required'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="height-8 d-flex flex-row align-items-center flex-wrap flex-shrink-0">
                        <button class="btn btn-info shadow-0 ml-auto" type="submit" ><i class="fas fa-save"></i> Editar Registro</button>
                    </div>
                <?php echo form_close(); ?>
                </div>
            </div>
        </div>                          
    </div>
    <div class="col-xl-12">
        <div class="col-md-12 ml-auto text-left" style="margin-bottom:20px;">
            <a href="<?php echo base_url("documentacion/pendientes"); ?>" class="btn btn-primary waves-effect waves-themed" type="button"><i class="fal fa-chevron-circle-left"></i> Atrás</a>
        </div> 
    </div>
</div>
                      