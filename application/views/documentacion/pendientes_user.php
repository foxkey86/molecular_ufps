<div class="row">
                            <div class="col-xl-12">
                                <div id="panel-1" class="panel">
                                  
                                    <div class="panel-container show">
                                        <div class="panel-content">
                                            <!-- datatable start -->
                                            <table id="dt-basic-example" class="table table-bordered table-hover table-striped w-100">
                                                <thead>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Código</th>
                                                        <th>Periodo</th>
                                                        <th>Fecha de Documento</th>
                                                        <th>Tipo de Documento</th>
                                                        <th>Nombre de Documento </th>
                                                        <th>Perfíl</th>
                                                        <th>Encargado</th>
                                                        <th>Observaciones</th>
                                                        <th>Documento</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php foreach($listado_documentos as $row): ?>
                                                    <tr>
                                                        <td><?php echo $row->id_doc; ?></td>
                                                        <td><?php echo $row->codigo; ?></td>
                                                        <td><span class="badge bg-info-900"><?php echo $row->nom_per; ?></span></td>
                                                        <td><span class="badge bg-danger-900"><?php echo $row->fecha_documento; ?></span></td>
                                                        <td><?php echo $row->tipo; ?></td>
                                                        <td><?php echo $row->nombre; ?></td>
                                                        <td><?php echo $row->id_perfil; ?></td>
                                                        <td><?php echo $row->nombres; ?> <?php echo $row->apellidos; ?></td>
                                                        <td><?php echo $row->observaciones; ?></td>
                                                        <td>
                                                            <div class="d-flex mt-2">
                                                                <a href="<?php echo base_url(); ?>public/archivos/lab/<?php echo $row->url_archivo; ?>" class="btn btn-sm btn-outline-info waves-effect waves-themed">
                                                                    <i class="fal fa-file-alt"></i> Archivo
                                                                </a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <?php endforeach;?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <th>Id</th>
                                                        <th>Código</th>
                                                        <th>Periodo</th>
                                                        <th>Fecha de Documento</th>
                                                        <th>Tipo de Documento</th>
                                                        <th>Nombre de Documento </th>
                                                        <th>Perfíl</th>
                                                        <th>Encargado</th>
                                                        <th>Observaciones</th>
                                                        <th>Documento</th>
                                                    </tr>
                                                </tfoot>
                                            </table>
                                            <!-- datatable end -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12">
        <div class="col-md-12 ml-auto text-left" style="margin-bottom:20px;">
            <a href="<?php echo base_url("documentacion/panel_documentacion_user"); ?>" class="btn btn-primary waves-effect waves-themed" type="button"><i class="fal fa-chevron-circle-left"></i> Atrás</a>
        </div> 
    </div>
    </div>
      
                      