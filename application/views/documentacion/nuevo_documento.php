<div class="row"> 
    <div class="col-xl-12">
        <div class="card border mb-g">
            <div class="card-body pl-4 pt-4 pr-4 pb-0">
                <div class="d-flex flex-column">
                <?php echo form_open_multipart('documentacion/nuevo_documento', ['class' => '', 'id' => 'form', 'role' => 'form'], ['nuevo' => 1]); ?>
                    <div class="form-group row">
                            <?php echo form_input(['id' => 'id_perfil_archivo', 'name' => 'id_perfil_archivo', 'maxlength' => 10, 'placeholder' => '', 'type' => 'hidden', 'class' => 'form-control']); ?>
                            <div class="col-6 pr-1">
                                <?php $label = 'Documento *'; ?>
                                <?php $name = 'id_archivo'; ?>
                                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                <div class="form-group col-md-12">
                                    <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                    <?php echo form_dropdown($name, $listado_documentos, $this->input->post($name), "class='form-control $invalid' id='$name' placeholder='Seleccione el documento'  onchange=\"cambia_tipo_usuario(this.value)\" required"); ?>
                                </div>
                            </div>
                            <div class="col-3 pr-1">
                                <?php $label = 'Código de documento'; ?>
                                <?php $name = 'codigo'; ?>
                                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                <div class="form-group col-md-12">
                                    <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                    <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 10, 'placeholder' => $name, 'class' => 'form-control', 'readonly' => 'readonly'], set_value($name)); ?>
                                </div>
                            </div>
                            <div class="col-3 pr-1">
                                <?php $label = 'Tipo de documento'; ?>
                                <?php $name = 'tipo'; ?>
                                <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                                <div class="form-group col-md-12">
                                    <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                    <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 20, 'placeholder' => $name, 'class' => 'form-control', 'readonly' => 'readonly'], set_value($name)); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                        <div class="col-3 pr-1">
                            <?php $label = 'Periodo de documento'; ?>
                            <?php $name = 'periodo'; ?>
                            <?php $invalid = (form_error($name) ? 'is-invalid' : ''); ?> 
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'maxlength' => 30, 'placeholder' => $name, 'class' => 'form-control', 'readonly' => 'readonly'], set_value($name)); ?>
                            </div>
                        </div>
                        <div class="col-3 pr-1">
                            <?php $label = 'Fecha del documento *'; ?>
                            <?php $name = 'fecha_documento'; ?>
                            <div class="form-group col-md-12">
                                <?php echo form_label($label, $name, ['class' => 'control-label']); ?>
                                <?php echo form_input(['id' => $name, 'name' => $name, 'type' => 'date', 'placeholder' => $label, 'class' => 'form-control'], set_value($name), 'required'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-12 pr-1">
                            <?php $label = 'Cargar documento'; ?>
                            <?php $name = 'documento'; ?>
                            <div class="form-group col-md-12">
                                <input type="file" name="<?php echo $name; ?>" required accept=".csv, .CSV, .xls, .xlsx, .doc, .docx, .PDF, .pdf" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="height-8 d-flex flex-row align-items-center flex-wrap flex-shrink-0">
                        <button class="btn btn-info shadow-0 ml-auto" type="submit" ><i class="fas fa-save"></i> Enviar Solicitud</button>
                    </div>
                <?php echo form_close(); ?>
                </div>
            </div>
        </div>                          
    </div>
    <div class="col-xl-12">
        <div class="col-md-12 ml-auto text-left" style="margin-bottom:20px;">
            <a href="<?php echo base_url("documentacion/panel_documentacion_user"); ?>" class="btn btn-primary waves-effect waves-themed" type="button"><i class="fal fa-chevron-circle-left"></i> Atrás</a>
        </div> 
    </div>
</div>