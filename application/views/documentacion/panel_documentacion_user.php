<div class="row">
    <div class="col-sm-6 col-xl-3">
        <a href="<?php echo base_url("documentacion/nuevo_documento"); ?>">
            <div class="p-3 bg-info-600 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <i class="fa fa-file-plus" style="font-size:3rem"></i>
                        <small class="m-0 l-h-n">Cargar Documento</small>
                    </h3>
                </div>
                <i class="fa fa-file-alt position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-xl-3">
        <a href="<?php echo base_url("documentacion/form_resumen_registro"); ?>">
            <div class="p-3 bg-info-600 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <i class="fa fa-plus-square" style="font-size:3rem"></i>
                        <small class="m-0 l-h-n">Diligenciar Formulario</small>
                    </h3>
                </div>
                <i class="fa fa-list-alt position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-xl-3">
            <a href="<?php echo base_url("documentacion/pendientes_user"); ?>">
                <div class="p-3 bg-fusion-200 rounded overflow-hidden position-relative text-white mb-g">
                    <div class="">
                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?php echo $num_pendientes->num; ?>
                            <small class="m-0 l-h-n">Pendientes</small>
                        </h3>
                    </div>
                    <i class="fa fa-file-alt position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
                </div>
            </a>
    </div>
    <div class="col-sm-6 col-xl-3">
        <a href="<?php echo base_url("documentacion/rechazadas_user"); ?>">
            <div class="p-3 bg-warning-600 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?php echo $num_rechazados->num; ?>
                        <small class="m-0 l-h-n">Rechazadas</small>
                    </h3>
                </div>
                <i class="fa fa-exclamation-triangle position-absolute pos-right pos-bottom opacity-15  mb-n1 mr-n4" style="font-size: 6rem;"></i>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-xl-3">
        <a href="<?php echo base_url("documentacion/procesada_user"); ?>">
            <div class="p-3 bg-success-600 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        <?php echo $num_procesados->num; ?>
                        <small class="m-0 l-h-n">Procesadas</small>
                    </h3>
                </div>
                <i class="fal fa-check-circle position-absolute pos-right pos-bottom opacity-15 mb-n5 mr-n6" style="font-size: 8rem;"></i>
            </div>
        </a>
    </div>
</div>
