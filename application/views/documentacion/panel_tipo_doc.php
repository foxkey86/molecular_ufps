<div class="row">
    <div class="col-sm-6 col-xl-3">
            <a href="<?php echo base_url("documentacion/formatos"); ?>">
                <div class="p-3 bg-primary-900 rounded overflow-hidden position-relative text-white mb-g">
                    <div class="">
                        <h3 class="display-4 d-block l-h-n m-0 fw-500">
                            Formatos
                            <small class="m-0 l-h-n">&nbsp;</small>
                        </h3>
                    </div>
                    <i class="fa fa-file-alt position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n1" style="font-size:6rem"></i>
                </div>
            </a>
    </div>
    <div class="col-sm-6 col-xl-3">
        <a href="<?php echo base_url("documentacion/guias"); ?>">
            <div class="p-3  bg-info-900 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        Guías
                        <small class="m-0 l-h-n">&nbsp;</small>
                    </h3>
                </div>
                <i class="fa fa-file-alt position-absolute pos-right pos-bottom opacity-15  mb-n1 mr-n4" style="font-size: 6rem;"></i>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-xl-3">
        <a href="<?php echo base_url("documentacion/procedimientos"); ?>">
            <div class="p-3 bg-success-900 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        Procedimientos
                        <small class="m-0 l-h-n">&nbsp;</small>
                    </h3>
                </div>
                <i class="fa fa-file-alt position-absolute pos-right pos-bottom opacity-15 mb-n5 mr-n6" style="font-size: 8rem;"></i>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-xl-3">
        <a href="<?php echo base_url("documentacion/instructivos"); ?>">
            <div class="p-3 bg-warning-900 rounded overflow-hidden position-relative text-white mb-g">
                <div class="">
                    <h3 class="display-4 d-block l-h-n m-0 fw-500">
                        Instructivos
                        <small class="m-0 l-h-n">&nbsp;</small>
                    </h3>
                </div>
                <i class="fa fa-file-alt position-absolute pos-right pos-bottom opacity-15 mb-n1 mr-n4" style="font-size: 6rem;"></i>
            </div>
        </a>
    </div>
</div>
