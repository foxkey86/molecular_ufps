
<div class="row">
    <div class="col-md-12">
        <div id="panel-1" class="panel">
            <div class="panel-hdr">
                <h2>
                        NUMERAL <span class="fw-300"><i>4.1,  4.2 y 8.5.</i></span>
                </h2>
                <div class="panel-toolbar">
                    <button class="btn btn-panel" data-action="panel-collapse" data-toggle="tooltip" data-offset="0,10" data-original-title="Collapse"></button>
                    <button class="btn btn-panel" data-action="panel-fullscreen" data-toggle="tooltip" data-offset="0,10" data-original-title="Fullscreen"></button>
                    <button class="btn btn-panel" data-action="panel-close" data-toggle="tooltip" data-offset="0,10" data-original-title="Close"></button>
                </div>
            </div>
            <div class="panel-container show">
                <div class="panel-content">
                    <!-- datatable start -->
                    <table id="dt-basic-documentacion" class="table table-bordered table-hover table-striped w-100">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Código</th>
                            <th>Nombre de Documento</th>
                            <th>Tipo de Documento</th>
                            <th>Archivo</th>
                        </tr>
                    </thead>
                    <tbody>
                    <?php $i = 0; ?>
                    <?php foreach($listado_documentos as $row): $i++;?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $row->codigo; ?></td>
                            <td><?php echo $row->nombre; ?></td>
                            <td><?php echo $row->tipo; ?></td>
                            <td style="text-align: center;">
                                <a href="<?php echo base_url(); ?>public/archivos/documentacion/<?php echo $row->url; ?>" class="btn btn-lg btn-outline-info waves-effect waves-themed">
                                                        <i class="fal fa-download"></i> Descargar
                                </a>
                                                
                            </td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>No.</th>
                            <th>Código</th>
                            <th>Nombre de Documento</th>
                            <th>Tipo de Documento</th>
                            <th>Archivo</th>
                        </tr>
                    </tfoot>
                    </table>
                    <!-- datatable end -->
                </div>
            </div>
        </div>
    </div>
    <div class="col-xl-12">
        <div class="col-md-12 ml-auto text-left" style="margin-bottom:20px;">
            <a href="<?php echo base_url("documentacion/clasificacion_doc"); ?>" class="btn btn-primary waves-effect waves-themed" type="button"><i class="fal fa-chevron-circle-left"></i> Atrás</a>
        </div> 
    </div>
</div>



           