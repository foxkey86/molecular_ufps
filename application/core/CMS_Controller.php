<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CMS_Controller extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->_init_globals();
        if ($userdata = $this->session->userdata(SESSION_NAME)) {
            $this->template->set('userdata', $userdata);
        }

        if (ENVIRONMENT == 'development') {
            $this->output->enable_profiler(TRUE);
        } else {
            $this->output->enable_profiler(FALSE);
        }

        $this->session->userdata['queries'] = array();
        $this->session->userdata['query_time'] = array();
        $this->session->userdata['query_nrows'] = array();

          //Captura la fecha con el proposito de informar al usuario
          $this->template->set('fecha_actual', $this->ajuste_fecha(date_format(new DateTime(), 'd/m/Y')));

         //Procesos que se deben ejecutar cuando el usuario ha iniciado sesión
        $usuario = $this->session->userdata(SESSION_NAME);
        $profesional = $this->session->userdata('profesional');
        $roles = $this->session->userdata('usuario_roles');
        $this->unread_notifications = NULL;

 

        if ($usuario) {
            $this->template->set('informacion_usuario', $usuario);
            $this->template->set('profesional_usuario', $profesional);
            $this->template->set('usuario_roles', $roles);

            //Notificaciones
//            $this->load->model('notifications_model');
//            $this->unread_notifications = $this->notifications_model->unreaded_notifications($usuario->CODIGO);
        }
        $this->template->set('unread_notifications', $this->unread_notifications);  

    }

    private function _init_globals()
    {

        //Documento Archivo
        define('PATH_ARCHIVO_LAB', "public/archivos/lab/");
        define('MAX_SIZE_ARCHIVO', 20480); 

        // keys
        if (ENVIRONMENT == 'development') {
            define('SITE_KEY', '6LdNPUEbAAAAAGgu81KGxPEZCGtRXLyY6V3LEVO7');
            define('SECRET_KEY', '6LdNPUEbAAAAAP6IZWLUbTSrmG41_LzODaIpIyMP');
        } else {
            define('SITE_KEY', '6LeclJocAAAAAFZPC0FR-oAVWvuJELah6NsHIZqr');
            define('SECRET_KEY', '6LeclJocAAAAAAZEidM_EA0wpbDtN2-4S-zGZwjD');
        }
        

        //Sesion
        define('SESSION_NAME','user_biomol');
        //Portal
        define('ID_PORTAL', 2);
        //Noticia
        define('MAX_SIZE_NOTICIA', 1024);
        define('MAX_WIDTH_NOTICIA', 1500);
        define('MAX_HEIGHT_NOTICIA', 768);
        define('PATH_NOTICIA', "public/noticia/");
        //Seccion
        define('PATH_SECCION', "public/seccion/");
        //Contenido
        define('MAX_SIZE_IMG_CONTENIDO', 1024);
        define('MAX_WIDTH_IMG_CONTENIDO', 1500);
        define('MAX_HEIGHT_IMG_CONTENIDO', 768);
        define('PATH_IMG_CONTENIDO', "public/contenido/imagen/");
        define('MAX_SIZE_PDF_CNOTENIDO', 10240);
        define('PATH_PDF_CONTENIDO', "public/contenido/archivo/");
        //Documento
        define('PATH_PDF_DOCUMENTO_CONTENIDO', "public/documento/");
        //Plantilla
        define('ID_TEMPLATE', 2);
        define('PATH_TEMPLATE', "public/template/");
        define('MAX_SIZE_LOGO', 1024);
        define('MAX_WIDTH_LOGO', 1500);
        define('MAX_HEIGHT_LOGO', 768);
        define('MAX_SIZE_ESCUDO', 1024);
        define('MAX_WIDTH_ESCUDO', 1500);
        define('MAX_HEIGHT_ESCUDO', 768);
        define('MAX_SIZE_LOGO_FOOTER', 1024);
        define('MAX_WIDTH_LOGO_FOOTER', 1500);
        define('MAX_HEIGHT_LOGO_FOOTER', 768);
        define('MAX_SIZE_ESCUDO_FOOTER', 1024);
        define('MAX_WIDTH_ESCUDO_FOOTER', 1500);
        define('MAX_HEIGHT_ESCUDO_FOOTER', 768);
        //Extra
        define('WEBSITE', "Programa Licenciatura en Matemáticas");
        define('CORREO', 'licmatematica@ufps.edu.co');
        define('INSTITUCION', "Universidad Francisco de Paula Santander - Cúcuta");
        define('URL_INSTITUCION', 'http://www.ufps.edu.co/');
        define('URL_WEBSITE', 'licmatematica.ufps.edu.co');
        define('URL_SELECCION', 'http://www.ufps.edu.co/ufps/proceso_seleccion_ESE/Presentacion.php');
        define('URL_CONVOCATORIA', 'http://www.ufps.edu.co/convocatorias/');
        define('URL_CALENDARIO', 'http://www.ufps.edu.co/ufpsnuevo/modulos/contenido/show.php?item=25,132');
        define('URL_PROCESO', 'http://www.ufps.edu.co/informacion/proceso-democratico-2016');
        define('URL_DERECHO', 'http://www.ufps.edu.co/ufpsnuevo/archivos/DERECHOS_PECUNIARIOS_2016.pdf');



        define('URL_FACEBOOK', 'https://www.facebook.com/UFPS-C%C3%BAcuta-553833261409690');
        define('URL_TWITTER', 'https://twitter.com/UFPSCUCUTA');
        define('URL_YOUTUBE', 'https://www.youtube.com/channel/UCgPz-qqaAk4lbHfr0XH3k2g');
        define('URL_CORREO', 'mailto:licmatematica.ufps.edu.co');




    }

    /**
     * Metodo para imprimir en pantalla el valor de un objeto, arreglo o variable. Solo desplegará la información si el
     * entorno en el que se ejecuta es <b>development</b>.
     * @param type $var
     */
    public function dump($var)
    {
        if (ENVIRONMENT == 'development') {
            echo "<pre>";
            echo var_dump($var);
            echo "</pre><hr>";
        }
    }

    /**
     * Función utilizada como callback en las reglas de form validation. Valida que una cadena de texto sea
     * alfabética y permite incluir 'ñÑ' y acentos en las vocales.
     *
     * @param String $str
     * @return boolean
     */
    public function alpha_es($str)
    {
        if (!preg_match("/^([ a-zA-Z0-9ñÑáéíóú\.,;:])+$/i", $str)) {
            $this->form_validation->set_message('alpha_es', 'El campo {field} solo puede contener caracteres alfabéticos');
            return FALSE;
        } else {
            return TRUE;
        }
    }

    public function ajax_request() {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }
    }

    
    /**
     * Función que recibe una fecha en formato DD/MM/YYY y la retorna en un formato legible.
     *
     * Ejemplo:
     * recibe   01/01/2016
     * retorna  Viernes 1 de Enero de 2016
     *
     * @param String $fecha
     * @return string
     */
    public function ajuste_fecha($fecha)
    {

        if (!$this->valid_date($fecha)) {
            return "fecha invalida";
        }

        $dias = array('Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
        $meses = array('Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiempre', 'Octubre', 'Noviembre', 'Diciembre');
        $date = date_create_from_format("d/m/Y", $fecha);
        $wday = $date->format('w');
        $tmp = explode('/', $fecha);
        return sprintf("%s %s de %s de %s", $dias[$wday], $tmp[0], $meses[$tmp[1] - 1], $tmp[2]);
    }

      /**
     * Funcion para ser utilizada en validaciones de formularios,
     * retorna TRUE si la fecha coincide con el formado DD/MM/YYY (oracle) d/m/Y (php),
     * FALSE de lo contrario
     * @param string $date
     * @return boolean
     */
    public function valid_date($date)
    {
        if (date_create_from_format('d/m/Y', $date)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('valid_date', '%s no cumple con el formato aceptado.');
            return FALSE;
        }
    }

    public function validar_ajax()
    {
        if (!$this->input->is_ajax_request()) {
            show_404();
        }
    }

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */